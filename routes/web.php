<?php

header("Cache-Control: no-cache, must-revalidate");
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Search by MAP:

Route::get('/{lang}/searchbymap', 'IndexController@searchByMap')->name('searchbymap');
Route::get('/{lang}/kerko-ne-harte', 'IndexController@searchByMap')->name('searchbymap');

Route::get('en/agency-commissions.1880', function () {
	return redirect('en/commisions',301);
});

Route::get('sq/apartament-ne-qera-ne-Tirane', function() {
	return redirect('sq/apartament-me-qera-ne-Tirane',301);
});

Route::get('sq/apartament-me-qera-ne-Tirane{asd}', function() {
	return redirect('sq/apartament-me-qera-ne-Tirane',301);
});

Route::get('en/.3120', function() {
	return redirect('/',301);
});

Route::get('sq/vile-1-kateshe-per-shitje-prane-unazes-se-madhe-ne-tirane-trs-418-56l.4433', function() {
	return redirect('/',301);
});

Route::get('en/office -for-rent-in-Tirana', function() {
	return redirect('en/office-for-rent-in-Tirana',301);
});

Route::get('en/albanian-citites.108', function() {
	return redirect('/',301);
});

// Home page:
Route::get('/', 'IndexController@enIndex')->name('home');
Route::get('/sq', 'IndexController@sqIndex')->name('homesq');
#Route::get('/en', 'IndexController@enIndex')->name('homeen');
Route::redirect('/en', '/', 301);
Route::get('/italian', 'IndexController@enIndex');
// Register property:
Route::get('/{lang}/regjistro-pronen', 'IndexController@registerProperty')->name('register-property');
Route::get('/{lang}/register-property', 'IndexController@registerProperty')->name('register-property');
Route::post('/property/register-property', 'PropertyController@registerProperty');

Route::post('/{lang}/property/send/{id}', 'IndexController@sendFromProperty');

Route::post('/{lang}/search', 'IndexController@search')->where('lang', '(en|sq|it)')->name('search');
Route::get('/{lang}/search', 'IndexController@search')->where('lang', '(en|sq|it)');

//NEW REDIRECTS 

Route::get('sq/prona-ne-Kavaja', function(){
	return redirect('sq/prona-ne-Kavaje',301);
});
Route::get('sq/prona-ne-Gjiri Lalzit', function(){
	return redirect('sq/prona-ne-Gjiri i Lalzit',301);
});
Route::get('en/properties-in-Velipoje', function(){
	return redirect('en/properties-in-Velipoja',301);
});
Route::get('en/properties-in-Lezhe', function(){
	return redirect('en/properties-in-Lezha',301);
});
Route::get('en/properties-in-Kruje', function(){
	return redirect('en/properties-in-Kruja',301);
});
Route::get('en/apartment-for-rent-in-lezha-city-ler-1212-1.479', function(){
	return redirect('en/properties-in-albania',301);
});
Route::get('sq/apartament-me-qera-ne-qytetin-e-lezhes-ler-1212-1.479', function(){
	return redirect('sq/prona-ne-shqiperi',301);
});
Route::get('brenda/shfaqart/gj2/479/Apartment_for_rent_in_Lezha_City___(LER-1212-1).html', function(){
	return redirect('/',301);
});
Route::get('mobile/artikull.php?id=115&gj=gj1', function(){
	return redirect('sq/blog');
});
Route::get('brenda/shfaqart/gj1/479/Apartament_me_qera_ne_qytetin_e_Lezhes___', function(){
	return redirect('/',301);
});
Route::get('sq/apartament-me-shitje-ne-Rreth{asd}Shqiperise', function(){
	return redirect('sq',301);
});
Route::get('sq/apartament-11-me-qera-prane-stadiumit-air-albania-ne-tirane.5856', function(){
	return redirect('sq',301);
});
Route::get('en/one-bedroom-apartment-close-to-air-albania-stadium-in-tirana.5856', function(){
	return redirect('/',301);
});
Route::get('en/test.5752', function(){
	return redirect('en',301);
});
Route::get('sq/test.5752', function(){
	return redirect('sq',301);
});
Route::get('{lang}/article.{id}', 'PropertyController@redirectArticle');

// Property details:
Route::get('/en/{title}.{id}', 'PropertyController@enShow')->where('title', '.*')->where('id', '[0-9]+')->name('propertyEn');
Route::get('/sq/{title}.{id}', 'PropertyController@sqShow')->where('title', '.*')->where('id', '[0-9]+')->name('propertySq');

Route::get('/{lang}/{propertyType}-{perme}-{action}-{in}-{city}/0', 'PropertyController@redirect')
    ->where('lang', '(en|sq)')
    ->where('in', '(ne|in)')
    ->where('propertyType', '(magazine|toke|zyre|vile|apartament|office|store|club|warehouse|hotel|business|parkingspace|apartment|villa|tjeter|land|commercial|biznes|ambient tregtar)')
    ->where('perme', '(me|ne|for)')
    ->where('action', '(rent|sale|qera|shitje)')
    ->name('category-cities-redirect1');
    
Route::get('/{lang}/{propertyType}-{perme}-{action}-{in}-{city}/1', 'PropertyController@redirect')
    ->where('lang', '(en|sq)')
    ->where('in', '(ne|in)')
    ->where('propertyType', '(magazine|toke|zyre|vile|apartament|office|store|club|warehouse|hotel|business|parkingspace|apartment|villa|tjeter|land|commercial|biznes|ambient tregtar)')
    ->where('perme', '(me|ne|for)')
    ->where('action', '(rent|sale|qera|shitje)')
    ->name('category-cities-redirect2');
    
Route::get('/{lang}/{propertyType}-{perme}-{action}-{in}-{city}/{page?}', 'PropertyController@category')
    ->where('lang', '(en|sq)')
    ->where('in', '(ne|in)')
    ->where('propertyType', '(magazine|toke|zyre|vile|apartament|office|store|club|warehouse|hotel|business|parkingspace|apartment|villa|tjeter|land|commercial|biznes|ambient tregtar)')
    ->where('perme', '(me|ne|for)')
    ->where('action', '(rent|sale|qera|shitje)')
    ->name('category-cities');

Route::get('/{lang}/{propertyType}-{propertySelection}-{perme}-{action}-{in}-{city}/0', 'PropertyController@redirect')
    ->where('lang', '(en|sq)')
    ->where('in', '(ne|in)')
    ->where('propertyType', '(magazine|toke|zyre|vile|apartament|office|store|club|warehouse|hotel|business|parkingspace|apartment|villa|tjeter|land|commercial|biznes|ambient tregtar|commercial)')
    ->where('perme', '(me|ne|for)')
    ->where('action', '(rent|sale|qera|shitje)')
    ->where('city', '[0-9a-zA-Z][0-9a-zA-Z\-]*')
    ->name('category-cities-selection-page1');
    
Route::get('/{lang}/{propertyType}-{propertySelection}-{perme}-{action}-{in}-{city}/1', 'PropertyController@redirect')
    ->where('lang', '(en|sq)')
    ->where('in', '(ne|in)')
    ->where('propertyType', '(magazine|toke|zyre|vile|apartament|office|store|club|warehouse|hotel|business|parkingspace|apartment|villa|tjeter|land|commercial|biznes|ambient tregtar|commercial)')
    ->where('perme', '(me|ne|for)')
    ->where('action', '(rent|sale|qera|shitje)')
    ->where('city', '[0-9a-zA-Z][0-9a-zA-Z\-]*')
    ->name('category-cities-selection-page2');

Route::get('/{lang}/vile-2+dhoma-{perme}-{action}-{in}-{city}', 'PropertyController@redirectVileSq')
    ->where('lang', '(en|sq)')
    ->where('in', '(ne|in)')
    ->where('propertyType', '(magazine|toke|zyre|vile|apartament|office|store|club|warehouse|hotel|business|parkingspace|apartment|villa|tjeter|land|commercial|biznes|ambient tregtar|commercial)')
    ->where('perme', '(me|ne|for)')
    ->where('action', '(rent|sale|qera|shitje)')
    ->where('city', '[0-9a-zA-Z][0-9a-zA-Z\-]*')
    ->name('category-cities-selection-page3');

Route::get('/{lang}/villa-2+bedrooms-{perme}-{action}-{in}-{city}', 'PropertyController@redirectVileEn')
    ->where('lang', '(en|sq)')
    ->where('in', '(ne|in)')
    ->where('propertyType', '(magazine|toke|zyre|vile|apartament|office|store|club|warehouse|hotel|business|parkingspace|apartment|villa|tjeter|land|commercial|biznes|ambient tregtar|commercial)')
    ->where('perme', '(me|ne|for)')
    ->where('action', '(rent|sale|qera|shitje)')
    ->where('city', '[0-9a-zA-Z][0-9a-zA-Z\-]*')
    ->name('category-cities-selection-page4');
      
Route::get('/{lang}/{propertyType}-{propertySelection}-{perme}-{action}-{in}-{city}/{page?}', 'PropertyController@categorySelection')
    ->where('lang', '(en|sq)')
    ->where('in', '(ne|in)')
    ->where('propertyType', '(magazine|toke|zyre|vile|apartament|office|store|club|warehouse|hotel|business|parkingspace|apartment|villa|tjeter|land|commercial|biznes|ambient tregtar|commercial)')
    ->where('perme', '(me|ne|for)')
    ->where('action', '(rent|sale|qera|shitje)')
    ->where('city', '[0-9a-zA-Z][0-9a-zA-Z\-]*')
    ->name('category-cities-selection-page');

Route::get('/{propertySelection}/{$propertyType}', 'PropertyController@findSelection')
	->name('findSelection');

// Contact:
// Route::get('/contact', 'IndexController@contact')->name('contact');
// Static content
Route::get('/{lang}/commisions', 'StaticController@commisions');
Route::get('/{lang}/komisionet', 'StaticController@commisions');
Route::get('/sitemap.xml', 'IndexController@sitemap');
Route::get('/sitemap123.xml', 'IndexController@sitemap_new');
Route::get('/rss.xml', 'IndexController@rss');

Route::get('/{lang}/properties-in-albania', 'IndexController@links');
Route::get('/{lang}/prona-ne-shqiperi', 'IndexController@links');

Route::get('/{lang}/properties-in-{city}/{page?}', 'PropertyController@cityProperties')->name('properties-city-en');
Route::get('/{lang}/prona-ne-{city}/{page?}', 'PropertyController@cityProperties')->name('properties-city-sq');

Route::get('/{lang}/{pageType}', 'StaticController@page')->where('lang', '(en|sq)')->where('pageLang', '(about-us|rreth-nesh|contact|kontakt|sitemap|harta-faqes)');

Route::post('/{lang}/contact/post', 'IndexController@send');


Route::get('/clear-cache/done', function () {
    return response("Cache has been cleared successfully.");
})->name('clear-cache-success');

Route::get('/clear-cache/{token}', function ($token) {
    if ($token === env('CACHE_CLEAR_TOKEN')) {
        \Illuminate\Support\Facades\Artisan::call('cache:clear');
        return redirect()->route('clear-cache-success');
    }

    abort(404);
});

/* ################################################################


LEGACY ROUTES

#################################################################*/


//mobile/artikull.php?id=5537&gj=gj2
	

Route::get('/mobile/artikull.php?id={id}&gj=gj2', function ($id) {
   return redirect()->route('propertyEn', ["", $id], 301);
})->where(['id' => '[0-9]+']);

Route::get('apartments_for_rent/gj2/52/Apartments_for_rent_in_Albania.html', function () {
    return redirect('en/apartment-for-rent-in-Tirana',301);
});
Route::get('apartaments_for_sale/gj2/54/Apartments_for_Sale_in_Albania.html', function () {
    return redirect('en/apartment-for-sale-in-Tirana',301);
});
Route::get('villa_for_rent/gj2/58/Villas_for_Rent_in_Albania.html', function () {
    return redirect('en/villa-for-rent-in-Tirana',301);
});
Route::get('propertytype/gj2/157/Vile/Tirane/Villa_for_Rent_in_Tirana.html', function () {
    return redirect('en/villa-for-rent-in-Tirana',301);
});
Route::get('propertytype/gj2/174/Toke/Tirane/Lands_for_Sale_in_Tirana.html', function () {
    return redirect('en/land-for-sale-in-Tirana',301);
});
Route::get('lands_for_sale/gj2/56/Lands_for_Sale_in_Albania.html', function () {
    return redirect('en/land-for-sale-in-Tirana',301);
});
Route::get('hotels_for_sale/gj2/55/Hotels_for_Sale_in_Albania.html', function () {
    return redirect('en/commercial-for-sale-in-Tirana',301);
});
Route::get('brenda/gj2/35/Properties_in_Tirana.html', function () {
    return redirect()->route('home','', 301);
});
Route::get('property_for_rent/gj2/property_for_rent.html', function () {
    return redirect('en/apartment-for-rent-in-Tirana',301);
});
Route::get('property_for_sale/gj2/property_for_sale.html', function () {
    return redirect('en/apartment-for-sale-in-Tirana',301);
});

Route::get('brenda/gj2/{id}/{title}.html', function ($id, $title) {
   return redirect('/',301);
})->where([ 'title' => '.*','id' =>'(2|31|35|69|4|27|10|12|14|26|28|51|68)']);
/*
Route::get('brenda/gj2/{id}/{title}.html', function ($id, $title) {
   return redirect('/en/about-us',301);
})->where([ 'title' => '.*','id' =>'(1|8)']);
*/

Route::get('brenda/gj2/3/Management.html', function () {
    return redirect()->route('home','', 301);
});
Route::get('brenda/gj2/31/About_Real_Estate_in_Albania.html', function () {
    return redirect()->route('home','', 301);
});
Route::get('kontakt/gj2/6/Contact.html', function () {
    return redirect('en/contact',301);
});
Route::get('brenda/gj2/4/Commissions.html', function () {
    return redirect('en/commisions',301);
});
Route::get('property_for_investment/gj2/property_for_investment.html', function () {
    return redirect()->route('home','', 301);
});
Route::get('html', function () {
    return redirect()->route('home','', 301);
});
Route::get('com', function () {
    return redirect()->route('home','',301);
});
Route::get('gj2.html', function () {
    return redirect()->route('home','',301);
});
Route::get('gj1.html', function () {
    return redirect()->route('homesq','',301);
});
Route::get('fr', function () {
    return redirect()->route('home','',301);
});
Route::get('mentions-legales', function () {
    return redirect()->route('home','',301);
});
Route::get('wp-content/uploads/2017/05/Boys-Baseball-Bedroom-Ideas-themed-rooms_33.jpg', function () {
    return redirect()->route('home','',301);
});
Route::get('mon-compte', function () {
    return redirect()->route('home','',301);
});
Route::get('contact', function () {
    return redirect('en/contact',301);
});
Route::get('commande', function () {
    return redirect()->route('home','',301);
});
Route::get('plan-du-site', function () {
    return redirect()->route('home','',301);
});
Route::get('okazion/okazion.html', function () {
    return redirect()->route('home','', 301);
});

Route::get('brenda/gj1/1/Rreth_Nesh.html', function () {
    return redirect('sq/rreth-nesh',301);
});
Route::get('kontakt/gj1/6/Kontakt.html', function () {
    return redirect('sq/kontakt',301);
});
Route::get('sitemap/gj1/sitemap.html', function () {
    return redirect('sitemap.xml',301);
});
Route::get('sitemap/gj2/sitemap.html', function () {
    return redirect('sitemap.xml',301);
});
Route::get('property_for_rent/gj1/prona_me_qera.html', function () {
    return redirect('sq/apartament-me-qera-ne-Tirane',301);
});

Route::get('property_for_sale/gj1/prona_ne_shitje.html', function () {
    return redirect('sq/apartament-ne-shitje-ne-Tirane',301);
});

Route::get('villa_for_sale/gj1/57/Vila_per_shitje_ne_Shqiperi.html', function () {
    return redirect('sq/vile-ne-shitje-ne-Tirane',301);
});

/*
Route::get('brenda/shfaqart/gj2/{id}/{title}', function ($id, $title) {
   return redirect()->route('propertyEn', [$title, $id], 301);
})->where([ 'title' => '.*','id' => '[0-9]+']);

Route::get('brenda/shfaqart/gj1/{id}/{title}', function ($id, $title) {
   return redirect()->route('propertySq', [$title, $id], 301);
})->where([ 'title' => '.*','id' => '[0-9]+']);
*/

Route::get('brenda/shfaqart/gj2/{id}/{title}', function ($id, $title) {
   return app('App\Http\Controllers\PropertyController')->redirectArticle('en',$id);
})->where([ 'title' => '.*','id' => '[0-9]+']);

Route::get('brenda/shfaqart/gj1/{id}/{title}', function ($id, $title) {
   return app('App\Http\Controllers\PropertyController')->redirectArticle('en',$id);
})->where([ 'title' => '.*','id' => '[0-9]+']);

Route::get('brenda/gj1/46/Prona_ne_Himare.html ', function () {
   return redirect('sq/prona-ne-Himare',301);
}); 
Route::get('brenda/gj1/39/Prona_ne_Shengjin.html', function () {
   return redirect('sq/prona-ne-Lezhe',301);
}); 
Route::get('brenda/gj1/40/Prona_ne_Elbasan.html', function () {
   return redirect('sq/prona-ne-Elbasan',301);
}); 
Route::get('brenda/gj1/41/Prona_ne_Golem.html', function () {
   return redirect('sq/prona-ne-Golem',301);
}); 
Route::get('brenda/gj1/43/Prona_ne_Dhermi.html', function () {
   return redirect('sq/prona-ne-Dhermi',301);
}); 
Route::get('brenda/gj1/57/Vila_per_shitje_ne_Shqiperi.html', function () {
   return redirect('sq/vile-ne-shitje-ne-Tirane',301);
}); 
Route::get('brenda/gj1/60/Prona_ne_Lezhe.html', function () {
   return redirect('sq/prona-ne-Lezhe',301);
}); 
Route::get('brenda/gj1/61/Prona_ne_Velipoje.html', function () {
   return redirect('sq/prona-ne-Velipoje',301);
});  
//lands_for_sale/gj1/56/Troje_per_Shitje_ne_Shqiperi.html
Route::get('lands_for_sale/gj1/56/Troje_per_Shitje_ne_Shqiperi.html', function () {
   return redirect('sq/toke-ne-shitje-ne-Tirane',301);
});
Route::get('villa_for_rent/gj1/58/Vila_me_Qera_ne_Shqiperi.html', function () {
   return redirect('sq/vile-me-qera-ne-Tirane',301);
});

Route::get('villa_for_rent/gj1/58/Vila_me_Qera_ne_Shqiperi.html', function () {
   return redirect('sq/vile-me-qera-ne-Tirane',301);
});


//brenda/gj1/8/Rreth_Shqiperise.html     
Route::get('brenda/gj1/{id}/{title}html', function ($id, $title) {
   return redirect()->route('homesq','', 301);
})->where([ 'title' => '.*','id' =>'(2|3|4|27|8|10|12|14|26|28|31|68|62|63|59|42|38)']);
/*
Route::get('brenda/gj1/{id}/{title}html', function ($id, $title) {
   return redirect('sq/prona-ne-Vlore',301);
})->where([ 'title' => '.*','id' =>'(44|45)']);
*/
//propertytype/gj1/158/Apartament/Sarande/Apartment__per_shitje_ne_Sarande.html 
Route::get('propertytype/gj1/{id}/{proptype}/{city}/{title}', function ($id, $proptype,$city,$title) {
   return redirect()->route('homesq','', 301);
})->where([ 'title' => '.*','id' => '(158|156|159|160|161|162|163|165|166|167|168|169)']);


//register_alb/gj1/9/register_alb.html
Route::get('register_alb/gj1/9/register_alb.html', function () {
   return redirect()->route('homesq','', 301);
});
Route::get('okazion/gj1/69/Prona_Okazion.html', function () {
   return redirect()->route('homesq','', 301);
});
Route::get('property_in_albania/gj1/51/Prona_ne_Shqiperi.html', function () {
   return redirect()->route('homesq','', 301);
});
Route::get('property_for_investment/gj1/prona_per_investime.html', function () {
   return redirect()->route('homesq','', 301);
});

Route::get('hotels_for_sale/gj1/55/Hotele_per_Shitje_ne_Shqiperi.html', function () {
   return redirect()->route('homesq','', 301);
});

Route::get('apartments_for_rent/gj1/52/Apartamente_me_Qera_ne_Shqiperi.html', function () {
   return redirect('sq/apartament-me-qera-ne-Tirane',301);
});
Route::get('apartaments_for_sale/gj1/54/Apartamente_per_Shitje_ne_Shqiperi.html', function () {
   return redirect('sq/apartament-ne-shitje-ne-Tirane',301);
});
Route::get('apartaments_for_sale/gj1/54/Apartamente_per_Shitje_ne_Shqiperi.html', function () {
   return redirect('sq/apartament-ne-shitje-ne-Tirane',301);
   
});
Route::get('propertytype/gj2/.html', function () {
   return redirect()->route('homeen','', 301);
});


Route::get('/brenda/gj2/41/Properties_in_Golem.html', function () {
   return redirect('en/properties-in-Durres',301);
});

Route::get('propertytype/gj2/155/Apartament/Tirane/Apartment__for_sale_in_Tirana.html', function () {
   return redirect('en/apartment-for-sale-in-Tirana',301);
});

Route::get('propertytype/gj1/155/Apartament/Tirane/Apartment__per_shitje_ne_Tirane.html', function () {
	return redirect('sq/apartament-ne-shitje-ne-Tirane',301);
});

Route::get('propertytype/gj1/154/Apartament/Tirane/Apartament_me_qera__Tirane.html', function () {
	return redirect('sq/apartament-me-qera-ne-Tirane',301);
});

Route::get('propertytype/gj2/154/Apartament/Tirane/Apartment_for_rent_in_Tirana_.html', function () {
   return redirect('en/apartment-for-rent-in-Tirana',301);
});

Route::get('propertytype/gj1/174/Toke/Tirane/Truall_per_Shitje_ne_Tirane.html', function () {
	return redirect('sq/toke-ne-shitje-ne-Tirane',301);
});

Route::get('property_for_rent/gj2/prona_me_qera.html', function () {
	return redirect('sq',301);
});

Route::get('property_for_sale/gj2/prona_ne_shitje.html', function () {
	return redirect('sq/apartament-ne-shitje-ne-Tirane',301);
});

Route::get('brenda/gj1/35/Prona_ne_Tirane.html', function () {
	return redirect('sq/prona-ne-Tirane',301);
});

Route::get('brenda/gj2/1/Rreth_Nesh.html', function () {
	return redirect('sq/rreth-nesh',301);
});

Route::get('brenda/gj2/3/Menaxhim.html', function () {
	return redirect('/',301);
});

Route::get('register_alb/gj2/9/register_alb.html', function () {
	return redirect('sq/regjistro-pronen',301);
});

Route::get('kontakt/gj2/6/Kontakt.html', function () {
	return redirect('sq/kontakt',301);
});

Route::get('brenda/gj1/6/Kontakt.html', function () {
	return redirect('sq/kontakt',301);
});	

Route::get('brenda/gj1/69/Prona_Okazion.html', function () {
	return redirect('sq/apartament-ne-shitje-ne-Tirane',301);
});

Route::get('propertytype/gj1/172/Toke/Vlore/Truall_per_Shitje_ne_Dhermi.html', function () {
	return redirect('sq/prona-ne-Dhermi',301);
});

Route::get('brenda/gj1/37/Prona_ne_Durres.html', function (){
	return redirect('sq/prona-ne-Durres',301);
});

Route::get('propertytype/gj1/157/Vile/Tirane/Vila_me_Qera_ne_Tirane.html', function (){
	return redirect('sq/vile-me-qera-ne-Tirane',301);
});

Route::get('brenda/gj1/55/Hotele_per_Shitje_ne_Shqiperi.html', function (){
	return redirect('sq/ambient%20tregtar-ne-shitje-ne-Tirane',301);
});

Route::get('propertytype/gj1/171/Toke/Vlore/Truall_per_Shitje_ne_Vlore.html', function (){
	return redirect('sq/toke-ne-shitje-ne-Vlore',301);
});

Route::get('brenda/gj1/51/Prona_ne_Shqiperi.html', function (){
	return redirect('sq/prona-ne-shqiperi',301);
});

Route::get('brenda/gj2/56/Lands_for_Sale_in_Albania.html', function (){
	return redirect('sq/prona-ne-shqiperi',301);
});

Route::get('brenda/gj1/54/Apartamente_per_Shitje_ne_Shqiperi.html', function (){
	return redirect('sq/apartament-ne-shitje-ne-Tirane',301);
});

Route::get('brenda/gj1/25/Prona_ne_Sarande.html', function (){
	return redirect('sq/prona-ne-Sarande',301);
});

Route::get('brenda/gj2/55/Hotels_for_Sale_in_Albania.html', function (){
	return redirect('/',301);
});

Route::get('brenda/gj2/57/Villas_for_Sale_in_Albania.html', function() {
	return redirect('en/villa-for-sale-in-Tirana',301);
});

Route::get('brenda/gj2/54/Apartments_for_Sale_in_Albania.html', function () {
	return redirect('en/apartment-for-sale-in-Tirana',301);
});

Route::get('brenda/gj1/33/Prona_ne_Vlore.html', function () {
	return redirect('sq/prona-ne-vlore',301);
});

Route::get('brenda/gj2/73/Properties_in_Kruja.html', function () {
	return redirect('/',301);
});

Route::get('propertytype/gj2/{id}/{proptype}/{city}/{title}html', function ($id, $proptype, $city,$title) {
   return redirect('en/properties-in-albania',301);
})->where([ 'title' => '.*','id' =>'(158|160|161|162|163|165|166|167|169|170|171|172|173|175|176|177|183|184|185|186|187|188|190|189|191|192|193|194|195|196|197|198|199|200|201|202|203|205|205|206)']);

Route::get('brenda/gj2/{id}/{title}html', function ($id, $title) {
   return redirect('en/properties-in-albania',301);
})->where([ 'title' => '.*','id' =>'(44|45|46|30|64|66|70|33|25|37|38|39|40|42|41|43|59|60|61|62|63)']);

Route::get('propertytype/gj2/168/Apartament/Vlore/Apartment__for_sale_in_Radhima.html', function () {
   return redirect('en/properties-in-Vlora',301);
});

Route::get('property_in_albania/gj2/51/Properties_in_Albania.html', function () {
    return redirect('en/properties-in-albania',301);
});

Route::get('villa_for_sale/gj2/57/Villas_for_Sale_in_Albania.html', function () {
    return redirect('en/villa-for-sale-in-Tirana',301);
});
Route::get('register_eng/gj2/9/register_eng.html', function () {
    return redirect('en/register-property ',301);
});
Route::get('brenda/gj2/28/Albanian_Map.html', function () {
    return redirect('en/searchbymap',301);
});

/* LEGACY ROUTES END HERE */

//Redirect old routes

//Test views

Route::view('/test123', 'contents.test');

Route::view('/test1234', 'contents.test2');