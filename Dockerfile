FROM realestate_base-php-image:latest

USER root

RUN apk add php7-exif php7-ctype

USER www-data

COPY --chown=www-data:www-data composer.json composer.lock ./
RUN composer install --no-scripts --no-autoloader --prefer-dist

ARG APP_ENV

COPY --chown=www-data:www-data deployment/supervisord.${APP_ENV}.conf /etc/supervisord.conf
COPY --chown=www-data:www-data deployment/.env.${APP_ENV} ./.env
COPY --chown=www-data:www-data deployment/cronjob.${APP_ENV}  /cronjob

COPY --chown=www-data:www-data ./ ./
RUN rm -Rf deployment

RUN composer install

USER root
RUN mv -f /home/www-data/scripts/start.${APP_ENV}.sh /start.sh
RUN echo "xdebug.idekey=real-estate" >> /etc/php7/conf.d/xdebug.ini

ENTRYPOINT ["/start.sh"]