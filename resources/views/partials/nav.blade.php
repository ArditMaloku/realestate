<nav class="navbar navbar-default navbar-fixed-top">

    @include('partials.topbar')

    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-top"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span> <span
                        class="icon-bar"></span> <span class="icon-bar"></span> <span
                        class="icon-bar"></span>
            </button>
            @if (Lang::getLocale() == "sq" )
                <a href="/{{Lang::getLocale()}}">
                    @elseif (Lang::getLocale() == "en" )
                        <a href="/">
                            @endif
                            <img src="/img/logo.jpg" class="logo" alt="Logo" title="Logo"/>
                        </a>
                        <div class="languages">
                            <ul class="list-unstyled">

                                @stack('language-switcher')

                            </ul>
                        </div>
        </div>
        <div class="collapse navbar-collapse" id="navbar-top">
            <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="/{{ Lang::getLocale() }}">Home</a></li>
                <li class=""><a
                            href="/{{Lang::getLocale()}}/{{strtolower(trans('menu.menu11'))}}">{{trans('menu.menu1')}}</a>
                </li>
                <li class=""><a
                            href="/{{Lang::getLocale()}}/{{strtolower(trans('menu.menu22'))}}">{{trans('menu.menu2')}}</a>
                </li>
                <li class=""><a
                            href="/{{Lang::getLocale()}}/{{strtolower(trans('menu.menu44'))}}">{{trans('menu.menu4')}}</a>
                </li>
                <li class=""><a
                            href="/{{Lang::getLocale()}}/{{strtolower(trans('menu.menu5'))}}">{{trans('menu.menu5')}}</a>
                </li>
            </ul>
        </div>
    </div>
    <hr class="customized-line clear-marg">
</nav>