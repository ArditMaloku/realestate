<script type="application/ld+json">
{
  "@context": "https://schema.org/",
  "@type": "Apartment",
  "name": "{{ $property->title }}",
  "image": [
    "{{ url('/foto/' . $prona->pic) }}"
   ],
  "description": "{{ strip_tags($prona->body) }}",
  "brand": {
    "@type": "Company",
    "name": "Real Estate"
  },
  "numberOfRooms": "{{ $prona->rooms }}",
  "review": {
    "@type": "Review",
    "reviewRating": {
      "@type": "Rating",
      "ratingValue": "5",
      "bestRating": "5"
    }
  },
  "aggregateRating": {
    "@type": "AggregateRating",
    "ratingValue": "5",
    "reviewCount": "100"
  }
}






</script>