<div class="row" id="propertyLinks">

    <div class="col-md-8 col-sm-8 col-xs-12">

        <h3 class="linksTitle">{{trans('messages.links-to-property-categories')}}</h3>


        <div class="box">
            <ul class="list-unstyled">
                @if (Lang::getLocale() == "en" )
                    <?php
                    $permeRent = "for";
                    $permesale = "for";
                    $page = 'sitemap';
                    ?> @else
                    <?php
                    $permeRent = "me";
                    $permesale = "ne";
                    $page = 'harta-faqes';
                    ?> @endif
                <li>
                    <h5>
                        <a href="/">{{trans('messages.properties-in')}} Albania</a>
                    </h5>
                </li>
            </ul>
        </div>
    </div>

    <div class="col-md-4 col-sm-4 col-xs-12">

    </div>
</div>
