<div id="header" class="header-slide">
    <div class="container">
        <div class="row">
            <div id="quick-search" class="col-md-4">
                <div class="box-kerkimi">
                    <div class="row">
                        <form action="/{{Lang::getLocale()}}/search" method="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div id="simpleSearch" class="col-sm-12">
                                <div class="row">
                                    <div class="col-12">
                                        <p class="search-title">{{trans('messages.search-for-properties')}}</p>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-3 col-12">
                                                    <label>{{trans('messages.location')}}</label>
                                                </div>
                                                <div class="col-sm-9 col-12">
                                                    <select class="form-control input-sm" name="city">
                                                        <option value="all-cities">{{trans('messages.all-village-city')}}</option>
                                                        <option selected="selected" value="Tirana">Tirana</option>
                                                        <option value="Vlora">Vlora</option>
                                                        <option value="Berat">Berat</option>
                                                        <option value="Borsh">Borsh</option>
                                                        <option value="Dellenje">Dellenje</option>
                                                        <option value="Dhermi">Dhermi</option>
                                                        <option value="Durres">Durres</option>
                                                        <option value="Elbasan">Elbasan</option>
                                                        <option value="Fier">Fier</option>
                                                        <option value="Golem">Golem</option>
                                                        <option value="Himara">Himara</option>
                                                        <option value="Kavaja">Kavaja</option>
                                                        <option value="Korca">Korca</option>
                                                        <option value="Kruja">Kruja</option>
                                                        <option value="Kruje">Kruje</option>
                                                        <option value="Ksamil">Ksamil</option>
                                                        <option value="Kucova">Kucova</option>
                                                        <option value="Lalzit Bay">Lalzit Bay</option>
                                                        <option value="Lezha">Lezha</option>
                                                        <option value="Lushnja">Lushnja</option>
                                                        <option value="Mat">Mat</option>
                                                        <option value="Mirdita">Mirdita</option>
                                                        <option value="Qeparo">Qeparo</option>
                                                        <option value="Rinas">Rinas</option>
                                                        <option value="Rreshen">Rreshen</option>
                                                        <option value="Saranda">Saranda</option>
                                                        <option value="Shengjin">Shengjin</option>
                                                        <option value="Shkodra">Shkodra</option>
                                                        <option value="Tirana">Tirana</option>
                                                        <option value="Velipoja">Velipoja</option>
                                                        <option value="Vlora">Vlora</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-3 col-12">
                                                    <label>{{trans('messages.type')}}</label>
                                                </div>
                                                <div class="col-sm-9 col-12">
                                                    <select name="propertyType" class="form-control input-sm"
                                                            id="property-type">
                                                        <option selected="selected"
                                                                value="apartment">{{trans('messages.apartment')}}</option>
                                                        <option value="villa">{{trans('messages.villa')}}</option>
                                                        <option value="office">{{trans('messages.office')}}</option>
                                                        <option value="warehouse">{{trans('messages.warehouse')}}</option>
                                                        <option value="land">{{trans('messages.land')}}</option>
                                                        <option value="commercial">{{trans('messages.otherlokaledyqane')}}</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-3 col-12">
                                                    <label>{{trans('messages.status')}}</label>
                                                </div>
                                                <div class="col-sm-9 col-12">
                                                    <select name="saleStatus" class="form-control input-sm">
                                                        <option selected="selected"
                                                                value="rent">{{trans('messages.for-rent')}}</option>
                                                        <option value="sale">{{trans('messages.for-sale')}}</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- advancedTabApp -->
                            <div id="advancedTabApp"
                                 class="col-sm-6 detailedTab detail-apartment hidden propertyTypeOptions">
                                <div class="row">
                                    <div class="col-12">
                                        <p class="search-title">&nbsp;</p>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">

                                            <div class="row">
                                                <div class="col-sm-3 col-12">
                                                    <label>{{trans('messages.shperndarjasearch')}}</label>
                                                </div>
                                                <div id="morehidden" style="display:none;">
                                                    <option value="more">{{trans('messages.moreoptionsearch')}}</option>
                                                </div>
                                                <div id="alloptionhidden" style="display:none;">
                                                    <option value="all">{{trans('messages.tegjithaoption')}}</option>
                                                </div>
                                                <div class="col-sm-9 col-12">
                                                    <select name="organizimi" class="form-control input-sm">
                                                        <option selected="selected" value="undefined">-</option>
                                                        <option value="Garsoniere">{{trans('messages.garsoniere')}}</option>
                                                        <option value="11">{{trans('messages.onebedroom')}}</option>
                                                        <option value="21">{{trans('messages.twobedroom')}}</option>
                                                        <option value="31">{{trans('messages.threebedroom')}}</option>
                                                        <option value="more">{{trans('messages.moreoptionsearch')}}</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">

                                            <div class="row">
                                                <div class="col-sm-3 col-12">
                                                    <label>{{trans('messages.price')}}</label>
                                                </div>
                                                <div class="col-sm-9 col-12" style="position:relative;">
                                                    <select name="cmimi" class="form-control input-sm">
                                                        <option selected="selected" value="undefined">-</option>
                                                        <option value="0-300">0-300 &euro;</option>
                                                        <option value="300-500">300-500 &euro;</option>
                                                        <option value="500-1000">500-1000 &euro;</option>
                                                        <option value="more">{{trans('messages.moreoptionsearch')}}</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- advancedTabApp end-->

                            <!-- advancedTabVila -->
                            <div id="advancedTabVila"
                                 class="col-sm-6 detailedTab detail-villa hidden propertyTypeOptions">
                                <div class="row">
                                    <div class="col-12">
                                        <p class="search-title">&nbsp;</p>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">

                                            <div class="row">
                                                <div class="col-sm-3 col-12">
                                                    <label>{{trans('messages.shperndarjasearch')}}</label>
                                                </div>
                                                <div class="col-sm-9 col-12">
                                                    <select name="shperndarja-villa" class="form-control input-sm">
                                                        <option value="2">{{trans('messages.tworoomsvilla')}}</option>
                                                        <option selected="selected"
                                                                value="3">{{trans('messages.threeroomsvilla')}}</option>
                                                        <option value="4">{{trans('messages.fourroomsvilla')}}</option>
                                                        <option value="more">{{trans('messages.moreoptionsearch')}}</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">

                                            <div class="row">
                                                <div class="col-sm-3 col-12">
                                                    <label>{{trans('messages.price')}}</label>
                                                </div>
                                                <div class="col-sm-9 col-12">
                                                    <select name="cmimi-villa" class="form-control input-sm">
                                                        <option value="0-1000">0-1000 &euro;</option>
                                                        <option value="1000-2000">1000-2000 &euro;</option>
                                                        <option value="2000-3000">2000-3000 &euro;</option>
                                                        <option value="more">{{trans('messages.moreoptionsearch')}}</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- advancedTabVila end-->

                            <!-- advancedTabZyra -->
                            <div id="advancedTabZyra"
                                 class="col-sm-6 detailedTab detail-office hidden propertyTypeOptions">
                                <div class="row">
                                    <div class="col-12">
                                        <p class="search-title">&nbsp;</p>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-3 col-12">
                                                    <label>{{trans('messages.siperfaqe')}}</label>
                                                </div>
                                                <div class="col-sm-9 col-12">
                                                    <select name="siperfaqeZyra" class="form-control input-sm">
                                                        <option selected="selected" value="undefined">-</option>
                                                        <option value="0-50">0-50 m2</option>
                                                        <option value="50-100">50-100 m2</option>
                                                        <option value="100-200">100-200 m2</option>
                                                        <option value="more">200+ m2</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">

                                            <div class="row">
                                                <div class="col-sm-3 col-12">
                                                    <label>{{trans('messages.price')}}</label>
                                                </div>
                                                <div class="col-sm-9 col-12">
                                                    <select name="cmimiZyra" class="form-control input-sm">
                                                        <option value="undefined">-</option>
                                                        <option value="0-300">0-300 &euro;</option>
                                                        <option value="300-500">300-500 &euro;</option>
                                                        <option value="500-1000">500-1000 &euro;</option>
                                                        <option value="1000-2000">1000-2000 &euro;</option>
                                                        <option value="more">more</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- advancedTabZyra end-->

                            <!-- advancedTabToka -->
                            <div id="advancedTabToka"
                                 class="col-sm-6 detailedTab detail-land hidden propertyTypeOptions">
                                <div class="row">
                                    <div class="col-12">
                                        <p class="search-title">&nbsp;</p>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">

                                            <div class="row">
                                                <div class="col-sm-3 col-12">
                                                    <label>{{trans('messages.siperfaqe')}}</label>
                                                </div>
                                                <div class="col-sm-9 col-12">
                                                    <select name="siperfaqe-toka" class="form-control input-sm">
                                                        <option value="0-1000">0-1000 m2</option>
                                                        <option selected="selected" value="1000-2000">1000-2000 m2
                                                        </option>
                                                        <option value="2000-5000">2000-5000 m2</option>
                                                        <option value="more">{{trans('messages.moreoptionsearch')}}</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-3 col-12">
                                                    <label>{{trans('messages.price')}}</label>
                                                </div>
                                                <div class="col-sm-9 col-12">
                                                    <select name="cmimi-toka" class="form-control input-sm">
                                                        <option value="all">{{trans('messages.tegjithaoption')}}</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- advancedTabToka end-->

                            <!-- advancedTabMag -->
                            <div id="advancedTabMag"
                                 class="col-sm-6 detailedTab detail-warehouse hidden propertyTypeOptions">
                                <div class="row">
                                    <div class="col-12">
                                        <p class="search-title">&nbsp;</p>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">

                                            <div class="row">
                                                <div class="col-sm-3 col-12">
                                                    <label>{{trans('messages.siperfaqe')}}</label>
                                                </div>
                                                <div class="col-sm-9 col-12">
                                                    <select name="siperfaqe-mag" class="form-control input-sm">
                                                        <option value="0-500">0-500 m2</option>
                                                        <option selected="selected" value="500-1000">500-1000 m2
                                                        </option>
                                                        <option value="1000-2000">1000-2000 m2</option>
                                                        <option value="more">{{trans('messages.moreoptionsearch')}}</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-3 col-12">
                                                    <label>{{trans('messages.price')}}</label>
                                                </div>
                                                <div class="col-sm-9 col-12">
                                                    <select name="cmimi-mag" class="form-control input-sm">
                                                        <option value="0-500">0-500 &euro;</option>
                                                        <option value="500-1000">500-1000 &euro;</option>
                                                        <option value="1000-2000">1000-2000 &euro;</option>
                                                        <option value="more">{{trans('messages.moreoptionsearch')}}</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- advancedTabMag end-->

                            <!-- advancedStore -->
                            <div id="advancedStore"
                                 class="col-sm-6 detailedTab detail-commercial hidden propertyTypeOptions">
                                <div class="row">
                                    <div class="col-12">
                                        <p class="search-title">&nbsp;</p>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">

                                            <div class="row">
                                                <div class="col-sm-3 col-12">
                                                    <label>{{trans('messages.siperfaqe')}}</label>
                                                </div>
                                                <div class="col-sm-9 col-12">
                                                    <select name="siperfaqe-store" class="form-control input-sm">
                                                        <option value="0-50">0-50 m2</option>
                                                        <option selected="selected" value="50-100">50-100 m2</option>
                                                        <option value="100-200">100-200 m2</option>
                                                        <option value="more">{{trans('messages.moreoptionsearch')}}</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-3 col-12">
                                                    <label>{{trans('messages.price')}}</label>
                                                </div>
                                                <div class="col-sm-9 col-12">
                                                    <select name="cmimi-store" class="form-control input-sm">
                                                        <option value="0-500">0-500 &euro;</option>
                                                        <option value="500-1000">500-1000 &euro;</option>
                                                        <option value="1000-2000">1000-2000 &euro;</option>
                                                        <option value="more">{{trans('messages.moreoptionsearch')}}</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- advancedStore end-->

                            <div class="clearfix"></div>
                            <div class="col-sm-12 search-footer">
                                <a href="#" id="detailedSearchC"><i id="chevron"
                                                                    class="fa fa-chevron-right fa-2x pull-right"
                                                                    style="margin-top: 4px"></i></a>

                                <p class="det-search-link">
                                    <a id="detailedSearch" href="#">{{trans('messages.detailed-search')}}</a>
                                </p>
                                <input id="state" name="statesearch" type="hidden" value="closed"/>
                                <button type="submit" class="search-btn btn btn-default btn-md">
                                    <i class="fa fa-search"
                                       style="font-size: 15px;"></i>&nbsp;&nbsp;{{trans('messages.search')}}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="vertical-absolute-btn hidden-xs">
        <h5>
            @if (Lang::getLocale() == "sq" )
                <a href='/sq/registro-pronen' class="btn btn-default btn-block">{{trans('messages.your-property')}}</a>
            @endif
            @if (Lang::getLocale() == "en" )
                <a href='/en/register-property'
                   class="btn btn-default btn-block">{{trans('messages.your-property')}}</a>
            @endif
        </h5>
    </div>

</div>