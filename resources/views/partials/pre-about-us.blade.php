<div class="row">
    <div class="col-md-4 noPadRight">
        <div class="company-container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="company-description">
                        <a href="/{{Lang::getLocale()}}/{{strtolower(trans('menu.menu22'))}}"><h4 class="module-title">{{trans('menu.menu2')}}</h4>
                        <div class="company-image">
                            <img src="/img/map.jpg" style="width:130px" title="Albanian RealEstate"
                                 alt="Albanian RealEstate">
                        </div>
                        </a>
                        <div class="company-info">
							<p class="noMargBtm">{{trans('aboutus.desc1')}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

   <div class="col-md-4 noPadRight">
        <div class="company-container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="company-description">
                        <a href="/{{Lang::getLocale()}}/{{strtolower(trans('menu.menu5'))}}"><h4 class="module-title">{{trans('messages.juristical-advice')}}</h4>
                        <div class="company-image">
                            <img src="/img/logo.jpg" style="width:250px" title="Albanian RealEstate"
                                 alt="Albanian RealEstate">
                        </div>
                        </a>
                        <div class="company-info">
                            <p class="noMargBtm">{{trans('aboutus.desc2')}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   	</div>  


    <div class="col-md-4 noPadRight">
      <div class="company-container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="company-description">            
	                    <h4 class="module-title">{{trans('messages.search-by-keyword')}}</h4>
			            <div class="custom-search">
			                <div class="csearch">
			                    <script>
			                        (function () {
			                            var cx = '018134650729431091831:xay75cylzuw';
			                            var gcse = document.createElement('script');
			                            gcse.type = 'text/javascript';
			                            gcse.async = true;
			                            gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
			                            var s = document.getElementsByTagName('script')[0];
			                            s.parentNode.insertBefore(gcse, s);
			                        })();
			                        // document.getElementById('gsc-i-id1').placeholder = "";
			                    </script>
			                    <gcse:search></gcse:search>
				            </div>
				        </div>
				        <div class="company-info">
                            <p class="noMargBtm">{{trans('aboutus.desc3')}}</p>
                        </div>
                    </div>
                </div>
            </div>
		</div>
    </div>
</div>