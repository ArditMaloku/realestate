<div class="col-md-2 col-md-pull-10 sidebar noPadRight">
    <div class="box box-sidebar box-white">
        <div class="box-header">
            <h2>{{trans('messages.properties-in')}} {{trans('messages.tirana')}}</h2>
        </div>
        <ul class="list-check">
            @if(Lang::getLocale()=="en")
                <?php $propertyTypes = array("Apartment", "Villa", "Land", "Office", "Warehouse", "Commercial");?>
                @foreach ($propertyTypes as $propertyType)
                    @if($propertyType=="Land")
                        <li><h2>
                                <a href="{{route('category-cities', [Lang::getLocale(),strtolower($propertyType),"for","sale","in","Tirana"])}}">{{trans('messages.'.strtolower($propertyType).'-for-sale')}}
                                    Tirana</a></h2>
                    @else
                        <li><h2>
                                <a href="{{route('category-cities', [Lang::getLocale(),strtolower($propertyType),"for","rent","in","Tirana"])}}">{{trans('messages.'.strtolower($propertyType).'-for-rent')}}
                                    Tirana</a></h2>
                        <li><h2>
                                <a href="{{route('category-cities', [Lang::getLocale(),strtolower($propertyType),"for","sale","in","Tirana"])}}">{{trans('messages.'.strtolower($propertyType).'-for-sale')}}
                                    Tirana</a></h2>
                    @endif
                @endforeach

                    <li>
                        <h2>
                            <a href="/{{Lang::getLocale()}}/properties-in-albania">{{trans('messages.properties-in')}}
                                Albania</a>
                        </h2>
                    </li>
            @else
                <?php
                $propertyTypes = array("Apartament", "Vile", "Toke", "Zyre", "Magazine", "Ambient tregtar");
                $propertyTypesMessages = array("Apartment", "Villa", "Land", "Office", "Warehouse", "Commercial");
                ?>
                @foreach ($propertyTypes as $keyProperty=>$propertyType)
                    @if($propertyType=="Toke")
                        <li><h2>
                                <a href="{{route('category-cities', [Lang::getLocale(),strtolower($propertyType),"ne","shitje","ne","Tirane"])}}">{{trans('messages.'.strtolower($propertyTypesMessages[$keyProperty]).'-for-sale')}}
                                    Tirane</a></h2>
                    @else
                        <li><h2>
                                <a href="{{route('category-cities', [Lang::getLocale(),strtolower($propertyType),"me","qera","ne","Tirane"])}}">{{trans('messages.'.strtolower($propertyTypesMessages[$keyProperty]).'-for-rent')}}
                                    Tirane</a></h2>
                        <li><h2>
                                <a href="{{route('category-cities', [Lang::getLocale(),strtolower($propertyType),"ne","shitje","ne","Tirane"])}}">{{trans('messages.'.strtolower($propertyTypesMessages[$keyProperty]).'-for-sale')}}
                                    Tirane</a></h2>
                    @endif
                @endforeach

                    <li>
                        <h2>
                            <a href="/{{Lang::getLocale()}}/prona-ne-shqiperi">{{trans('messages.properties-in')}}
                                Shqiperi</a>
                        </h2>
                    </li>
            @endif
        </ul>
    </div>

    <div class="box box-sidebar box-white social-media">
        <div class="box-header">
            <p>{{trans('messages.social-media')}}</p>
        </div>

        <div>
            <iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fwww.realestate.al&width=150&layout=button_count&action=like&size=large&show_faces=false&share=false&height=30"
                    width="150" height="30" style="border:none;overflow:hidden" scrolling="no" frameborder="0"
                    allowTransparency="true" allow="encrypted-media"></iframe>
        </div>

        <div>
            <a class="btn btn-primary twitter-button"
               href="https://twitter.com/RealEstate_al" target="_blank">
                <i class="fa fa-twitter"></i>&nbsp;&nbsp;<strong>{{trans('messages.follow')}} @realestate_al</strong>
            </a>
        </div>

        <div>
            <a class="btn btn-primary border-0" style="padding: 4px 10px;"
               href="https://www.linkedin.com/company/albanian-real-estate/" target="_blank">
                <i class="fa fa-linkedin"></i>&nbsp;&nbsp;<strong>Follow</strong>
            </a>
        </div>
    </div>
</div>