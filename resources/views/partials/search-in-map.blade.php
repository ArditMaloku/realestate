<!-- Search in MAP -->
<div class="row">
    <div class="col-md-12 noPadRight">
        <div class="map-div" style="min-height:400px;">
            <h2 class="module-title">{{trans('messages.search-in-map')}}</h2>
            <div id="map"></div>
            <?php
            $pronatHarta = '';
            ?>
        </div>
        <div class="col-md-12 noPadRight">

            @php
                $grouped = $grouped ?? false;
            @endphp

            @if($grouped)
                @foreach($properties as $p)
                    @foreach($p as $property)

                        <?php
                        $typeSaleRent = "";
                        $sip = "";
                        $price = "Price";
                        if (Lang::getLocale() == "en") {
                            $typeSaleRent = ($property->sale) ? "Type: For Sale" : "Type: For Rent";
                            $sip = "Sq";
                            $price = "Price";
                        } else {
                            $typeSaleRent = ($property->sale) ? "Tipi: Ne Shitje" : "Tipi: Me Qira";
                            $sip = "Sip";
                            $price = "Cmimi";
                        }

                        $splitKordinata = explode(", ", $property->koordinata);
                        if (count($splitKordinata) == 2 && strlen($property->koordinata) < 25) {
                            $pronatHarta .= "['<div class=\"tooltip-on-map\"><img src=\"https://www.realestate.al/foto/" . $property->pic . "\" class=\"tooltip-property-image\" alt=\"" . addslashes($property->title) . "\" title=\"" . addslashes($property->title) . "\" /><div class=\"tooltip-property-body\"><p class=\"tooltip-property-title\"><a target=\"_blank\" href=\"/" . Lang::getLocale() . "/" . str_slug($property->title) . "." . $property->id . "\">" . str_limit(addslashes($property->title), 60) . "</a></p><p class=\"tooltip-property-type\">" . $typeSaleRent . "</p><p class=\"tooltip-property-area\">" . $sip . ": " . $property->area . "</p><p class=\"tooltip-property-price\">" . $price . ": " . $property->price . "</p></div></div>',
                                                                " . $property->koordinata . ", 4,'" . strtolower($property->type) . "-" . (($property->sale == 1) ? "sale" : "rent") . "'],";
                        }
                        ?>
                    @endforeach
                @endforeach

            @else

                @foreach($properties as $property)
                    <?php
                    $typeSaleRent = "";
                    $sip = "";
                    $price = "Price";
                    if (Lang::getLocale() == "en") {
                        $typeSaleRent = ($property->sale) ? "Type: For Sale" : "Type: For Rent";
                        $sip = "Sq";
                        $price = "Price";
                    } else {
                        $typeSaleRent = ($property->sale) ? "Tipi: Ne Shitje" : "Tipi: Me Qira";
                        $sip = "Sip";
                        $price = "Cmimi";
                    }

                    $splitKordinata = explode(", ", $property->koordinata);
                    if (count($splitKordinata) == 2 && strlen($property->koordinata) < 25) {
                        $pronatHarta .= "['<div class=\"tooltip-on-map\"><img src=\"https://www.realestate.al/foto/" . $property->pic . "\" class=\"tooltip-property-image\" alt=\"" . addslashes($property->title) . "\" title=\"" . addslashes($property->title) . "\" /><div class=\"tooltip-property-body\"><p class=\"tooltip-property-title\"><a target=\"_blank\" href=\"/" . Lang::getLocale() . "/" . str_slug($property->title) . "." . $property->id . "\">" . str_limit(addslashes($property->title), 60) . "</a></p><p class=\"tooltip-property-type\">" . $typeSaleRent . "</p><p class=\"tooltip-property-area\">" . $sip . ": " . $property->area . "</p><p class=\"tooltip-property-price\">" . $price . ": " . $property->price . "</p></div></div>',
                                                                " . $property->koordinata . ", 4,'" . strtolower($property->type) . "-" . (($property->sale == 1) ? "sale" : "rent") . "'],";
                    }
                    ?>
                @endforeach
            @endif


            <script>
                var reviews = [];
                var locations = [
                    <?php echo $pronatHarta; ?>
                ];


                function initMap() {
                    var map = new google.maps.Map(document.getElementById('map'), {
                        zoom: 13,
                        center: new google.maps.LatLng(41.328808, 19.817365),
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        styles: [{
                            "featureType": "poi.business",
                            "elementType": "geometry.fill",
                            "stylers": [{
                                "color": "#97d163"
                            }]
                        }, {
                            "featureType": "poi.park",
                            "elementType": "geometry.fill",
                            "stylers": [{
                                "color": "#83c24b"
                            }]
                        }, {
                            "featureType": "water",
                            "elementType": "geometry.fill",
                            "stylers": [{
                                "color": "#8ed4eb"
                            }]
                        }, {
                            "featureType": "water",
                            "elementType": "labels.text",
                            "stylers": [{
                                "gamma": "0.8"
                            }]
                        }]
                    });

                    var infowindow = new google.maps.InfoWindow();
                    var marker, i;
                    var image = "https://cdn2.iconfinder.com/data/icons/places-4/100/home_place_marker_location_house_apartment-32.png";

                    var request = {
                        placeId: 'ChIJFThnUggxUBMRpY_Y86UFsgY' // example: ChIJN1t_tDeuEmsRUsoyG83frY4
                    };

                    var service = new google.maps.places.PlacesService(map);

                    service.getDetails(request, function (place, status) {


                        if (status == google.maps.places.PlacesServiceStatus.OK) {
                            for (i = 0; i < 2; i++) {
                                reviews.push(place.reviews[i]);
                                // insertReviews(place.reviews[i]);
                            }
                        }
                    });

                    for (i = 0; i < locations.length; i++) {
                        if (locations[i][4] == "apartment-rent" || locations[i][4] == "apartament-rent") {
                            image = '/img/1.png';
                        } else if (locations[i][4] == "villa-rent" || locations[i][4] == "vile-rent") {
                            image = '/img/2.png';
                        } else if (locations[i][4] == "office-rent" || locations[i][4] == "zyre-rent" || locations[i][4] == "store-rent" || locations[i][4] == "dyqan-rent") {
                            image = '/img/5.png';
                        } else if (locations[i][4] == "warehouse-rent" || locations[i][4] == "magazine-rent") {
                            image = '/img/3.png';
                        } else if (locations[i][4] == "land-rent" || locations[i][4] == "toke-rent") {
                            image = '/img/4.png';
                        } else if (locations[i][4] == "lokal-rent" || locations[i][4] == "club-rent") {
                            image = '/img/www.png';
                        } else if (locations[i][4] == "apartment-sale" || locations[i][4] == "apartament-sale") {
                            image = '/img/6s.png';
                        } else if (locations[i][4] == "lokal-sale" || locations[i][4] == "club-sale") {
                            image = '/img/1s.png';
                        } else if (locations[i][4] == "villa-sale" || locations[i][4] == "vile-sale") {
                            image = '/img/2s.png';
                        } else if (locations[i][4] == "office-sale" || locations[i][4] == "zyra-sale" || locations[i][4] == "store-sale") {
                            image = '/img/5s.png';
                        } else if (locations[i][4] == "warehouse-sale" || locations[i][4] == "magazine-sale") {
                            image = '/img/3s.png';
                        } else if (locations[i][4] == "land-sale" || locations[i][4] == "toke-sale") {
                            image = '/img/4s.png';
                        } else if (locations[i][4] == "bar-sale" || locations[i][4] == "lokal-sale" || locations[i][4] == "business-sale" || locations[i][4] == "biznes-sale") {
                            image = '/img/1s.png';
                        } else if (locations[i][4] == "store-sale" || locations[i][4] == "dyqan-sale") {
                            image = '/img/7.png';
                        } else if (locations[i][4] == "business-rent" || locations[i][4] == "biznes-rent") {
                            image = '/img/7s.png';
                        } else {
                            image = "https://cdn2.iconfinder.com/data/icons/places-4/100/home_place_marker_location_house_apartment-32.png";
                        }
                        //console.log(locations[i][3]);
                        marker = new google.maps.Marker({
                            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                            icon: image,
                            map: map
                        });

                        google.maps.event.addListener(marker, 'click', (function (marker, i) {
                            return function () {
                                infowindow.setContent(locations[i][0]);
                                infowindow.open(map, marker);
                            }
                        })(marker, i));

                        google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                            return function () {
                                infowindow.setContent(locations[i][0]);
                                infowindow.open(map, marker);
                            }
                        })(marker, i));

                    }
                }
            </script>
            <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyAqXh2RBOhtWKtPEsvfcOwYoHjcTVkiUXI&callback=initMap">

            </script>
        </div>
    </div>
</div>