@include('partials.topbar')

<!-- Header -->
<header>
    <!-- Navbar -->
    <nav class="js-navbar-scroll navbar fixed-top navbar-expand-lg navbar-dark">
        <div class="container-fluid">
            <a class="navbar-brand" href="home-page.html">
                <img src="assets/img/logo-white.png" alt="Stream UI Kit" style="width: 100px;">
            </a>

            <button class="navbar-toggler" type="button"
                    data-toggle="collapse"
                    data-target="#navbarTogglerDemo"

                    aria-controls="navbarTogglerDemo"
                    aria-expanded="false"
                    aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarTogglerDemo">
                <ul class="navbar-nav mt-2 mt-lg-0">
                    <li class="nav-item mr-4 mb-2 mb-lg-0">
                        <a class="nav-link active" href="/">back to realestate.al</a>
                    </li>
                </ul>

                <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                    <li class="nav-item mr-4 mb-2 mb-lg-0">
                        <a class="nav-link active" href="/{{ Lang::getLocale() }}">Home</a>
                    </li>
                    <li class="nav-item mr-4 mb-2 mb-lg-0">
                        <a class="nav-link" href="/{{Lang::getLocale()}}/about-us">{{trans('menu.menu1')}}</a>
                    </li>
                    <li class="nav-item mr-4 mb-2 mb-lg-0">
                        <a class="nav-link" href="/{{Lang::getLocale()}}/searchbymap">{{trans('menu.menu2')}}</a>
                    </li>
                    <li class="nav-item mr-4 mb-2 mb-lg-0">
                        <a class="nav-link" href="/{{Lang::getLocale()}}/commisions">{{trans('menu.menu4')}}</a>
                    </li>
                    <li class="nav-item mr-4 mb-2 mb-lg-0">
                        <a class="nav-link"
                           href="/{{Lang::getLocale()}}/{{strtolower(trans('menu.menu5'))}}">{{trans('menu.menu5')}}</a>
                    </li>
                </ul>
                <div>
                    <a class="btn btn-primary" data-toggle="modal" href="#callUsModal" href="#!">
                        <i class="fas fa-phone-square mr-1"></i> Call us
                    </a>
                </div>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->

    <!-- Promo Block -->
    <section class="js-parallax u-promo-block u-promo-block--mheight-450 u-overlay u-overlay--dark text-white"
             style="background-image: url(/img/wallpaper_1.jpg);">
        <!-- Promo Content -->
        <div class="container u-overlay__inner u-ver-center u-content-space">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="text-center">
                        @include ('partials.search-header4')

                        <p class="text-uppercase u-letter-spacing-sm mb-0">Clean and Fully Responsive Template</p>
                        <h1 class="display-sm-4 display-lg-3 mb-3">We are <span class="js-display-typing"></span></h1>
                        <a class="btn btn-primary" href="services.html">
                            Learn More <i class="fas fa-arrow-alt-circle-right ml-2"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Promo Content -->
    </section>
    <!-- End Promo Block -->
</header>
<!-- End Header -->
