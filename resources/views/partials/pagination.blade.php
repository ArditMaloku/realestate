@if ($paginator->hasPages())
    <ul class="pagination">
        <?php
        $url = Request::url();
        $pageUrl = explode("?page=", $url);
        $idPage = substr($pageUrl[0], strrpos($pageUrl[0], '/') + 1);
        $idPage = is_numeric($idPage) ? $idPage : 1;
        $pageUrl = str_ireplace("/" . $idPage, "", $pageUrl[0]);
        $idPagePrevious = $idPage > 1 ? $idPage - 1 : $idPage;
        ?>

        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="page-item disabled"><span class="page-link">&laquo;</span></li>
        @else
            <li class="page-item"><a class="page-link" href="{{ $pageUrl }}/{{$idPagePrevious}}"
                                     rel="prev">&laquo;</a></li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="page-item disabled"><span class="page-link">{{ $element }}</span></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="page-item active"><span class="page-link">{{ $page }}</span></li>
                    @else
                        <?php
                        $pageUrl = explode("?page=", $url)[0];
                        $pageUrl = str_ireplace("/" . $idPage, "", $pageUrl);
                        ?>
						@if ($page==1)
							<li class="page-item"><a class="page-link" href="{{ $pageUrl }}">{{ $page }}</a>
	                        </li>
						@else
	                        <li class="page-item"><a class="page-link" href="{{ $pageUrl }}/{{$page}}">{{ $page }}</a>
	                        </li>
						@endif
                    @endif
                @endforeach
            @endif
        @endforeach
        <?php
        $url = Request::url();
        $pageUrl = explode("?page=", $url);
        $idPage = substr($pageUrl[0], strrpos($pageUrl[0], '/') + 1);
        $idPage = is_numeric($idPage) ? $idPage : 1;
        $pageUrl = str_ireplace("/" . $idPage, "", $pageUrl[0]);
        $nextPageId = $idPage + 1;
        ?>
        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li class="page-item"><a class="page-link" href="{{ $pageUrl }}/{{$nextPageId}}" rel="next">&raquo;</a></li>
        @else
            <li class="page-item disabled"><span class="page-link">&raquo;</span></li>
        @endif
    </ul>
@endif
