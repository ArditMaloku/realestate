@if (Lang::getLocale() == "en" )
    <link rel="alternate" hreflang="sq" href="https://www.realestate.al/sq"/>
	<link rel="alternate" hreflang="en" href="https://www.realestate.al/"/>
	<link rel="canonical" href="<?php echo url()->current() ?>"/>
@endif
@if (Lang::getLocale() == "sq" )
    <link rel="alternate" hreflang="sq" href="https://www.realestate.al/sq"/>    
    <link rel="alternate" hreflang="en" href="https://www.realestate.al/"/>
    <link rel="canonical" href="<?php echo url()->current() ?>" />
@endif
