<div id="footer" class="bottomPart">
    <div class="container">
        <div class="row">

            <div class="col-md-8 col-sm-12 col-xs-12">
                <div class="box network">
                    <ul class="list-unstyled">
                        <li>
                            <h6>
                                @if (Lang::getLocale() =="en")
                                    <a href="/sitemap.xml">Sitemap</a>
                                @elseif (Lang::getLocale() =="sq")
                                    <a href="/sitemap.xml">Harta e faqes</a>
                                @endif
                            </h6>
                        </li>
                        <br/>
                        <li>RealEstate-al Sh.p.k . Nuis Nr <strong>L61313026R</strong></li>
						<li>Copyright &copy; {{ date('Y') }} All Rights Reserved.</li>
                    </ul>
                </div>
            </div>

			<div class="col-md-4 col-sm-12 col-xs-12" id="footerlogo">
			          <img src="/img/logo-footer.png" alt="Real Estate" border="0" height="90" width="210">
			</div>
        </div>
    </div>
</div>