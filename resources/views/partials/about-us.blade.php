<div class="row">
    <div class="col-md-6 noPadRight">
        <div class="company-container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="company-description">
                        <h4 class="module-title">{{trans('messages.about-us')}}</h4>
                        <div class="company-image">
                            <img src="/img/company.jpg" title="Albanian RealEstate"
                                 alt="Albanian RealEstate">
                        </div>
                        <div class="company-info">
                            <p class="noMargBtm">{{trans('aboutus.description1')}}</p>

                            <p class="noMargBtm">{{trans('aboutus.description2')}}</p>
                            <p>
                                <a class="pull-right">{{trans('messages.read-more')}}</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="col-md-3 noPadRight">
        <div class="posts reviews review_link">
            <h4 class="module-title">{{trans('messages.reviews')}}</h4>

            <?php

            $reviewsLink = "https://realestateal-apartment-villa-office-in-tirana.business.site/";

            ?>


            <div class="post-container review-container">
                <div class="post-image review-image"
                     style="background: url({{ asset('img/google1.png') }}) no-repeat; background-size: cover"></div>
                <div class="post-content review-content">
                    <div class="heading-title">
                        <p>
                            <a>Very happy with
                                their services. I strongly
                                recommend.</a>
                        </p>
                    </div>
                    <div class="post-description">
                        <p class="author">
                            <small class="review-author">Law Group</small>
                        </p>
                    </div>
                    <div class="post-date">
                        <span><em class="review-date">4 months ago</em></span>
                    </div>
                </div>
            </div>

			
            <div class="post-container review-container">
                <div class="post-image review-image"
                     style="background: url({{ asset('img/google2.png') }}) no-repeat; background-size: cover"></div>
                <div class="post-content review-content">
                    <div class="heading-title">
                        <p>
                            <a>Ottimo servizio,
                                efficienti, seri e con
                                rapporto qualità prezzo senza pari.
                                Complimenti, un'ottima spalla per chi recandosi a Tirana non sà da dove
                                cominciare...</a>
                        </p>
                    </div>
                    <div class="post-description">
                        <p class="author">
                            <small class="review-author">Massimiliano Conticini</small>
                        </p>
                    </div>
                    <div class="post-date">
                        <span><em class="review-date">3 years ago</em></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="col-md-3 noPadRight">

        <div class="posts">
            <h4 class="module-title">{{trans('messages.from-our-blog')}}</h4>

            <div class="post-container">
                <div class="post-image"
                     style="background: url({{ asset('img/linkedin1.jpeg') }}) no-repeat; background-size: cover"></div>
                <div class="post-content">
                    <div class="heading-title">
                        <p>
                            <a href="https://www.linkedin.com/pulse/where-do-you-want-rent-your-villa-albanian-real-estate/"
                               target="_blank">Where do you want to rent your villa?</a>
                        </p>
                    </div>
                    <div class="post-description">
                        <p>
                            <small>With us you have more possibilities to find your ideal home...</small>
                        </p>
                    </div>
                    <div class="post-date">
                        <span><em>January 22, 2016</em></span>
                    </div>
                </div>
            </div>

            <div class="post-container">
                <div class="post-image"
                     style="background: url({{ asset('img/linkedin2.jpeg')  }}) no-repeat; background-size: cover"></div>
                <div class="post-content">
                    <div class="heading-title">
                        <p>
                            <a href="https://www.linkedin.com/pulse/find-you-perfect-office-space-albanian-real-estate/"
                               target="_blank">Find you perfect office space!</a>
                        </p>
                    </div>
                    <div class="post-description">
                        <p>
                            <small>Albanian Real Estate ofron nje shumellojshmeri ambjentesh per zyra ne Tirane...
                            </small>
                        </p>
                    </div>
                    <div class="post-date">
                        <span><em>November 7, 2015</em></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>