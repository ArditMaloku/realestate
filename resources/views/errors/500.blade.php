@extends('layouts.main')
@section('title', "Not Found - 500")

@push('hreflang')
    @if (Lang::getLocale() == "en" )
        <link rel="alternate" hreflang="sq" href="/sq"/>
    @endif
    @if (Lang::getLocale() == "sq" )
        <link rel="alternate" hreflang="en" href="/en"/>
    @endif
    <link rel="canonical" href="https://www.realestate/{{Lang::getLocale()}}"/>
@endpush

@push('language-switcher')
    @if (Lang::getLocale() == "en" )
        <li>
            <a href="/sq">
                <img class="flag" src="{{ asset('/img/flags/Albania.png') }}" title="Albania.png"
                     alt="Albania.png"/>
            </a>
        </li>
    @else
        <li>
            <a href="/en">
                <img class="flag" src="{{ asset('/img/flags/UK.png') }}" title="UK.png" alt="UK.png"/>
            </a>
        </li>
    @endif

    <style>
        #google_translate_element {
            position: absolute;
            background: #fff;
            padding: 20px;
        }
    </style>

<!--     @include('partials.google-trans') -->
@endpush

@section('content')

    <div id="content" style="padding-top: 70px">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <section id="error404" style="padding: 80px 0;">
                        <div class="row">
                            <div class="col-sm-2">
                                <a class="btn btn-info btn-sm btn-block" href="javascript:history.go(-1)">&laquo;
                                    Back</a>
                            </div>
                            <div class="col-sm-8 error-text">
                                <h1>500 Error!</h1>
                                <p><strong>For Some Reason The Page You Requested Could Not Be Found On Our
                                        Server</strong></p>

                                <p>


                                    <?php $propertyTypes = array("Apartment", "Villa", "Land", "Office", "Warehouse", "Commercial");?>
                                    @foreach ($propertyTypes as $propertyType)

                                        @if($propertyType=="Land")
                                            <a style="padding: 0 8px;"
                                               href="{{route('category-cities', ['en',strtolower($propertyType),"for","sale","in","Tirana"])}}">{{trans('messages.'.strtolower($propertyType).'-for-sale', [], 'en')}}
                                                Tirana</a>
                                        @else
                                            <a style="padding: 0 8px;"
                                               href="{{route('category-cities', ['en',strtolower($propertyType),"for","rent","in","Tirana"])}}">{{trans('messages.'.strtolower($propertyType).'-for-rent', [], 'en')}}
                                                Tirana</a>
                                            <a style="padding: 0 8px;"
                                               href="{{route('category-cities', ['en',strtolower($propertyType),"for","sale","in","Tirana"])}}">{{trans('messages.'.strtolower($propertyType).'-for-sale', [], 'en')}}
                                                Tirana</a>
                                        @endif
                                    @endforeach

                                    <a style="padding: 0 8px;"
                                       href="/en/properties-in-albania">{{trans('messages.properties-in', [], 'en')}}
                                        Albania</a>

                                <hr/>
                                <?php
                                $propertyTypes = array("Apartament", "Vile", "Toke", "Zyre", "Magazine", "Ambient tregtar");
                                $propertyTypesMessages = array("Apartment", "Villa", "Land", "Office", "Warehouse", "Commercial");
                                ?>
                                @foreach ($propertyTypes as $keyProperty=>$propertyType)

                                    @if($propertyType=="Toke")
                                        <a style="padding: 0 8px;"
                                           href="{{route('category-cities', ['sq',strtolower($propertyType),"ne","shitje","ne","Tirane"])}}">{{trans('messages.'.strtolower($propertyTypesMessages[$keyProperty]).'-for-sale', [], 'sq')}}
                                            Tirane</a>
                                    @else
                                        <a style="padding: 0 8px;"
                                           href="{{route('category-cities', ['sq',strtolower($propertyType),"me","qera","ne","Tirane"])}}">{{trans('messages.'.strtolower($propertyTypesMessages[$keyProperty]).'-for-rent', [], 'sq')}}
                                            Tirane</a>
                                        <a style="padding: 0 8px;"
                                           href="{{route('category-cities', ['sq',strtolower($propertyType),"ne","shitje","ne","Tirane"])}}">{{trans('messages.'.strtolower($propertyTypesMessages[$keyProperty]).'-for-sale', [], 'sq')}}
                                            Tirane</a>
                                    @endif
                                @endforeach

                                <a style="padding: 0 8px;"
                                   href="/sq/prona-ne-shqiperi">{{trans('messages.properties-in', [], 'sq')}}
                                    Shqiperi</a>

                                </p>

                            </div>
                            <div class="col-sm-2 text-right">
                                <a class="btn btn-info btn-sm btn-block" href="/">Home &raquo;</a>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

    <div id="pre-footer" class="bottomPart">
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <ol class="breadcrumb">
                        <li><a href="/">{{trans('messages.home')}}</a></li>
                        <li class="active">Not Found</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection
