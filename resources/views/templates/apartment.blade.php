<div class="col-md-12 col-sm-12 col-xs-12 noPadRight noPadLeft p-0">
    <div class="property-container" style="cursor:pointer;"
         onclick="goToLinkClickCell('/{{ Lang::getLocale()."/".str_slug($apartment->title).".".$apartment->id}}')">
        <div class="property-title">
            <h3>
                <a href="/{{ Lang::getLocale()}}/{{str_slug($apartment->title).".".$apartment->id}}">{{ strip_tags($apartment->title) }}
                </a>
            </h3>
        </div>
        <div class="middle-content">
            <div class="property-image">
                <img src="{{ url('/thumbs/350x/foto/' . $apartment->image) }}"
                     alt="{{substr(substr( strip_tags($apartment->content),0,100),0,strrpos(substr( strip_tags($apartment->content),0,250),' '))}}"
                     title="{{substr(substr( strip_tags($apartment->content),0,100),0,strrpos(substr( strip_tags($apartment->content),0,250),' '))}}"
                     height="128">
            </div>

        </div>
        <div class="middle-content property-features">
				<span><i class="fa fa-building-o"></i>&nbsp;
                    @if ($apartment->forsale == 1 )
                        {{trans('messages.for-sale')}}
                    @elseif ($apartment->forrent == 1 )
                        {{trans('messages.for-rent')}}
                    @endif<small>{{ str_replace(['TRS-', 'TRR-'], '', $apartment->kodi) }}</small></span> <span
                    class="price">{{str_replace('euro','',strtolower($apartment->price))}} &euro;</span>
        </div>
        <div class="property-content">
            <p>
                <small>{!! substr(substr( strip_tags($apartment->content),0,185),0,strrpos(substr( strip_tags($apartment->content),0,185),' ')) !!}
                    ...
                </small>
            </p>
        </div>
    </div>
</div>
