<div class="col-md-12 col-sm-6 col-xs-12 noPadRight propertylist">
    <div class="property-container" style="cursor:pointer;"
         onclick="goToLinkClickCell('/{{ Lang::getLocale()."/".str_slug($prona->title).".".$prona->id}}')">
        <div class="property-title">
            <h3>
                <a href="/{{ Lang::getLocale()}}/{{str_slug($prona->title).".".$prona->id}}">{{ strip_tags($prona->title) }}
                </a>
            </h3>
        </div>
        <div class="middle-content">
            <div class="property-image">
                <img src="{{ url('thumbs/350x/foto/' . $prona->pic) }}"
                     alt="{{substr(substr( strip_tags($prona->body),0,100),0,strrpos(substr( strip_tags($prona->body),0,250),' '))}}"
                     title="{{substr(substr( strip_tags($prona->body),0,100),0,strrpos(substr( strip_tags($prona->body),0,250),' '))}}"
                     height="128">
            </div>
        </div>
        <div class="property-features middle-content">
            <span><i class="fa fa-building-o"></i>
                                         @if ($prona->sale == 1 )
                    {{trans('messages.for-sale')}}
                @endif
                @if ($prona->rent == 1 )
                    {{trans('messages.for-rent')}}
                @endif
            <small>{{ str_replace(['TRS-', 'TRR-'], '', $prona->kodi) }}</small></span> <span
                    class="price">{{str_replace('euro','',strtolower($prona->price))}} &euro;</span>
        </div>
        <div class="property-content">
            <h3>
                <small>{!! substr(substr( strip_tags($prona->body),0,165),0,strrpos(substr( strip_tags($prona->body),0,165),' ')) !!}
                    ...
                </small>
            </h3>
        </div>
    </div>
</div>
