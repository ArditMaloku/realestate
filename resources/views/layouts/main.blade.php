<!DOCTYPE html>
<html lang="{{ Lang::getLocale() ?? 'en' }}">

<head>

    @include('partials.analytics')

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
    <meta name="description" content="@yield('description')">
    <meta name="keywords" content="@yield('keywords')"/>
    <meta name="title" content="@yield('titleMeta')"/>
    <meta name="author" content="RealEstate.Al">
    <link rel="shortcut icon" href="/favicon.ico">

    <title>@yield('title')</title>

    <link href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" type="text/css"
          rel="stylesheet">
    <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css"
          rel="stylesheet">
		 
    @stack('styles')
	<style>
		[data-text]::after {
		  content: attr(data-text);
		}
	</style>
    <link href="{{ asset('/css/style.min.css?id=17') }}" type="text/css" rel="stylesheet">

    <script>
        function goToLinkClickCell(urlLink) {
            window.location = urlLink;
        }
    </script>
	
    @stack('hreflang')

    @stack('before-head')

</head>

<body id="top">

@include('partials.nav')

@yield('content')

@include ('partials.footer')

<script src="//ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="{{ asset('js/lib/collapse.min.js') }}"></script>
<script src="{{ asset('/js/app.js?id=7') }}"></script>


<script>
	$(".review_link").click(function() {
	  window.open('https://realestateal-apartment-villa-office-in-tirana.business.site/');
	});
</script>

@stack('scripts')

@stack('post-scripts')
<script type="application/ld+json">
@if (Lang::getLocale() =="en")
{
    "@context": "https://schema.org",
    "@type": "Organization",
    "name": "RealEstate - Al sh.p.k , Real estate agency - Tirana",
    "description": "Our official page with the latest option of houses, apartments, villas, office spaces and commercial units for rent or for sale in Tirana, Albania.",
    "image": "https://www.realestate.al/img/logo.jpg"
}
@elseif (Lang::getLocale() =="sq")
{
    "@context": "https://schema.org",
    "@type": "Organization",
    "name": "RealEstate - Al sh.p.k , Agjensi imobiliare - Tirane.",
    "description": "Faqeja jone zyrtare me listemet e me te fundit per Shtepi, apartamente, vila, zyra dhe ambiente tregtare ne shitje dhe qera ne Tirane dhe gjithe Shqiperine.",
    "image": "https://www.realestate.al/img/logo.jpg"
@endif
</script>
</body>

</html>
