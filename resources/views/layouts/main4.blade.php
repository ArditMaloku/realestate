<!DOCTYPE html>
<html lang="{{ Lang::getLocale() ?? 'en' }}" class="no-js">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
    <meta name="description" content="@yield('description')">
    <meta name="author" content="RealEstate.Al">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">

    <title>@yield('title')</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- Components Vendor Styles -->
    <link rel="stylesheet" type="text/css" href="/assets/vendors/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/vendors/slick-carousel/slick.css">
    <!-- Theme Styles -->
    <link rel="stylesheet" type="text/css" href="assets/css/styles.css">
    <!-- Web Fonts -->
    <link href="//fonts.googleapis.com/css?family=Playfair+Display:400,700%7COpen+Sans:300,400,600,700"
          rel="stylesheet">

    @stack('styles')

    @stack('hreflang')

    @stack('before-head')

</head>

<body>

@include('partials.nav4')

@yield('content')

@include ('partials.footer')
@include('partials.analytics')
<script src="//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
<script src="{{ asset('/js/app.js?id=7') }}"></script>

@stack('scripts')
<script>
    function goToLinkClickCell(urlLink) {
        window.location = urlLink;
    }
</script>

@stack('post-scripts')
</body>

</html>
