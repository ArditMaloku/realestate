Tipi i apartamentit: {{ $data['tipi'] }}
Lloji i sherbimit: {{ $data['lloji'] }}
Qyteti i prones: {{ $data['qyteti'] }}
Fshati: {{ $data['fshati'] }}
Cmimi: {{ $data['cmimi'] }}
Pershkrimi i prones: {{ $data['pkoment'] }}

Detaje te klientit:

Emri: {{ $data['emri'] }}
Mbiemri: {{ $data['mbiemri'] }}
Telefoni: {{ $data['telefoni'] }}
Email: {{ $data['email'] }}
Zip code: {{ $data['zip'] }}
Qyteti: {{ $data['city'] }}
Shteti: {{ $data['shteti'] }}

Termat:

Imazhi 1: {{ $data['fileUrl1'] }}
Imazhi 2: {{ $data['fileUrl2'] }}
Imazhi 3: {{ $data['fileUrl3'] }}
Imazhi 4: {{ $data['fileUrl4'] }}
Imazhi 5: {{ $data['fileUrl5'] }}