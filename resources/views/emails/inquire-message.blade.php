@if((strpos($article->ArtTitull_gj1, '(') && strpos($article->ArtTitull_gj1, ')')) !== false)
<p>Titulli: {{ $article->ArtTitull_gj1 }}</p>
@else
<p>Titulli: {{ $article->ArtTitull_gj1.' ('.$article->kodi.')' }}</p>
@endif
<p>Qyteti: {{ $article->Qyteti_gj1 }}</p>
<p>Cmimi: {{ $article->Cmimi }}</p>
<p>Siperfaqia: {{$article->Siperfaqia }}</p>
<p>Email: <a href='mailto:{{ $data['email'] }}'>{{ $data['email'] }}</a></p>
<p>Emri: {{ $data['name'] }}</p>
<p>Telefoni: {{ $data['phone'] }}</p>
<p>Mesazhi:<br/> {{ $data['message'] }}</p>
<p>SenderIP: {{ request()->ip() }}</p>