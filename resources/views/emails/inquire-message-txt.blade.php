@if((strpos($article->ArtTitull_gj1, '(') && strpos($article->ArtTitull_gj1, ')')) !== false)
Titulli: {{ $article->ArtTitull_gj1 }}
@else
Titulli: {{ $article->ArtTitull_gj1.' ('.$article->kodi.')' }}
@endif
Qyteti: {{ $article->Qyteti_gj1 }}
Cmimi: {{ $article->Cmimi }}
Siperfaqia: {{$article->Siperfaqia }}
Email: {{ $data['email'] }}
Emri: {{ $data['name'] }}
Telefoni: {{ $data['phone'] }}
Mesazhi:
{{ $data['message'] }}
SenderIP: {{ request()->ip() }}