<p>Contact mail sent by:</p>
<p>Name: {{ $data['name']  }}</p>
<p>Email: <a href='mailto:{{ $data['email'] }}'>{{ $data['email'] }}</a></p>
<p>Phone: {{ $data['phone'] }}</p>
<p>Message:<br/> {{ $data['message'] }}</p>