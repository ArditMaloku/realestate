<p>Tipi i apartamentit: {{ $data['tipi'] }}</p>
<p>Lloji i sherbimit: {{ $data['lloji'] }}</p>
<p>Qyteti i prones: {{ $data['qyteti'] }}</p>
<p>Fshati: {{ $data['fshati'] }}</p>
<p>Cmimi: {{ $data['cmimi'] }}</p>
<p>Pershkrimi i prones: {{ $data['pkoment'] }}</p><br/>

<p>Detaje te klientit:</p>
<p>Emri: {{ $data['emri'] }}</p>
<p>Mbiemri: {{ $data['mbiemri'] }}</p>
<p>Telefoni: {{ $data['telefoni'] }}</p>
<p>Email: <a href='mailto:{{ $data['email'] }}'>{{ $data['email'] }}</a></p>
<p>Zip code: {{ $data['zip'] }}</p>
<p>Qyteti: {{ $data['city'] }}</p>
<p>Shteti: {{ $data['shteti'] }}</p><br/>

<p>Termat:</p>
<p>Imazhi 1: <a href="{{ $data['fileUrl1'] }}">{{ $data['fileUrl1'] }}</a></p>
<p>Imazhi 2: <a href="{{ $data['fileUrl2'] }}">{{ $data['fileUrl2'] }}</a></p>
<p>Imazhi 3: <a href="{{ $data['fileUrl3'] }}">{{ $data['fileUrl3'] }}</a></p>
<p>Imazhi 4: <a href="{{ $data['fileUrl4'] }}">{{ $data['fileUrl4'] }}</a></p>
<p>Imazhi 5: <a href="{{ $data['fileUrl5'] }}">{{ $data['fileUrl5'] }}</a></p>