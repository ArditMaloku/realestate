<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
<atom:link href="https://www.realestate.al/rss.xml" rel="self" type="application/rss+xml" />
<channel>
<title>Prona Real Estate Apartament Vila Zyra Magazina Truall per shitje e qira  Tirane Shqiperi</title>
  <link>https://www.realestate.al/</link>
  <description>Agjensi Imobiliare: Kerko per shitje apo blerje pasuri te patundeshme: Prona, Qira, Apartamente, Vila, Shtepi, troje, Zyra, Biznese, Magazina ne Tirane, RealEstate.al - Shqiperi"</description>
	@foreach($properties as $prona)
	<item>
    <title>{{$prona->title}}</title>
    <link>https://www.realestate.al/en/{{Str::slug($prona->title)}}.{{$prona->id}}</link>
    <description>{{$prona->description}}</description>
	  </item>
	<item>
    <title>{{$prona->titull}}</title>
    <link>https://www.realestate.al/sq/{{Str::slug($prona->titull)}}.{{$prona->id}}</link>
    <description>{{$prona->pershkrim}}</description>
	  </item>
	@endforeach	
</channel>
</rss>