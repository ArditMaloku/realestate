@extends('layouts.main')
@section('title', trans('messages.index-title'))

@push('language-switcher')
    @if (Lang::getLocale() == "en" )
        <li><a href="/sq"><img class="flag" src="/img/flags/Albania.png" alt="Zgjidhni Shqip si gjuhen e deshiruar"
                               title="Zgjidhni Shqip si gjuhen e deshiruar"/></a>
        </li>
    @endif
    @if (Lang::getLocale() == "sq" )
        <li><a href="/en"><img class="flag" src="/img/flags/UK.png" alt="Choose English as the desired Language"
                               title="Choose English as the desired Language"/></a></li>
    @endif

    <style>
        #google_translate_element {
            position: absolute;
            background: #fff;
            padding: 20px;
        }
    </style>

<!--     @include('partials.google-trans') -->
@endpush

@push('hreflang')
    @include('partials.hreflang')
@endpush

@section('content')
    {{-- @include ('layouts.searchheader') --}}

    <div id="header" class="header-slide">
        <div class="container">
            <div class="row">
                <div id="quick-search" class="col-md-4">
                    <div class="box-kerkimi">
                        <div class="row">
                            <form action="/{{Lang::getLocale()}}/search" method="GET">
                                {{--<input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
                                <div id="simpleSearch" class="col-sm-12">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <p class="search-title">{{trans('messages.search-for-properties')}}</p>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-sm-3 col-xs-12">
                                                        <label>{{trans('messages.location')}}</label>
                                                    </div>

                                                    <div class="col-sm-9 col-xs-12">
                                                        {!! Form::select('city',
                                                        array('all-cities' => trans('messages.all-village-city'),
                                                        'Tirana' => 'Tirana',
                                                        'Berat' => 'Berat',
                                                        'Borsh' => 'Borsh',
                                                        'Dellenje' => 'Dellenje',
                                                        'Dhermi' => 'Dhermi',
                                                        'Durres' => 'Durres',
                                                        'Elbasan' => 'Elbasan',
                                                        'Fier' => 'Fier',
                                                        'Golem' => 'Golem',
                                                        'Himara' => 'Himara',
                                                        'Kavaja' => 'Kavaja',
                                                        'Korca' => 'Korca',
                                                        'Kruja' => 'Kruja',
                                                        'Kruje' => 'Kruje',
                                                        'Ksamil' => 'Ksamil',
                                                        'Kucova' => 'Kucova',
                                                        'Lalzit Bay' => 'Lalzit Bay',
                                                        'Lezha' => 'Lezha',
                                                        'Lushnja' => 'Lushnja',
                                                        'Mat' => 'Mat',
                                                        'Mirdita' => 'Mirdita',
                                                        'Qeparo' => 'Qeparo',
                                                        'Rinas' => 'Rinas',
                                                        'Rreshen' => 'Rreshen',
                                                        'Saranda' => 'Saranda',
                                                        'Shengjin' => 'Shengjin',
                                                        'Shkodra' => 'Shkodra',
                                                        'Tirana' => 'Tirana',
                                                        'Velipoja' => 'Velipoja',
                                                        'Vlora' => 'Vlora'
                                                        ),
                                                        $values["city"],
                                                        array('class' => 'form-control input-sm')); !!}
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-sm-3 col-xs-12">
                                                        <label>{{trans('messages.type')}}</label>
                                                    </div>
                                                    <div class="col-sm-9 col-xs-12">
                                                        {!! Form::select('propertyType',
                                                                        array('apartment' => trans('messages.apartment'),
                                                                            'villa' => trans('messages.villa'),
                                                                            'office' => trans('messages.office'),
                                                                            'warehouse' => trans('messages.warehouse'),
                                                                            'land' => trans('messages.land'),
                                                                            'commercial' => trans('messages.otherlokaledyqane')),
                                                                        $values["propertyType"],
                                                                        array('class' => 'form-control input-sm', 'id' => 'property-type')); !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-sm-3 col-xs-12">
                                                        <label>{{trans('messages.status')}}</label>
                                                    </div>
                                                    <div class="col-sm-9 col-xs-12">
                                                        {!! Form::select('saleStatus',
                                                        array('rent' => trans('messages.for-rent'),
                                                            'sale' => trans('messages.for-sale')),
                                                        $values["saleStatus"],
                                                        array('class' => 'form-control input-sm')); !!}
                                                        {{-- <select name="saleStatus" class="form-control input-sm">
                                                            <option value="rent">{{trans('messages.for-rent')}}</option>
                                                            <option value="sale">{{trans('messages.for-sale')}}</option>
                                                        </select> --}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- advancedTabApp -->
                                <div id="advancedTabApp"
                                     class="col-sm-6 detailedTab detail-apartment hidden propertyTypeOptions">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <p class="search-title">&nbsp;</p>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="form-group">

                                                <div class="row">
                                                    <div class="col-sm-3 col-xs-12">
                                                        <label>{{trans('messages.shperndarjasearch')}}</label>
                                                    </div>
                                                    <div id="morehidden" style="display:none;">
                                                        <option value="more">{{trans('messages.moreoptionsearch')}}</option>
                                                    </div>
                                                    <div id="alloptionhidden" style="display:none;">
                                                        <option value="all">{{trans('messages.tegjithaoption')}}</option>
                                                    </div>
                                                    <div class="col-sm-9 col-xs-12">
                                                        <select name="organizimi" class="form-control input-sm">
                                                            <option selected="selected" value="undefined">-</option>
                                                            <option value="Garsoniere">{{trans('messages.garsoniere')}}</option>
                                                            <option value="11">{{trans('messages.onebedroom')}}</option>
                                                            <option value="21">{{trans('messages.twobedroom')}}</option>
                                                            <option value="31">{{trans('messages.threebedroom')}}</option>
                                                            <option value="more">{{trans('messages.moreoptionsearch')}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="form-group">

                                                <div class="row">
                                                    <div class="col-sm-3 col-xs-12">
                                                        <label>{{trans('messages.price')}}</label>
                                                    </div>
                                                    <div class="col-sm-9 col-xs-12" style="position:relative;">
                                                        <select name="cmimi" class="form-control input-sm">
                                                            <option selected="selected" value="undefined">-</option>
                                                            <option value="0-300">0-300 &euro;</option>
                                                            <option value="300-500">300-500 &euro;</option>
                                                            <option value="500-1000">500-1000 &euro;</option>
                                                            <option value="more">{{trans('messages.moreoptionsearch')}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!-- advancedTabApp end-->

                                <!-- advancedTabVila -->
                                <div id="advancedTabVila"
                                     class="col-sm-6 detailedTab detail-villa hidden propertyTypeOptions">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <p class="search-title">&nbsp;</p>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="form-group">

                                                <div class="row">
                                                    <div class="col-sm-3 col-xs-12">
                                                        <label>{{trans('messages.shperndarjasearch')}}</label>
                                                    </div>
                                                    <div class="col-sm-9 col-xs-12">
                                                        <select name="shperndarja-villa" class="form-control input-sm">
	                                                        <option selected="selected" value="undefined">-</option>
                                                            <option value="2">{{trans('messages.tworoomsvilla')}}</option>
                                                            <option value="4">{{trans('messages.fourroomsvilla')}}</option>
                                                            <option value="5">{{trans('messages.fiveroomsvilla')}}</option>
                                                            <option value="more">{{trans('messages.moreoptionsearch')}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="form-group">

                                                <div class="row">
                                                    <div class="col-sm-3 col-xs-12">
                                                        <label>{{trans('messages.price')}}</label>
                                                    </div>
                                                    <div class="col-sm-9 col-xs-12">
                                                        <select name="cmimi-villa" class="form-control input-sm">
                                                            <option value="0-1000">0-1000 &euro;</option>
                                                            <option value="1000-2000">1000-2000 &euro;</option>
                                                            <option value="2000-3000">2000-3000 &euro;</option>
                                                            <option value="more">{{trans('messages.moreoptionsearch')}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- advancedTabVila end-->

                                <!-- advancedTabZyra -->
                                <div id="advancedTabZyra"
                                     class="col-sm-6 detailedTab detail-office hidden propertyTypeOptions">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <p class="search-title">&nbsp;</p>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-sm-3 col-xs-12">
                                                        <label>{{trans('messages.siperfaqe')}}</label>
                                                    </div>
                                                    <div class="col-sm-9 col-xs-12">
                                                        <select name="siperfaqeZyra" class="form-control input-sm">
                                                            <option selected="selected" value="undefined">-</option>
                                                            <option value="0-50">0-50 m2</option>
                                                            <option value="50-100">50-100 m2</option>
                                                            <option value="100-200">100-200 m2</option>
                                                            <option value="more">200+ m2</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="form-group">

                                                <div class="row">
                                                    <div class="col-sm-3 col-xs-12">
                                                        <label>{{trans('messages.price')}}</label>
                                                    </div>
                                                    <div class="col-sm-9 col-xs-12">
                                                        <select name="cmimiZyra" class="form-control input-sm">
                                                            <option value="undefined">-</option>
                                                            <option value="0-300">0-300 &euro;</option>
                                                            <option value="300-500">300-500 &euro;</option>
                                                            <option value="500-1000">500-1000 &euro;</option>
                                                            <option value="1000-2000">1000-2000 &euro;</option>
                                                            <option value="more">more</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- advancedTabZyra end-->

                                <!-- advancedTabToka -->
                                <div id="advancedTabToka"
                                     class="col-sm-6 detailedTab detail-land hidden propertyTypeOptions">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <p class="search-title">&nbsp;</p>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="form-group">

                                                <div class="row">
                                                    <div class="col-sm-3 col-xs-12">
                                                        <label>{{trans('messages.siperfaqe')}}</label>
                                                    </div>
                                                    <div class="col-sm-9 col-xs-12">
                                                        <select name="siperfaqe-toka" class="form-control input-sm">
															<option selected="selected" value="undefined">-</option>
                                                            <option value="0-1000">0-1000 m2</option>
                                                            <option value="1000-2000">1000-2000 m2
                                                            </option>
                                                            <option value="2000-5000">2000-5000 m2</option>
                                                            <option value="more">{{trans('messages.moreoptionsearch')}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-sm-3 col-xs-12">
                                                        <label>{{trans('messages.price')}}</label>
                                                    </div>
                                                    <div class="col-sm-9 col-xs-12">
                                                        <select name="cmimi-toka" class="form-control input-sm">
                                                            <option value="all">{{trans('messages.tegjithaoption')}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- advancedTabToka end-->

                                <!-- advancedTabMag -->
                                <div id="advancedTabMag"
                                     class="col-sm-6 detailedTab detail-warehouse hidden propertyTypeOptions">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <p class="search-title">&nbsp;</p>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="form-group">

                                                <div class="row">
                                                    <div class="col-sm-3 col-xs-12">
                                                        <label>{{trans('messages.siperfaqe')}}</label>
                                                    </div>
                                                    <div class="col-sm-9 col-xs-12">
                                                        <select name="siperfaqe-mag" class="form-control input-sm">
	                                                        <option selected="selected" value="undefined">-</option>
                                                            <option value="0-500">0-500 m2</option>
                                                            <option value="500-1000">500-1000 m2
                                                            </option>
                                                            <option value="1000-2000">1000-2000 m2</option>
                                                            <option value="more">{{trans('messages.moreoptionsearch')}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-sm-3 col-xs-12">
                                                        <label>{{trans('messages.price')}}</label>
                                                    </div>
                                                    <div class="col-sm-9 col-xs-12">
                                                        <select name="cmimi-mag" class="form-control input-sm">
                                                            <option value="0-500">0-500 &euro;</option>
                                                            <option value="500-1000">500-1000 &euro;</option>
                                                            <option value="1000-2000">1000-2000 &euro;</option>
                                                            <option value="more">{{trans('messages.moreoptionsearch')}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- advancedTabMag end-->

                                <!-- advancedStore -->
                                <div id="advancedStore"
                                     class="col-sm-6 detailedTab detail-commercial hidden propertyTypeOptions">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <p class="search-title">&nbsp;</p>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="form-group">

                                                <div class="row">
                                                    <div class="col-sm-3 col-xs-12">
                                                        <label>{{trans('messages.siperfaqe')}}</label>
                                                    </div>
                                                    <div class="col-sm-9 col-xs-12">
                                                        <select name="siperfaqe-store" class="form-control input-sm">
	                                                        <option selected="selected" value="undefined">-</option>
                                                            <option value="0-50">0-50 m2</option>
                                                            <option value="50-100">50-100 m2</option>
                                                            <option value="100-200">100-200 m2</option>
                                                            <option value="more">{{trans('messages.moreoptionsearch')}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-sm-3 col-xs-12">
                                                        <label>{{trans('messages.price')}}</label>
                                                    </div>
                                                    <div class="col-sm-9 col-xs-12">
                                                        <select name="cmimi-store" class="form-control input-sm">
                                                            <option value="0-500">0-500 &euro;</option>
                                                            <option value="500-1000">500-1000 &euro;</option>
                                                            <option value="1000-2000">1000-2000 &euro;</option>
                                                            <option value="more">{{trans('messages.moreoptionsearch')}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- advancedStore end-->

                                <div class="clearfix"></div>
                                <div class="col-sm-12 search-footer">
                                    <a id="detailedSearchC"><i id="chevron"
                                                               class="fa fa-chevron-right fa-2x pull-right"
                                                               style="margin-top: 4px"></i></a>

                                    <p class="det-search-link">
                                        <a id="detailedSearch">{{trans('messages.detailed-search')}}</a>
                                    </p>
                                    <input id="state" name="statesearch" type="hidden" value="closed"/>
                                    <button type="submit" class="search-btn btn btn-default btn-md">
                                        <i class="fa fa-search"
                                           style="font-size: 15px;"></i>&nbsp;&nbsp;{{trans('messages.search')}}
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="vertical-absolute-btn hidden-xs">
            <h5>
                @if (Lang::getLocale() == "sq" )
                    <a href='/sq/registro-pronen'
                       class="btn btn-default btn-block">{{trans('messages.your-property')}}</a>
                @endif
                @if (Lang::getLocale() == "en" )
                    <a href='/en/register-property'
                       class="btn btn-default btn-block">{{trans('messages.your-property')}}</a>
                @endif
            </h5>
        </div>

    </div>

    <!-- Content -->
    <div id="content">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-push-2">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12 noPadRight">
                                <h1>{{trans('messages.searchFor')}} {{$searchFor}}</h1>
                            </div>
                            <div class="row container-realestate">
                                @if (count($properties) === 0)
                                    <div class="well">
                                        <div class="alert alert-info" role="alert">Nuk kishte asnje objekt per kerkimin
                                            tuaj
                                        </div>
                                    </div>
                                @endif

                                <div class="clearfix"></div>
                                @for ($i = 0; $i < $totalProperties/4; $i++)
                                    @each('templates.pronasearch', array_chunk($properties,4)[$i],'prona')

                                @endfor

                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-12 text-center">
                            <div class="pagination pagination-centered">
                                {!! $pagination->appends($values)->links() !!}
                            </div>
                        </div>
                    </div>
                </div>

                @include ('partials.sidebar')

            </div>
        </div>
    </div>
    <div id="pre-footer" class="bottomPart">
        <div class="container">

            <?php 
// 	            include('partials.search-in-map', ['properties' => $propertiesMap])
	        ?>

            @include('partials.about-us')

            <div class="row">
                <div class="col-md-12">
                    <ol class="breadcrumb">
                        <li><a href="/{{ Lang::getLocale() }}">{{trans('messages.home')}}</a></li>
                        <li class="active">{{trans('messages.search-for-properties')}}</li>
                    </ol>
                </div>
            </div>


        </div>
    </div>

@endsection

@push('post-scripts')
    @if($request['statesearch'] === 'opened')
        <script>
            $(document).ready(function () {
                window.changeAdvancedDetails(false, '{{ $request["propertyType"] }}');

                var propertyType = '{{ $request["propertyType"] }}';
                var saleType = '{{ $request["saleStatus"] }}';

                var value1;
                var value2;

                switch (propertyType) {
                    case 'apartment':
                        value1 = '{{ $request["cmimi"] ?? '' }}';
                        value2 = '{{ $request["organizimi"] ?? '' }}';
                        break;
                    case 'villa':
                        value1 = '{{ $request["cmimi-villa"] ?? '' }}';
                        value2 = '{{ $request["organizimi"] ?? '' }}';
                        break;
                    case 'office':
                        value1 = '{{ $request["cmimiZyra"] ?? '' }}';
                        value2 = '{{ $request["siperfaqeZyra"] ?? '' }}';
                        break;
                    case 'warehouse':
                        value1 = '{{ $request["cmimi-mag"] ?? '' }}';
                        value2 = '{{ $request["siperfaqe-mag"] ?? '' }}';
                        break;
                    case 'land':
                        value1 = '{{ $request["cmimi-toka"] ?? '' }}';
                        value2 = '{{ $request["siperfaqe-toka"] ?? '' }}';
                        break;
                    case 'commercial':
                        value1 = '{{ $request["cmimi-store"] ?? '' }}';
                        value2 = '{{ $request["siperfaqe-store"] ?? '' }}';
                        break;
                }

                window.updateSearchInputs(saleType, propertyType, value1, value2);

            });
        </script>
    @endif
@endpush
