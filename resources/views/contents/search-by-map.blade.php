@extends('layouts.main')
@section('title', trans('messages.sbm'))
@section('description', trans('messages.sbm-description'))
@section('keywords', trans('messages.sbm-keywords'))
@section('titleMeta', trans('messages.sbm-meta-title'))

<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css" integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ==" crossorigin=""/>

<link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
   integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
   crossorigin=""/>
   
<script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
    integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
    crossorigin=""></script>

@push('language-switcher')
    @if (Lang::getLocale() == "en" )
        <li>
            <a href="/sq/kerko-ne-harte">
                <img class="flag" src="{{ asset('/img/flags/Albania.png') }}"
                     alt="Zgjidhni Shqip si gjuhen e deshiruar"
                     title="Zgjidhni Shqip si gjuhen e deshiruar"/>
            </a>
        </li>
    @endif
    @if (Lang::getLocale() == "sq" )
        <li>
            <a href="/en/searchbymap">
                <img class="flag" src="{{ asset('/img/flags/UK.png') }}"
                     alt="Choose English as the desired Language"
                     title="Choose English as the desired Language"/>
            </a>
        </li>
    @endif

    <style>
        #google_translate_element {
            position: absolute;
            background: #fff;
            padding: 20px;
        }
        .leaflet-marker-icon.leaflet-interactive{
	        width: 30px;
        }
    </style>

<!--     @include('partials.google-trans') -->
@endpush


@push('hreflang')
    @include('partials.hreflang')
@endpush

@section('content')
    @include ('partials.search-header')

    <!-- Content -->
    <div id="content">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-push-2">

                    <div class="col-sm-12">
                        <h1>{{trans('messages.search-in-map')}}</h1>
                        
						<div class="about">
						  <div class="tab-content">
						  	<p>
								{{$description}} 
								@if (Lang::getLocale() == "sq" )
									<a href="{{ url('/sq/kontakt') }}"> kontaktoni</a>
								@else
									<a href="{{ url('/sq/contact') }}"> contact us</a>
								@endif
							</p>
						  </div>
						</div>
						
                        <div class="company-description">
                            <h4 class="module-title">{{trans('messages.legend')}}</h4>
                            <div class="company-info">
                                <div class="well col-md-12" style="background:white">
                                    <div class="col-md-3">
                                        <div>
                                            <a class="anchor" id="img-1.png"
                                               href="{{ url('/' . Lang::getLocale() . '/searchbymap?type=Apartment&status=rent') }}"><img
                                                        src="/img/1.png"
                                                        height="25"
                                                        width="20"
                                                        alt='Apartment for rent'/>&nbsp;{{trans('messages.apartment-rent')}}
                                            </a>
                                        </div>
                                        <div><a id="img-2.png" class="anchor"
                                                href="{{ url('/' . Lang::getLocale() . '/searchbymap?type=Villa&status=rent') }}"><img
                                                        src="/img/2.png"
                                                        height="25"
                                                        width="20"
                                                        alt='Villa for rent'/>&nbsp;{{trans('messages.villa-rent')}}
                                            </a>
                                        </div>
                                        <div>
                                            <a id="img-5.png" class="anchor"
                                               href="{{ url('/' . Lang::getLocale() . '/searchbymap?type=Office&status=rent') }}"><img
                                                        src="/img/5.png"
                                                        height="25"
                                                        width="20"
                                                        alt='Office for rent'/>&nbsp;{{trans('messages.office-rent')}}
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div><a id="img-4.png" class="anchor"
                                                href="{{ url('/' . Lang::getLocale() . '/searchbymap?type=Land&status=rent') }}"><img
                                                        src="/img/4.png"
                                                        height="25"
                                                        width="20"
                                                        alt='Land for rent'/>&nbsp;{{trans('messages.land-rent')}}
                                            </a>
                                        </div>
                                        <div>
                                            <a id="img-3.png" class="anchor"
                                               href="{{ url('/' . Lang::getLocale() . '/searchbymap?type=Warehouse&status=rent') }}"><img
                                                        src="/img/3.png"
                                                        height="25"
                                                        width="20"
                                                        alt='Warehouse for rent'/>&nbsp;{{trans('messages.wh-rent')}}
                                            </a>
                                        </div>
                                        {{--<div><a id="img-www.png" class="anchor"--}}
                                        {{--href="{{ url('/' . Lang::getLocale() . '/searchbymap?type=Club&status=rent') }}"><img--}}
                                        {{--src="/img/www.png"--}}
                                        {{--height="25"--}}
                                        {{--width="20"--}}
                                        {{--alt='Bar club for rent'/>&nbsp;{{trans('messages.bar-rent')}}--}}
                                        {{--</a>--}}
                                        {{--</div>--}}
                                        <div><a id="img-6s.png" class="anchor"
                                                href="{{ url('/' . Lang::getLocale() . '/searchbymap?type=Apartment&status=sale') }}"><img
                                                        src="/img/6s.png"
                                                        height="25"
                                                        width="20"
                                                        alt='Apartment for sale'/>&nbsp;{{trans('messages.apartment-sale')}}
                                            </a>
                                        </div>
                                        {{--<div><a id="img-1s.png" class="anchor"--}}
                                        {{--href="{{ url('/' . Lang::getLocale() . '/searchbymap?type=Club&status=sale') }}"><img--}}
                                        {{--src="/img/1s.png"--}}
                                        {{--height="25"--}}
                                        {{--width="20"--}}
                                        {{--alt='Bar for rent'/>&nbsp;{{trans('messages.bar-sale')}}--}}
                                        {{--</a>--}}
                                        {{--</div>--}}
                                    </div>
                                    <div class="col-md-3">
                                        <div><a id="img-2s.png" class="anchor"
                                                href="{{ url('/' . Lang::getLocale() . '/searchbymap?type=Villa&status=sale') }}"><img
                                                        src="/img/2s.png"
                                                        height="25"
                                                        width="20"
                                                        alt='villa-sale'/>&nbsp;{{trans('messages.villa-sale')}}
                                            </a>
                                        </div>
                                        <div><a id="img-5s.png" class="anchor"
                                                href="{{ url('/' . Lang::getLocale() . '/searchbymap?type=Office&status=sale') }}"><img
                                                        src="/img/5s.png"
                                                        height="25"
                                                        width="20"
                                                        alt='office-sale'/>&nbsp;{{trans('messages.office-sale')}}
                                            </a>
                                        </div>
                                        <div><a id="img-2.png" class="anchor"
                                                href="{{ url('/' . Lang::getLocale() . '/searchbymap?type=Warehouse&status=sale') }}"><img
                                                        src="/img/3s.png"
                                                        height="25"
                                                        width="20"
                                                        alt='warehouse-sale'/>&nbsp;{{trans('messages.wh-sale')}}
                                            </a>
                                        </div>

                                    </div>
                                    <div class="col-md-3">
                                        <div><a id="img-4s.png"
                                                href="{{ url('/' . Lang::getLocale() . '/searchbymap?type=Land&status=sale') }}">
                                                <img src="/img/4s.png"
                                                     height="25"
                                                     width="20"
                                                     alt='land-sale'/>&nbsp;{{trans('messages.land-sale')}}
                                            </a>
                                        </div>
                                        <div><a id="img-4s.png"
                                                href="{{ url('/' . Lang::getLocale() . '/searchbymap?type=Commercial&status=rent') }}">
                                                <img src="/img/7.png"
                                                     height="25"
                                                     width="20"
                                                     alt='land-sale'/>&nbsp;{{trans('messages.commercial-rent')}}
                                            </a>
                                        </div>
                                        <div><a id="img-4s.png"
                                                href="{{ url('/' . Lang::getLocale() . '/searchbymap?type=Commercial&status=sale') }}">
                                                <img src="/img/7s.png"
                                                     height="25"
                                                     width="20"
                                                     alt='land-sale'/>&nbsp;{{trans('messages.commercial-sale')}}
                                            </a>
                                        </div>
                                        {{--<div><a id="img-7.png" class="anchor"--}}
                                        {{--href="{{ url('/' . Lang::getLocale() . '/searchbymap?type=Office&status=sale') }}"><img--}}
                                        {{--src="/img/7.png"--}}
                                        {{--height="25"--}}
                                        {{--width="20"--}}
                                        {{--alt='store-sale'/>&nbsp;{{trans('messages.office-sale')}}--}}
                                        {{--</a>--}}
                                        {{--</div>--}}
                                        {{--<div><a id="img-7s.png" class="anchor"--}}
                                        {{--href="{{ url('/' . Lang::getLocale() . '/searchbymap?type=Store&status=sale') }}"><img--}}
                                        {{--src="/img/7s.png"--}}
                                        {{--height="25"--}}
                                        {{--width="20"--}}
                                        {{--alt='business-rent'/>&nbsp;{{trans('messages.business-rent')}}--}}
                                        {{--</a>--}}
                                        {{--</div>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12 noPadRight">

                                <style>
                                    #map {
                                        border: 1px dashed #ddd;
                                        width: 100%;
                                        height: 900px;
                                    }
                                </style>
                                <div id="map"></div>
                            </div>
                            <div class="row">
                                <?php
                                $pronatHarta = '';
                                ?>
                                <div class="col-md-12 noPadRight">
                                    @foreach($properties as $property)
                                        <?php
                                        $typeSaleRent = "";
                                        $sip = "";
                                        $price = "Price";
                                        if (Lang::getLocale() == "en") {
                                            $typeSaleRent = ($property->sale) ? "Type: For Sale" : "Type: For Rent";
                                            $sip = "Sq";
                                            $price = "Price";
                                        } else {
                                            $typeSaleRent = ($property->sale) ? "Tipi: Ne Shitje" : "Tipi: Me Qira";
                                            $sip = "Sip";
                                            $price = "Cmimi";
                                        }

                                        $splitKordinata = explode(", ", $property->koordinata);
                                        if (count($splitKordinata) == 2 && strlen($property->koordinata) < 25) {
                                            $pronatHarta .= "['<div class=\"tooltip-on-map\"><img src=\"https://www.realestate.al/foto/" . $property->pic . "\" class=\"tooltip-property-image\" alt=\"" . addslashes($property->title) . "\" title=\"" . addslashes($property->title) . "\" /><div class=\"tooltip-property-body\"><p class=\"tooltip-property-title\"><a target=\"_blank\" href=\"/" . Lang::getLocale() . "/" . str_slug($property->title) . "." . $property->id . "\">" . str_limit(addslashes($property->title), 60) . "</a></p><p class=\"tooltip-property-type\">" . $typeSaleRent . "</p><p class=\"tooltip-property-area\">" . $sip . ": " . $property->area . "</p><p class=\"tooltip-property-price\">" . $price . ": " . $property->price . "</p></div></div>',
                                                                " . $property->koordinata . ", 4,'" . strtolower($property->type) . "-" . (($property->sale == 1) ? "sale" : "rent") . "'],";
                                        }
                                        ?>
                                    @endforeach

                                    <script>
	                                    initMap();
	                                    
                                        function initMap(a) {
                                            if (a === undefined) {
                                                var imgid = '/';
                                            } else {
                                                var imgid = '/' + a.id.replace('-', '/');
                                            }

                                            var locations = [
                                                <?php
                                                echo rtrim($pronatHarta, ',');
                                                ?>
                                            ];
                                            //console.log(locations);
											
											var lat = 41.328808;
											var lon = 19.817365;
											
											var map = L.map('map').setView([lat, lon], 13);
											
											 // set map tiles source
										    L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
										      attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors',
										    }).addTo(map);

 						                    var marker, i;
											
						                    for (i = 0; i < locations.length; i++) {
						                        if (locations[i][4] == "apartment-rent" || locations[i][4] == "apartament-rent") {
// 						                            image = '/img/1.png';
						                            image = L.icon({
													    iconUrl: '/img/1.png'
													});
						                        } else if (locations[i][4] == "villa-rent" || locations[i][4] == "vile-rent") {
// 						                            image = '/img/2.png';
						                            image = L.icon({
													    iconUrl: '/img/2.png'
													});

						                        } else if (locations[i][4] == "office-rent" || locations[i][4] == "zyre-rent" || locations[i][4] == "store-rent" || locations[i][4] == "dyqan-rent") {
// 						                            image = '/img/5.png';
						                            image = L.icon({
													    iconUrl: '/img/5.png'
													});
													
						                        } else if (locations[i][4] == "warehouse-rent" || locations[i][4] == "magazine-rent") {
// 						                            image = '/img/3.png';
						                            image = L.icon({
													    iconUrl: '/img/3.png'
													});

						                        } else if (locations[i][4] == "land-rent" || locations[i][4] == "toke-rent") {
// 						                            image = '/img/4.png';
						                            image = L.icon({
													    iconUrl: '/img/4.png'
													});

						                        } else if (locations[i][4] == "lokal-rent" || locations[i][4] == "club-rent") {
// 						                            image = '/img/www.png';
						                            image = L.icon({
													    iconUrl: '/img/www.png'
													});

						                        } else if (locations[i][4] == "apartment-sale" || locations[i][4] == "apartament-sale") {
// 						                            image = '/img/6s.png';
						                            image = L.icon({
													    iconUrl: '/img/6s.png'
													});

						                        } else if (locations[i][4] == "lokal-sale" || locations[i][4] == "club-sale") {
// 						                            image = '/img/1s.png';
						                            image = L.icon({
													    iconUrl: '/img/1s.png'
													});

						                        } else if (locations[i][4] == "villa-sale" || locations[i][4] == "vile-sale") {
// 						                            image = '/img/2s.png';
						                            image = L.icon({
													    iconUrl: '/img/2s.png'
													});

						                        } else if (locations[i][4] == "office-sale" || locations[i][4] == "zyra-sale" || locations[i][4] == "store-sale") {
// 						                            image = '/img/5s.png';
						                            image = L.icon({
													    iconUrl: '/img/5s.png'
													});

						                        } else if (locations[i][4] == "warehouse-sale" || locations[i][4] == "magazine-sale") {
// 						                            image = '/img/3s.png';
						                            image = L.icon({
													    iconUrl: '/img/3s.png'
													});

						                        } else if (locations[i][4] == "land-sale" || locations[i][4] == "toke-sale") {
// 						                            image = '/img/4s.png';
						                            image = L.icon({
													    iconUrl: '/img/4s.png'
													});

						                        } else if (locations[i][4] == "bar-sale" || locations[i][4] == "lokal-sale" || locations[i][4] == "business-sale" || locations[i][4] == "biznes-sale") {
// 						                            image = '/img/1s.png';
						                            image = L.icon({
													    iconUrl: '/img/1s.png'
													});

						                        } else if (locations[i][4] == "store-sale" || locations[i][4] == "dyqan-sale") {
// 						                            image = '/img/7.png';
						                            image = L.icon({
													    iconUrl: '/img/7.png'
													});

						                        } else if (locations[i][4] == "business-rent" || locations[i][4] == "biznes-rent") {
// 						                            image = '/img/7s.png';
						                            image = L.icon({
													    iconUrl: '/img/7s.png'
													});

						                        } else {
// 						                            image = "https://cdn2.iconfinder.com/data/icons/places-4/100/home_place_marker_location_house_apartment-32.png";
						                            image = L.icon({
													    iconUrl: 'https://cdn2.iconfinder.com/data/icons/places-4/100/home_place_marker_location_house_apartment-32.png'
													});

						                        }
												
												marker = L.marker([locations[i][1], locations[i][2]], {icon: image}).addTo(map);
												
												marker.bindPopup(locations[i][0]);
/*
												marker = L.marker({
						                            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
						                            icon: image,
						                            map: map
						                        });						                        
*/

/*
						                        marker = new google.maps.Marker({
						                            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
						                            icon: image,
						                            map: map
						                        });
*/
						
/*
												L.DomEvent.on(marker, 'click', (function (marker, i) {
													return function () {
						                                infowindow.setContent(locations[i][0]);
						                                infowindow.open(map, marker);
						                            }
						                        })(marker, i));
*/
						                        
/*
						                        google.maps.event.addListener(marker, 'click', (function (marker, i) {
						                            return function () {
						                                infowindow.setContent(locations[i][0]);
						                                infowindow.open(map, marker);
						                            }
						                        })(marker, i));
						
						                        google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
						                            return function () {
						                                infowindow.setContent(locations[i][0]);
						                                infowindow.open(map, marker);
						                            }
						                        })(marker, i));
*/
						
						                    }
						
						                }
            </script>

			<script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js" integrity="sha512-/Nsx9X4HebavoBvEBuyp3I7od5tA0UzAxs+j83KgC8PU0kgB4XiK4Lfe4y4cgBtaRJQEIFCW+oC506aPT2L1zw==" crossorigin=""></script>
<!--             <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyAqXh2RBOhtWKtPEsvfcOwYoHjcTVkiUXI&callback=initMap"></script> -->
        </div>
    </div>
</div>
                    </div>
                </div>
                @include ('partials.sidebar')
            </div>

        </div>
    </div>

    <div id="pre-footer" class="bottomPart">
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <ol class="breadcrumb">
                        <li><a href="/{{ Lang::getLocale() }}">{{trans('messages.home')}}</a></li>
                        <li class="active"><a
                                    href="/{{Lang::getLocale()}}/{{trans('menu.menu22')}}">{{trans('menu.menu2')}}</a>
                        </li>
                    </ol>
                </div>
            </div>

        </div>
    </div>
@endsection

@push('scripts')
    <script>
        function showOnlyThisType(type) {
            var a = type.id.replace('-', '/');
            return '/' + a;
        }
    </script>
@endpush
