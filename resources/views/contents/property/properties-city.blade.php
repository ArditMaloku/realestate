@extends('layouts.main')
@section('title', trans('messages.properties-in') . " " . $city . " - Page " . $page)

@push('language-switcher')

    @php
        $citySwitch = $city;

        foreach($properties as $prop) {
            $citySwitch = $prop->citylangswitch;
            break;
        }

    @endphp

    @if (Lang::getLocale() == "en" )

        <li>
            <a href="{{route('properties-city-sq', ["sq",strtolower($citySwitch),$page])}}">
                <img class="flag" src="{{ asset('/img/flags/Albania.png') }}" title="Albania.png"
                     alt="Albania.png"/></a>
        </li>
    @elseif (Lang::getLocale() == "sq" )
        <li>
            <a href="{{route('properties-city-en', ["en",strtolower($citySwitch),$page])}}">
                <img class="flag" src="{{ asset('/img/flags/UK.png') }}" title="UK.png" alt="UK.png"/></a>
        </li>
    @endif

    <style>
        #google_translate_element {
            position: absolute;
            background: #fff;
            padding: 20px;
        }
    </style>

<!--     @include('partials.google-trans') -->
@endpush

@section('content')
    @include ('partials.search-header')
    <!-- Content -->
    <div id="content" style="padding-top: 25px">
        <div class="container">
            <div class="row">
               <div class="col-md-10 col-md-push-2">
                    <div class="row container-realestate">
                        <h1 class="prop-sec-title insidecategory">
                            <span class="btn btn-title btn-default cursordefault">{{ trans('messages.properties-in') . " " . $city }}</span>
                        </h1>
                        @if (count($properties) === 0)
                            <div class="well">
                                <div class="alert alert-info" role="alert">Nuk kishte asnje objekt per kerkimin tuaj.
                                </div>
                            </div>
                        @else
                            @if ($city=='Tirane' || $city=='Tirana')
                            <div class="col-md-12 box" style="position: unset!important">
                                <div class="about">
                                <div class="tab-content">
                                    <p>
                                        <?php echo $description; ?>
                                    </p>
                                </div>
                                </div>
                            </div>
                            @else
                                <div class="clearfix"></div>
                            @endif
                        
                        @endif

                        @for ($i = 0; $i < count($properties)/4; $i++)
                            @each('templates.pronaselection', array_chunk($properties,4)[$i],'prona')
                        @endfor

                        <div class="clearfix"></div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 text-center">
                            <div class="pagination pagination-centered">
                                {!! $pagination->links('partials.pagination') !!}
                            </div>
                        </div>
                    </div>

                </div>
                @include ('partials.sidebar')

            </div>

        </div>

    </div>
    </div>
    <div id="pre-footer" class="bottomPart">
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <ol class="breadcrumb">

                        <?php

                        $lang = Lang::getLocale();

                        ?>

                        <li><a href="/{{ $lang }}">{{trans('messages.home')}}</a></li>

                        <li class="active"><a
                                    href="{{route("properties-city-$lang", [$lang,strtolower($city)])}}"> {{ trans('messages.properties-in') . " " . $city }} </a>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection