<?php 
	$lastSegment = request()->segment(count(request()->segments())); 
	if (strlen($lastSegment)>3)
	{
		$lastSegment = 1;
	}
?>

@extends('layouts.main')


@if(Lang::getLocale()=='en')
    @if(($propertyType=='Apartment' && strtolower(str_slug($propertySelection))!='twobedroom') || $propertyType=='Villa')
		@section('title',$titleMeta." - Page $page")
	@else
		@section('title', trans('messages.'.strtolower(str_slug($propertySelection)).'') . ' ' . trans('messages.'.strtolower($propertyType).'').' '.trans('messages.for-'.strtolower($action).'')." ".$city . " - Page $page")
	@endif
@else
    @if($propertyType=='Apartament' || $propertyType=='Vile')
		@section('title',$titleMeta." - Faqe $page")
	@else
		@section('title', trans('messages.'.strtolower($propertyType).'') . ' ' . trans('messages.'.strtolower(str_slug($propertySelection)).'') .' '.trans('messages.for-'.strtolower($action).'')." ".$city . " - Faqe $page")
	@endif
@endif

@if($lastSegment==1)
	@section('description', $descriptionMeta)
	@section('keywords', $keywordsMeta)
@endif
@section('titleMeta',$titleMeta)

@push('hreflang')

    @if (Lang::getLocale() == "en" )
        <?php
        $propertyTypesEN = array("Apartment", "Villa", "Land", "Office", "Warehouse");
        $propertyTypesSQ = array("Apartament", "Vile", "Toke", "Zyre", "Magazine");
        $keyProperty = array_search($propertyType, $propertyTypesEN);
        $permeShqip = ($action == "rent") ? "me" : "ne";
        $permeEnglish = ($action == "rent") ? "for" : "for";
        $actionRoute = ($action == "rent") ? "qera" : "shitje";
        $inCity = "in";

        $propertySelectionEN = array(
            'one+bedroom',
            'two+bedroom',
            'three+bedroom',
            'more+than+three+bedroom',
            '2+bedrooms',
            '3+bedrooms',
            '4+bedrooms',
            'more+than+4+bedrooms',
            'more+than+5+bedrooms',
            'from+0+to+50+m2',
            'from+50+to+100+m2',
            'from+100+to+200+m2',
            'more+than+200+m2',
            'from+0+to+500+m2',
            'from+500+to+1000+m2',
            'from+1000+to+2000+m2',
            'more+than+2000+m2',
            'from+0+to+1000+m2',
            'from+1000+to+2000+m2',
            'from+2000+to+5000+m2',
            'more+than+5000+m2'
        );

        $propertySelectionSQ = array(
            '1+1',
            '2+1',
            '3+1',
            'me+shume+se+3+dhoma',
            '2+dhoma',
            '3+dhoma',
            '4+dhoma',
            'me+shume+se+4+dhoma',
            'me+shume+se+5+dhoma',
            'nga+0+deri+50+m2',
            'nga+50+deri+100+m2',
            'nga+100+deri+200+m2',
            'me+shume+se+200+m2',
            'nga+0+deri+500+m2',
            'nga+500+deri+1000+m2',
            'nga+1000+deri+2000+m2',
            'me+shume+se+2000+m2',
            'nga+0+deri+1000+m2',
            'nga+1000+deri+2000+m2',
            'nga+2000+deri+5000+m2',
            'me+shume+se+5000+m2'
        );

        $keyPropertySelection = array_search($propertySelection, $propertySelectionEN);
        $cityLanguangeSwitch = ($properties[0]->citylangswitch) ? $properties[0]->citylangswitch : "Tirane";

        $alternateSq = route('category-cities-selection-page', ["sq",strtolower($propertyTypesSQ[$keyProperty]),$propertySelectionSQ[$keyPropertySelection],$permeShqip,$actionRoute,'ne',$cityLanguangeSwitch,$page]);
        $linkArray = explode('/',$alternateSq);
		$alternatePage = end($linkArray);
		if($alternatePage==1)
		{
			$alternatePage = null;
		}else
		{
			$alternatePage = $page;
		}
        ?>        
        <link rel="alternate" hreflang="sq"
              href="{{route('category-cities-selection-page', ["sq",strtolower($propertyTypesSQ[$keyProperty]),$propertySelectionSQ[$keyPropertySelection],$permeShqip,$actionRoute,'ne',$cityLanguangeSwitch,$alternatePage])}}"/>

		<link rel="canonical"
			  href="{{url()->current()}}">
		@if($page==2)
		<link rel="prev"
              href="{{route('category-cities-selection-page', ["en",strtolower($propertyType),$propertySelection,'for',$action,'in',$city,null])}}"/>
		@elseif($page==1)
		
		@else
		<link rel="prev"
              href="{{route('category-cities-selection-page', ["en",strtolower($propertyType),$propertySelection,'for',$action,'in',$city,((($page-1)>0)?($page-1):0)])}}"/>
		@endif
				
		@if(((($page+1)>0)?($page+1):0) <= $pagination->lastPage())	
        <link rel="next"
              href="{{route('category-cities-selection-page', ["en",strtolower($propertyType),$propertySelection,'for',$action,'in',$city,((($page+1)>0)?($page+1):0)])}}"/>
		@endif
    @endif

    @if (Lang::getLocale() == "sq" )
        <?php
        $propertyTypesEN = array("Apartment", "Villa", "Land", "Office", "Warehouse");
        $propertyTypesSQ = array("Apartament", "Vile", "Toke", "Zyre", "Magazine");
        $keyProperty = array_search($propertyType, $propertyTypesSQ);
        $permeShqip = ($action == "qera") ? "me" : "ne";
        $permeEnglish = ($action == "qera") ? "for" : "for";
        $actionRoute = ($action == "qera") ? "rent" : "sale";

        $propertySelectionEN = array(
            'one+bedroom',
            'two+bedroom',
            'three+bedroom',
            'more+than+three+bedroom',
            '2+bedrooms',
            '3+bedrooms',
            '4+bedrooms',
            'more+than+4+bedrooms',
			'more+than+5+bedrooms',
            'from+0+to+50+m2',
            'from+50+to+100+m2',
            'from+100+to+200+m2',
            'more+than+200+m2',
            'from+0+to+500+m2',
            'from+500+to+1000+m2',
            'from+1000+to+2000+m2',
            'more+than+2000+m2',
            'from+0+to+1000+m2',
            'from+1000+to+2000+m2',
            'from+2000+to+5000+m2',
            'more+than+5000+m2'
        );

        $propertySelectionSQ = array(
            '1+1',
            '2+1',
            '3+1',
            'me+shume+se+3+dhoma',
            '2+dhoma',
            '3+dhoma',
            '4+dhoma',
            'me+shume+se+4+dhoma',
            'me+shume+se+5+dhoma',
            'nga+0+deri+50+m2',
            'nga+50+deri+100+m2',
            'nga+100+deri+200+m2',
            'me+shume+se+200+m2',
            'nga+0+deri+500+m2',
            'nga+500+deri+1000+m2',
            'nga+1000+deri+2000+m2',
            'me+shume+se+2000+m2',
            'nga+0+deri+1000+m2',
            'nga+1000+deri+2000+m2',
            'nga+2000+deri+5000+m2',
            'me+shume+se+5000+m2'
        );

        $keyPropertySelection = array_search($propertySelection, $propertySelectionSQ);
        $cityLanguangeSwitch = ($properties[0]->citylangswitch) ? $properties[0]->citylangswitch : "Tirana";

		$alternateEn = route('category-cities-selection-page', ["en",strtolower($propertyTypesEN[$keyProperty]),$propertySelectionEN[$keyPropertySelection],'for',$actionRoute,'in',$cityLanguangeSwitch,$page]);
        $linkArray = explode('/',$alternateEn);
		$alternatePage = end($linkArray);
		if($alternatePage==1)
		{
			$alternatePage = null;
		}else
		{
			$alternatePage = $page;
		}
        ?>
        <link rel="alternate" hreflang="en"
              href="{{route('category-cities-selection-page', ["en",strtolower($propertyTypesEN[$keyProperty]),$propertySelectionEN[$keyPropertySelection],'for',$actionRoute,'in',$cityLanguangeSwitch,$alternatePage])}}"/>

		<link rel="canonical"
			  href="{{url()->current()}}">

		@if($page==2)
        <link rel="prev"
              href="{{route('category-cities-selection-page', ["sq",strtolower($propertyType),$propertySelection,$permeShqip,$action,'ne',$city,null])}}"/>
		@elseif($page==1)
		
		@else
        <link rel="prev"
              href="{{route('category-cities-selection-page', ["sq",strtolower($propertyType),$propertySelection,$permeShqip,$action,'ne',$city,((($page-1)>0)?($page-1):0)])}}"/>
		@endif
		
		@if(((($page+1)>0)?($page+1):0) <= $pagination->lastPage())
        <link rel="next"
              href="{{route('category-cities-selection-page', ["sq",strtolower($propertyType),$propertySelection,$permeShqip,$action,'ne',$city,((($page+1)>0)?($page+1):0)])}}"/>
		@endif
    @endif
@endpush

@push('language-switcher')
    @if (Lang::getLocale() == "en" )
        <li>
            <a href="{{route('category-cities-selection-page', ["sq",strtolower($propertyTypesSQ[$keyProperty]),$propertySelectionSQ[$keyPropertySelection],$permeShqip,$actionRoute,'ne',$properties[0]->citylangswitch,$page])}}">
                <img class="flag" src="{{ asset('/img/flags/Albania.png') }}" title="Albania.png"
                     alt="Albania.png"/></a>
        </li>
    @elseif (Lang::getLocale() == "sq" )
        <li>
            <a href="{{route('category-cities-selection-page', ["en",strtolower($propertyTypesEN[$keyProperty]),$propertySelectionEN[$keyPropertySelection],$permeEnglish,$actionRoute,'in',$properties[0]->citylangswitch,$page])}}">
                <img class="flag" src="{{ asset('/img/flags/UK.png') }}" title="UK.png" alt="UK.png"/></a>
        </li>
    @endif

    <style>
        #google_translate_element {
            position: absolute;
            background: #fff;
            padding: 20px;
        }
    </style>

<!--     @include('partials.google-trans') -->
@endpush

@section('content')
    @include ('partials.search-header')
    <!-- Content -->
    <div id="content" style="padding-top: 25px">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-push-2">
                    <div class="row container-realestate">
                        <h1 class="prop-sec-title insidecategory">
                            <span class="btn btn-title btn-default cursordefault">
                            @if($propertySelection=='more+than+5+bedrooms' && $propertyType=='Villa')
	                            {{$messageSelection.' '.trans('messages.'.strtolower($propertyType).'').' '.trans('messages.for-'.strtolower($action).'')." ".$city}}</span>							
	                        @else
							    @if($propertyType=='Apartament')
									{{trans('messages.house')}}
	                            @else
	                            	{{trans('messages.'.strtolower($propertyType).'')}}
	                            @endif
	                            {{$messageSelection.' '.trans('messages.for-'.strtolower($action).'')." ".$city}}</span>
							@endif
                        </h1>
						
						@if ($lastSegment==1 && ($city=='Tirane' || $city=='Tirana') && ($propertyType=='Apartament' || $propertyType=='Apartment' || ($propertyType=='Vile' && $action=='qera') || ($propertyType=='Villa' && $action=='rent')))
						<div class="col-md-12 box" style="position: unset!important">
							<div class="about">
							  <div class="tab-content">
							  	<p>
									{{$descriptionBox}} 
								</p>
							  </div>
							</div>
						</div>
						@else
							<div class="clearfix"></div>
						@endif
						
                        @if (count($properties) === 0)
                            <div class="well">
                                <div class="alert alert-info" role="alert">Nuk kishte asnje objekt per kerkimin tuaj.
                                </div>
                            </div>
                        @endif

                        @for ($i = 0; $i < $totalproperties/4; $i++)
                            @each('templates.pronaselection', array_chunk($properties,4)[$i],'prona')
                        @endfor

                        <div class="clearfix"></div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 text-center">
                            <div class="pagination pagination-centered">
                                {!! $pagination->links('partials.pagination') !!}
                            </div>
                        </div>
                    </div>

                </div>

                @include ('partials.sidebar')

            </div>

        </div>

    </div>
    </div>
    <div id="pre-footer" class="bottomPart">
        <div class="container">

            <?php

            $for = 'for';
            $in = 'in';

            if (Lang::getLocale() === 'sq') {
                $for = 'ne';
                $in = 'ne';
            }

            ?>

            <div class="row">
                <div class="col-md-12">
                    <ol class="breadcrumb">

                        @php
                            $lang = Lang::getLocale();
                        @endphp

                        <li><a href="/{{ $lang }}">{{trans('messages.home')}}</a></li>

                        <?php

                        if ($action === 'shitje' && Lang::getLocale() === 'sq') {
                            $for = 'ne';
                        } else if ($action === 'qera' && Lang::getLocale() === 'sq') {
                            $for = 'me';
                        }

                        ?>

                        <li class="active"><a
                                    href="{{route('category-cities', [$lang,strtolower($propertyType),$for,$action,$in,$city])}}">{{trans('messages.'.strtolower($propertyType).'').' '.trans('messages.for-'.strtolower($action).'')." ".$city}}</a>
                        </li>
                        
                        <li class="active"><a
                                    href="{{route('category-cities-selection-page', [$lang,strtolower($propertyType),$propertySelection,$for,$action,$in,$city])}}"> {{$messageSelection}} {{trans('messages.'.strtolower($propertyType).'').' '.trans('messages.for-'.strtolower($action).'')." ".$city}} </a>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection