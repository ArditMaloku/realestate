@extends('layouts.main')
@section('title', $prona->title)

@section('description', $prona->meta_desc)
@section('keywords', $prona->keywords)

@push('hreflang') 
@if (Lang::getLocale() == "en" )
    <link rel="alternate" hreflang="sq"
          href="https://www.realestate.al/sq/{{Str::slug($prona->alternatetitle).".".$prona->id}}"/>
    <link rel="alternate" hreflang="en"
          href="https://www.realestate.al/en/{{Str::slug($prona->title).".".$prona->id}}"/>
@endif

@if (Lang::getLocale() == "sq" )
    <link rel="alternate" hreflang="en"
          href="https://www.realestate.al/en/{{Str::slug($prona->alternatetitle).".".$prona->id}}"/>
    <link rel="alternate" hreflang="sq"
          href="https://www.realestate.al/sq/{{Str::slug($prona->title).".".$prona->id}}"/>

@endif
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
   integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
   crossorigin=""/>
   
<script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
    integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
    crossorigin=""></script>

<link rel="canonical"
      href="https://www.realestate.al/{{Lang::getLocale()}}/{{Str::slug($prona->title).".".$prona->id}}"/>

<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@RealEstate_al">
<meta name="twitter:creator" content="@RealEstate_al">
<meta name="twitter:title" content="{{$prona->title}}">
<meta name="twitter:description"
      content="{{substr(substr( strip_tags($prona->body),0,125),0,strrpos(substr( strip_tags($prona->body),0,125),' '))}}">
<meta name="twitter:image" content="{{ url('foto/' . $prona->pic) }}">
@endpush

@push('language-switcher')
    @if (Lang::getLocale() == "en" )
        <li>
            <a href="/sq/{{Str::slug($prona->alternatetitle).".".$prona->id}}">
                <img class="flag" src="{{ asset('/img/flags/Albania.png') }}" title="ALflag" alt="ALflag"/>
            </a>
        </li>
    @elseif(Lang::getLocale() == "sq" )
        <li>
            <a href="/en/{{Str::slug($prona->alternatetitle).".".$prona->id}}">
                <img class="flag" src="{{ asset('/img/flags/UK.png') }}" title="Ukflag" alt="Ukflag"/>
            </a>
        </li>
    @endif

    <style>
        #google_translate_element {
            position: absolute;
            background: #fff;
            padding: 20px;
        }
        
        .single-property .property-images .additional-images .img-item img {
	       width: 100%!important;
	    }
	    
        @media (min-width: 991px)
        {
	        #carousel {
		        width:66%!important;
	        }

			.title1{
				display: block;
				position: fixed;
				right: 15px;	
	
		    }
		    
			.title2{
				display: none;
		    }

	    }
	    .leaflet-popup-content{
		    width: 245px;
	    }

        #main-image{
            background-repeat: no-repeat!important;
        }

	    @media (max-width: 990px)
        {
		    .title1{
				display: none;
		    }
		    
		    .title2{
				display: block;
		    }

		}
		@media (max-width: 737px)
		{
			#main-image{
				background-size: cover!important;
			}
		}
    </style>

<!--     @include('partials.google-trans') -->
@endpush

@section('content')

    <!-- Content -->
    <div id="content" class="single-prop-content">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-push-2">
                    <div class="single-property">
                        <div class="tab-content">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="p-title">
                                        <h1>{{$prona->title}}</h1>
                                    </div>
                                </div>
                                <div class="col-md-8 noPadRight" id="carousel" style="">
                                    <div class="property-images">

                                        <div id="p-main-img" class="p-main-img p-image">

                                            <div style="height: 100%; background-color: #f4f4f4">
                                                <div id="main-image" class="main-image"
                                                     style="background: url('{{ url('/thumbs/640x/skedaret/' . (isset($pics[0]) ? $pics[0]->path : $prona->pic)) }}'); background-repeat: no-repeat">

                                                    <div id="previous-div">
                                                        <a id="previous-anchor"> <img id="previous-image"
                                                                                      class="previous-image"
                                                                                      src="/img/prev.png"
                                                                                      alt="previous-image"
                                                                                      title="previous-image"/>
                                                        </a>
                                                    </div>

                                                    <a class="anchor-main" id="anchor-main"
                                                       href="{{ url('/thumbs/640x/skedaret/' . (isset($pics[0]) ? $pics[0]->path : $prona->pic)) }}">
                                                        <div class="middle-div"
                                                             style="position: absolute; width: 100%; left: 0; top: 0; height: 100%;"></div>
                                                    </a>

                                                    <script>

                                                        window.popupImages = JSON.parse('{!! $popUpImages !!}');

                                                    </script>


                                                    <div id="next-div">
                                                        <a id="next-anchor"> <img id="next-image"
                                                                                  class="next-image" src="/img/next.png"
                                                                                  alt="next-image" title="next-image"/>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div id="additional-images" class="additional-images">
                                            @foreach ($pics as $pic)
                                                <div id="slickItem_{{$loop->iteration}}" class="img-item p-image">
                                                    <img class="add-image"
                                                         src="{{ url('/thumbs/640x/skedaret/' . $pic->path) }}"
                                                         alt="Image"
                                                         title="Image" data-index="{{$loop->iteration}}"/>
                                                </div>
                                            @endforeach
                                        </div>


                                    </div>
                                </div>

                                <div class="col-md-4">
                                    @if ($prona->type == "Apartament" || ($prona->type == "Apartment")||($prona->type=="Vile")||($prona->type=="Villa") || $prona->type
                                    == "Zyre" || ($prona->type == "Office" ) || ($prona->type == "Store" ) || ($prona->type == "Dyqan" ))
                                        @includeIf('contents.property.partials.show-office-apart', ['prona' => $prona])
                                    @endIf

                                    @if ($prona->type=="Toke" || $prona->type=="Land")
                                        @includeIf('contents.property.partials.show-land', ['prona' => $prona])
                                    @endIf

                                    @if ($prona->type=="Magazine-Depo" || $prona->type=="Warehouse")

                                        @includeIf('contents.property.partials.show-depo', ['prona' => $prona])
                                    @endIf

                                    @if ($prona->type=="Ambient tregtar" || $prona->type=="Commercial")
                                        @includeIf('contents.property.partials.show-business', ['prona' => $prona])
                                    @endIf
                                </div>

                                <div class="col-md-12">
                                    <div class="p-text">
                                        <?php echo $prona->body;?>
                                    </div>
                                </div>

                                <div class="col-md-4 noPadRight">
                                    <h3 class="module-title blue-title">{{trans('messages.property-in-map')}}</h3>
                                    <div id="map" style="height: 300px" class="bottomItem single-map"></div>
                                    <script>
	                                    initMap();
	                                    
                                        function initMap() {
											
											var locations = [
                                                ['{{$prona->title}}', {{$prona->koordinata}}, 4]
                                            ];
																						
											var map = L.map('map').setView([{{$prona->koordinata}}], 13);
											
											 // set map tiles source
										    L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
										      attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors',
										    }).addTo(map);

											
					                        
// 					                        var infowindow = new google.maps.InfoWindow();
                                            var marker, i;
											var image = L.icon({
											    iconUrl: 'https://cdn2.iconfinder.com/data/icons/places-4/100/home_place_marker_location_house_apartment-32.png'
											});


											
											for (i = 0; i < locations.length; i++) {
												marker = L.marker([locations[i][1], locations[i][2]], {icon: image}).addTo(map);
												marker.bindPopup(locations[i][0]);
												
												L.DomEvent.on(marker, 'click', (function (marker, i) {
													return function () {
														marker.bindPopup(locations[i][0]).openPopup();
/*
						                                infowindow.setContent(locations[i][0]);
						                                infowindow.open(map, marker);
*/
						                            }
						                        })(marker, i));
											}	 

/*
                                            var locations = [
                                                ['{{$prona->title}}', {{$prona->koordinata}}, 4]
                                            ];
                                            var map = new google.maps.Map(document.getElementById('mapinside'), {
                                                zoom: 16,
                                                center: new google.maps.LatLng({{$prona->koordinata}}),
                                                mapTypeId: google.maps.MapTypeId.ROADMAP,
                                                styles: [{
                                                    "featureType": "poi.business",
                                                    "elementType": "geometry.fill",
                                                    "stylers": [{"color": "#97d163"}]
                                                }, {
                                                    "featureType": "poi.park",
                                                    "elementType": "geometry.fill",
                                                    "stylers": [{"color": "#83c24b"}]
                                                }, {
                                                    "featureType": "water",
                                                    "elementType": "geometry.fill",
                                                    "stylers": [{"color": "#8ed4eb"}]
                                                }, {
                                                    "featureType": "water",
                                                    "elementType": "labels.text",
                                                    "stylers": [{"gamma": "0.8"}]
                                                }]
                                            });

                                            var infowindow = new google.maps.InfoWindow();
                                            var marker, i;
                                            var image = "https://cdn2.iconfinder.com/data/icons/places-4/100/home_place_marker_location_house_apartment-32.png";
                                            for (i = 0; i < locations.length; i++) {
                                                marker = new google.maps.Marker({
                                                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                                                    icon: image,
                                                    map: map
                                                });

                                                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                                                    return function () {
                                                        infowindow.setContent(locations[i][0]);
                                                        infowindow.open(map, marker);
                                                    }
                                                })(marker, i));
                                            }
*/
                                        }
                                    </script>

<!--
                                    <script async defer
                                            src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyAqXh2RBOhtWKtPEsvfcOwYoHjcTVkiUXI&callback=initMap">
                                    </script>
-->

                                </div>
                                <div class="col-md-4">
                                    <h3 class="module-title blue-title">{{trans('messages.our-contacts')}}</h3>

                                    <div class="contacts bottomItem">
                                        <div class="row">
                                            <div class="col-xs-2">
                                                <img src="/img/at.png"
                                                     style="width: 22px; padding-left: 7px; padding-top: 0px;"
                                                     title="at.png" alt="at.png"/>
                                            </div>
                                            <div class="col-xs-8 item">
                                                <img src="/img/mail-text.png" style="height: 20px; margin-bottom: 2px"
                                                     title="mail-text.png" alt="mail-text.png"/>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-2">
                                                <img src="/img/phone-icon.png"
                                                     style="width: 22px; padding-left: 7px; padding-bottom: 2px"
                                                     title="phone-icon.png" alt="phone-icon.png"
                                                />
                                            </div>
                                            <div class="col-xs-8 item">+355 4480 2288</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-2">
                                                <i class="fa fa-2x fa-mobile"
                                                   style="padding-left: 8px; font-size: 23px; color: #1f69a4"></i>
                                            </div>
                                            <div class="col-xs-8 item noPadRight">
                                                +355 6960 88288 &nbsp;&nbsp; <a
                                                        href="sms:+355696088288?body={{Request::fullUrl()}}"><i
                                                            style="font-size: 17px; color: deepskyblue;"
                                                            class="fa fa-envelope-o"></i></a>&nbsp; <a
                                                        href="https://api.whatsapp.com/send?phone=355696088288&text={{Request::fullUrl()}}"><img
                                                            src="/img/whatsapp.png"
                                                            style="width: 18px; padding-bottom: 6px; display: inline-block; border-radius: 2px"
                                                            title="whatsapp.png" alt="whatsapp.png"/></a>
                                                <a href="viber://chat/?number=%2B355696088288&text={{Request::fullUrl()}}"><img
                                                            src="/img/viber.png"
                                                            style="width: 16px; padding-bottom: 5px; display: inline-block; border-radius: 2px"
                                                            title="viber.png" alt="viber.png"/></a>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <img class="comp-img" src="/img/contact_company.jpg"
                                                     title="contact_company.jpg" alt="contact_company.jpg"/>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-2">
                                                <i class="fa fa-2x fa-home"
                                                   style="font-size: 20px; padding: 5px; color: #ec4446"></i>
                                            </div>
                                            <div class="col-xs-8 item noPadRight">Rr. Durresit, kryqezimi me Rr. Milan
                                                Shuflaj. Nr. 5 Tirana, Albania
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 noPadLeft">
                                    <h3 class="module-title blue-title">{{trans('messages.inquire-about-property')}}</h3>

                                    @if(!empty(session('message')))
                                        <div class="alert alert-success">
                                            {{ session('message') }}
                                        </div>
                                    @endif

                                    @if(!empty(session('errorMessage')))
                                        <div class="alert alert-danger">
                                            {{ session('errorMessage') }}
                                        </div>
                                    @endif

                                    @if (count($errors) > 0)
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li> @endforeach
                                            </ul>
                                        </div>
                                    @endif

                                    <div class="bottomItem contactForm">
                                        <form method="POST"
                                              action="/{{ app()->getLocale() }}/property/send/{{ $prona->id }}">
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <label>{{trans('messages.name')}}</label>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <input name="name" required="required" type="text"
                                                               class="form-control"
                                                               value="{{ old('name') }}"
                                                               placeholder="{{trans('messages.enter-name')}}: ">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-3 label-left">
                                                        <label>{{trans('messages.phone')}}</label>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <input type="text" name="phone" class="form-control"
                                                               value="{{ old('phone') }}"
                                                               placeholder="{{trans('messages.enter-phone')}} : ">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-3 label-left">
                                                        <label>{{trans('messages.email')}}</label>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <input name="email" required="required" type="email"
                                                               class="form-control"
                                                               value="{{ old('email') }}"
                                                               placeholder="{{trans('messages.enter-email')}} : ">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-3 label-left">
                                                        <label>{{trans('messages.message-form')}}</label>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <textarea class="form-control" rows="4" name="message"
                                                                  required="required"
                                                                  placeholder="{{trans('messages.type-message')}} : ">{{ old('message') }}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="hidden" name="id" value="{{  $prona->id }}">
                                            <input type="hidden" name="kodi" value="{{  $prona->kodi }}">
                                            <div class="row">
                                                <div class="col-md-9 col-md-offset-3">
                                                    <input type="submit" name="submit"
                                                           value="{{trans('messages.send-message')}}"
                                                           class="btn btn-default btn-sm">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="row container-realestate similar-properties" style="margin-right: 0px">

                                        <div class="col-md-12 noPadRight">
											<h2 class="prop-sec-title">
									            <a href="javascript:void()" style="cursor:default;"
									               class="btn btn-title btn-default btn-darkcyan">{{trans('messages.similar-properties')}}</a>
									        </h2>
                                        </div>
                                        <div class="clearfix"></div>
                                        @for ($i = 0; $i
                                        < count($similar)/4; $i++) @each( 'templates.pronaselectionsimilar',
                                        array_chunk($similar,4)[$i], 'prona') @endfor </div>
                                </div>
                                <!-- end of similar posts-->

                            </div>
                        </div>
                    </div>
                </div>
                @include ('partials.sidebar')

            </div>
            
            <?php 
	            $test = explode("+",$propertySelection);
                                                        
                if(!(ctype_digit($test[0]) && ctype_digit($test[1])))
                {	                            
					$propertySelectiontitle = str_replace('+',' ',$propertySelection);
				}else{
					$propertySelectiontitle = $propertySelection;
				}




            $for = 'for';
            $action = 'sale';
            $in = 'in';

            if (Lang::getLocale() === 'sq') {
                $for = 'ne';
                $in = 'ne';
            }

            ?>

            <div class="row">
                <div class="col-md-12">

                    <ol class="breadcrumb">
                        <li><a href="/{{ Lang::getLocale() }}">{{trans('messages.home')}}</a></li>

                        @if ($prona->sale)


                            <?php

                            if (Lang::getLocale() === 'sq') {
                                $action = 'shitje';
                                $for = 'ne';
                            } else {
                                $action = 'sale';
                            }

                            ?>

                            <li>
                                <a href="{{route('category-cities', [ Lang::getLocale(),strtolower($prona->type),$for,$action,$in,$prona->city])}}">{{trans('messages.'.strtolower($prona->type).'').' '.trans('messages.for-'.strtolower('sale').'')." ".$prona->city}}</a>
                            </li>
                            
                        @endif @if ($prona->rent)


                            <?php

                            if (Lang::getLocale() === 'sq') {
                                $action = 'qera';
                                $for = 'me';
                            } else {
                                $action = 'rent';
                            }
                            
                            							
                            ?>
                           

                            <li>
                                <a href="{{route('category-cities', [ Lang::getLocale(),strtolower($prona->type),$for,$action,$in,$prona->city])}}">{{trans('messages.'.strtolower($prona->type).'').' '.trans('messages.for-'.strtolower('rent').'')." ".$prona->city}}</a>
                            </li>							
							
							                        
                        @endif
						
						<?php if($prona->type=="Magazine-Depo"){
							$pronaBreadcrumb = "Magazine";	
						}else{
							$pronaBreadcrumb = $prona->type;	
						}
						?>
						
						<li class="active">
	                        <a href="{{route('category-cities-selection-page', [Lang::getLocale(),strtolower($pronaBreadcrumb),$propertySelection,$for,$action,$in,$prona->city])}}">  {{ucfirst($propertySelectiontitle).' '.trans('messages.'.strtolower($prona->type).'').' '.trans('messages.for-'.strtolower($action).'')." ".$prona->city}} 							
	                        </a>
                        </li>

                        <li class="active"><a
                                    href="/{{Lang::getLocale()}}/{{Str::slug($prona->title).".".$prona->id}}">{{$prona->title}}</a>
                        </li>
                    </ol>
                </div>
            </div>

        </div>
    </div>

    @include('partials.whats-app')


@endsection

@push('scripts')
    <script src="//cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js"></script>
    <script src="{{ asset('/js/lib/jquery.touchSwipe.min.js') }}"></script>

<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "Residence",
    "name": "{{ $prona->title }}",
    "description": "{{ $prona->meta_desc }}",
    "url": "{{ url()->current() }}",
    "image": "{{ url('/thumbs/640x/skedaret/' . (isset($pics[0]) ? $pics[0]->path : $prona->pic)) }}",
    "address": {
        "@context": "https://schema.org",
        "@type": "PostalAddress",
        "streetAddress": "{{ $prona->address }}",
        "addressCountry": {
            "@type": "Country",
            "name": "Albania"
        }
    },
    "photo": [
    @for ($i=1; $i<5; $i++)
        {
            "@type": "ImageObject",
            "contentUrl": "{{ url('/thumbs/640x/skedaret/' . $pics[$i]->path) }}"
        }@if($i<4),@endif
    @endfor
    ]
}
</script>
@endpush

@push('styles')
    <link href="//cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css"/>
    <link rel="stylesheet" type="text/css"
          href="//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css"/>
@endpush