@extends('layouts.main')
<?php $lastSegment = request()->segment(count(request()->segments())); 
if (strlen($lastSegment)>3)
{
	$lastSegment = 1;
}
?>

@if (Lang::getLocale() == "en")
	@if($propertyType=='Apartment' || $propertyType=='Villa')
		@section('title',$titleMeta.' - Page '.$lastSegment)
	@else
		@section('title', trans('messages.'.strtolower($propertyType).'').' '.trans('messages.for-'.strtolower($action).'')." ".$city.' Page '.$lastSegment)
	@endif
@else
	@if($propertyType=='Apartament' || $propertyType=='Vile')
		@section('title',$titleMeta.' - Faqe '.$lastSegment)
	@else
		@section('title', trans('messages.'.strtolower($propertyType).'').' '.trans('messages.for-'.strtolower($action).'')." ".$city.' Faqe '.$lastSegment)
	@endif
@endif

@if($lastSegment==1)
	@section('description', $descriptionMeta)
	@section('keywords', $keywordsMeta)
@endif

@if($propertyType=='Apartament' || $propertyType=='Apartment' || $propertyType=='Vile' || $propertyType=='Villa')
	@section('titleMeta',$titleMeta)
@endif 

@push('hreflang')
    @if (Lang::getLocale() == "en" )
        <?php
        $propertyTypesEN = array("Apartment", "Villa", "Land", "Office", "Commercial", "Warehouse");
        $propertyTypesSQ = array("Apartament", "Vile", "Toke", "Zyre", "Ambient tregtar", "Magazine");
        $keyProperty = array_search($propertyType, $propertyTypesEN);
        $permeShqip = ($action == "rent") ? "me" : "ne";
        $permeEnglish = ($action == "rent") ? "for" : "for";
        $actionRoute = ($action == "rent") ? "qera" : "shitje";
        $inCity = "in";

/*
		$urlArray = explode('/',url()->current());
		$urlArray = array_filter($urlArray); 
		array_pop($urlArray);
		$urlString = implode('/',$urlArray).'/';
*/

        ?>
        <link rel="alternate" hreflang="sq"
              href="{{route('category-cities', ["sq",strtolower($propertyTypesSQ[$keyProperty]),$permeShqip,$actionRoute,'ne',$properties[0]->citylangswitch])}}"/>

	    <link rel="canonical"
	              href="{{url()->current()}}"/>	
    @endif

    @if (Lang::getLocale() == "sq" )
        <?php
        $propertyTypesEN = array("Apartment", "Villa", "Land", "Office", "Commercial", "Warehouse");
        $propertyTypesSQ = array("Apartament", "Vile", "Toke", "Zyre", "Ambient tregtar", "Magazine");
        $keyProperty = array_search($propertyType, $propertyTypesSQ);
        $permeShqip = ($action == "rent") ? "me" : "ne";
        $permeEnglish = ($action == "rent") ? "for" : "for";
        $actionRoute = ($action == "qera") ? "rent" : "sale";
        ?>
        <link rel="alternate" hreflang="en"
              href="{{route('category-cities', ["en",strtolower($propertyTypesEN[$keyProperty]),'for',$actionRoute,'in',$properties[0]->citylangswitch])}}"/>
		 <link rel="canonical" href="{{url()->current()}}"/>			
    @endif
@endpush

@push('language-switcher')
    @if (Lang::getLocale() == "en" )
        <li>
            <a href="{{route('category-cities', ["sq",strtolower($propertyTypesSQ[$keyProperty]),$permeShqip,$actionRoute,'ne',$properties[0]->citylangswitch])}}"><img
                        class="flag" src="{{ asset('/img/flags/Albania.png') }}" title="Albania.png" alt="Albania.png"/></a>
        </li>
    @endif

    @if (Lang::getLocale() == "sq" )
        <li>
            <a href="{{route('category-cities', ["en",strtolower($propertyTypesEN[$keyProperty]),$permeEnglish,$actionRoute,'in',$properties[0]->citylangswitch])}}"><img
                        class="flag" src="{{ asset('/img/flags/UK.png') }}" title="UK.png" alt="UK.png"/></a></li>
    @endif

    <style>
        #google_translate_element {
            position: absolute;
            background: #fff;
            padding: 20px;
        }
        .prop-sec-title{
	        float:unset!important;
        }
        
        
    </style>

<!--     @include('partials.google-trans') -->
@endpush

@section('content')

    @include ('partials.search-header')

    <!-- Content -->
    <div id="content" style="padding-top: 25px">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-push-2">
                    <div class="row container-realestate {{$classnrcolumns}}">
                        <h1 class="prop-sec-title insidecategory">
                            <span class="btn btn-title btn-default cursordefault">{{trans('messages.'.strtolower($propertyType).'').' '.trans('messages.for-'.strtolower($action).'')." ".$city}}</span>
                        </h1>
                        
                       @if ($lastSegment==1 && ($city=='Tirane' || $city=='Tirana') && ($propertyType=='Apartament' || $propertyType=='Apartment' || $propertyType=='Vile'  || $propertyType=='Villa') || (($propertyType=='Zyre' || $propertyType=='Office' || $propertyType=='Ambient tregtar' || $propertyType=='Commercial') && (strtolower($action)=='qera' || strtolower($action)=='rent') || ($propertyType=='Toke' && strtolower($action)=='shitje')))
						<div class="col-md-12 box" style="position: unset!important">
							<div class="about">
							  <div class="tab-content">
							  	<p>
									<?php 
										echo $descriptionP1; 
										
										if($propertyType=='Apartament' || $propertyType=='Apartment'){ 
											$propertyTypeTitle = trans('messages.house');
									?>
									<a href="{{$linkFirstColumn}}">{{ $propertyTypeTitle.' '.$titleFirstColumn.' '.trans('messages.for-'.strtolower($action).'').' '.$city}},</a>
									<a href="{{$linkSecondColumn}}">{{ $propertyTypeTitle.' '.$titleSecondColumn.' '.trans('messages.for-'.strtolower($action).'').' '.$city}},</a> 
									<a href="{{$linkThirdColumn}}">{{ $propertyTypeTitle.' '.$titleThirdColumn.' '.trans('messages.for-'.strtolower($action).'').' '.$city}},</a>
									<a href="{{$linkFourthColumn}}">{{ $propertyTypeTitle.' '.$titleFourthColumn.' '.trans('messages.for-'.strtolower($action).'').' '.$city}}.</a><br>
									<?php
										}
										else{
											$propertyTypeTitle = $propertyType;
										}
									?>
									
									<?php echo $descriptionP2; ?>
								</p>
							  </div>
							</div>
						</div>
						@else
							<div class="clearfix"></div>
						@endif
						
                        @if (count($properties) === 0)
                            <div class="well">
                                <div class="alert alert-info" role="alert">Nuk kishte asnje objekt per kerkimin tuaj.
                                </div>
                            </div>
                        @endif

                        @for ($i = 0; $i < $totalproperties/4; $i++)
                            @each('templates.pronaselection', array_chunk($properties,4)[$i],'prona')
                        @endfor
						
						<div class="clearfix"></div>
                    </div>

					<div class="row">
		                <div class="col-md-12 text-center">
		                    <div class="pagination pagination-centered">
		                        {!! $pagination->links('partials.pagination') !!}
		                    </div>
		                </div>
		            </div>	
		            
		            <div class="col-md-6 col-md-20">
			                <div class="col-md-12 noPadRight">
			                    <h2 class="prop-sec-title listing-category"><a href="{{$linkFirstColumn}}"
			                                                                   class="btn btn-title btn-default">
																			    @if (Lang::getLocale() === 'sq')
				                                                                   	@if ($propertyType=='Apartment' || $propertyType=='Apartament')
																				   		{{trans('messages.house') }}
																					@else 
																						{{trans('messages.'.strtolower($propertyType).'') }}
																					@endif
					                                                                   	{{$titleFirstColumn.' '.trans('messages.for-'.strtolower($action).'')}} {{ $city }}
																				@else
																					{{$titleFirstColumn}}
				                                                                   	@if ($propertyType=='Apartment' || $propertyType=='Apartament')
																				   		{{trans('messages.house') }}
																					@else 
																						{{trans('messages.'.strtolower($propertyType).'') }}
																					@endif
				                                                                   		{{trans('messages.for-'.strtolower($action).'')}} {{ $city }}	
																				@endif
					                                                                   </a>
			                    </h2>
			                </div>
					</div>
					
					<div class="col-md-6 col-md-20">
							<div class="col-md-12 noPadRight">
		                        <h2 class="prop-sec-title listing-category"><a href="{{$linkSecondColumn}}"
		                                                                       class="btn btn-title btn-default btn-jeshil">
			                                                                    @if (Lang::getLocale() === 'sq')
			                                                                        @if ($propertyType=='Apartment' || $propertyType=='Apartament')
																				   		{{trans('messages.house') }}
																					@else 
																						{{trans('messages.'.strtolower($propertyType).'') }}
																					@endif
																						{{$titleSecondColumn.' '.trans('messages.for-'.strtolower($action).'')}} {{ $city }}
																				@else
																					{{$titleSecondColumn}}
																					@if ($propertyType=='Apartment' || $propertyType=='Apartament')
																				   		{{trans('messages.house') }}
																					@else 
																						{{trans('messages.'.strtolower($propertyType).'') }}
																					@endif
																						{{trans('messages.for-'.strtolower($action).'')}} {{ $city }}
																				@endif
																					</a>
		                		</h2>
		            		</div>
					</div>
					
					<div class="col-md-6 col-md-20">
							<div class="col-md-12 noPadRight">
		                        <h2 class="prop-sec-title listing-category"><a href="{{$linkThirdColumn}}"
		                                                                       class="btn btn-title btn-default btn-danger">
																			    @if (Lang::getLocale() === 'sq')
			                                                                        @if ($propertyType=='Apartment' || $propertyType=='Apartament')
																				   		{{trans('messages.house') }}
																					@else 
																						{{trans('messages.'.strtolower($propertyType).'') }}
																					@endif
																						{{$titleThirdColumn.' '.trans('messages.for-'.strtolower($action).'')}} {{ $city }}
																				@else	
																					{{$titleThirdColumn}}
																					@if ($propertyType=='Apartment' || $propertyType=='Apartament')
																				   		{{trans('messages.house') }}
																					@else 
																						{{trans('messages.'.strtolower($propertyType).'') }}
																					@endif
																						{{trans('messages.for-'.strtolower($action).'')}} {{ $city }}
																				@endif
																			</a>
		                        </h2>
		                    </div>
					</div>
					
					<div class="col-md-6 col-md-20">
							<div class="col-md-12 noPadRight">
		                        <h2 class="prop-sec-title listing-category"><a href="{{$linkFourthColumn}}"
		                                                                       class="btn btn-title btn-default btn-darkcyan">
			                                                                    @if (Lang::getLocale() === 'sq')
			                                                                        @if ($propertyType=='Apartment' || $propertyType=='Apartament')
																				   		{{trans('messages.house') }}
																					@else 
																						{{trans('messages.'.strtolower($propertyType).'') }}
																					@endif
																						{{$titleFourthColumn.' '.trans('messages.for-'.strtolower($action).'')}} {{ $city }}
																				@else
																					{{$titleFourthColumn}}
																					@if ($propertyType=='Apartment' || $propertyType=='Apartament')
																				   		{{trans('messages.house') }}
																					@else 
																						{{trans('messages.'.strtolower($propertyType).'') }}
																					@endif
																						{{trans('messages.for-'.strtolower($action).'')}} {{ $city }}

																				@endif
																			</a>
		                        </h2>
		                    </div>
					</div>

                </div>
                @include ('partials.sidebar-inside-category')
            </div>

        </div>
    </div>
    
    
    <div id="pre-footer" class="bottomPart">
        <div class="container">

            <?php

            $for = 'for';
            $in = 'in';

            if (Lang::getLocale() === 'sq') {
                $for = 'ne';
                $in = 'ne';
            }

            ?>

            <div class="row">
                <div class="col-md-12">
                    <ol class="breadcrumb">
                        <li><a href="/{{ Lang::getLocale() }}">{{trans('messages.home')}}</a></li>

                        <?php

                        if ($action === 'shitje' && Lang::getLocale() === 'sq') {
                            $for = 'ne';
                        } else if ($action === 'qera' && Lang::getLocale() === 'sq') {
                            $for = 'me';
                        }

                        ?>

                        <li class="active"><a
                                    href="{{route('category-cities', [ Lang::getLocale(),strtolower($propertyType),$for,$action,$in,$city])}}">{{trans('messages.'.strtolower($propertyType).'').' '.trans('messages.for-'.strtolower($action).'')." ".$city}}</a>
                        </li>
                    </ol>
                </div>
            </div>
            
            
        </div>
    </div>
@endsection
