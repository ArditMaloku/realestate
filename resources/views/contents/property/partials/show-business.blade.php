<table class="table table-bordered table-striped infotable">
    <tbody>
    <tr>
        <td width="20%"><strong>{{trans('messages.id')}}</strong></td>
        <td>{{$prona->kodi}}</td>
    </tr>
    <tr>
        <td><strong>{{trans('messages.type')}}</strong></td>
        <td>{{$prona->type}}</td>
    </tr>
    <tr>
        <td><strong>{{trans('messages.status')}}</strong></td>
        <td>@if ($prona->sale == 1 ) {{trans('messages.for-sale')}} @endif
            @if ($prona->rent == 1 ) {{trans('messages.for-rent')}} @endif</td>
    </tr>
    <tr>
        <td><strong>{{trans('messages.price')}} &euro;</strong></td>
        <td>{{$prona->price}}</td>
    </tr>

    <tr>
        <td><strong>{{trans('messages.area')}} m2</strong></td>
        <td>{{$prona->area}}</td>
    </tr>


    <tr>
        <td><strong>{{trans('messages.bathrooms')}}</strong></td>
        <td>{{$prona->bathrooms}}</td>
    </tr>

    <tr>
        <td><strong>{{trans('messages.parking')}}</strong></td>
        <td>@if ($prona->parking == 1 ) {{trans('messages.yes')}} @else
                {{trans('messages.no')}} @endif</td>
    </tr>
    <tr>
        <td><strong>{{trans('messages.city')}}</strong></td>
        <td>{{$prona->city}}</td>
    </tr>
    <tr>
        <td><strong>{{trans('messages.address')}}</strong></td>
        <td>{{$prona->address}}</td>
    </tr>

    </tbody>
</table>