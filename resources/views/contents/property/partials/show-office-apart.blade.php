<table class="table table-bordered table-striped infotable">
    <tbody>
    <tr>
        <td width="20%"><strong>{{trans('messages.id')}}</strong></td>
        <td>{{$prona->kodi}}</td>
    </tr>
    <tr>
        <td><strong>{{trans('messages.type')}}</strong></td>
        <td>{{$prona->type}}</td>
    </tr>
    <tr>
        <td><strong>{{trans('messages.status')}}</strong></td>
        <td>@if ($prona->sale == 1 ) {{trans('messages.for-sale')}} @endif
            @if ($prona->rent == 1 ) {{trans('messages.for-rent')}} @endif</td>
    </tr>
    <tr>
        <td><strong>{{trans('messages.price')}} &euro;</strong></td>
        <td>@if ($prona->sale == 1 ) {{$prona->price}} @endIf
            @if ($prona->rent == 1 ) {{$prona->price}} @endIf</td>
    </tr>
	@if ($prona->type == "Apartament" || $prona->type == "Apartment" || $prona->type == 'Zyre' || $prona->type == 'Office')
	<tr>
        <td><strong>{{trans('messages.floor')}}</strong></td>
        <td>{{$prona->kati}}</td>
    </tr>
    @endif
    <tr>
        <td><strong>{{trans('messages.area')}} m2</strong></td>
        <td>{{$prona->area}}</td>
    </tr>
    @if ($prona->type == "Apartament" || ($prona->type == "Apartment")||($prona->type=="Vile")||($prona->type=="Villa"))
        <tr>
            <td><strong>{{trans('messages.bedrooms')}}</strong></td>
            <td>{{$prona->rooms}}</td>
        </tr>
    @endif @if ($prona->type == "Zyre" || ($prona->type == "Office" ))
        <tr>
            <td><strong>{{trans('messages.ambients')}}</strong></td>
            <td>{{$prona->ambients}}</td>
        </tr>
    @endif
    <tr>
        <td><strong>{{trans('messages.bathrooms')}}</strong></td>
        <td>{{$prona->bathrooms}}</td>
    </tr>
    <tr>
        <td><strong>{{trans('messages.balcony')}}</strong></td>
        <td>{{$prona->balcony}}</td>
    </tr>
    <tr>
        <td><strong>{{trans('messages.parking')}}</strong></td>
        <td>@if ($prona->parking == 1 ) {{trans('messages.yes')}} @else
                {{trans('messages.no')}} @endif</td>
    </tr>
    <tr>
        <td><strong>{{trans('messages.city')}}</strong></td>
        <td>{{$prona->city}}</td>
    </tr>
    <tr>
        <td><strong>{{trans('messages.address')}}</strong></td>
        <td>{{$prona->address}}</td>
    </tr>
    
    
    @if ($prona->planimetri)
        <tr>
            <td><strong>{{trans('messages.planimetri')}}</strong></td>
            <td><a download href="{{ url('/foto/' . $prona->planimetri) }}">{{ trans('general.download') }}</a></td>
        </tr>
    @endIf


    </tbody>
</table>