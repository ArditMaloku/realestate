@extends('layouts.main')
@section('title', $title)

@push('hreflang')
    @if (Lang::getLocale() == "en" )
        <link rel="alternate" hreflang="sq" href="/sq/{{$alternatelink}}"/>
    @endif
    @if (Lang::getLocale() == "sq" )
        <link rel="alternate" hreflang="en" href="/en/{{$alternatelink}}"/>
    @endif
    <link rel="canonical" href="https://www.realestate/{{Lang::getLocale()}}/{{$currentlink}}"/>
@endpush

@push('language-switcher')
    @if (Lang::getLocale() == "en" )
        <li>
            <a href="/sq/{{$alternatelink}}">
                <img class="flag" src="{{ asset('/img/flags/Albania.png') }}" title="Albania.png"
                     alt="Albania.png"/>
            </a>
        </li>
    @endif

    @if (Lang::getLocale() == "sq" )
        <li>
            <a href="/en/{{$alternatelink}}">
                <img class="flag" src="{{ asset('/img/flags/UK.png') }}" title="UK.png" alt="UK.png"/>
            </a>
        </li>
    @endif

    <style>
        #google_translate_element {
            position: absolute;
            background: #fff;
            padding: 20px;
        }
    </style>

<!--     @include('partials.google-trans') -->
@endpush

<?php

$lang = Lang::getLocale();

function cityText($city)
{
    $lang = Lang::getLocale();

    $shqip = [
        "Tirana" => "Tirane",
        "Saranda" => "Sarande",
        "Durres" => "Durres",
        "Vlora" => "Vlore",
        "Himara" => "Himare",
        "Dhermi" => "Dhermi",
        "Pogradec" => "Pogradec",
        "Golem" => "Golem",
        "Orikum" => "Orikum",
        "Fier" => "Fier",
        "Radhima" => "Radhima",
        "Ksamil" => "Ksamil",
        "Kavaja" => "Kavaja",
        "Qeparo" => "Qeparo",
        "Borsh" => "Borsh",
        "Lalzit Bay" => "Gjiri Lalzit",
        "Pish Poro" => "Pish Poro",
        "Porto Palermo" => "Porto Palermo",
        "Darezeze" => "Darezeze",
        "Rinas Airport" => "Rinas",
        "Rinas" => "Rinas",
        "Spille" => "Spille",
        "Gose" => "Gose",
        "Lukove" => "Lukove",
        "Velipoje" => "Velipoje",
        "Korca" => "Korce",
        "Lezhe" => "Lezhe",
        "Shkoder" => "Shkoder",
        "Lushnja" => "Lushnje",
        "Elbasan" => "Elbasan",
        "Gjirokaster" => "Gjirokaster",
        "Berat" => "Berat",
        "Kruje" => "Kruje",
        "Divjake" => "Divjake",
    ];

    if ($lang === 'en') {
        return $city;
    }

    return $shqip[$city];
}

?>

@section('content')
    <!-- Content -->
    <div id="content" style="padding-top: 45px">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-push-2">
                    <div class="about">
                        <div class="tab-content">
                        <p>
                           <?php echo $description; ?>
                        </p>
                        </div>
                    </div>
                    <div class="about">
                        <div class="tab-content">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="about-title">
                                        <h1>{{ $title }}</h1>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="about-text">

                                        <div class="box">
                                            <ul class="col-md-3 col-sm-6 col-xs-12">
                                                <li>
                                                    <h5>
                                                        <a href="{{ route("properties-city-$lang", [$lang, cityText('Tirana')]) }}">{{ trans('messages.properties-in') }} {{ cityText('Tirana') }}</a>
                                                    </h5>
                                                </li>

                                                <li>
                                                    <h5>
                                                        <a href="{{ route("properties-city-$lang", [$lang, cityText('Saranda')]) }}">{{ trans('messages.properties-in') }} {{ cityText('Saranda') }}</a>
                                                    </h5>
                                                </li>
                                                <li>
                                                    <h5>
                                                        <a href="{{ route("properties-city-$lang", [$lang, cityText('Durres')]) }}">{{ trans('messages.properties-in') }} {{ cityText('Durres') }}</a>
                                                    </h5>
                                                </li>
                                                <li>
                                                    <h5>
                                                        <a href="{{ route("properties-city-$lang", [$lang, cityText('Vlora')]) }}">{{ trans('messages.properties-in') }} {{ cityText('Vlora') }}</a>
                                                    </h5>
                                                </li>
                                                <li>
                                                    <h5>
                                                        <a href="{{ route("properties-city-$lang", [$lang, cityText('Himara')]) }}">{{ trans('messages.properties-in') }} {{ cityText('Himara') }}</a>
                                                    </h5>
                                                </li>
                                                <li>
                                                    <h5>
                                                        <a href="{{ route("properties-city-$lang", [$lang, cityText('Dhermi')]) }}">{{ trans('messages.properties-in') }} {{ cityText('Dhermi') }}</a>
                                                    </h5>
                                                </li>
                                                <li>
                                                    <h5>
                                                        <a href="{{ route("properties-city-$lang", [$lang, cityText('Pogradec')]) }}">{{ trans('messages.properties-in') }} {{ cityText('Pogradec') }}</a>
                                                    </h5>
                                                </li>
                                                <li>
                                                    <h5>
                                                        <a href="{{ route("properties-city-$lang", [$lang, cityText('Golem')]) }}">{{ trans('messages.properties-in') }} {{ cityText('Golem') }}</a>
                                                    </h5>
                                                </li>
                                                <li>
                                                    <h5>
                                                        <a href="{{ route("properties-city-$lang", [$lang, cityText('Orikum')]) }}">{{ trans('messages.properties-in') }} {{ cityText('Orikum') }}</a>
                                                    </h5>
                                                </li>
                                            </ul>
                                            <ul class="col-md-3 col-sm-6 col-xs-12">
                                                <li>
                                                    <h5>
                                                        <a href="{{ route("properties-city-$lang", [$lang, cityText('Fier')]) }}">{{ trans('messages.properties-in') }} {{ cityText('Fier') }}</a>
                                                    </h5>
                                                </li>
                                                <li>
                                                    <h5>
                                                        <a href="{{ route("properties-city-$lang", [$lang, cityText('Radhima')]) }}">{{ trans('messages.properties-in') }} {{ cityText('Radhima') }}</a>
                                                    </h5>
                                                </li>
                                                <li>
                                                    <h5>
                                                        <a href="{{ route("properties-city-$lang", [$lang, cityText('Ksamil')]) }}">{{ trans('messages.properties-in') }} {{ cityText('Ksamil') }}</a>
                                                    </h5>
                                                </li>
                                                <li>
                                                    <h5>
                                                        <a href="{{ route("properties-city-$lang", [$lang, cityText('Kavaja')]) }}">{{ trans('messages.properties-in') }} {{ cityText('Kavaja') }}</a>
                                                    </h5>
                                                </li>
                                                <li>
                                                    <h5>
                                                        <a href="{{ route("properties-city-$lang", [$lang, cityText('Qeparo')]) }}">{{ trans('messages.properties-in') }} {{ cityText('Qeparo') }}</a>
                                                    </h5>
                                                </li>
                                                <li>
                                                    <h5>
                                                        <a href="{{ route("properties-city-$lang", [$lang, cityText('Borsh')]) }}">{{ trans('messages.properties-in') }} {{ cityText('Borsh') }}</a>
                                                    </h5>
                                                </li>
                                                <li>
                                                    <h5>
                                                        <a href="{{ route("properties-city-$lang", [$lang, cityText('Velipoje')]) }}">{{ trans('messages.properties-in') }} {{ cityText('Velipoje') }}</a>
                                                    </h5>
                                                </li>
                                                <li>
                                                    <h5>
                                                        <a href="{{ route("properties-city-$lang", [$lang, cityText('Divjake')]) }}">{{ trans('messages.properties-in') }} {{ cityText('Divjake') }}</a>
                                                    </h5>
                                                </li>
                                            </ul>
                                            <ul class="col-md-3 col-sm-6 col-xs-12">
                                                <li>
                                                    <h5>
                                                        <a href="{{ route("properties-city-$lang", [$lang, cityText('Lalzit Bay')]) }}">{{ trans('messages.properties-in') }} {{ cityText('Lalzit Bay') }}</a>
                                                    </h5>
                                                </li>
                                                <li>
                                                    <h5>
                                                        <a href="{{ route("properties-city-$lang", [$lang, cityText('Pish Poro')]) }}">{{ trans('messages.properties-in') }} {{ cityText('Pish Poro') }}</a>
                                                    </h5>
                                                </li>
                                                <li>
                                                    <h5>
                                                        <a href="{{ route("properties-city-$lang", [$lang, cityText('Porto Palermo')]) }}">{{ trans('messages.properties-in') }} {{ cityText('Porto Palermo') }}</a>
                                                    </h5>
                                                </li>
                                                <li>
                                                    <h5>
                                                        <a href="{{ route("properties-city-$lang", [$lang, cityText('Darezeze')]) }}">{{ trans('messages.properties-in') }} {{ cityText('Darezeze') }}</a>
                                                    </h5>
                                                </li>
                                                <li>
                                                    <h5>
                                                        <a href="{{ route("properties-city-$lang", [$lang, cityText('Rinas')]) }}">{{ trans('messages.properties-in') }} {{ cityText('Rinas Airport') }}</a>
                                                    </h5>
                                                </li>
                                                <li>
                                                    <h5>
                                                        <a href="{{ route("properties-city-$lang", [$lang, cityText('Spille')]) }}">{{ trans('messages.properties-in') }} {{ cityText('Spille') }}</a>
                                                    </h5>
                                                </li>
                                                <li>
                                                    <h5>
                                                        <a href="{{ route("properties-city-$lang", [$lang, cityText('Gose')]) }}">{{ trans('messages.properties-in') }} {{ cityText('Gose') }}</a>
                                                    </h5>
                                                </li>
                                                <li>
                                                    <h5>
                                                        <a href="{{ route("properties-city-$lang", [$lang, cityText('Lukove')]) }}">{{ trans('messages.properties-in') }} {{ cityText('Lukove') }}</a>
                                                    </h5>
                                                </li>
                                            </ul>
                                            <ul class="col-md-3 col-sm-6 col-xs-12">
                                                <li>
                                                    <h5>
                                                        <a href="{{ route("properties-city-$lang", [$lang, cityText('Korca')]) }}">{{ trans('messages.properties-in') }} {{ cityText('Korca') }}</a>
                                                    </h5>
                                                </li>
                                                <li>
                                                    <h5>
                                                        <a href="{{ route("properties-city-$lang", [$lang, cityText('Lezhe')]) }}">{{ trans('messages.properties-in') }} {{ cityText('Lezhe') }}</a>
                                                    </h5>
                                                </li>
                                                <li>
                                                    <h5>
                                                        <a href="{{ route("properties-city-$lang", [$lang, cityText('Shkoder')]) }}">{{ trans('messages.properties-in') }} {{ cityText('Shkoder') }}</a>
                                                    </h5>
                                                </li>
                                                <li>
                                                    <h5>
                                                        <a href="{{ route("properties-city-$lang", [$lang, cityText('Lushnja')]) }}">{{ trans('messages.properties-in') }} {{ cityText('Lushnja') }}</a>
                                                    </h5>
                                                </li>
                                                <li>
                                                    <h5>
                                                        <a href="{{ route("properties-city-$lang", [$lang, cityText('Elbasan')]) }}">{{ trans('messages.properties-in') }} {{ cityText('Elbasan') }}</a>
                                                    </h5>
                                                </li>
                                                <li>
                                                    <h5>
                                                        <a href="{{ route("properties-city-$lang", [$lang, cityText('Gjirokaster')]) }}">{{ trans('messages.properties-in') }} {{ cityText('Gjirokaster') }}</a>
                                                    </h5>
                                                </li>
                                                <li>
                                                    <h5>
                                                        <a href="{{ route("properties-city-$lang", [$lang, cityText('Berat')]) }}">{{ trans('messages.properties-in') }} {{ cityText('Berat') }}</a>
                                                    </h5>
                                                </li>
                                                <li>
                                                    <h5>
                                                        <a href="{{ route("properties-city-$lang", [$lang, cityText('Kruje')]) }}">{{ trans('messages.properties-in') }} {{ cityText('Kruje') }}</a>
                                                    </h5>
                                                </li>
                                            </ul>
                                        </div>


                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                @include ('partials.sidebar')
            </div>

        </div>
    </div>

    <div id="pre-footer" class="bottomPart">
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <ol class="breadcrumb">
                        <li><a href="/{{ Lang::getLocale() }}">{{trans('messages.home')}}</a></li>
                        @if(Lang::getLocale() === 'en')
                            <li class="active"><a
                                        href="/en/properties-in-albania">Properties in Albania</a>
                            </li>
                        @else
                            <li class="active"><a
                                        href="/sq/prona-ne-shqiperi">Prona ne Shqiperi</a>
                            </li>
                        @endif
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection
