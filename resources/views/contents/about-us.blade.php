@extends('layouts.main')
@section('title', $title)
@section('titleMeta', trans('messages.aboutus-meta-title'))
@section('description', trans('messages.aboutus-description'))
@section('keywords', trans('messages.aboutus-keywords'))

@push('hreflang')
    @if (Lang::getLocale() == "en" )
        <link rel="alternate" hreflang="sq" href="/sq/{{$alternatelink}}"/>
    @endif
    @if (Lang::getLocale() == "sq" )
        <link rel="alternate" hreflang="en" href="/en/{{$alternatelink}}"/>
    @endif
    <link rel="canonical" href="https://www.realestate/{{Lang::getLocale()}}/{{$currentlink}}"/>
@endpush

@push('language-switcher')
    @if (Lang::getLocale() == "en" )
        <li>
            <a href="/sq/{{$alternatelink}}">
                <img class="flag" src="{{ asset('/img/flags/Albania.png') }}" title="Albania.png"
                     alt="Albania.png"/>
            </a>
        </li>
    @endif

    @if (Lang::getLocale() == "sq" )
        <li>
            <a href="/en/{{$alternatelink}}">
                <img class="flag" src="{{ asset('/img/flags/UK.png') }}" title="UK.png" alt="UK.png"/>
            </a>
        </li>
    @endif

    <style>
        #google_translate_element {
            position: absolute;
            background: #fff;
            padding: 20px;
        }
    </style>

<!--     @include('partials.google-trans') -->
@endpush

@section('content')
    <!-- Content -->
    <div id="content" style="padding-top: 45px">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-push-2">
                    <div class="about">
                        <div class="tab-content">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="about-title">
                                        <h1>{{trans('aboutus.title')}}</h1>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="about-text">
                                        <img class="comp-img" src="{{ asset('/img/company.jpg') }}" alt="company.jpg"
                                             title="company.jpg">
                                        <p>{{trans('aboutus.description1')}}</p>

                                        <p>{{trans('aboutus.description2')}}
                                        </p>
                                        <p>{{trans('aboutus.description3')}}
                                        </p>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                @include ('partials.sidebar')
            </div>

        </div>
    </div>


    <div id="pre-footer" class="bottomPart">
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <ol class="breadcrumb">
                        <li><a href="/{{ Lang::getLocale() }}">{{trans('messages.home')}}</a></li>
                        <li class="active"><a href="/{{Lang::getLocale()}}/{{$currentlink}}">{{trans('menu.menu1')}}</a>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection
