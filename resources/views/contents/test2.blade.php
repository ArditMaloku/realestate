<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<meta name="description" content=" A simple page with useful lings to the city of Tirana and it properties . .">
<meta name="keywords" content="apartment for rent in tirana, home for rent in tirana, flat for rent in tirana, apartment for rent in kavaja street, home for rent in kavaja street, flat for rent in kavaja street, apartment for rent in delijorgji complex,"/>

<title>Properties in Tirana , Albania for rent or sale  </title>
<!--

<p>Tirana, Albanian Tiranë, city, capital of Albania. It lies 17 miles (27 km) east of the Adriatic Sea coast and along the Ishm River, at the end of a fertile plain. Tourists usually find Tirana a beautiful and charming city, where the cosmopolitan and small town feeling is intertwined with a lively night life. Tirana is where the old and new Albania meet. Temperatures vary throughout the year from an average of 6.7 ëC (44.1 ëF) in January to 24 ëC (75 ëF) in July. Springs and summers are very warm to hot often reaching over 20 ëC (68 ëF) from June to September. Tirana is the heart of the economy of Albania and the most industrialised and economically fastest growing region in Albania.</p>


  <meta name="description" content=" A simple page with useful lings to the city of Tirana and it properties . .">
    <meta name="keywords" content="apartment for rent in tirana, home for rent in tirana, flat for rent in tirana, apartment for rent in kavaja street, home for rent in kavaja street, flat for rent in kavaja street, apartment for rent in delijorgji complex,"/>


    <title>Properties in Tirana , Albania for rent or sale  </title>
	<style>
		li{
			font-size:10px;
		}
		a{
			color:black;
		}
	</style>

<div class="container">
	<h1>Properties in Albania by Category</h1>
	
	<div class="row">
		<ul class='col-md-3 list-group'> 
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/apartment-for-rent-in-Tirana">Apartment for rent in Tirana</a>
		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/apartment-for-sale-in-Tirana">Apartment for sale in Tirana</a>
		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/villa-for-rent-in-Tirana">Villa for rent in Tirana</a>
		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/apartment-for-sale-in-Saranda">Apartment for sale in Saranda</a>
		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/apartment-for-sale-in-Durres">Apartment for rent in Durres</a>
		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/apartment-for-sale-in-Vlora">Apartment for sale in Vlora</a>
		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/apartment-for-sale-in-Vlora">Apartment for sale in Vlora</a>

		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/apartment-for-sale-in-Vlora">Apartment for sale in Vlora</a>

		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/apartment-for-sale-in-Vlora">Apartment for sale in Vlora</a>

		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/apartment-for-sale-in-Vlora">Apartment for sale in Vlora</a>

		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/apartment-for-sale-in-Vlora">Apartment for sale in Vlora</a>

		    </h6>
		  </li>
		</ul>
		
		<ul class='col-md-3'> 
			<li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/apartment-for-sale-in-Vlora">Apartment for sale in Vlora</a>

		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/apartment-for-sale-in-Vlora">Apartment for sale in Vlora</a>

		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/apartment-for-sale-in-Vlora">Apartment for sale in Vlora</a>

		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/apartment-for-sale-in-Saranda">Apartment for sale in Saranda</a>
		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/apartment-for-sale-in-Durres">Apartment for rent in Durres</a>
		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/apartment-for-sale-in-Vlora">Apartment for sale in Vlora</a>
		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/apartment-for-sale-in-Vlora">Apartment for sale in Vlora</a>

		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/apartment-for-sale-in-Vlora">Apartment for sale in Vlora</a>

		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/apartment-for-sale-in-Vlora">Apartment for sale in Vlora</a>

		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/apartment-for-sale-in-Vlora">Apartment for sale in Vlora</a>

		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/apartment-for-sale-in-Vlora">Apartment for sale in Vlora</a>

		    </h6>
		  </li>	
		</ul>
		<ul class='col-md-3'> 
		<li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/apartment-for-sale-in-Vlora">Apartment for sale in Vlora</a>

		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/apartment-for-sale-in-Vlora">Apartment for sale in Vlora</a>

		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/apartment-for-sale-in-Vlora">Apartment for sale in Vlora</a>

		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/apartment-for-sale-in-Saranda">Apartment for sale in Saranda</a>
		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/apartment-for-sale-in-Durres">Apartment for rent in Durres</a>
		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/apartment-for-sale-in-Vlora">Apartment for sale in Vlora</a>
		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/apartment-for-sale-in-Vlora">Apartment for sale in Vlora</a>

		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/apartment-for-sale-in-Vlora">Apartment for sale in Vlora</a>

		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/apartment-for-sale-in-Vlora">Apartment for sale in Vlora</a>

		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/apartment-for-sale-in-Vlora">Apartment for sale in Vlora</a>

		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/apartment-for-sale-in-Vlora">Apartment for sale in Vlora</a>
		    </h6>
		  </li>			</ul>
		<ul class='col-md-3'> 
			<li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/apartment-for-sale-in-Vlora">Apartment for sale in Vlora</a>

		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/apartment-for-sale-in-Vlora">Apartment for sale in Vlora</a>

		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/apartment-for-sale-in-Vlora">Apartment for sale in Vlora</a>
		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/apartment-for-sale-in-Saranda">Apartment for sale in Saranda</a>
		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/apartment-for-sale-in-Durres">Apartment for rent in Durres</a>
		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/apartment-for-sale-in-Vlora">Apartment for sale in Vlora</a>
		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/apartment-for-sale-in-Vlora">Apartment for sale in Vlora</a>

		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/apartment-for-sale-in-Vlora">Apartment for sale in Vlora</a>
		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/apartment-for-sale-in-Vlora">Apartment for sale in Vlora</a>

		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/apartment-for-sale-in-Vlora">Apartment for sale in Vlora</a>

		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/apartment-for-sale-in-Vlora">Apartment for sale in Vlora</a>

		    </h6>
		  </li>			
		</ul>
	</div>	
</div>
-->

<p>Tirana, Albanian Tiranë, city, capital of Albania. It lies 17 miles (27 km) east of the Adriatic Sea coast and along the Ishm River, at the end of a fertile plain. Tourists usually find Tirana a beautiful and charming city, where the cosmopolitan and small town feeling is intertwined with a lively night life. Tirana is where the old and new Albania meet. Temperatures vary throughout the year from an average of 6.7 ëC (44.1 ëF) in January to 24 ëC (75 ëF) in July. Springs and summers are very warm to hot often reaching over 20 ëC (68 ëF) from June to September. Tirana is the heart of the economy of Albania and the most industrialised and economically fastest growing region in Albania.</p>




<div class="container">
	<h1>Properties in Albania by Category</h1>
	
	<div class="row">
		<ul class='col-md-3 list-group'> 
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/apartment-for-rent-in-Tirana">Apartment for rent Tirana</a>
		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/apartment-for-sale-in-Tirana">Apartment for sale Tirana</a>
		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/villa-for-rent-in-Tirana">Villa for rent Tirana</a>
		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/villa-for-sale-in-Tirana">Villa for sale Tirana</a>
		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/land-for-sale-in-Tirana">Land for sale Tirana</a>
		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/office-for-rent-in-Tirana">Office for rent Tirana</a>
		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/office-for-sale-in-Tirana">Office for sale Tirana</a>
		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/warehouse-for-rent-in-Tirana">Warehouse for rent Tirana</a>
		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/warehouse-for-sale-in-Tirana">Warehouse for sale Tirana</a>
		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/commercial-for-rent-in-Tirana">Commercial property for rent Tirana</a>
		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="https://www.realestate.al/en/commercial-for-sale-in-Tirana">Commercial property for sale Tirana</a>
		    </h6>
		  </li>
		  <li class="list-group-item list-group-item-secondary">
		    <h6>
		      <a href="/en/properties-in-albania">Properties in Albania</a>
		    </h6>
		  </li>
		</ul>
		<ul class='col-md-3 list-group'> 
		      <li class="list-group-item list-group-item-secondary">
		        <h6>
		          <a href="https://www.realestate.al/sq/apartament-me-qera-ne-Tirane">Apartament me qira Tirane</a>
		        </h6>
		      </li>
		      <li class="list-group-item list-group-item-secondary">
		        <h6>
		          <a href="https://www.realestate.al/sq/apartament-ne-shitje-ne-Tirane">Apartment ne shitje Tirane</a>
		        </h6>
		      </li>
		      <li class="list-group-item list-group-item-secondary">
		        <h6>
		          <a href="https://www.realestate.al/sq/vile-me-qera-ne-Tirane">Vilë me qira Tirane</a>
		        </h6>
		      </li>
		      <li class="list-group-item list-group-item-secondary">
		        <h6>
		          <a href="https://www.realestate.al/sq/vile-ne-shitje-ne-Tirane">Vilë në shitje Tirane</a>
		        </h6>
		      </li>
		      <li class="list-group-item list-group-item-secondary">
		        <h6>
		          <a href="https://www.realestate.al/sq/toke-ne-shitje-ne-Tirane">Tokë në shitje Tirane</a>
		        </h6>
		      </li>
		      <li class="list-group-item list-group-item-secondary">
		        <h6>
		          <a href="https://www.realestate.al/sq/zyre-me-qera-ne-Tirane">Zyre me qira Tirane</a>
		        </h6>
		      </li>
		      <li class="list-group-item list-group-item-secondary">
		        <h6>
		          <a href="https://www.realestate.al/sq/zyre-ne-shitje-ne-Tirane">Zyre ne shitje Tirane</a>
		        </h6>
		      </li>
		      <li class="list-group-item list-group-item-secondary">
		        <h6>
		          <a href="https://www.realestate.al/sq/magazine-me-qera-ne-Tirane">Magazine me qira Tirane</a>
		        </h6>
		      </li>
		      <li class="list-group-item list-group-item-secondary">
		        <h6>
		          <a href="https://www.realestate.al/sq/magazine-ne-shitje-ne-Tirane">Magazine ne shitje Tirane</a>
		        </h6>
		      </li>
		      <li class="list-group-item list-group-item-secondary">
		        <h6>
		          <a href="https://www.realestate.al/sq/ambient%20tregtar-me-qera-ne-Tirane">Ambient tregtar me qira Tirane</a>
		        </h6>
		      </li>
		      <li class="list-group-item list-group-item-secondary">
		        <h6>
		          <a href="https://www.realestate.al/sq/ambient%20tregtar-ne-shitje-ne-Tirane">Ambient tregtar në shitje Tirane</a>
		        </h6>
		      </li>
		      <li class="list-group-item list-group-item-secondary">
		        <h6>
		          <a href="/sq/prona-ne-shqiperi">Prona në Shqiperi</a>
		        </h6>
		      </li>
		  </li>
		  </ul>
		  <ul class='col-md-3 list-group'> 
			  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/sq/apartament-1+1-me-qera-ne-Tirane">1+1 Apartament qera Tirane&nbsp;</a>
			    </h6>
			  </li>
			  <li class="list-group-item list-group-item-secondary">
				<h6 class="prop-sec-title listing-category">
		       		<a  href="https://www.realestate.al/sq/apartament-2+1-me-qera-ne-Tirane">2+1 Apartament qera Tirane</a>
			   	</h6>
			  </li>
			  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/sq/apartament-3+1-me-qera-ne-Tirane">3+1 Apartament qera Tirane&nbsp;</a>
			    </h6>
			  </li>
			  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/sq/apartament-me+shume+se+3+dhoma-me-qera-ne-Tirane">&gt;3 dhoma Gjumi Qera Tirane</a>&nbsp;
			    </h6>
			  </li>
	  		  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/en/apartment-one+bedroom-for-rent-in-Tirana">One bedroom apartment rent Tirana</a>
			    </h6>
	  		  </li>
	  		  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/en/apartment-two+bedroom-for-rent-in-Tirana">Two bedroom apartment rent Tirana</a>
			    </h6>
	  		  </li>
	  		  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a href="https://www.realestate.al/en/apartment-three+bedroom-for-rent-in-Tirana">Three bedroom apartment rent Tirana&nbsp;</a>
			    </h6>
	  		  </li>
			  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/en/apartment-more+than+three+bedroom-for-rent-in-Tirana">More than three bedroom apartment rent Tirana</a>
			    </h6>
			  </li>
			  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/sq/vile-2+dhoma-me-qera-ne-Tirane">Vile me qera tirane me 2 dhoma</a>
			    </h6>
			  </li>
			  <li class="list-group-item list-group-item-secondary">			  
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/sq/vile-3+dhoma-me-qera-ne-Tirane">&nbsp;Vile me qera tirane me 3 dhoma</a>
			    </h6>
			  </li>
			  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/sq/vile-4+dhoma-me-qera-ne-Tirane">&nbsp;Vile me qera tirane me 4 dhoma</a>
			    </h6>
			  </li>
			  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/sq/vile-me+shume+se+4+dhoma-me-qera-ne-Tirane">Vile me qera tirane me më shumë se 4 dhoma</a>
			    </h6>
			  </li>
			  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/en/villa-2+bedrooms-for-rent-in-Tirana">2 bedrooms Villa rent Tirane</a>
			    </h6>
			  </li>
			  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/en/villa-3+bedrooms-for-rent-in-Tirana">3 bedrooms Villa rent Tirane</a>
			    </h6>
			  </li>
			  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/en/villa-4+bedrooms-for-rent-in-Tirana">4 bedrooms Villa rent Tirane</a>
			    </h6>
			  </li>
			  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/sq/zyre-nga+0+deri+50+m2-me-qera-ne-Tirane">Zyre me qera Tirane0-50 m2</a>
			    </h6>
			  </li>
			  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/sq/zyre-nga+50+deri+100+m2-me-qera-ne-Tirane">Zyre me qera Tirane50-100 m2</a>
			    </h6>
			  </li>
			  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/sq/zyre-nga+100+deri+200+m2-me-qera-ne-Tirane">Zyre me qera Tirane100-200 m2</a>
			    </h6>
			  </li>
			  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/sq/zyre-me+shume+se+200+m2-me-qera-ne-Tirane">Zyre me qera Tirane 200+ m2</a>
			    </h6>
			  </li>
			  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/en/office-from+0+to+50+m2-for-rent-in-Tirana">Office for rent in Tirana0-50 m2</a>
			    </h6>
			  </li>
			  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/en/office-from+50+to+100+m2-for-rent-in-Tirana">Office for rent in Tirana50-100 m2</a>
			    </h6>
			  </li>
			  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/en/office-from+100+to+200+m2-for-rent-in-Tirana">Office for rent in Tirana100-200 m2</a>
			    </h6>
			  </li>
			  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/en/office-more+than+200+m2-for-rent-in-Tirana">Office for rent in Tirana 200+ m2</a>
			    </h6>
			  </li>
			  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/sq/zyre-nga+0+deri+50+m2-ne-shitje-ne-Tirane">Zyre Shitje ne Tirane 0-50 m2</a>
			    </h6>
			  </li>
			  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/sq/zyre-nga+50+deri+100+m2-ne-shitje-ne-Tirane">Zyre Shitje ne Tirane 50-100 m2</a>
			    </h6>
			  </li>
			  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/sq/zyre-nga+100+deri+200+m2-ne-shitje-ne-Tirane">Zyre Shitje ne Tirane 100-200 m2</a>
			    </h6>
			  </li>
			  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/sq/zyre-me+shume+se+200+m2-ne-shitje-ne-Tirane">Zyre Shitje ne Tirane 200+ m2</a>
			    </h6>
			  </li>
			  </ul>
			  <ul class='col-md-3 list-group'> 
			  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/en/office-from+0+to+50+m2-for-sale-in-Tirana">Office for sale in Tirana 0-50 m2</a>
			    </h6>
			  </li>
			  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/en/office-from+50+to+100+m2-for-sale-in-Tirana">Office for sale in Tirana50-100 m2</a>
			    </h6>
			  </li>
			  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/en/office-from+100+to+200+m2-for-sale-in-Tirana">Office for sale in Tirana 100-200 m2</a>
			    </h6>
			  </li>
			  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/en/office-more+than+200+m2-for-sale-in-Tirana">Office for sale in Tirana 200+ m2</a>
			    </h6>
			  </li>
			  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/sq/toke-nga+0+deri+1000+m2-ne-shitje-ne-Tirane">Toke ne shitje ne Tirane 0-1000 m2</a>
			    </h6>
			  </li>
			  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/sq/toke-nga+1000+deri+2000+m2-ne-shitje-ne-Tirane">Toke ne shitje ne Tirane 1000-2000 m2</a>
			    </h6>
			  </li>
			  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/sq/toke-me+shume+se+5000+m2-ne-shitje-ne-Tirane">Toke ne shitje ne Tirane 5000+ m2</a>
			    </h6>
			  </li>
			  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/sq/toke-nga+2000+deri+5000+m2-ne-shitje-ne-Tirane">Toke ne shitje ne Tirane 2000-5000 m2</a>
			    </h6>
			  </li>
			  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/sq/magazine-nga+0+deri+500+m2-me-qera-ne-Tirane">Magazine me qera 0-500 m2</a>
			    </h6>
			  </li>
			  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/sq/magazine-nga+500+deri+1000+m2-me-qera-ne-Tirane">Magazine me qera 500-1000 m2</a>
			    </h6>
			  </li>
			  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/sq/magazine-nga+1000+deri+2000+m2-me-qera-ne-Tirane">Magazine me qera 1000-2000 m2</a>
			    </h6>
			  </li>
			  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/sq/magazine-me+shume+se+2000+m2-me-qera-ne-Tirane">Magazine me qera 2000+ m2</a>
			    </h6>
			  </li>
			  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/en/warehouse-from+0+to+500+m2-for-rent-in-Tirana">Warehouse rent in Tirana 0-500 m2</a>
			    </h6>
			  </li>
			  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/en/warehouse-from+500+to+1000+m2-for-rent-in-Tirana">Warehouse rent in Tirana 500-1000 m2</a>
			    </h6>
			  </li>
			  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/en/warehouse-from+1000+to+2000+m2-for-rent-in-Tirana">Warehouse rent in Tirana 1000-2000 m2</a>
			    </h6>
			  </li>
			  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/en/warehouse-more+than+2000+m2-for-rent-in-Tirana">Warehouse rent in Tirana 2000+ m2</a>
			    </h6>
			  </li>
			  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/sq/ambient%20tregtar-nga+0+deri+50+m2-ne-shitje-ne-Tirane">Ambient tregtor qera 0-50 m2</a>
			    </h6>
			  </li>
			  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/sq/ambient%20tregtar-nga+50+deri+100+m2-ne-shitje-ne-Tirane">Ambient tregtar qera 50-100 m2</a>
			    </h6>
			  </li>
			  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/sq/ambient%20tregtar-nga+100+deri+200+m2-ne-shitje-ne-Tirane">Ambient tregtar qera 100-200 m2</a>
			    </h6>
			  </li>
			  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/sq/ambient%20tregtar-me+shume+se+200+m2-ne-shitje-ne-Tirane">Ambient tregtar qera 200+ m2</a>
			    </h6>
			  </li>
			  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/en/commercial-from+0+to+50+m2-for-sale-in-Tirana">Commercial property for rent in Tirana 0-50 m2</a>
			    </h6>
			  </li>
			  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/en/commercial-from+50+to+100+m2-for-sale-in-Tirana">Commercial property for rent in Tirana 50-100 m2</a>
			    </h6>
			  </li>
			  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/en/commercial-from+100+to+200+m2-for-sale-in-Tirana">Commercial property for rent in Tirana 100-200 m2</a>
			    </h6>
			  </li>
			  <li class="list-group-item list-group-item-secondary">
			    <h6 class="prop-sec-title listing-category">
			      <a  href="https://www.realestate.al/en/commercial-more+than+200+m2-for-sale-in-Tirana">Commercial property for rent in Tirana 200+ m2</a>
			    </h6>
			  </li>
		  </li>
		</ul>