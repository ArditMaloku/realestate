@extends('layouts.main')
@section('title', trans('messages.commissions-title'))
@section('description', trans('messages.commissions-description'))

@push('hreflang')
    @if (Lang::getLocale() == "en" )
        <link rel="alternate" hreflang="sq" href="/sq/{{$alternatelink}}"/>
    @endif
    @if (Lang::getLocale() == "sq" )
        <link rel="alternate" hreflang="en" href="/en/{{$alternatelink}}"/>
    @endif
    <link rel="canonical" href="https://www.realestate/{{Lang::getLocale()}}/{{$currentlink}}"/>
@endpush

@push('language-switcher')
    @if (Lang::getLocale() == "en" )
        <li>
            <a href="/sq/{{$alternatelink}}">
                <img class="flag" src="{{ asset('/img/flags/Albania.png') }}" title="Albania.png"
                     alt="Albania.png"/>
            </a>
        </li>
    @endif

    @if (Lang::getLocale() == "sq" )
        <li>
            <a href="/en/{{$alternatelink}}">
                <img class="flag" src="{{ asset('/img/flags/UK.png') }}" title="UK.png" alt="UK.png"/>
            </a>
        </li>
    @endif

    <style>
        #google_translate_element {
            position: absolute;
            background: #fff;
            padding: 20px;
        }
    </style>

<!--     @include('partials.google-trans') -->
@endpush

@section('content')
    <!-- Content -->
    <div id="content" style="padding-top: 45px">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-push-2">
                    <div class="about">
                        <div class="tab-content">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="about-title">
                                        <h1>{{trans('commisions.title')}}</h1>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="about-text">

                                        <h5><strong>Pershendetje vizitore te faqes sone!</strong></h5><br/>

                                        <p>
                                            Agjentet tane kane bere maksimumin qe te kemi nje shumellojshmeri pronash
                                            dhe
                                            shpresojme qe te keni gjetur ate ose ato te cilet i perputhen kerkeses suaj.
                                            Para se te avanconi me tej eshte detyre e jona ligjore t‘ju lajmerojme mbi
                                            komisionet (pagesat ) qe ne aplikojme.
                                            Bazuar ne Legjislacionin Shqiptar , te Drejten Tregtare dhe praktikave te
                                            konsoliduara te agjenteve te pasurive te patundshme ne te gjithe boten ne ,
                                            ne
                                            zotesine e Personit juridik aplikojme tarifat e meposhtme.
                                        </p>

                                        <p>
                                            Shebime falas ndaj paleve :
                                            Keshillim juridik i paleve ne maredhenien tregtare.
                                            Keshillim ekonomik dhe vleresim ne parim i prones.
                                            Vleresim ne total i tregut ne nje zone te paracaktuar.
                                            Hartim ose keshillim gjate hartimit te kontratave respektive.
                                            Shtim i pronave te shoqeruara me foto ne sistemin e menaxhur nga ne.
                                            Shoqerimi per te vizituar prona te zgjedhura nga klientet brenda qellimit te
                                            parashikuar nga fusha jone
                                            e aktiviteti.
                                        </p>


                                        <p>
                                            Sherbimet me pagese.<br/>
                                            Ato sherbime qe ne aplikojme pagesen quhen komisione . Komisionet aplikohen
                                            vetem ndaj paleve ne nje marredhenie tregtare e cila eshte arritur me
                                            ndermejtesimin e stafit te Agjensise sone.
                                        </p><br/>

                                        <h5><strong>Shitje</strong></h5><br/>

                                        <p>
                                            Komisioni ndaj pales e cila shet nje prone eshte 3% e vleres totale te
                                            shitjes
                                            se prones.Komisioni eshte i pavarur nga blerja me pagese te plote apo blerje
                                            me
                                            pagese me keste .Komisioni i paguhet agjensise ne momentin e sigurimit te
                                            blerjes nga ana e paleve me nje pagese financiare.
                                        </p>

                                        <p>
                                            Komisioni ndaj pales blerese te nje prone i cili eshte lajmeruar dhe/ose
                                            shoqeruar per te pare pronen ne fjale nga nje ose me shume persona te stafit
                                            tone eshte 1% e vleres totale te blerjes.Komisioni eshte i pavarur nga
                                            blerja me
                                            pagese te plote apo blerje me pagese me keste. Komisioni i paguhet agjensise
                                            ne
                                            momentin e sigurimit te blerjes nga ana e paleve me nje pagese financiare.
                                        </p><br/>

                                        <h5><strong>Qira</strong></h5><br/>


                                        <p>
                                            Komisioni i pales qe jep me qira nje prone eshte 1/12 e vleres totale te
                                            qirase
                                            ne vitin e pare.Ky komision zbatohet vetem per vitin e pare te parashikuar
                                            ne
                                            kontrate ose me pak se nje vit.Ky komision nuk aplikohet ne vitin e dyte e
                                            ne
                                            vazhdim te kontrates ose ne rinovimet e mundshme pas vitit te pare.
                                            Komisioni ndaj pales e cila merr me qira nje prone eshte 1/24 e vleres
                                            totale te
                                            qirase ne vitin e pare. Ky komision zbatohet per qiramarrjet nga 1 muaj deri
                                            ne
                                            12 muaj.Ky komision nuk aplikohet ne vitin e dyte e ne vazhdim te kontrates
                                            ose
                                            ne rinovimet e mundshme pas vitit te pare.
                                        </p>


                                        <p>
                                            Konfidencialiteti i marredhenies tregtare eshte nje nga parimet baze te
                                            biznesit
                                            ne te cilin operojme , nje nga pikat te cilat ne krenohemi dhe gjithashtu i
                                            garantuar nga Legjislacioni ne fuqi ne Republiken e Shqiperise.
                                        </p>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                @include ('partials.sidebar')
            </div>

        </div>
    </div>

    <div id="pre-footer" class="bottomPart">
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <ol class="breadcrumb">
                        <li><a href="/{{ Lang::getLocale() }}">{{trans('messages.home')}}</a></li>
                        <li class="active"><a href="/{{Lang::getLocale()}}/{{$currentlink}}">{{trans('commisions.title')}}</a>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection
