@extends('layouts.main')
@section('title', trans('messages.index-title'))
@section('description', trans('messages.index-description'))
@section('keywords', trans('messages.index-keywords'))
@section('titleMeta', trans('messages.index-meta-title'))


@push('language-switcher')

    @if (Lang::getLocale() == "en" )
        <li><a href="https://www.realestate.al/sq"><img class="flag" title="ALflag" alt="ALflag"
                               src="{{ asset('/img/flags/Albania.png') }}"/></a></li>
    @endif
    @if (Lang::getLocale() == "sq" )
        <li><a href="https://www.realestate.al/"><img class="flag" title="Ukflag" alt="Ukflag" src="{{ asset('/img/flags/UK.png') }}"/></a>
        </li>
    @endif

<!--     @include('partials.google-trans') -->

@endpush

@push('hreflang')
    @include('partials.hreflang')
@endpush

@section('content')

    @include ('partials.search-header')

    @php

        $records = [];

        for ($i = 0; $i < 4; $i++) {

            $records[] = $apartments[$i];
            $records[] = $apartmentsForSale[$i];
            $records[] = $villaForRentTirana[$i];
            $records[] = $officeForRentTirana[$i];
        }

    @endphp

    <!-- Content -->
    <div id="content">

        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-push-2">

                    <div class="row container-realestate">

                        <div class="col-md-3">

                            <div class="row">
                                <p class="col-md-12 prop-sec-title">
                                    <a href="{{$firstColLink}}"
                                       class="btn btn-title btn-default">{{trans('messages.apartment-for-rent')}}
                                        {{trans('messages.tirana')}}</a> 
                                </p>

                                @foreach($apartments as $index => $apartment)
                                	@if($index==0)
	                                    <div class="col-sm-12 noPadRight positionrelativehome">
									@else
	                                    <div class="col-sm-12 noPadRight positionrelativehome">
									@endif
                                        @include('templates.apartment', ['apartment' => $apartment])
                                    </div>
                                @endforeach
                            </div>


                        </div>
                        <div class="col-md-3">
                            <div class="row">
                                <p class="col-md-12 prop-sec-title category-title">
                                    <a class="btn btn-title btn-default btn-jeshil"
                                       href="{{$secondColLink}}">{{trans('messages.apartment-for-sale')}} {{trans('messages.tirana')}}</a>
                                </p>

                                @foreach($apartmentsForSale as $index => $apartment)
                                    @if($index==0)
	                                    <div class="col-sm-12 noPadRight positionrelativehome">
									@else
	                                    <div class="col-sm-12 noPadRight positionrelativehome">
									@endif
                                        @include('templates.apartment', ['apartment' => $apartment])
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-md-3">

                            <div class="row">
                                <p class="col-md-12 prop-sec-title category-title">
                                    <a class="btn btn-title btn-danger"
                                       href="{{$thirdColLink}}">{{trans('messages.villa-for-rent')}} {{trans('messages.tirana')}}</a>
                                </p>

                                @foreach($villaForRentTirana as $index => $villa)
                                    @if($index==0)
	                                    <div class="col-sm-12 noPadRight positionrelativehome">
									@else
	                                    <div class="col-sm-12 noPadRight positionrelativehome">
									@endif                                        
										@include('templates.apartment', ['apartment' => $villa])
                                    </div>
                                @endforeach
                            </div>

                        </div>
                        <div class="col-md-3">

                            <div class="row">
                                <p class="col-md-12 prop-sec-title category-title">
                                    <a class="btn btn-title btn-default btn-darkcyan"
                                       href="{{$fourthColLink}}">{{trans('messages.office-for-rent')}} {{trans('messages.tirana')}}</a>
                                </p>

                                @foreach($officeForRentTirana as $index => $office)
                                    @if($index==0)
	                                    <div class="col-sm-12 noPadRight positionrelativehome">
									@else
	                                    <div class="col-sm-12 noPadRight positionrelativehome">
									@endif
                                        @include('templates.apartment', ['apartment' => $office])
                                    </div>
                                @endforeach
                            </div>

                        </div>
						<div class="col-md-12">

                            <div class="row">
                                <p class="col-md-12 prop-sec-title">
                                    <a class="btn btn-title btn-default"
	                                    href="{{$fifthColLink}}">{{trans('messages.latest-properties')}}</a>
                                </p>

                                @foreach($latestProperties as $index => $latest)
	                                    <div class="col-md-3 noPadRight">
										@include('templates.apartment', ['apartment' => $latest])
                                    </div>
                                @endforeach
                            </div>

                        </div>

                    </div>

                </div>

                @include ('partials.sidebar')

            </div>
        </div>
    </div>
    
    <div id="pre-footer" class="bottomPart">
        <div class="container">


            @include('partials.about-us')

        </div>
    </div>
    
    <div id="pre-footer" class="bottomPart">
        <div class="container">

            @include('partials.pre-about-us')

			<div class="row">
                <div class="col-md-12">
                    <ol class="breadcrumb">
                        <li class="active"><a href="/{{ Lang::getLocale() }}">{{trans('messages.home')}}</a></li>
                    </ol>
                </div>
            </div>

        </div>
    </div>
    
    <script type="application/ld+json">{"@context" : "http://schema.org", "@type" : "RealEstateAgent", "address" : { "@type": "PostalAddress", "addressLocality": "Tirane", "addressRegion": "Albania", "postalCode": "1016", "streetAddress": "Rruga Durresit Tirane" }, "name":"Realestate.al", "url":"https://www.realestate.al", "email":"info@realestate.al", "telephone":"0696088288", "openingHours": [ "Mo-Sa 09:00-18:00"], "image": {"@type": "ImageObject", "@id": "https://www.realestate.al/en/contact", "url":"https://www.realestate.al/img/contact_company.jpg"}} </script>
@endsection