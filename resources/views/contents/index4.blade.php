@extends('layouts.main4')
@section('title', trans('messages.index-title'))
@section('description', trans('messages.index-description'))


@push('language-switcher')

    @if (Lang::getLocale() == "en" )
        <li><a href="/sq"><img class="flag" title="ALflag" alt="ALflag"
                               src="{{ asset('/img/flags/Albania.png') }}"/></a></li>
    @endif
    @if (Lang::getLocale() == "sq" )
        <li><a href="/en"><img class="flag" title="Ukflag" alt="Ukflag" src="{{ asset('/img/flags/UK.png') }}"/></a>
        </li>
    @endif

<!--     @include('partials.google-trans') -->

@endpush

@push('hreflang')
    @include('partials.hreflang')
@endpush

@section('content')


    @php

        $records = [];

        for ($i = 0; $i < 4; $i++) {

            $records[] = $apartments[$i];
            $records[] = $apartmentsForSale[$i];
            $records[] = $villaForRentTirana[$i];
            $records[] = $officeForRentTirana[$i];
        }

    @endphp

    <!-- Content -->
    <div id="content">

        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-push-2">

                    <div class="row container-realestate">

                        <div class="col-md-3">

                            <div class="row">
                                <h2 class="col-md-12 prop-sec-title">
                                    <a href="{{$firstColLink}}"
                                       class="btn btn-title btn-default">{{trans('messages.apartment-for-rent')}}
                                        Tirana</a>
                                </h2>

                                @foreach($apartments as $index => $apartment)
                                    <div class="col-sm-12 noPadRight positionrelativehome">
                                        @include('templates.apartment', ['apartment' => $apartment])
                                    </div>
                                @endforeach
                            </div>


                        </div>
                        <div class="col-md-3">
                            <div class="row">
                                <h2 class="col-md-12 prop-sec-title">
                                    <a class="btn btn-title btn-default btn-jeshil"
                                       href="{{$secondColLink}}">{{trans('messages.apartment-for-sale')}} Tirana</a>
                                </h2>

                                @foreach($apartmentsForSale as $index => $apartment)
                                    <div class="col-sm-12 noPadRight positionrelativehome">
                                        @include('templates.apartment', ['apartment' => $apartment])
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-md-3">

                            <div class="row">
                                <h2 class="col-md-12 prop-sec-title">
                                    <a class="btn btn-title btn-danger"
                                       href="{{$thirdColLink}}">{{trans('messages.villa-for-rent')}} Tirana</a>
                                </h2>

                                @foreach($villaForRentTirana as $index => $villa)
                                    <div class="col-sm-12 noPadRight positionrelativehome">
                                        @include('templates.apartment', ['apartment' => $villa])
                                    </div>
                                @endforeach
                            </div>

                        </div>
                        <div class="col-md-3">

                            <div class="row">
                                <h2 class="col-md-12 prop-sec-title">
                                    <a class="btn btn-title btn-default btn-darkcyan"
                                       href="{{$fourthColLink}}">{{trans('messages.office-for-rent')}} Tirana</a>
                                </h2>

                                @foreach($officeForRentTirana as $index => $office)
                                    <div class="col-sm-12 noPadRight positionrelativehome">
                                        @include('templates.apartment', ['apartment' => $office])
                                    </div>
                                @endforeach
                            </div>

                        </div>


                    </div>

                </div>

                @include ('partials.sidebar')

            </div>
        </div>
    </div>

    <div id="pre-footer" class="bottomPart">
        <div class="container">

            @include('partials.search-in-map', ['properties' => $properties, 'grouped' => true])


            @include('partials.about-us')

            <div class="row">
                <div class="col-md-12">
                    <ol class="breadcrumb">
                        <li class="active"><a href="/{{ Lang::getLocale() }}">{{trans('messages.home')}}</a></li>
                    </ol>
                </div>
            </div>

        </div>
    </div>
@endsection