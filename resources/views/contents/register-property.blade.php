@extends('layouts.main')
@section('title', trans('messages.register-property-title'))
@section('description', trans('messages.register-property-description'))
@section('keywords', trans('messages.register-property-keywords'))

@push('hreflang')
    @include('partials.hreflang')
@endpush

@push('language-switcher')
    @if (Lang::getLocale() == "en" )
        <li>
            <a href="/sq/regjistro-pronen">
                <img class="flag" src="{{ asset('/img/flags/Albania.png') }}" title="ALflag" alt="ALflag"/>
            </a>
        </li>
    @endif
    @if (Lang::getLocale() == "sq" )
        <li><a href="/en/register-property">
                <img class="flag" src="{{ asset('/img/flags/UK.png') }}" title="Ukflag" alt="Ukflag"/>
            </a>
        </li>
    @endif
@endpush

@section('content')
    @include ('partials.search-header')

    <!-- Content -->
    <div id="content">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-push-2">
                    <div class="row container-realestate">

                        <div id="rightrightcol">
                            <div class="col-md-12">
                                <h3 class="text-danger" style="margin:0">{{ trans('regproperty.regproph3') }}</h3>
                            </div>
                            <hr/>
                            <br/>
                            <br/>

                            <div class="col-md-12">
                                @if(!empty(session('message')))
                                    <div class="alert alert-success">
                                        {{ session('message') }}
                                    </div>
                                @endif

                                @if(!empty(session('errorMessage')))
                                    <div class="alert alert-danger">
                                        {{ session('errorMessage') }}
                                    </div>
                                @endif

                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                            </div>

                            <form id="feedbackform" method="post" enctype="multipart/form-data"
                                  action="{{ url('/property/register-property') }}">
                                {{ csrf_field() }}
                                <div class="col-md-8 col-md-push-2">
                                    <legend>{{ trans('regproperty.your-property') }}</legend>


                                    <div class="form-group">
                                        <label class="form-text">
                                            <span>{{ trans('regproperty.property-type') }}: * </span> </label>
                                        <select class="form-control" name="tipi" id="tipi">
                                            <option selected="selected"
                                                    value="">{{ trans('regproperty.select-type') }}</option>
                                            <option value="Apartment"> {{ trans('regproperty.apartment') }}</option>
                                            <option value="Store">{{ trans('regproperty.store') }}</option>
                                            <option value="Hotel">{{ trans('regproperty.hotel') }}</option>
                                            <option value="Club"> {{ trans('regproperty.club') }}</option>
                                            <option value="Warehouse">{{ trans('regproperty.warehouse') }}</option>
                                            <option value="Land">{{ trans('regproperty.land') }}</option>
                                            <option value="Villa"> {{ trans('regproperty.villa') }}</option>
                                            <option value="Office">{{ trans('regproperty.office') }}</option>
                                            <option value="Parking space">{{ trans('regproperty.parking-space') }}</option>
                                        </select>
                                    </div>


                                    <div class="form-group">
                                        <label class="form-text">
                                            <span>{{ trans('regproperty.property-for') }} * </span> </label>
                                        <select class="form-control" name="lloji" id="lloji">
                                            <option value="">{{ trans('regproperty.select-type') }}</option>
                                            <option value="For rent">{{ trans('regproperty.for-rent') }}</option>
                                            <option value="For Sale">{{ trans('regproperty.for-sale') }}</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-text"> <span> {{ trans('regproperty.city') }}: * </span>
                                        </label>
                                        <select class="form-control" name="qyteti" id="qyteti">
                                            <option selected="selected"
                                                    value="">{{ trans('regproperty.select-location') }}</option>
                                            <option value="Berat">Berat</option>
                                            <option value="Borsh">Borsh</option>
                                            <option value="Dellenje">Dellenje</option>
                                            <option value="Dhermi">Dhermi</option>
                                            <option value="Durres">Durres</option>
                                            <option value="Elbasan">Elbasan</option>
                                            <option value="Fier">Fier</option>
                                            <option value="Golem">Golem</option>
                                            <option value="Himara">Himara</option>
                                            <option value="Kavaja">Kavaja</option>
                                            <option value="Korca">Korca</option>
                                            <option value="Kruja">Kruja</option>
                                            <option value="Kruje">Kruje</option>
                                            <option value="Ksamil">Ksamil</option>
                                            <option value="Kucova">Kucova</option>
                                            <option value="Lalzit Bay">Lalzit Bay</option>
                                            <option value="Lezha">Lezha</option>
                                            <option value="Lukova">Lukova</option>
                                            <option value="Lushnja">Lushnja</option>
                                            <option value="Mat">Mat</option>
                                            <option value="Mirdita">Mirdita</option>
                                            <option value="Qeparo">Qeparo</option>
                                            <option value="Rinas">Rinas</option>
                                            <option value="Rreshen">Rreshen</option>
                                            <option value="Saranda">Saranda</option>
                                            <option value="Shengjin">Shengjin</option>
                                            <option value="Shkodra">Shkodra</option>
                                            <option value="Tirana">Tirana</option>
                                            <option value="Valona">Valona</option>
                                            <option value="Velipoja">Velipoja</option>
                                            <option value="Vlora">Vlora</option>
                                            <option value="Vore">Vore</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-text"> <span> {{ trans('regproperty.price') }}: *</span>
                                        </label>
                                        <input name="cmimi" id="cmimi" type="text" class="form-control"
                                               placeholder="{{ trans('regproperty.price') }}"
                                               value="{{ old('cmimi') }}"/>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-text">
                                            <span>{{ trans('regproperty.detailed-desc') }}: * </span>
                                        </label>
                                        <textarea rows="12"
                                                  style=" background:#fff; border:1px solid  #CCC; color:#000; font-family: Arial, Helvetica, sans-serif; font-size:13px; font-style:italic;"
                                                  name="pkoment" id="pkoment"
                                                  class="form-control"
                                                  placeholder="{{ trans('regproperty.detailed-desc') }}.">{{ old('pkoment') }}</textarea>
                                    </div>

                                    <legend>{{ trans('regproperty.property-images') }}</legend>
                                    <div class="clear"></div>

                                    <div class="form-group">
                                        <label class="form-text">
                                            <span>{{ trans('regproperty.select-image') }}: </span>
                                        </label>
                                        <input type="file" class="form-control-file" name="datafile1"
                                               accept="image/*"/>
                                    </div>


                                    <div class="form-group">
                                        <label class="form-text">
                                            <span>{{ trans('regproperty.select-image') }}: </span>
                                        </label>
                                        <input type="file" name="datafile2" size="40"/>
                                    </div>


                                    <div class="form-group">
                                        <label class="form-text">
                                            <span>{{ trans('regproperty.select-image') }}: </span>
                                        </label>
                                        <input type="file" name="datafile3" size="40"/>
                                    </div>


                                    <div class="form-group">
                                        <label class="form-text">
                                            <span>{{ trans('regproperty.select-image') }}: </span>
                                        </label>
                                        <input type="file" name="datafile4" size="40"/>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-text">
                                            <span>{{ trans('regproperty.select-image') }}: </span>
                                        </label>
                                        <input type="file" name="datafile5" size="40"/>
                                    </div>

                                    <legend>{{ trans('regproperty.personal-data') }}</legend>

                                    <div class="form-group">
                                        <label class="form-text"> <span> {{ trans('regproperty.name') }}: *</span>
                                        </label>
                                        <input type="text" name="emri" id="emri" class="form-control"
                                               value="{{ old('emri') }}"
                                               placeholder="{{ trans('regproperty.name') }}"/>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-text">
                                            <span> {{ trans('regproperty.surname') }}: *</span>
                                        </label>
                                        <input type="text" name="mbiemri" id="mbiemri" class="form-control"
                                               value="{{ old('mbiemri') }}"
                                               placeholder="{{ trans('regproperty.surname') }}"/>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-text"> <span> {{ trans('regproperty.phone') }}: *</span>
                                        </label>
                                        <input type="text" name="telefoni" id="telefoni" class="form-control"
                                               value="{{ old('telefoni') }}"
                                               placeholder="{{ trans('regproperty.phone') }}"/>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-text"> <span> {{ trans('regproperty.email') }}: *</span>
                                        </label>
                                        <input type="email" name="email" id="email" class="form-control"
                                               value="{{ old('email') }}"
                                               placeholder="{{ trans('regproperty.email') }}"/>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-text"> <span>{{ trans('regproperty.address') }}:</span>
                                        </label>
                                        <input type="text" name="adresa" id="adresa" class="form-control"
                                               value="{{ old('adresa') }}"
                                               placeholder="{{ trans('regproperty.address') }}"/>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-text"> <span> {{ trans('regproperty.zip-code') }}:</span>
                                        </label>
                                        <input type="text" name="zip" id="zip" class="form-control"
                                               value="{{ old('zip') }}"
                                               placeholder="{{ trans('regproperty.zip-code') }}"/>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-text"> <span> {{ trans('regproperty.city2') }}:</span>
                                        </label>
                                        <input type="text" name="city" id="city" class="form-control"
                                               placeholder="{{ trans('regproperty.city2') }}"/>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-text"> <span>{{ trans('regproperty.state') }}:</span>
                                        </label>
                                        <input type="text" name="shteti" id="shteti" class="form-control"
                                               value="{{ old('shteti') }}"
                                               placeholder="{{ trans('regproperty.state') }}"/>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-text">
                                            <span>{{ trans('regproperty.your-comment') }}:</span> </label>
                                        <textarea class="form-control" name="yourcomment"
                                                  id="yourcomment"
                                                  placeholder="{{ trans('regproperty.your-comment') }}">{{ old('yourcomment') }}</textarea>
                                    </div>

                                    <div class="register">
                                        <p>{{ trans('regproperty.notice-lbl') }}</p>
                                        <h1 class="m-0">{{ trans('regproperty.notice') }}</h1>
                                    </div>

                                    <br/>
                                    <br/>
                                    <br/>
                                    <label>{{ trans('regproperty.captcha') }}</label>
                                    <h4>{{$firstNr}} + {{$secondNr}} = <input type="number"
                                                                              name="captcha_input"/></h4>
                                    <input type="hidden" name="captcha_result" value="{{ $firstNr + $secondNr }}"/>
                                    <br/>

                                    <div class="form-group" style="font-size: 16px">
                                        <input type="radio" value="Jam dakort"
                                               name="terms"/>&nbsp;{{ trans('regproperty.agree') }}&nbsp;&nbsp;&nbsp;
                                        <input type="radio" value="Nuk jam dakort" name="terms"/>&nbsp;{{ trans('regproperty.not-agree')
                                    }}
                                    </div>

                                    <input type="submit" value="{{ trans('regproperty.send') }}" id="button"/>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                @include ('partials.sidebar')
            </div>
        </div>
    </div>

    <div id="pre-footer" class="bottomPart">
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <ol class="breadcrumb">
                        <li><a href="/{{ Lang::getLocale() }}">{{trans('messages.home')}}</a></li>
                        @if (Lang::getLocale() == "en")
                            <li class="active"><a
                                        href="/{{Lang::getLocale()}}/register-property">{{ trans('regproperty.regproph3') }}</a>
                            </li>
                        @endif @if (Lang::getLocale() == "sq")
                            <li class="active"><a
                                        href="/{{Lang::getLocale()}}/regjistro-pronen">{{ trans('regproperty.regproph3') }}</a>
                            </li>
                        @endif
                    </ol>
                </div>
            </div>

        </div>
    </div>
@endsection
