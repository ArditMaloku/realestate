<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
	<url><loc>https://realestate.al/</loc><changefreq>daily</changefreq><priority>1.0</priority></url>
    <url>
        <loc>https://realestate.al/en</loc><changefreq>daily</changefreq><priority>1.0</priority></url>
    <url>
        <loc>https://realestate.al/sq</loc><changefreq>daily</changefreq><priority>1.0</priority></url>
    <url>
        <loc>https://realestate.al/contact</loc><changefreq>daily</changefreq><priority>1.0</priority></url>
	<?php $propertyTypes = array("Apartment", "Villa", "Land", "Office", "Warehouse", "Commercial");?>
	@foreach ($propertyTypes as $propertyType)
		@if($propertyType=="Land")
		<url><loc>{{route('category-cities', ["en",strtolower($propertyType),"for","sale","in","Tirana"])}}</loc><changefreq>daily</changefreq><priority>1.0</priority></url>					
		@else
			<url><loc>{{route('category-cities', ["en",strtolower($propertyType),"for","rent","in","Tirana"])}}</loc><changefreq>daily</changefreq><priority>1.0</priority></url>
		@endif
	@endforeach
	<?php
	$propertyTypes = array("Apartament", "Vile", "Toke", "Zyre", "Magazine", "Ambient tregtar");
	$propertyTypesMessages = array("Apartment", "Villa", "Land", "Office", "Warehouse", "Commercial");
	?>
	@foreach ($propertyTypes as $keyProperty=>$propertyType)
		@if($propertyType=="Toke")
			<url><loc>{{route('category-cities', ["sq",strtolower($propertyType),"ne","shitje","ne","Tirane"])}}</loc><changefreq>daily</changefreq><priority>1.0</priority></url>
		@else
			<url><loc>{{route('category-cities', ["sq",strtolower($propertyType),"me","qera","ne","Tirane"])}}</loc><changefreq>daily</changefreq><priority>1.0</priority></url>
		@endif
	@endforeach
	@foreach ($subCats as $cats)
		<url><loc>{{$cats}}</loc><changefreq>daily</changefreq><priority>0.9</priority></url>
	@endforeach
	
	<?php $step=0;?>
	@foreach($properties as $prona)
    <url>
	 @if($prona->lastupdate!="")
		 <lastmod>{{date('c', strtotime($prona->lastupdate))}}</lastmod>
	 @endif
	 @if($step<=$nrArticles)
		 <changefreq>daily</changefreq><priority>0.8</priority>
	 @else
		 <changefreq>daily</changefreq><priority>0.6</priority>
	 @endif
        <loc>https://www.realestate.al/en/{{Str::slug($prona->title)}}.{{$prona->id}}</loc>
		
	</url>
	<url>
	 @if($prona->lastupdate!="")
		 <lastmod>{{date('c', strtotime($prona->lastupdate))}}</lastmod>
	 @endif
	 @if($step<=$nrArticles)
		 <changefreq>daily</changefreq><priority>0.8</priority>		
	 @else
		 <changefreq>daily</changefreq><priority>0.6</priority>
	 @endif
		<loc>https://www.realestate.al/sq/{{Str::slug($prona->titull)}}.{{$prona->id}}</loc>
	</url>
	<?php $step++;?>
	@endforeach	
</urlset>
