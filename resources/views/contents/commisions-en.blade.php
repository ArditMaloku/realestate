@extends('layouts.main')
@section('title', trans('messages.commissions-title'))
@section('description', trans('messages.commissions-description'))

@push('hreflang')
    @if (Lang::getLocale() == "en" )
        <link rel="alternate" hreflang="sq" href="/sq/{{$alternatelink}}"/>
    @endif
    @if (Lang::getLocale() == "sq" )
        <link rel="alternate" hreflang="en" href="/en/{{$alternatelink}}"/>
    @endif
    <link rel="canonical" href="https://www.realestate/{{Lang::getLocale()}}/{{$currentlink}}"/>
@endpush

@push('language-switcher')
    @if (Lang::getLocale() == "en" )
        <li>
            <a href="/sq/{{$alternatelink}}">
                <img class="flag" src="{{ asset('/img/flags/Albania.png') }}" title="Albania.png"
                     alt="Albania.png"/>
            </a>
        </li>
    @endif

    @if (Lang::getLocale() == "sq" )
        <li>
            <a href="/en/{{$alternatelink}}">
                <img class="flag" src="{{ asset('/img/flags/UK.png') }}" title="UK.png" alt="UK.png"/>
            </a>
        </li>
    @endif

    <style>
        #google_translate_element {
            position: absolute;
            background: #fff;
            padding: 20px;
        }
    </style>

<!--     @include('partials.google-trans') -->
@endpush

@section('content')
    <!-- Content -->
    <div id="content" style="padding-top: 45px">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-push-2">
                    <div class="about">
                        <div class="tab-content">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="about-title">
                                        <h1>{{trans('commisions.title')}}</h1>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="about-text">

                                        <h5><strong>Welcome to our site!</strong></h5><br/>

                                        <p>
                                            Our Agents have done their best to have a large variety of properties and we
                                            hope that you will find those who match your request.
                                            Before advancing further is our legal duty to notify you on the commissions
                                            (payments) that we apply.
                                            Based on the Albanian law, commercial and consolidated practices of real
                                            estate
                                            agencies worldwide we, in the capacity of a legal person apply the following
                                            fees.
                                        </p>

                                        <p>
                                            Services offered free of charge to the parties:
                                            Legal Advice to the interested parties in the economical relationship.
                                            Economical advice and property appraisal.
                                            Estimate of the market value in a predetermined area.
                                            Drafting or legal advice during the preparation of the respective contracts.
                                            Addition of properties associated with pictures in our internet system.
                                            Accompaniment to visit the property selected by customers within the scope
                                            provided by our field
                                            the activity.
                                        </p>

                                        <p>
                                            Paid service.<br/>
                                            Those services that we apply a payment are called commissions. Commissions
                                            apply
                                            only to the parties in a commercial relationship which is successful with
                                            the
                                            mediation of our Agency staff.
                                        </p><br/>


                                        <h5><strong>Sales</strong></h5><br/>
                                        <p>
                                            Commission to seller of a property is 3% of the total value of the agreed
                                            property price. The commission is independent from the purchase with full
                                            payment or installments payments. The Commission is due to be paid to the
                                            agency
                                            at the time of purchase or the time of the guarantee payment on the agreed
                                            purchase by the seller and buyer.
                                        </p>

                                        <p>
                                            Commission to the party that purchases a property which has been introduced
                                            and
                                            / or accompanied to see the property in question by one or more staff
                                            members,
                                            is 1% of the total value of the agreed property price The Commission is due
                                            to
                                            be paid to the agency at the time of purchase or the time of the guarantee
                                            payment on the agreed purchase by the seller and buyer.
                                        </p><br/>


                                        <h5><strong>Rentals</strong></h5><br/>


                                        <p>
                                            Commission on the party that gives a property in lease is 1/12 of the total
                                            value of the lease in the first year .This commission applies only to the
                                            first
                                            year of the contract or less .This commission does not apply in the second
                                            year
                                            and ongoing or contract renewals.
                                        </p>

                                        <p>
                                            Commission to the party which leases a property is 1/24 of the total value
                                            of
                                            the lease in the first year. This commission applies to rental contracts
                                            from 1
                                            months to 12 months .This commission does not apply in the second year
                                            following
                                            the contract or renovations possible after the first year.
                                        </p>

                                        <p>
                                            In the case of lease for a term of less than 1 month Agency applies a fixed
                                            commission to the Lessee of 7000 ALL.
                                        </p>

                                        <p>
                                            Confidentiality of trade relationship is one of the basic principles of
                                            business
                                            in which we operate, one of the points which we take pride and also is
                                            guaranteed by the legislation in force in the Republic of Albania.
                                        </p>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                @include ('partials.sidebar')
            </div>

        </div>
    </div>


    <div id="pre-footer" class="bottomPart">
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <ol class="breadcrumb">
                        <li><a href="/{{ Lang::getLocale() }}">{{trans('messages.home')}}</a></li>
                        <li class="active"><a href="/{{Lang::getLocale()}}/{{$currentlink}}">{{trans('commisions.title')}}</a>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection
