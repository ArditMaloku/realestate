var $ = jQuery;
$(document).ready(function () {

    if ($('.additional-images').length === 0) {
        return;
    }

    $('.additional-images').slick({
        dots: false,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 4,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            }
        ],
        speed: 300,
        slidesToShow: 6,
        slidesToScroll: 3,
        pauseOnFocus: true,
        infinite: false
    });

    //Ndryshimi i imazhint kryesor ne carousel
    var _currentImage = 0;
    var additionalImages = $("#additional-images img.add-image").toArray();
    var magnificPopup = $.magnificPopup.instance;

    $('.add-image').click(function () {
        var imageUrl = $(this).attr('src');
        _currentImage = $(this).attr("data-index");

        console.log('set index ' + _currentImage);

        magnificPopup.index = _currentImage;

        $('#main-image').fadeOut('fast', function () {
            $('#main-image').css({'background': 'url(' + imageUrl.replace(/(?=[() ])/g, '\\') + ')'});
            $("#anchor-main").attr("href", imageUrl);
            $('#main-image').fadeIn('fast');
        });

        if (_currentImage < additionalImages.length - 1) {
            $("#next-div").css({'display': 'block'});
        } else {
            $("#next-div").css({'display': 'none'});
        }
        if (_currentImage > 0) {
            $("#previous-div").show(30);
        }
    });

    var mainImg = "<img src='" + $('#main-image').css('background').replace('url(', '').replace(')', '').replace(/\"/gi, "").replace(/(?=[() ])/g, '\\') + "'/>";
    additionalImages.unshift(mainImg);

    $("#previous-div").click(function () {
        if (_currentImage > 0) {
            $('#main-image').fadeOut('fast', function () {
                --_currentImage;

                $('#main-image').css({'background': 'url(' + $(additionalImages[_currentImage]).attr('src').replace(/(?=[() ])/g, '\\') + ')'});
                $('#main-image').fadeIn('fast');
                $(".additional-images #slickItem_" + (_currentImage)).focus();
                $('.additional-images').slick('slickGoTo', _currentImage - 1);
                $("#anchor-main").attr("href", $(additionalImages[_currentImage]).attr('src'));

                if (_currentImage < additionalImages.length) {
                    $("#next-div").css({'display': 'block'});
                }
                if (_currentImage == 0) {
                    $("#previous-div").hide(30);
                }
            });
        }
    });

    $("#next-div").click(function () {

        if (_currentImage < additionalImages.length - 1) {
            ++_currentImage;

            $('#main-image').fadeOut('fast', function () {
                $('#main-image').css({'background': 'url(' + $(additionalImages[_currentImage]).attr('src').replace(/(?=[() ])/g, '\\') + ')'});
                $('#main-image').fadeIn('fast');
                $(".additional-images #slickItem_" + (_currentImage)).focus();
                $('.additional-images').slick('slickGoTo', _currentImage - 1);
                $("#anchor-main").attr("href", $(additionalImages[_currentImage]).attr('src'))
            });

            if (_currentImage == additionalImages.length - 1) {
                $("#next-div").css({'display': 'none'});
            }

            if (_currentImage > 0) {
                $("#previous-div").show(30);
            }
        }
    });


    $('.anchor-main').on('mfpOpen', function (e) {

        var magnificPopup = $.magnificPopup.instance;

        $(".mfp-container").swipe({
            swipeLeft: function (event, direction, distance, duration, fingerCount) {
                magnificPopup.next();
            },

            swipeRight: function (event, direction, distance, duration, fingerCount) {
                magnificPopup.prev();
            },
        });
    });
});