var $ = jQuery;
$(document).ready(function () {

    if (window.popupImages && window.popupImages.length) {
        $('.anchor-main').magnificPopup({
            items: window.popupImages,
            gallery: {
                enabled: true,
                preload: [0, 2],
                navigateByImgClick: false,
                arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>',
                tPrev: 'Previous', // title for left button
                tNext: 'Next', // title for right button
                tCounter: '<span class="mfp-counter">%curr% of %total% images</span>' //counter
            },
            type: 'image',
            image: {
                tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                titleSrc: function (item) {
                    // console.log(item);
                    return '';
                }
            },
            callbacks: {
                beforeOpen: function () {
                    var magnificPopup = $.magnificPopup.instance;
                    for (i = 0; i < magnificPopup.items.length; ++i) {

                        if (magnificPopup.items[i].src === $('#anchor-main').attr("href")) {
                            magnificPopup.index = i;
                        }
                    }

                }
            },
            removalDelay: 300,
            mainClass: 'mfp-fade',
            // swipe: true,
            // swipeToSlide: true,
            // touchMove: true
        });

        $('#p-main-img').swipe({
            swipe: function (event, direction, distance, duration, fingerCount, fingerData) {

                if (direction === "left") {
                    document.getElementById('next-anchor').click();
                }
                if (direction === "right") {
                    document.getElementById('previous-anchor').click();
                }
            }
        });

        $('#previous-anchor').click(function () {
            $('.anchor-main').magnificPopup('prev');
        });
        $('#next-anchor').click(function () {
            $('.anchor-main').magnificPopup('next');
        });
    }
});