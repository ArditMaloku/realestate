var $ = jQuery;
$(document).ready(function () {

    // Scroll top button.
    $('.scroltop').on('click', function (event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });

    // Tooltip
    // $('[rel="tooltip"]').tooltip();

    setTimeout(function () {
        $(".gsc-input").attr("placeholder", "");
        $('.gsc-input').css('background', 'transparent');
    }, 1000);

    $('#detailedSearch').click(function () {

        if ($('#state').val() == 'closed') {
            var propertyType = $('select[name=propertyType]').val();
            var saleType = $('select[name=saleStatus]').val();

            changeAdvancedDetails(false, propertyType);

            updateSearchInputs(saleType, propertyType);

        } else {
            changeAdvancedDetails(true);
        }
    });

    $('#detailedSearchC').click(function () {
        if ($('#state').val() == 'closed') {
            var propertyType = $('select[name=propertyType]').val();
            changeAdvancedDetails(false, propertyType);
        } else {
            changeAdvancedDetails(true);
        }
    });

    $("select[name=saleStatus]").on('change', function () {

        var saleType = $(this).val();
        var propertyType = $('select[name=propertyType]').val();

        updateSearchInputs(saleType, propertyType);
    });

    $("#property-type").on('change', function () {

        var propertyType = $(this).val();
        var saleType = $('select[name=saleStatus]').val();

        updateSearchInputs(saleType, propertyType);

        if ($('#state').val() === 'closed') return;

        // We close all advanced details.
        changeAdvancedDetails(true);

        // Then open the specific details.
        changeAdvancedDetails(false, propertyType);
    });
});

function changeAdvancedDetails(close, propertyType = null) {
    if (close) {
        $('.detailedTab').addClass("hidden");
        $('#state').val('closed');

        $('#simpleSearch').addClass("col-sm-12");
        $('#simpleSearch').removeClass("col-sm-6");
        $('#chevron').removeClass("fa-chevron-left");
        $('#chevron').addClass("fa-chevron-right");
        $('#quick-search').addClass("col-md-4");
        $('#quick-search').removeClass("col-md-7");
    } else {
        $('.detail-' + propertyType).removeClass("hidden");
        $('#state').val('opened');

        $('#quick-search').removeClass("col-md-4");
        $('#quick-search').addClass("col-md-7");
        $('#simpleSearch').removeClass("col-sm-12");
        $('#simpleSearch').addClass("col-sm-6");
        $('#chevron').removeClass("fa-chevron-right");
        $('#chevron').addClass("fa-chevron-left");
    }
}

window.changeAdvancedDetails = changeAdvancedDetails;

/**
 * Update form search inputs.
 *
 * @param saleType
 * @param propertyType
 * @param value1
 * @param value2
 */
function updateSearchInputs(saleType, propertyType, value1 = null, value2 = null) {

    if (saleType === 'rent' && propertyType === "apartment") {
        $('select[name=cmimi]').html('<option selected="selected" value="undefined">-</option><option value="0-300">0-300 &euro;</option><option value="300-500">300-500 &euro;</option><option value="500-1000">500-1000 &euro;</option>' + $('#morehidden').html());

        setSelectValue('cmimi', value1);
        setSelectValue('organizimi', value2);

    } else if (saleType === 'sale' && propertyType === "apartment") {
        $('select[name=cmimi]').html('<option selected="selected" value="undefined">-</option><option value="0-50.000">0-50.000 &euro;</option><option value="50.000-100.000">50.000-100.000 &euro;</option><option value="100.000-200.000">100.000-200.000 &euro;</option>' + $('#morehidden').html());

        setSelectValue('cmimi', value1);
        setSelectValue('organizimi', value2);

    } else if (saleType === 'rent' && propertyType === "villa") {
        $('select[name=cmimi-villa]').html('<option selected="selected" value="undefined">-</option><option value="0-1000">0-1000 &euro;</option><option value="1000-2000">1000-2000 &euro;</option><option value="2000-3000">2000-3000 &euro;</option>' + $('#morehidden').html());

        setSelectValue('cmimi-villa', value1);
        setSelectValue('shperndarja-villa', value2);

    } else if (saleType === 'sale' && propertyType === "villa") {
        $('select[name=cmimi-villa]').html('<option selected="selected" value="undefined">-</option><option value="0-100.000">0-100.000 &euro;</option><option value="100.000-300.000">100.000-300.000 &euro;</option><option value="300.000-500.000">300.000-500.000 &euro;</option>' + $('#morehidden').html());

        setSelectValue('cmimi-villa', value1);
        setSelectValue('shperndarja-villa', value2);

    } else if (saleType === 'rent' && propertyType === "office") {
        $('select[name=cmimiZyra]').html('<option selected="selected" value="undefined">-</option><option value="0-300">0-300 &euro;</option><option value="300-500">300-500 &euro;</option><option value="500-1000">500-1000 &euro;</option><option value="1000-2000">1000-2000 &euro;</option>' + $('#morehidden').html());

        setSelectValue('cmimiZyra', value1);
        setSelectValue('siperfaqeZyra', value2);

    } else if (saleType === 'sale' && propertyType === "office") {
        $('select[name=cmimiZyra]').html('<option selected="selected" value="undefined">-</option><option value="0-100.000">0-100.000 &euro;</option><option value="100.000-150.000">100.000-150.000 &euro;</option><option value="150.000-200.000">150.000-200.000 &euro;</option>' + $('#morehidden').html());

        setSelectValue('cmimiZyra', value1);
        setSelectValue('siperfaqeZyra', value2);

    } else if (saleType === 'rent' && propertyType === "warehouse") {
        $('select[name=cmimi-mag]').html('<option selected="selected" value="undefined">-</option><option value="0-500">0-500 &euro;</option><option value="500-1000">500-1000 &euro;</option><option value="1000-2000">1000-2000 &euro;</option>' + $('#morehidden').html());

        setSelectValue('cmimi-mag', value1);
        setSelectValue('siperfaqe-mag', value2);

    } else if (saleType === 'sale' && propertyType === "warehouse") {
        $('select[name=cmimi-mag]').html($('#alloptionhidden').html());

        setSelectValue('cmimi-mag', value1);
        setSelectValue('siperfaqe-mag', value2);

    } else if (saleType === 'rent' && propertyType === "land") {
        $('select[name=cmimi-toka]').html($('#alloptionhidden').html());

        setSelectValue('cmimi-toka', value1);
        setSelectValue('siperfaqe-toka', value2);

    } else if (saleType === 'sale' && propertyType === "land") {
        $('select[name=cmimi-toka]').html('<option selected="selected" value="undefined">-</option><option value="0-50.000">0-50.000 &euro;</option><option value="50.000-100.000">50.000-100.000 &euro;</option>' + $('#morehidden').html());

        setSelectValue('cmimi-toka', value1);
        setSelectValue('siperfaqe-toka', value2);

    } else if (saleType === 'rent' && propertyType === "commercial") {
        $('select[name=cmimi-store]').html('<option selected="selected" value="undefined">-</option><option value="0-500">0-500 &euro;</option><option value="500-1000">500-1000 &euro;</option><option value="1000-2000">1000-2000 &euro;</option>' + $('#morehidden').html());

        setSelectValue('cmimi-store', value1);
        setSelectValue('siperfaqe-store', value2);

    } else if (saleType === 'sale' && propertyType === "commercial") {
        $('select[name=cmimi-store]').html('<option selected="selected" value="undefined">-</option><option value="0-100.000">0-100.000 &euro;</option><option value="100.000-200.000">100.000-200.000 &euro;</option><option value="200.000-300.000">200.000-300.000 &euro;</option>' + $('#morehidden').html());

        setSelectValue('cmimi-store', value1);
        setSelectValue('siperfaqe-store', value2);
    }
}

window.updateSearchInputs = updateSearchInputs;


/**
 * Set selection value.
 *
 * @param name
 * @param value
 */
function setSelectValue(name, value) {
    if (value !== null) {
        $('select[name=' + name + ']').val(value);
    }
}

/*
$(document).ready(function () {
    var showedTranslator = false;


    $('#googletranslateclick').click(function () {
        if (!showedTranslator) {
            showedTranslator = true;
            $('#google_translate_element').css('display', 'block');
        } else {
            $('#google_translate_element').css('display', 'none');
            showedTranslator = false;
        }
    });


});
*/