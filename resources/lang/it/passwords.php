<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Le password devono essere di almeno sei caratteri e abbinare la conferma.',
    'reset' => 'La tua password � stata resettata!',
    'sent' => 'Abbiamo mandato il tuo link di reimpostazione password via email!',
    'token' => 'Questo strumento di reimpostazione della password non � valida.',
    'user' => "Non possiamo trovare un utente con questo indirizzo e-mail.",

];
