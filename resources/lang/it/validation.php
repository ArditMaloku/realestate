<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

  
    'accepted'             => 'L\' :attribute deve essere accettato.',
    'active_url'           => 'L\'  :attribute non � un URL valido.',
    'after'                => 'L\'  :attribute deve essere una data dopo: Data.',
    'alpha'                => 'L\' :attribute pu� contenere solo lettere.',
    'alpha_dash'           => 'L\' :attribute pu� contenere solo lettere, numeri e trattini.',
    'alpha_num'            => 'L\' :attribute pu� contenere solo lettere e numeri.',
    'array'                => 'L\' :attribute deve essere un array.',
    'before'               => 'L\' :attribute deve essere una data prima: Data.',
    'between'              => [
        'numeric' => 'L\' :attribute deve essere compresa tra: min e max',
        'file'    => 'L\' :attribute deve essere compresa tra: min e:max kilobyte.', 
        'string'  => 'L\' :attribute deve essere compresa tra: min e:max caratteri.', 
        'array'   => 'L\' :attribute deve avere tra: min e:max articoli.', 
    ],
    'boolean'              => 'Il: campo attributo deve essere vero o falso.',
    'confirmed'            => 'La: conferma attributo non corrisponde.',
    'date'                 => 'L\' :attribute non � una data valida.',
    'date_format'          => 'L\' :attribute non corrisponde al formato: Formato.',
    'different'            => 'L\': attributi e: altri deve essere diverso.',
    'digits'               => 'L\' :attribute deve essere: cifre cifre.',
    'digits_between'       => 'L\' :attribute deve essere compresa tra: min e: max cifre.',
    'dimensions'           => 'L\' :attribute ha dimensioni delL\'immagine non validi.',
    'distinct'             => 'L\': campo attributo ha un valore duplicato.',
    'email'                => 'L\' :attribute deve essere un indirizzo email valido.',
    'exists'               => 'Il selezionato :attribute non � valido.',
    'file'                 => 'L\' :attribute deve essere un file.',
    'filled'               => 'Il: campo attributo � obbligatorio.',
    'image'                => 'L\' :attribute deve essere un\'immagine.',
    'in'                   => 'Il selezionato :attribute non � valido.',
    'in_array'             => 'Il: campo attributo non esiste in: altri.',
    'integer'              => 'L\' :attribute deve essere un numero intero.',
    'ip'                   => 'L\' :attribute deve essere un indirizzo IP valido.',
    'json'                 => 'L\' :attribute deve essere una stringa valida JSON.',
    'max'                  => [
        'numeric' => 'L\' :attribute pu� non essere superiore: max.',
        'file'    => 'L\' :attribute pu� non essere superiore a:max kilobyte.',
        'string'  => 'L\' :attribute pu� non essere superiore: caratteri max.',
        'array'   => 'L\' :attribute non pu� avere pi� di:max articoli .',
    ],
    'mimes'                => 'L\' :attribute deve essere un file di tipo:: valori.',
    'mimetypes'            => 'L\' :attribute deve essere un file di tipo:: valori.',
    'min'                  => [
        'numeric' => 'L\' :attribute deve essere almeno :min.',
        'file'    => 'L\' :attribute deve essere almeno :min kilobyte.',
        'string'  => 'L\' :attribute deve essere almeno :min caratteris.',
        'array'   => 'L\' :attribute deve avere almeno: min articoli.',
    ],
    'not_in'               => 'Il selezionato :attribute non � valido.',
    'numeric'              => 'L\' :attribute deve essere un numero.',
    'present'              => 'Il: campo attributo deve essere presente.',
    'regex'                => 'Il: formato delL\'attributo non � valido.',
    'required'             => 'L\': campo attributo � obbligatorio.',
    'required_if'          => 'Il: campo attributo � necessaria quando: altro �: valore.',
    'required_unless'      => 'Il: campo attributo � obbligatorio se non: altro � in: valori.',
    'required_with'        => 'Il: campo attributo � necessaria quando: valori � presente.',
    'required_with_all'    => 'Il: campo attributo � necessaria quando: valori � presente.',
    'required_without'     => 'Il: campo attributo � necessaria quando: valori non � presente.',
    'required_without_all' => 'Il: campo attributo � richiesto quando nessuno di: valori sono presenti.',
    'same'                 => 'L\': attributi e: altri deve corrispondere.',
    'size'                 => [
        'numeric' => 'L\' :attribute deve essere: dimensioni.',
        'file'    => 'L\' :attribute deve essere: dimensioni.',
        'string'  => 'L\' :attribute deve essere: caratteri di dimensione.',
        'array'   => 'L\' :attribute deve contenere: articoli di formato.',
    ],
    'string'               => 'L\' :attribute deve essere una stringa.',
    'timezone'             => 'L\' :attribute deve essere un orario valido.',
    'unique'               => 'L\' :attribute � gi� stata presa.',
    'uploaded'             => 'L\' :attribute non � riuscito a caricare.',
    'url'                  => 'Il: formato del l\'attributo non � valido.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
