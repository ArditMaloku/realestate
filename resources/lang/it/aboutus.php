<?php
return [ 
		'description1' => 'Here at Albania Real Estate, our main goal is to make it easier for everyone to buy or sell their property. We use the internet to bring the choices to you. Whether you reside in Albania or not you can view our full list of properties online, with detailed pictures and information. Our properties vary from very large land plots in the Albanian Riviera (40 to 300+ Ha) destined for tourism development or commercial sites, to villas, apartments or smaller land plots, for rent or sale in almost all areas of Albania.',
		'description2' => 'After you\'ve done your research on our
										site please contact us and let us know what you are
										considering. One of our experienced agents will inform you
										more on your decision and maybe offer a few more ideas that
										may benefit you. "An informed customer is a happy customer"
										and happy customers are who we\'re after...' 
]
;