<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Fjalekalimi duhet te jete minimalisht 6 karaktere.',
    'reset' => 'Fjalkalimi juaj u ndryshua!',
    'sent' => 'Fjalkalimi i ri ju eshte derguar me mail!',
    'token' => 'Kerkesa juaj per ndryshim fjalkalimin eshte e pavlefshme.',
    'user' => "Nuk ekziston asnje perdorues me kete adrese maili.",

];

