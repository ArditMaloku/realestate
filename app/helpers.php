<?php


if (!function_exists('clean_price')) {
    function clean_price($price)
    {
        $price = strtolower($price);
        $price = str_replace(["euro", "m2", "/", ".", "€", "leke", "muaj", "month"], "", $price);
        $price = preg_replace('/\s+/', "", $price);

        return $price;
    }
}