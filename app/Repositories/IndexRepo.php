<?php

namespace App\Repositories;


use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class IndexRepo extends Repository
{
    /**
     * Get articles.
     *
     * @param string $gjuha
     * @return array
     */
    public function getArticles(string $gjuha)
    {
        $apartamente = Cache::remember('apartamente' . $gjuha, 15, function () use ($gjuha) {
            return DB::select('SELECT
            ArtID as id,
            ArtTitull_' . $gjuha . ' as title,
            case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
            Cmimi AS price,
            -- fullprice as price,
            ArtPermbajtja_' . $gjuha . ' as body,
            Llojiprones_' . $gjuha . ' as type,
            Qyteti_' . $gjuha . ' as city,
            Forsale as sale,
            Forrent as rent,
            Siperfaqia as area,
            koordinata
            from `cms_artikujt`
            WHERE (Llojiprones_' . $gjuha . ' = \'Apartament\' OR Llojiprones_' . $gjuha . ' = \'Apartment\')
            and ifnull(fshih,0)=0
            and koordinata<>""
            and koordinata IS NOT NULL
           ORDER BY ArtRend ASC, created_at DESC, id DESC limit 10');
        });


        $vila = Cache::remember('vila' . $gjuha, 15, function () use ($gjuha) {
            return DB::select('SELECT
				 ArtID as id,
				 ArtTitull_' . $gjuha . ' as title,
				 case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
				 Cmimi AS price,
				 -- fullprice as price,
				 ArtPermbajtja_' . $gjuha . ' as body,
				 Llojiprones_' . $gjuha . ' as type,
				 Qyteti_' . $gjuha . ' as city,
				 Forsale as sale,
				 Forrent as rent,
				 Siperfaqia as area,
				 koordinata
				 from `cms_artikujt`
				 WHERE (Llojiprones_' . $gjuha . ' = \'Vile\' OR Llojiprones_' . $gjuha . ' = \'Villa\')
				 and ifnull(fshih,0)=0
				 and koordinata<>""
				 and koordinata IS NOT NULL
                ORDER BY ArtRend ASC, created_at DESC, id DESC limit 10');
        });

        $toke = Cache::remember('toke' . $gjuha, 15, function () use ($gjuha) {
            return DB::select('SELECT
				 ArtID as id,
				 ArtTitull_' . $gjuha . ' as title,
				 case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
				 Cmimi AS price,
				 -- fullprice as price,
				 ArtPermbajtja_' . $gjuha . ' as body,
				 Llojiprones_' . $gjuha . ' as type,
				 Qyteti_' . $gjuha . ' as city,
				 Forsale as sale,
				 Forrent as rent,
				 Siperfaqia as area,
				 koordinata
				 from `cms_artikujt`
				 WHERE (Llojiprones_' . $gjuha . ' = \'Toke\' OR Llojiprones_' . $gjuha . ' = \'Land\')
				 and ifnull(fshih,0)=0
				 and koordinata<>""
				 and koordinata IS NOT NULL
                ORDER BY ArtRend ASC, created_at DESC, id DESC limit 10');
        });

        $zyre = Cache::remember('zyre' . $gjuha, 15, function () use ($gjuha) {
            return DB::select('SELECT
				 ArtID as id,
				 ArtTitull_' . $gjuha . ' as title,
				 case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
				 Cmimi AS price,
				 -- fullprice as price,
				 ArtPermbajtja_' . $gjuha . ' as body,
				 Llojiprones_' . $gjuha . ' as type,
				 Qyteti_' . $gjuha . ' as city,
				 Forsale as sale,
				 Forrent as rent,
				 Siperfaqia as area,
				 koordinata
				 from `cms_artikujt`
				 WHERE (Llojiprones_' . $gjuha . ' = \'Zyre\' OR Llojiprones_' . $gjuha . ' = \'Office\')
				 and ifnull(fshih,0)=0
				 and koordinata<>""
				 and koordinata IS NOT NULL
                ORDER BY ArtRend ASC, created_at DESC, id DESC limit 10');
        });

        $lokal = Cache::remember('lokal' . $gjuha, 15, function () use ($gjuha) {
            return DB::select('SELECT
				 ArtID as id,
				 ArtTitull_' . $gjuha . ' as title,
				 case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
				 Cmimi AS price,
				 -- fullprice as price,
				 ArtPermbajtja_' . $gjuha . ' as body,
				 Llojiprones_' . $gjuha . ' as type,
				 Qyteti_' . $gjuha . ' as city,
				 Forsale as sale,
				 Forrent as rent,
				 Siperfaqia as area,
				 koordinata
				 from `cms_artikujt`
				 WHERE (Llojiprones_' . $gjuha . ' = \'Lokal\' OR Llojiprones_' . $gjuha . ' = \'Club\')
				 and ifnull(fshih,0)=0
				 and koordinata<>""
				 and koordinata IS NOT NULL
                ORDER BY ArtRend ASC, created_at DESC, id DESC limit 10');
        });

        $dyqan = Cache::remember('dyqan' . $gjuha, 15, function () use ($gjuha) {
            return DB::select('SELECT
				 ArtID as id,
				 ArtTitull_' . $gjuha . ' as title,
				 case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
				 Cmimi AS price,
				 -- fullprice as price,
				 ArtPermbajtja_' . $gjuha . ' as body,
				 Llojiprones_' . $gjuha . ' as type,
				 Qyteti_' . $gjuha . ' as city,
				 Forsale as sale,
				 Forrent as rent,
				 Siperfaqia as area,
				 koordinata
				 from `cms_artikujt`
				 WHERE (Llojiprones_' . $gjuha . ' = \'Dyqan\' OR Llojiprones_' . $gjuha . ' = \'Store\')
				 and ifnull(fshih,0)=0
				 and koordinata<>""
				 and koordinata IS NOT NULL
                ORDER BY ArtRend ASC, created_at DESC, id DESC limit 10');
        });

        $magazine = Cache::remember('magazine' . $gjuha, 15, function () use ($gjuha) {
            return DB::select('SELECT
				 ArtID as id,
				 ArtTitull_' . $gjuha . ' as title,
				 case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
				 Cmimi AS price,
				 -- fullprice as price,
				 ArtPermbajtja_' . $gjuha . ' as body,
				 Llojiprones_' . $gjuha . ' as type,
				 Qyteti_' . $gjuha . ' as city,
				 Forsale as sale,
				 Forrent as rent,
				 Siperfaqia as area,
				 koordinata
				 from `cms_artikujt`
				 WHERE (Llojiprones_' . $gjuha . ' = \'Magazine-Depo\' OR Llojiprones_' . $gjuha . ' = \'Warehouse\')
				 and ifnull(fshih,0)=0
				 and koordinata<>""
				 and koordinata IS NOT NULL
                ORDER BY ArtRend ASC, created_at DESC, id DESC limit 10');
        });

        $hotel = Cache::remember('hotel' . $gjuha, 15, function () use ($gjuha) {
            return DB::select('SELECT
				 ArtID as id,
				 ArtTitull_' . $gjuha . ' as title,
				 case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
				 Cmimi AS price,
				 -- fullprice as price,
				 ArtPermbajtja_' . $gjuha . ' as body,
				 Llojiprones_' . $gjuha . ' as type,
				 Qyteti_' . $gjuha . ' as city,
				 Forsale as sale,
				 Forrent as rent,
				 Siperfaqia as area,
				 koordinata
				 from `cms_artikujt`
				 WHERE (Llojiprones_' . $gjuha . ' = \'Hotel\' OR Llojiprones_' . $gjuha . ' = \'Hotel\')
				 and ifnull(fshih,0)=0
				 and koordinata<>""
				 and koordinata IS NOT NULL
                ORDER BY ArtRend ASC, created_at DESC, id DESC limit 10');
        });

        $biznes = Cache::remember('biznes' . $gjuha, 15, function () use ($gjuha) {
            return DB::select('SELECT
				 ArtID as id,
				 ArtTitull_' . $gjuha . ' as title,
				 case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
				 Cmimi AS price,
				 -- fullprice as price,
				 ArtPermbajtja_' . $gjuha . ' as body,
				 Llojiprones_' . $gjuha . ' as type,
				 Qyteti_' . $gjuha . ' as city,
				 Forsale as sale,
				 Forrent as rent,
				 Siperfaqia as area,
				 koordinata
				 from `cms_artikujt`
				 WHERE (Llojiprones_' . $gjuha . ' = \'Biznes\' OR Llojiprones_' . $gjuha . ' = \'Business\')
				 and ifnull(fshih,0)=0
				 and koordinata<>""
				 and koordinata IS NOT NULL
                ORDER BY ArtRend ASC, created_at DESC, id DESC limit 10');
        });

        $parking = Cache::remember('parking' . $gjuha, 15, function () use ($gjuha) {
            return DB::select('SELECT
				 ArtID as id,
				 ArtTitull_' . $gjuha . ' as title,
				 case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
				 Cmimi AS price,
				 -- fullprice as price,
				 ArtPermbajtja_' . $gjuha . ' as body,
				 Llojiprones_' . $gjuha . ' as type,
				 Qyteti_' . $gjuha . ' as city,
				 Forsale as sale,
				 Forrent as rent,
				 Siperfaqia as area,
				 koordinata
				 from `cms_artikujt`
				 WHERE (Llojiprones_' . $gjuha . ' = \'Parking space\' OR Llojiprones_' . $gjuha . ' = \'Vend Parkimi\')
				 and ifnull(fshih,0)=0
				 and koordinata<>""
				 and koordinata IS NOT NULL
                ORDER BY ArtRend ASC, created_at DESC, id DESC limit 10');
        });


        $sr = array();
        array_push($sr, $apartamente, $vila, $dyqan, $biznes, $toke, $zyre);

        return $sr;
    }
}