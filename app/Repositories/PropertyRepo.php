<?php

namespace App\Repositories;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class PropertyRepo extends Repository
{
    /**
     * Category Apartment listing.
     *
     * @param Request $request
     * @return array
     */
    public function categoryApartment(Request $request)
    {
        $lang = $request->route('lang');
        $gjuha = $lang == 'en' ? 'gj2' : 'gj1';
        $gjuhaSelectLanguage = $lang == 'en' ? 'gj1' : 'gj2';
        $city = ucfirst($request->route('city'));
        $action = $request->route('action');
        $forSale = ($action == "sale" || $action == "shitje") ? 1 : 0;
        $forRent = ($action == "rent" || $action == "qera") ? 1 : 0;

        $propertyType = "Apartment";

		$properties = Cache::remember('category_apartmentFirstColumn' . $propertyType . '_' . $city . '_' . $forSale . '_' . $forRent, 15,
            function () use ($gjuha, $gjuhaSelectLanguage, $propertyType, $city, $forSale, $forRent) {
                return DB::select('SELECT
						ArtID as id,
						ArtTitull_' . $gjuha . ' as title,
						case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
						Cmimi AS price,
						ArtPermbajtja_' . $gjuha . ' as body,
						Tipi as type,
						Qyteti_' . $gjuha . ' as city,
						Qyteti_' . $gjuhaSelectLanguage . ' as citylangswitch,
						Forsale as sale,
						Forrent as rent,
						Siperfaqia as area,
						kodi
						from `cms_artikujt`
						WHERE
						ifnull(fshih,0)=0 AND
						Llojiprones_gj2 = ?
						AND
						Qyteti_' . $gjuha . ' = ?
						AND (Forsale=? || Forrent =?)
						AND deleted_at IS NULL
						ORDER BY created_at DESC, priority DESC, ArtRend ASC, id DESC', [$propertyType, $city, $forSale, $forRent]);
        });
        
        return $properties;

/*
        $propertiesFirstColumn = Cache::remember('category_apartmentFirstColumn' . $propertyType . '_' . $city . '_' . $forSale . '_' . $forRent, 15,
            function () use ($gjuha, $gjuhaSelectLanguage, $propertyType, $city, $forSale, $forRent) {
                return DB::select('SELECT
						ArtID as id,
						ArtTitull_' . $gjuha . ' as title,
						case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
						Cmimi AS price,
						ArtPermbajtja_' . $gjuha . ' as body,
						Tipi as type,
						Qyteti_' . $gjuha . ' as city,
						Qyteti_' . $gjuhaSelectLanguage . ' as citylangswitch,
						Forsale as sale,
						Forrent as rent,
						Siperfaqia as area,
						kodi
						from `cms_artikujt`
						WHERE
						ifnull(fshih,0)=0 AND
						Llojiprones_gj2 = ?
						AND
						Qyteti_' . $gjuha . ' = ?
						AND (Forsale=? || Forrent =?)
						AND dhomaGjumi=1
						AND deleted_at IS NULL
						ORDER BY priority DESC, ArtRend ASC, id DESC', [$propertyType, $city, $forSale, $forRent]);
            });

        $propertiesSecondColumn = Cache::remember('category_apartmentSecondColumn' . $propertyType . '_' . $city . '_' . $forSale . '_' . $forRent, 15,
            function () use ($gjuha, $gjuhaSelectLanguage, $propertyType, $city, $forSale, $forRent) {
                return DB::select('SELECT
						ArtID as id,
						ArtTitull_' . $gjuha . ' as title,
						case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
						Cmimi AS price,
						ArtPermbajtja_' . $gjuha . ' as body,
						Tipi as type,
						Qyteti_' . $gjuha . ' as city,
						Qyteti_' . $gjuhaSelectLanguage . ' as citylangswitch,
						Forsale as sale,
						Forrent as rent,
						Siperfaqia as area,
						kodi
						from `cms_artikujt`
						WHERE
						ifnull(fshih,0)=0 AND
						Llojiprones_gj2 = ?
						AND
						Qyteti_' . $gjuha . ' = ?
						AND (Forsale=? || Forrent =?)
						AND dhomaGjumi=2
						AND deleted_at IS NULL
						ORDER BY priority DESC, ArtRend ASC, created_at DESC, id DESC', [$propertyType, $city, $forSale, $forRent]);
            });

        $propertiesThirdColumn = Cache::remember('category_apartmentThirdColumn' . $propertyType . '_' . $city . '_' . $forSale . '_' . $forRent, 15,
            function () use ($gjuha, $gjuhaSelectLanguage, $propertyType, $city, $forSale, $forRent) {
                return DB::select('SELECT
						ArtID as id,
						ArtTitull_' . $gjuha . ' as title,
						case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
						Cmimi AS price,
						ArtPermbajtja_' . $gjuha . ' as body,
						Tipi as type,
						Qyteti_' . $gjuha . ' as city,
						Qyteti_' . $gjuhaSelectLanguage . ' as citylangswitch,
						Forsale as sale,
						Forrent as rent,
						Siperfaqia as area,
						kodi
						from `cms_artikujt`
						WHERE
						ifnull(fshih,0)=0 AND
						Llojiprones_gj2 = ?
						AND
						Qyteti_' . $gjuha . ' = ?
						AND (Forsale=? || Forrent =?)
						AND dhomaGjumi=3
						AND deleted_at IS NULL
						ORDER BY priority DESC, created_at DESC, id DESC', [$propertyType, $city, $forSale, $forRent]);
            });

        $propertiesFourthColumn = Cache::remember('category_apartmentFourthColumn' . $propertyType . '_' . $city . '_' . $forSale . '_' . $forRent, 15,
            function () use ($gjuha, $gjuhaSelectLanguage, $propertyType, $city, $forSale, $forRent) {
                return DB::select('SELECT
						ArtID as id,
						ArtTitull_' . $gjuha . ' as title,
						case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
						Cmimi AS price,
						ArtPermbajtja_' . $gjuha . ' as body,
						Tipi as type,
						Qyteti_' . $gjuha . ' as city,
						Qyteti_' . $gjuhaSelectLanguage . ' as citylangswitch,
						Forsale as sale,
						Forrent as rent,
						Siperfaqia as area,
						kodi
						from `cms_artikujt`
						WHERE
						ifnull(fshih,0)=0 AND
						Llojiprones_gj2 = ?
						AND
						Qyteti_' . $gjuha . ' = ?
						AND (Forsale=? || Forrent =?)
						AND dhomaGjumi>3
						AND deleted_at IS NULL
						ORDER BY priority DESC, ArtRend ASC, created_at DESC, id DESC', [$propertyType, $city, $forSale, $forRent]);
            });
        $propertiesFifthColumn = '';

        return [$propertiesFirstColumn, $propertiesSecondColumn, $propertiesThirdColumn, $propertiesFourthColumn, $propertiesFifthColumn];
*/
    }

    /**
     * Category Villa listing.
     *
     * @param Request $request
     * @return array
     */
    public function categoryVilla(Request $request)
    {
        $lang = $request->route('lang');
        $gjuha = $lang == 'en' ? 'gj2' : 'gj1';
        $gjuhaSelectLanguage = $lang == 'en' ? 'gj1' : 'gj2';
        $city = ucfirst($request->route('city'));
        $action = $request->route('action');
        $forSale = ($action == "sale" || $action == "shitje") ? 1 : 0;
        $forRent = ($action == "rent" || $action == "qera") ? 1 : 0;
        $propertyType = "Villa";
        
        $properties = Cache::remember('category_villaFirstColumn' . $propertyType . '_' . $city . '_' . $forSale . '_' . $forRent, 15,
            function () use ($gjuha, $gjuhaSelectLanguage, $propertyType, $city, $forSale, $forRent) {
                return DB::select('SELECT
					ArtID as id,
					ArtTitull_' . $gjuha . ' as title,
					case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
					Cmimi AS price,
					ArtPermbajtja_' . $gjuha . ' as body,
					Tipi as type,
					Qyteti_' . $gjuha . ' as city,
					Qyteti_' . $gjuhaSelectLanguage . ' as citylangswitch,
					Forsale as sale,
					Forrent as rent,
					Siperfaqia as area,
					kodi
					from `cms_artikujt`
					WHERE
					ifnull(fshih,0)=0 AND
					Llojiprones_gj2 = ?
					AND
					Qyteti_' . $gjuha . ' = ?
					AND (Forsale=? || Forrent =?)
					AND deleted_at IS NULL
					ORDER BY created_at DESC, priority DESC, ArtRend ASC, created_at DESC, id DESC', [$propertyType, $city, $forSale, $forRent]);
        });
        
        return $properties;
/*
        $propertiesFirstColumn = Cache::remember('category_villaFirstColumn' . $propertyType . '_' . $city . '_' . $forSale . '_' . $forRent, 15,
            function () use ($gjuha, $gjuhaSelectLanguage, $propertyType, $city, $forSale, $forRent) {
                return DB::select('SELECT
					ArtID as id,
					ArtTitull_' . $gjuha . ' as title,
					case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
					Cmimi AS price,
					ArtPermbajtja_' . $gjuha . ' as body,
					Tipi as type,
					Qyteti_' . $gjuha . ' as city,
					Qyteti_' . $gjuhaSelectLanguage . ' as citylangswitch,
					Forsale as sale,
					Forrent as rent,
					Siperfaqia as area,
					kodi
					from `cms_artikujt`
					WHERE
					ifnull(fshih,0)=0 AND
					Llojiprones_gj2 = ?
					AND
					Qyteti_' . $gjuha . ' = ?
					AND (Forsale=? || Forrent =?)
					AND dhomaGjumi=2
					AND deleted_at IS NULL
					ORDER BY priority DESC, ArtRend ASC, created_at DESC, id DESC', [$propertyType, $city, $forSale, $forRent]);
            });
        $propertiesSecondColumn = Cache::remember('category_villaSecondColumn' . $propertyType . '_' . $city . '_' . $forSale . '_' . $forRent, 15,
            function () use ($gjuha, $propertyType, $city, $forSale, $forRent) {
                return DB::select('SELECT
					ArtID as id,
					ArtTitull_' . $gjuha . ' as title,
					case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
					Cmimi AS price,
					ArtPermbajtja_' . $gjuha . ' as body,
					Tipi as type,
					Qyteti_' . $gjuha . ' as city,
					Forsale as sale,
					Forrent as rent,
					Siperfaqia as area,
					kodi
					from `cms_artikujt`
					WHERE
					ifnull(fshih,0)=0 AND
					Llojiprones_gj2 = ?
					AND
					Qyteti_' . $gjuha . ' = ?
					AND (Forsale=? || Forrent =?)
					AND dhomaGjumi=3
					AND deleted_at IS NULL
					ORDER BY priority DESC, ArtRend ASC, created_at DESC, id DESC', [$propertyType, $city, $forSale, $forRent]);
            });
        $propertiesThirdColumn = Cache::remember('category_villaThirdColumn' . $propertyType . '_' . $city . '_' . $forSale . '_' . $forRent, 15,
            function () use ($gjuha, $propertyType, $city, $forSale, $forRent) {
                return DB::select('SELECT
					ArtID as id,
					ArtTitull_' . $gjuha . ' as title,
					case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
					Cmimi AS price,
					ArtPermbajtja_' . $gjuha . ' as body,
					Tipi as type,
					Qyteti_' . $gjuha . ' as city,
					Forsale as sale,
					Forrent as rent,
					Siperfaqia as area,
					kodi
					from `cms_artikujt`
					WHERE
					ifnull(fshih,0)=0 AND
					Llojiprones_gj2 = ?
					AND
					Qyteti_' . $gjuha . ' = ?
					AND (Forsale=? || Forrent =?)
					AND dhomaGjumi=4
					AND deleted_at IS NULL
					ORDER BY priority DESC, ArtRend ASC, created_at DESC, id DESC', [$propertyType, $city, $forSale, $forRent]);
            });
        $propertiesFourthColumn = Cache::remember('category_villaFourthColumn' . $propertyType . '_' . $city . '_' . $forSale . '_' . $forRent, 15,
            function () use ($gjuha, $propertyType, $city, $forSale, $forRent) {
                return DB::select('SELECT
					ArtID as id,
					ArtTitull_' . $gjuha . ' as title,
					case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
					Cmimi AS price,
					ArtPermbajtja_' . $gjuha . ' as body,
					Tipi as type,
					Qyteti_' . $gjuha . ' as city,
					Forsale as sale,
					Forrent as rent,
					Siperfaqia as area,
					kodi
					from `cms_artikujt`
					WHERE
					ifnull(fshih,0)=0 AND
					Llojiprones_gj2 = ?
					AND
					Qyteti_' . $gjuha . ' = ?
					AND (Forsale=? || Forrent =?)
					AND dhomaGjumi>4
					AND deleted_at IS NULL
					ORDER BY priority DESC, ArtRend ASC, created_at DESC, id DESC', [$propertyType, $city, $forSale, $forRent]);
            });
        $propertiesFifthColumn = '';
        return [$propertiesFirstColumn, $propertiesSecondColumn, $propertiesThirdColumn, $propertiesFourthColumn, $propertiesFifthColumn];
*/
    }

    /**
     * Category Villa listing.
     *
     * @param Request $request
     * @return array
     */
    public function categoryOffice(Request $request)
    {
        $lang = $request->route('lang');
        $gjuha = $lang == 'en' ? 'gj2' : 'gj1';
        $gjuhaSelectLanguage = $lang == 'en' ? 'gj1' : 'gj2';
        $city = ucfirst($request->route('city'));
        $action = $request->route('action');
        $forSale = ($action == "sale" || $action == "shitje") ? 1 : 0;
        $forRent = ($action == "rent" || $action == "qera") ? 1 : 0;

        $propertyType = "Office";
        
        $properties = Cache::remember('category_officeFirstColumn' . $propertyType . '_' . $city . '_' . $forSale . '_' . $forRent, 15,
            function () use ($gjuha, $gjuhaSelectLanguage, $propertyType, $city, $forSale, $forRent) {
                return DB::select('SELECT
					ArtID as id,
					ArtTitull_' . $gjuha . ' as title,
					case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
					Cmimi AS price,
					ArtPermbajtja_' . $gjuha . ' as body,
					Tipi as type,
					Qyteti_' . $gjuha . ' as city,
					Qyteti_' . $gjuhaSelectLanguage . ' as citylangswitch,
					Forsale as sale,
					Forrent as rent,
					Siperfaqia as area,
					kodi
					from `cms_artikujt`
					WHERE
					ifnull(fshih,0)=0 AND
					Llojiprones_gj2 = ?
					AND
					Qyteti_' . $gjuha . ' = ?
					AND (Forsale=? || Forrent =?)
					AND deleted_at IS NULL
					ORDER BY created_at DESC, priority DESC, ArtRend ASC, created_at DESC, id DESC', [$propertyType, $city, $forSale, $forRent]);
        });
		
		return $properties;
/*
        $propertiesFirstColumn = Cache::remember('category_officeFirstColumn' . $propertyType . '_' . $city . '_' . $forSale . '_' . $forRent, 15,
            function () use ($gjuha, $gjuhaSelectLanguage, $propertyType, $city, $forSale, $forRent) {
                return DB::select('SELECT
					ArtID as id,
					ArtTitull_' . $gjuha . ' as title,
					case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
					Cmimi AS price,
					ArtPermbajtja_' . $gjuha . ' as body,
					Tipi as type,
					Qyteti_' . $gjuha . ' as city,
					Qyteti_' . $gjuhaSelectLanguage . ' as citylangswitch,
					Forsale as sale,
					Forrent as rent,
					Siperfaqia as area,
					kodi
					from `cms_artikujt`
					WHERE
					ifnull(fshih,0)=0 AND
					Llojiprones_gj2 = ?
					AND
					Qyteti_' . $gjuha . ' = ?
					AND (Forsale=? || Forrent =?)
					AND Siperfaqia BETWEEN 0 and 50
					AND deleted_at IS NULL
					ORDER BY priority DESC, ArtRend ASC, created_at DESC, id DESC', [$propertyType, $city, $forSale, $forRent]);
            });

        $propertiesSecondColumn = Cache::remember('category_officeSecondColumn' . $propertyType . '_' . $city . '_' . $forSale . '_' . $forRent, 15,
            function () use ($gjuha, $propertyType, $city, $forSale, $forRent) {
                return DB::select('SELECT
						ArtID as id,
						ArtTitull_' . $gjuha . ' as title,
						case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
						Cmimi AS price,
						ArtPermbajtja_' . $gjuha . ' as body,
						Tipi as type,
						Qyteti_' . $gjuha . ' as city,
						Forsale as sale,
						Forrent as rent,
						Siperfaqia as area,
						kodi
						from `cms_artikujt`
						WHERE
						ifnull(fshih,0)=0 AND
						Llojiprones_gj2 = ?
						AND
						Qyteti_' . $gjuha . ' = ?
						AND (Forsale=? || Forrent =?)
						AND Siperfaqia BETWEEN 50 and 100
						AND deleted_at IS NULL
						ORDER BY priority DESC, ArtRend ASC, created_at DESC, id DESC', [$propertyType, $city, $forSale, $forRent]);
            });
        $propertiesThirdColumn = Cache::remember('category_officeThirdColumn' . $propertyType . '_' . $city . '_' . $forSale . '_' . $forRent, 15,
            function () use ($gjuha, $propertyType, $city, $forSale, $forRent) {
                return DB::select('SELECT
						ArtID as id,
						ArtTitull_' . $gjuha . ' as title,
						case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
						Cmimi AS price,
						ArtPermbajtja_' . $gjuha . ' as body,
						Tipi as type,
						Qyteti_' . $gjuha . ' as city,
						Forsale as sale,
						Forrent as rent,
						Siperfaqia as area,
						kodi
						from `cms_artikujt`
						WHERE
						ifnull(fshih,0)=0 AND
						Llojiprones_gj2 = ?
						AND
						Qyteti_' . $gjuha . ' = ?
						AND (Forsale=? || Forrent =?)
						AND Siperfaqia BETWEEN 100 and 200
						AND deleted_at IS NULL
						ORDER BY priority DESC, ArtRend ASC, created_at DESC, id DESC', [$propertyType, $city, $forSale, $forRent]);
            });
        $propertiesFourthColumn = Cache::remember('category_officeFourthColumn' . $propertyType . '_' . $city . '_' . $forSale . '_' . $forRent, 15,
            function () use ($gjuha, $propertyType, $city, $forSale, $forRent) {
                return DB::select('SELECT
						ArtID as id,
						ArtTitull_' . $gjuha . ' as title,
						case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
						Cmimi AS price,
						ArtPermbajtja_' . $gjuha . ' as body,
						Tipi as type,
						Qyteti_' . $gjuha . ' as city,
						Forsale as sale,
						Forrent as rent,
						Siperfaqia as area,
						kodi
						from `cms_artikujt`
						WHERE
						ifnull(fshih,0)=0 AND
						Llojiprones_gj2 = ?
						AND
						Qyteti_' . $gjuha . ' = ?
						AND (Forsale=? || Forrent =?)
						AND Siperfaqia>200
						AND deleted_at IS NULL
						ORDER BY priority DESC, ArtRend ASC, created_at DESC, id DESC', [$propertyType, $city, $forSale, $forRent]);
            });
        $propertiesFifthColumn = '';

        return [$propertiesFirstColumn, $propertiesSecondColumn, $propertiesThirdColumn, $propertiesFourthColumn, $propertiesFifthColumn];
*/
    }

    /**
     * Category Warehouse listing.
     *
     * @param Request $request
     * @return array
     */
    public function categoryWarehouse(Request $request)
    {
        $lang = $request->route('lang');
        $gjuha = $lang == 'en' ? 'gj2' : 'gj1';
        $gjuhaSelectLanguage = $lang == 'en' ? 'gj1' : 'gj2';
        $city = ucfirst($request->route('city'));
        $action = $request->route('action');
        $forSale = ($action == "sale" || $action == "shitje") ? 1 : 0;
        $forRent = ($action == "rent" || $action == "qera") ? 1 : 0;

        $propertyType = "Warehouse";
        
        $properties =  Cache::remember('category_warehouseFirstColumn' . $propertyType . '_' . $city . '_' . $forSale . '_' . $forRent, 15,
            function () use ($gjuha, $gjuhaSelectLanguage, $propertyType, $city, $forSale, $forRent) {
                return DB::select('SELECT
						ArtID as id,
						ArtTitull_' . $gjuha . ' as title,
						case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
						Cmimi AS price,
						ArtPermbajtja_' . $gjuha . ' as body,
						Tipi as type,
						Qyteti_' . $gjuha . ' as city,
						Qyteti_' . $gjuhaSelectLanguage . ' as citylangswitch,
						Forsale as sale,
						Forrent as rent,
						Siperfaqia as area,
						kodi
						from `cms_artikujt`
						WHERE
						ifnull(fshih,0)=0 AND
						Llojiprones_gj2 = ?
						AND
						Qyteti_' . $gjuha . ' = ?
						AND (Forsale=? || Forrent =?)
						AND deleted_at IS NULL
						ORDER BY created_at DESC, priority DESC, ArtRend ASC, created_at DESC, id DESC', [$propertyType, $city, $forSale, $forRent]);
        });
		
		return $properties;            
/*
        $propertiesFirstColumn = Cache::remember('category_warehouseFirstColumn' . $propertyType . '_' . $city . '_' . $forSale . '_' . $forRent, 15,
            function () use ($gjuha, $gjuhaSelectLanguage, $propertyType, $city, $forSale, $forRent) {
                return DB::select('SELECT
						ArtID as id,
						ArtTitull_' . $gjuha . ' as title,
						case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
						Cmimi AS price,
						ArtPermbajtja_' . $gjuha . ' as body,
						Tipi as type,
						Qyteti_' . $gjuha . ' as city,
						Qyteti_' . $gjuhaSelectLanguage . ' as citylangswitch,
						Forsale as sale,
						Forrent as rent,
						Siperfaqia as area,
						kodi
						from `cms_artikujt`
						WHERE
						ifnull(fshih,0)=0 AND
						Llojiprones_gj2 = ?
						AND
						Qyteti_' . $gjuha . ' = ?
						AND (Forsale=? || Forrent =?)
						AND Siperfaqia BETWEEN 0 and 500
						AND deleted_at IS NULL
						ORDER BY priority DESC, ArtRend ASC, created_at DESC, id DESC', [$propertyType, $city, $forSale, $forRent]);
            });
        $propertiesSecondColumn = Cache::remember('category_warehouseSecondColumn' . $propertyType . '_' . $city . '_' . $forSale . '_' . $forRent, 15,
            function () use ($gjuha, $propertyType, $city, $forSale, $forRent) {
                return DB::select('SELECT
						ArtID as id,
						ArtTitull_' . $gjuha . ' as title,
						case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
						Cmimi AS price,
						ArtPermbajtja_' . $gjuha . ' as body,
						Tipi as type,
						Qyteti_' . $gjuha . ' as city,
						Forsale as sale,
						Forrent as rent,
						Siperfaqia as area,
						kodi
						from `cms_artikujt`
						WHERE
						ifnull(fshih,0)=0 AND
						Llojiprones_gj2 = ?
						AND
						Qyteti_' . $gjuha . ' = ?
						AND (Forsale=? || Forrent =?)
						AND Siperfaqia BETWEEN 500 and 1000
						AND deleted_at IS NULL
						ORDER BY priority DESC, ArtRend ASC, created_at DESC, id DESC', [$propertyType, $city, $forSale, $forRent]);
            });
        $propertiesThirdColumn = Cache::remember('category_warehouseThirdColumn' . $propertyType . '_' . $city . '_' . $forSale . '_' . $forRent, 15,
            function () use ($gjuha, $propertyType, $city, $forSale, $forRent) {
                return DB::select('SELECT
						ArtID as id,
						ArtTitull_' . $gjuha . ' as title,
						case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
						Cmimi AS price,
						ArtPermbajtja_' . $gjuha . ' as body,
						Tipi as type,
						Qyteti_' . $gjuha . ' as city,
						Forsale as sale,
						Forrent as rent,
						Siperfaqia as area,
						kodi
						from `cms_artikujt`
						WHERE
						ifnull(fshih,0)=0 AND
						Llojiprones_gj2 = ?
						AND
						Qyteti_' . $gjuha . ' = ?
						AND (Forsale=? || Forrent =?)
						AND Siperfaqia BETWEEN 1000 and 2000
						AND deleted_at IS NULL
						ORDER BY priority DESC, ArtRend ASC, created_at DESC, id DESC', [$propertyType, $city, $forSale, $forRent]);
            });
        $propertiesFourthColumn = Cache::remember('category_warehouseFourthColumn' . $propertyType . '_' . $city . '_' . $forSale . '_' . $forRent, 15,
            function () use ($gjuha, $propertyType, $city, $forSale, $forRent) {
                return DB::select('SELECT
						ArtID as id,
						ArtTitull_' . $gjuha . ' as title,
						case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
						Cmimi AS price,
						ArtPermbajtja_' . $gjuha . ' as body,
						Tipi as type,
						Qyteti_' . $gjuha . ' as city,
						Forsale as sale,
						Forrent as rent,
						Siperfaqia as area,
						kodi
						from `cms_artikujt`
						WHERE
						ifnull(fshih,0)=0 AND
						Llojiprones_gj2 = ?
						AND
						Qyteti_' . $gjuha . ' = ?
						AND (Forsale=? || Forrent =?)
						AND Siperfaqia>2000
						AND deleted_at IS NULL
						ORDER BY priority DESC, ArtRend ASC, created_at DESC, id DESC', [$propertyType, $city, $forSale, $forRent]);
            });
        $propertiesFifthColumn = '';

        return [$propertiesFirstColumn, $propertiesSecondColumn, $propertiesThirdColumn, $propertiesFourthColumn, $propertiesFifthColumn];
*/
    }

    /**
     * Category Land listing.
     *
     * @param Request $request
     * @return array
     */
    public function categoryLand(Request $request)
    {
        $lang = $request->route('lang');
        $gjuha = $lang == 'en' ? 'gj2' : 'gj1';
        $gjuhaSelectLanguage = $lang == 'en' ? 'gj1' : 'gj2';
        $city = ucfirst($request->route('city'));
        $action = $request->route('action');
        $forSale = ($action == "sale" || $action == "shitje") ? 1 : 0;
        $forRent = ($action == "rent" || $action == "qera") ? 1 : 0;

        $propertyType = "Land";
        
        $properties = Cache::remember('category_landFirstColumn' . $propertyType . '_' . $city . '_' . $forSale . '_' . $forRent, 15,
            function () use ($gjuha, $gjuhaSelectLanguage, $propertyType, $city, $forSale, $forRent) {
                return DB::select('SELECT
						ArtID as id,
						ArtTitull_' . $gjuha . ' as title,
						case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
						Cmimi AS price,
						ArtPermbajtja_' . $gjuha . ' as body,
						Tipi as type,
						Qyteti_' . $gjuha . ' as city,
						Qyteti_' . $gjuhaSelectLanguage . ' as citylangswitch,
						Forsale as sale,
						Forrent as rent,
						Siperfaqia as area,
						kodi
						from `cms_artikujt`
						WHERE ifnull(fshih,0)=0
						AND Llojiprones_gj2 = ?
						AND Qyteti_' . $gjuha . ' = ?
						AND (Forsale=? || Forrent =?)
						AND deleted_at IS NULL
						ORDER BY created_at DESC, priority DESC, ArtRend ASC, created_at DESC, id DESC', [$propertyType, $city, $forSale, $forRent]);
        });
		
		return $properties;
/*
        $propertiesFirstColumn = Cache::remember('category_landFirstColumn' . $propertyType . '_' . $city . '_' . $forSale . '_' . $forRent, 15,
            function () use ($gjuha, $gjuhaSelectLanguage, $propertyType, $city, $forSale, $forRent) {
                return DB::select('SELECT
						ArtID as id,
						ArtTitull_' . $gjuha . ' as title,
						case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
						Cmimi AS price,
						ArtPermbajtja_' . $gjuha . ' as body,
						Tipi as type,
						Qyteti_' . $gjuha . ' as city,
						Qyteti_' . $gjuhaSelectLanguage . ' as citylangswitch,
						Forsale as sale,
						Forrent as rent,
						Siperfaqia as area,
						kodi
						from `cms_artikujt`
						WHERE ifnull(fshih,0)=0
						AND Llojiprones_gj2 = ?
						AND Qyteti_' . $gjuha . ' = ?
						AND (Forsale=? || Forrent =?)
						AND Siperfaqia BETWEEN 0 and 1000
						AND deleted_at IS NULL
						ORDER BY priority DESC, ArtRend ASC, created_at DESC, id DESC', [$propertyType, $city, $forSale, $forRent]);
            });
        $propertiesSecondColumn = Cache::remember('category_landSecondColumn' . $propertyType . '_' . $city . '_' . $forSale . '_' . $forRent, 15,
            function () use ($gjuha, $propertyType, $city, $forSale, $forRent) {
                return DB::select('SELECT
						ArtID as id,
						ArtTitull_' . $gjuha . ' as title,
						case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
						Cmimi AS price,
						ArtPermbajtja_' . $gjuha . ' as body,
						Tipi as type,
						Qyteti_' . $gjuha . ' as city,
						Forsale as sale,
						Forrent as rent,
						Siperfaqia as area,
						kodi
						from `cms_artikujt`
						WHERE
						ifnull(fshih,0)=0
						AND Llojiprones_gj2 = ?
						AND Qyteti_' . $gjuha . ' = ?
						AND (Forsale=? || Forrent =?)
						AND Siperfaqia BETWEEN 1000 and 2000
						AND deleted_at IS NULL
						ORDER BY priority DESC, ArtRend ASC, created_at DESC, id DESC', [$propertyType, $city, $forSale, $forRent]);
            });
        $propertiesThirdColumn = Cache::remember('category_landThirdColumn' . $propertyType . '_' . $city . '_' . $forSale . '_' . $forRent, 15,
            function () use ($gjuha, $propertyType, $city, $forSale, $forRent) {
                return DB::select('SELECT
						ArtID as id,
						ArtTitull_' . $gjuha . ' as title,
						case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
						Cmimi AS price,
						ArtPermbajtja_' . $gjuha . ' as body,
						Tipi as type,
						Qyteti_' . $gjuha . ' as city,
						Forsale as sale,
						Forrent as rent,
						Siperfaqia as area,
						kodi
						from `cms_artikujt`
						WHERE ifnull(fshih,0)=0
						AND Llojiprones_gj2 = ?
						AND	Qyteti_' . $gjuha . ' = ?
						AND (Forsale=? || Forrent =?)
						AND Siperfaqia BETWEEN 2000 and 5000
						AND deleted_at IS NULL
						ORDER BY priority DESC, ArtRend ASC, created_at DESC, id DESC', [$propertyType, $city, $forSale, $forRent]);
            });
        $propertiesFourthColumn = Cache::remember('category_landFourthColumn' . $propertyType . '_' . $city . '_' . $forSale . '_' . $forRent, 15,
            function () use ($gjuha, $propertyType, $city, $forSale, $forRent) {
                return DB::select('SELECT
						ArtID as id,
						ArtTitull_' . $gjuha . ' as title,
						case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
						Cmimi AS price,
						ArtPermbajtja_' . $gjuha . ' as body,
						Tipi as type,
						Qyteti_' . $gjuha . ' as city,
						Forsale as sale,
						Forrent as rent,
						Siperfaqia as area,
						kodi
						from `cms_artikujt`
						WHERE
						ifnull(fshih,0)=0
						AND Llojiprones_gj2 = ?
						AND	Qyteti_' . $gjuha . ' = ?
						AND (Forsale=? || Forrent =?)
						AND Siperfaqia>5000
						AND deleted_at IS NULL
						ORDER BY priority DESC, ArtRend ASC, created_at DESC, id DESC', [$propertyType, $city, $forSale, $forRent]);
            });
        $propertiesFifthColumn = '';

        return [$propertiesFirstColumn, $propertiesSecondColumn, $propertiesThirdColumn, $propertiesFourthColumn, $propertiesFifthColumn];
*/
    }

    /**
     * Category Commercial listing.
     *
     * @param Request $request
     * @return array
     */
    public function categoryCommercial(Request $request)
    {
        $lang = $request->route('lang');
        $gjuha = $lang == 'en' ? 'gj2' : 'gj1';
        $gjuhaSelectLanguage = $lang == 'en' ? 'gj1' : 'gj2';
        $city = ucfirst($request->route('city'));
        $action = $request->route('action');
        $forSale = ($action == "sale" || $action == "shitje") ? 1 : 0;
        $forRent = ($action == "rent" || $action == "qera") ? 1 : 0;

        $propertyType = "commercial";
        
        $properties = Cache::remember('category_commercialFirstColumn' . $propertyType . '_' . $city . '_' . $forSale . '_' . $forRent . '_' . $gjuha, 15,
            function () use ($gjuha, $gjuhaSelectLanguage, $propertyType, $city, $forSale, $forRent) {
                return DB::select('SELECT
						ArtID as id,
						ArtTitull_' . $gjuha . ' as title,
						case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
						Cmimi AS price,
						ArtPermbajtja_' . $gjuha . ' as body,
						Tipi as type,
						Qyteti_' . $gjuha . ' as city,
						Qyteti_' . $gjuhaSelectLanguage . ' as citylangswitch,
						Forsale as sale,
						Forrent as rent,
						Siperfaqia as area,
						kodi
						from `cms_artikujt`
						WHERE
						ifnull(fshih,0)=0 AND
						Llojiprones_gj2 in ("Commercial")
						AND
						Qyteti_' . $gjuha . ' = ?
						AND (Forsale=? || Forrent =?)
						AND deleted_at IS NULL
						ORDER BY created_at DESC, priority DESC, ArtRend ASC, created_at DESC, id DESC', [$city, $forSale, $forRent]);
        });

		return $properties;            
/*
        $propertiesFirstColumn = Cache::remember('category_commercialFirstColumn' . $propertyType . '_' . $city . '_' . $forSale . '_' . $forRent . '_' . $gjuha, 15,
            function () use ($gjuha, $gjuhaSelectLanguage, $propertyType, $city, $forSale, $forRent) {
                return DB::select('SELECT
						ArtID as id,
						ArtTitull_' . $gjuha . ' as title,
						case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
						Cmimi AS price,
						ArtPermbajtja_' . $gjuha . ' as body,
						Tipi as type,
						Qyteti_' . $gjuha . ' as city,
						Qyteti_' . $gjuhaSelectLanguage . ' as citylangswitch,
						Forsale as sale,
						Forrent as rent,
						Siperfaqia as area,
						kodi
						from `cms_artikujt`
						WHERE
						ifnull(fshih,0)=0 AND
						Llojiprones_gj2 in ("Commercial")
						AND
						Qyteti_' . $gjuha . ' = ?
						AND (Forsale=? || Forrent =?)
						AND Siperfaqia BETWEEN 0 and 50
						AND deleted_at IS NULL
						ORDER BY priority DESC, ArtRend ASC, created_at DESC, id DESC', [$city, $forSale, $forRent]);
            });
        $propertiesSecondColumn = Cache::remember('category_commercialSecondColumn' . $propertyType . '_' . $city . '_' . $forSale . '_' . $forRent, 15,
            function () use ($gjuha, $propertyType, $city, $forSale, $forRent) {
                return DB::select('SELECT
						ArtID as id,
						ArtTitull_' . $gjuha . ' as title,
						case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
						Cmimi AS price,
						ArtPermbajtja_' . $gjuha . ' as body,
						Tipi as type,
						Qyteti_' . $gjuha . ' as city,
						Forsale as sale,
						Forrent as rent,
						Siperfaqia as area,
						kodi
						from `cms_artikujt`
						WHERE
						ifnull(fshih,0)=0 AND
						Llojiprones_gj2 in ("Commercial")
						AND
						Qyteti_' . $gjuha . ' = ?
						AND (Forsale=? || Forrent =?)
						AND Siperfaqia BETWEEN 50 and 100
						AND deleted_at IS NULL
						ORDER BY priority DESC, ArtRend ASC, created_at DESC, id DESC', [$city, $forSale, $forRent]);
            });
        $propertiesThirdColumn = Cache::remember('category_commercialSThirdColumn' . $propertyType . '_' . $city . '_' . $forSale . '_' . $forRent, 15,
            function () use ($gjuha, $propertyType, $city, $forSale, $forRent) {
                return DB::select('SELECT
						ArtID as id,
						ArtTitull_' . $gjuha . ' as title,
						case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
						Cmimi AS price,
						ArtPermbajtja_' . $gjuha . ' as body,
						Tipi as type,
						Qyteti_' . $gjuha . ' as city,
						Forsale as sale,
						Forrent as rent,
						Siperfaqia as area,
						kodi
						from `cms_artikujt`
						WHERE
						ifnull(fshih,0)=0 AND
						Llojiprones_gj2 in ("Commercial")
						AND
						Qyteti_' . $gjuha . ' = ?
						AND (Forsale=? || Forrent =?)
						AND Siperfaqia BETWEEN 100 and 200
						AND deleted_at IS NULL
						ORDER BY priority DESC, ArtRend ASC, created_at DESC, id DESC', [$city, $forSale, $forRent]);
            });

        $propertiesFourthColumn = Cache::remember('category_commercialFourthColumn2' . $propertyType . '_' . $city . '_' . $forSale . '_' . $forRent, 15,
            function () use ($gjuha, $propertyType, $city, $forSale, $forRent) {
                return DB::select('SELECT
						ArtID as id,
						ArtTitull_' . $gjuha . ' as title,
						case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
						Cmimi AS price,
						ArtPermbajtja_' . $gjuha . ' as body,
						Tipi as type,
						Qyteti_' . $gjuha . ' as city,
						Forsale as sale,
						Forrent as rent,
						Siperfaqia as area,
						kodi
						from `cms_artikujt`
						WHERE
						ifnull(fshih,0)=0 AND
						Llojiprones_gj2 in ("Commercial")
						AND
						Qyteti_' . $gjuha . ' = ?
						AND (Forsale=? || Forrent =?)
						AND Siperfaqia>200
						AND deleted_at IS NULL
						ORDER BY priority DESC, ArtRend ASC, created_at DESC, id DESC', [$city, $forSale, $forRent]);
            });

        $propertiesFifthColumn = '';

        return [$propertiesFirstColumn, $propertiesSecondColumn, $propertiesThirdColumn, $propertiesFourthColumn, $propertiesFifthColumn];
*/
    }

    /**
     * Category selection apartment listing.
     *
     * @param Request $request
     * @param $conditionDhomaGjumi
     * @return mixed
     */
    public function categorySelectionApartment(Request $request, $conditionDhomaGjumi)
    {
        $lang = $request->route('lang');
        $gjuha = $lang == 'en' ? 'gj2' : 'gj1';
        $gjuhaSelectLanguage = $lang == 'en' ? 'gj1' : 'gj2';
        $city = ucfirst($request->route('city'));
        $action = $request->route('action');
        $forSale = ($action == "sale" || $action == "shitje") ? 1 : 0;
        $forRent = ($action == "rent" || $action == "qera") ? 1 : 0;

        $propertyType = "Apartment";

        $searchResult = Cache::remember('categoryselection_searchResultApartment' . $propertyType . '_' . $city . '_' . $forSale . '_' . $forRent . '_' . $conditionDhomaGjumi, 15,
            function () use ($gjuha, $gjuhaSelectLanguage, $conditionDhomaGjumi, $propertyType, $city, $forSale, $forRent) {
                return DB::select('SELECT
					ArtID as id,
					ArtTitull_' . $gjuha . ' as title,
					case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
					Cmimi as price,
					ArtPermbajtja_' . $gjuha . ' as body,
					Tipi as type,
					Qyteti_' . $gjuha . ' as city,
					Qyteti_' . $gjuhaSelectLanguage . ' as citylangswitch,
					Forsale as sale,
					Forrent as rent,
					Siperfaqia as area,
					kodi
					from `cms_artikujt`
					WHERE
					ifnull(fshih,0)=0 AND
					Llojiprones_gj2 = ?
					AND
					Qyteti_' . $gjuha . ' = ?
					AND (Forsale=? || Forrent =?)
					AND ' . $conditionDhomaGjumi . '
					AND deleted_at IS NULL
					ORDER BY priority DESC, ArtRend ASC, created_at DESC, id DESC', [$propertyType, $city, $forSale, $forRent]);
            });

        return $searchResult;
    }

    /**
     * Category selection villa listing.
     *
     * @param Request $request
     * @param $conditionDhomaGjumi
     * @return mixed
     */
    public function categorySelectionVilla(Request $request, $conditionDhomaGjumi)
    {
        $lang = $request->route('lang');
        $gjuha = $lang == 'en' ? 'gj2' : 'gj1';
        $gjuhaSelectLanguage = $lang == 'en' ? 'gj1' : 'gj2';
        $city = ucfirst($request->route('city'));
        $action = $request->route('action');
        $forSale = ($action == "sale" || $action == "shitje") ? 1 : 0;
        $forRent = ($action == "rent" || $action == "qera") ? 1 : 0;

        $propertyType = "Villa";
        $searchResult = Cache::remember('categoryselection_searchResultVilla' . $propertyType . '_' . $city . '_' . $forSale . '_' . $forRent . '_' . $conditionDhomaGjumi, 15,
            function () use ($gjuha, $gjuhaSelectLanguage, $conditionDhomaGjumi, $propertyType, $city, $forSale, $forRent) {

				preg_match_all('!\d+!', $conditionDhomaGjumi, $matches);
				if($matches[0][0]==2){
					$conditionDhomaGjumi = 'dhomaGjumi in (2,3)';
				}else if($matches[0][0]==3){
					$conditionDhomaGjumi = 'dhomaGjumi in (2,3)';
				}				
				
                return DB::select('SELECT
				ArtID as id,
				ArtTitull_' . $gjuha . ' as title,
				case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
				Cmimi as price,
				ArtPermbajtja_' . $gjuha . ' as body,
				Tipi as type,
				Qyteti_' . $gjuha . ' as city,
				Qyteti_' . $gjuhaSelectLanguage . ' as citylangswitch,
				Forsale as sale,
				Forrent as rent,
				Siperfaqia as area,
				kodi
				from `cms_artikujt`
				WHERE
				ifnull(fshih,0)=0 AND
				Llojiprones_gj2 = ?
				AND
				Qyteti_' . $gjuha . ' = ?
				AND (Forsale=? || Forrent =?)
				AND ' . $conditionDhomaGjumi . '
				AND deleted_at IS NULL
				ORDER BY priority DESC, ArtRend ASC, created_at DESC, id DESC', [$propertyType, $city, $forSale, $forRent]);
            });

        return $searchResult;
    }

    /**
     * Category selection office listing.
     *
     * @param Request $request
     * @param $conditionSiperfaqe
     * @return mixed
     */
    public function categorySelectionOffice(Request $request, $conditionSiperfaqe)
    {
        $lang = $request->route('lang');
        $gjuha = $lang == 'en' ? 'gj2' : 'gj1';
        $gjuhaSelectLanguage = $lang == 'en' ? 'gj1' : 'gj2';
        $city = ucfirst($request->route('city'));
        $action = $request->route('action');
        $forSale = ($action == "sale" || $action == "shitje") ? 1 : 0;
        $forRent = ($action == "rent" || $action == "qera") ? 1 : 0;

        $propertyType = "Office";

        $searchResult = Cache::remember('categoryselection_searchResultOffice' . $propertyType . '_' . $city . '_' . $forSale . '_' . $forRent . '_' . $conditionSiperfaqe, 15,
            function () use ($gjuha, $gjuhaSelectLanguage, $conditionSiperfaqe, $propertyType, $city, $forSale, $forRent) {
                return DB::select('SELECT
				ArtID as id,
				ArtTitull_' . $gjuha . ' as title,
				case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
				Cmimi as price,
				ArtPermbajtja_' . $gjuha . ' as body,
				Tipi as type,
				Qyteti_' . $gjuha . ' as city,
				Qyteti_' . $gjuhaSelectLanguage . ' as citylangswitch,
				Forsale as sale,
				Forrent as rent,
				Siperfaqia as area,
				kodi
				from `cms_artikujt`
				WHERE
				ifnull(fshih,0)=0 AND
				Llojiprones_gj2 = ?
				AND
				Qyteti_' . $gjuha . ' = ?
				AND (Forsale=? || Forrent =?)
				AND ' . $conditionSiperfaqe . '
				AND deleted_at IS NULL
				ORDER BY priority DESC, ArtRend ASC, created_at DESC, id DESC', [$propertyType, $city, $forSale, $forRent]);
            });

        return $searchResult;
    }

    /**
     * Category selection warehouse listing.
     *
     * @param Request $request
     * @param $conditionSiperfaqe
     * @return mixed
     */
    public function categorySelectionWarehouse(Request $request, $conditionSiperfaqe)
    {
        $lang = $request->route('lang');
        $gjuha = $lang == 'en' ? 'gj2' : 'gj1';
        $gjuhaSelectLanguage = $lang == 'en' ? 'gj1' : 'gj2';
        $city = ucfirst($request->route('city'));
        $action = $request->route('action');
        $forSale = ($action == "sale" || $action == "shitje") ? 1 : 0;
        $forRent = ($action == "rent" || $action == "qera") ? 1 : 0;

        $propertyType = "Warehouse";
        $searchResult = Cache::remember('categoryselection_searchResultWarehouse' . $propertyType . '_' . $city . '_' . $forSale . '_' . $forRent . '_' . $conditionSiperfaqe, 15,
            function () use ($gjuha, $gjuhaSelectLanguage, $conditionSiperfaqe, $propertyType, $city, $forSale, $forRent) {
                return DB::select('SELECT
				ArtID as id,
				ArtTitull_' . $gjuha . ' as title,
				case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
				Cmimi as price,
				ArtPermbajtja_' . $gjuha . ' as body,
				Tipi as type,
				Qyteti_' . $gjuha . ' as city,
				Qyteti_' . $gjuhaSelectLanguage . ' as citylangswitch,
				Forsale as sale,
				Forrent as rent,
				Siperfaqia as area,
				kodi
				from `cms_artikujt`
				WHERE
				ifnull(fshih,0)=0 AND
				Llojiprones_gj2 = ?
				AND
				Qyteti_' . $gjuha . ' = ?
				AND (Forsale=? || Forrent =?)
				AND ' . $conditionSiperfaqe . '
				AND deleted_at IS NULL
				ORDER BY priority DESC, ArtRend ASC, created_at DESC, id DESC', [$propertyType, $city, $forSale, $forRent]);
            });

        return $searchResult;
    }

    /**
     * Category selection land listing.
     *
     * @param Request $request
     * @param $conditionSiperfaqe
     * @return mixed
     */
    public function categorySelectionLand(Request $request, $conditionSiperfaqe)
    {
        $lang = $request->route('lang');
        $gjuha = $lang == 'en' ? 'gj2' : 'gj1';
        $gjuhaSelectLanguage = $lang == 'en' ? 'gj1' : 'gj2';
        $city = ucfirst($request->route('city'));
        $action = $request->route('action');
        $forSale = ($action == "sale" || $action == "shitje") ? 1 : 0;
        $forRent = ($action == "rent" || $action == "qera") ? 1 : 0;

        $propertyType = "Land";
        $searchResult = Cache::remember('categoryselection_searchResultLand' . $propertyType . '_' . $city . '_' . $forSale . '_' . $forRent . '_' . $conditionSiperfaqe, 15,
            function () use ($gjuha, $gjuhaSelectLanguage, $conditionSiperfaqe, $propertyType, $city, $forSale, $forRent) {
                return DB::select('SELECT
				ArtID as id,
				ArtTitull_' . $gjuha . ' as title,
				case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
				Cmimi as price,
				ArtPermbajtja_' . $gjuha . ' as body,
				Tipi as type,
				Qyteti_' . $gjuha . ' as city,
				Qyteti_' . $gjuhaSelectLanguage . ' as citylangswitch,
				Forsale as sale,
				Forrent as rent,
				Siperfaqia as area,
				kodi
				from `cms_artikujt`
				WHERE
				ifnull(fshih,0)=0 AND
				Llojiprones_gj2 = ?
				AND
				Qyteti_' . $gjuha . ' = ?
				AND (Forsale=? || Forrent =?)
				AND ' . $conditionSiperfaqe . '
				AND deleted_at IS NULL
				ORDER BY priority DESC, ArtRend ASC, created_at DESC, id DESC', [$propertyType, $city, $forSale, $forRent]);
            });

        return $searchResult;
    }

    /**
     * Category selection commercial listing.
     *
     * @param Request $request
     * @param $conditionSiperfaqe
     * @return mixed
     */
    public function categorySelectionCommercial(Request $request, $conditionSiperfaqe)
    {
        $lang = $request->route('lang');
        $gjuha = $lang == 'en' ? 'gj2' : 'gj1';
        $gjuhaSelectLanguage = $lang == 'en' ? 'gj1' : 'gj2';
        $city = ucfirst($request->route('city'));
        $action = $request->route('action');
        $forSale = ($action == "sale" || $action == "shitje") ? 1 : 0;
        $forRent = ($action == "rent" || $action == "qera") ? 1 : 0;

        $propertyType = "Commercial";

        $searchResult = Cache::remember('categoryselection_searchResultCommercial' . $propertyType . '_' . $city . '_' . $forSale . '_' . $forRent . '_' . $conditionSiperfaqe, 15,
            function () use ($gjuha, $gjuhaSelectLanguage, $conditionSiperfaqe, $propertyType, $city, $forSale, $forRent) {
                return DB::select('SELECT
				ArtID as id,
				ArtTitull_' . $gjuha . ' as title,
				case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
				Cmimi as price,
				ArtPermbajtja_' . $gjuha . ' as body,
				Tipi as type,
				Qyteti_' . $gjuha . ' as city,
				Qyteti_' . $gjuhaSelectLanguage . ' as citylangswitch,
				Forsale as sale,
				Forrent as rent,
				Siperfaqia as area,
				kodi
				from `cms_artikujt`
				WHERE
				ifnull(fshih,0)=0 AND
				Llojiprones_gj2 in ("Commercial")
				AND
				Qyteti_' . $gjuha . ' = ?
				AND (Forsale=? || Forrent =?)
				AND ' . $conditionSiperfaqe . '
				AND deleted_at IS NULL
				ORDER BY priority DESC, ArtRend ASC, created_at DESC, id DESC', [$city, $forSale, $forRent]);
            });

        return $searchResult;
    }

    /**
     * Get similar properties with similar prices.
     *
     * @param $properties
     * @param $basePrice
     * @param bool $isRent
     * @return \Illuminate\Support\Collection
     */
    public function getSimilarPrices($properties, $basePrice, $isRent = true)
    {
        if (is_array($properties)) {
            $properties = collect($properties);
        }

        return $properties->filter(function ($property) use ($basePrice, $isRent) {
            $price = (float)clean_price($property->price);

            try {

                $min = $price - $basePrice;
                $max = $basePrice - $price;

                if ($isRent) {

                    if (($min < 150 && $min > 0) || ($max < 150 && $max > 0)) {
                        return $property;
                    }

                } else {

                    if (($min < 15000 && $min > 0) || ($max < 15000 && $max > 0)) {
                        return $property;
                    }
                }
            } catch (\Exception $exception) {
                return false;
            }

            return false;

        });
    }
}