<?php

namespace App\Repositories;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class SearchRepo extends Repository
{

    /**
     * Search for apartments.
     *
     * @param Request $request
     * @param string $gjuha
     * @param string $searchFor
     * @param string $cmimiMore
     * @param int $forSale
     * @param int $forRent
     * @return mixed
     */
    public function searchApartment(Request $request, string $gjuha, string &$searchFor, string $cmimiMore, int $forSale, int $forRent)
    {
        $city = $request->input('city');
        $propertyType = $request->input('propertyType');

        $searchFor = "Apartament, " . $searchFor;
        $conditionNrRoom = ' AND dhomaGjumi>0';
        $conditionCmimi = ' AND Cmimi<99999999999';

        if ($request->statesearch == "opened") {

            if ($request->organizimi != "undefined") {

                switch ($request->organizimi) {
                    case 11:
                        $conditionNrRoom = 'AND dhomaGjumi=1';
                        $searchFor = $searchFor . trans('messages.onebedroom') . ", ";
                        break;
                    case "Garsoniere":
                        $conditionNrRoom = 'AND dhomaGjumi=0';
                        $searchFor = $searchFor . trans('messages.garsoniere') . ", ";
                        break;
                    case 21:
                        $conditionNrRoom = 'AND dhomaGjumi=2';
                        $searchFor = $searchFor . trans('messages.twobedroom') . ", ";
                        break;
                    case 31:
                        $conditionNrRoom = 'AND dhomaGjumi=3';
                        $searchFor = $searchFor . trans('messages.threebedroom') . ", ";
                        break;
                    case "more":
                        $conditionNrRoom = 'AND dhomaGjumi>3';
                        $searchFor = $searchFor . trans('messages.morethanthreebedroom') . "a, ";
                        break;
                    default:
                        $conditionNrRoom = '';
                        break;
                }
            }

            switch ($cmimiMore) {
                case "0-300":
                    $conditionCmimi = 'AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED) <=300';
                    $searchFor = $searchFor . "<300 Euro, ";
                    break;
                case "300-500":
                    $conditionCmimi = ' AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)>=300 AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED) <=500 ';
                    $searchFor = $searchFor . "300-500 Euro ";
                    break;
                case "50.000-100.000":
                    $conditionCmimi = 'AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED) >=50000 AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED) <=100000 ';
                    $searchFor = $searchFor . "50.000-100.000 Euro, ";
                    break;
                case "100.000-200.000":
                    $conditionCmimi = 'AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED) >=100000 AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED) <=200000 ';
                    $searchFor = $searchFor . "100.000-00.000 Euro, ";
                    break;
                case "500-1000":
                    $conditionCmimi = 'AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED) >=500 AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED) <=1000 ';
                    $searchFor = $searchFor . "500-1000 Euro, ";
                    break;
                case "0-50.000":
                    $conditionCmimi = 'AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED) >=0 AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)<=50000 ';
                    $searchFor = $searchFor . "0-50.000 Euro, ";
                    break;
                case "100000-200000":
                    $conditionCmimi = 'AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED) >=100000 AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED) <=200000 ';
                    $searchFor = $searchFor . "100000-200000 Euro, ";
                    break;
                case "morerent":
                    $conditionCmimi = 'AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED) >1000';
                    $searchFor = $searchFor . ">1000 Euro, ";
                    break;
                case "moresale":
                    $conditionCmimi = 'AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED) >200000';
                    $searchFor = $searchFor . ">200.000 Euro, ";
                    break;
                default:
                    $conditionCmimi = '';
                    break;
            }
        }


        $searchResult = Cache::remember('searchResult1' . $gjuha . '_' . $conditionNrRoom . '_' . $conditionCmimi . '_' . $city . '_' . $forSale . '_' . $forRent . '_' . $propertyType, 15, function () use ($gjuha, $conditionNrRoom, $conditionCmimi, $city, $forSale, $forRent, $propertyType) {
            return DB::select('SELECT
				ArtID as id,
				ArtTitull_' . $gjuha . ' as title,
				case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
				Cmimi AS price,
				ArtPermbajtja_' . $gjuha . ' as body,
				Llojiprones_' . $gjuha . ' as type,
				Qyteti_' . $gjuha . ' as city,
				Forsale as sale,
				Forrent as rent,
				Siperfaqia as area,
				kodi
				from `cms_artikujt`
				WHERE
				ifnull(fshih,0)=0
				AND Qyteti_gj2 = ?
				AND (Forsale=? || Forrent =?)
				AND Llojiprones_gj2=?
				AND deleted_at IS NULL 
					' . $conditionNrRoom . '
					' . $conditionCmimi . '
                ORDER BY priority DESC, ArtRend ASC, created_at DESC, id DESC', [$city, $forSale, $forRent, $propertyType]);
        });

        return $searchResult;
    }

    /**
     * Search for commercial.
     *
     * @param Request $request
     * @param string $gjuha
     * @param string $searchFor
     * @param string $saleStatus
     * @param int $forSale
     * @param int $forRent
     * @return mixed
     */
    public function searchCommercial(Request $request, string $gjuha, string &$searchFor, string $saleStatus, int $forSale, int $forRent)
    {
        $city = $request->input('city');
        $propertyType = $request->input('propertyType');

        $searchFor = trans('messages.otherlokaledyqane') . ", " . $searchFor;
        $siperfaqe = $request->input('siperfaqe-store');
        $cmimi = $request->input('cmimi-store');
        $cmimiMore = ($cmimi == "more") ? $cmimi . $saleStatus : $cmimi;
        $conditionSiperfaqe = '';
        $conditionCmimi = '';
        if ($request->statesearch == "opened") {
            switch ($siperfaqe) {
                case "0-50":
                    $conditionSiperfaqe = 'AND Siperfaqia>0 AND Siperfaqia<=50';
                    $searchFor = $searchFor . " 0-50 m2, ";
                    break;
                case "50-100":
                    $conditionSiperfaqe = 'AND Siperfaqia>50 AND Siperfaqia<=100';
                    $searchFor = $searchFor . " 50-100 m2, ";
                    break;
                case "100-200":
                    $conditionSiperfaqe = 'AND Siperfaqia>100 AND Siperfaqia<=200';
                    $searchFor = $searchFor . " 100-200 m2, ";
                    break;
                case "more":
                    $conditionSiperfaqe = 'AND Siperfaqia>200';
                    $searchFor = $searchFor . "> 200 m2, ";
                    break;
                default:
                    $conditionSiperfaqe = '';
                    break;
            }
            switch ($cmimiMore) {
                case "0-500":
                    $conditionCmimi = 'AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)<=500';
                    $searchFor = $searchFor . "<500 Euro, ";
                    break;
                case "500-1000":
                    $conditionCmimi = ' AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)>500 AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)<=1000';
                    $searchFor = $searchFor . "< 100000 Euro ";
                    break;
                case "1000-2000":
                    $conditionCmimi = 'AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)>1000 AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)<2000 ';
                    $searchFor = $searchFor . "1000-2000 Euro, ";
                    break;
                case "0-100.000":
                    $conditionCmimi = 'AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)>0 AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)<=100000 ';
                    $searchFor = $searchFor . "0-100.000 Euro, ";
                    break;
                case "100.000-200.000":
                    $conditionCmimi = 'AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)>100000 AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)<200000 ';
                    $searchFor = $searchFor . "100.000-200.000 Euro, ";
                    break;
                case "200.000-300.000":
                    $conditionCmimi = 'AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)>200000 AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)<=300000 ';
                    $searchFor = $searchFor . "200.000-300.000 Euro, ";
                    break;
                case "morerent":
                    $conditionCmimi = 'AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)>2000';
                    $searchFor = $searchFor . ">2000 Euro, ";
                case "moresale":
                    $conditionCmimi = 'AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)>300000';
                    $searchFor = $searchFor . ">300.000 Euro, ";
                default:
                    $conditionCmimi = '';
                    break;
            }
        }

        $searchResult = Cache::remember('searchResult2' . $gjuha . '_' . $conditionSiperfaqe . '_' . $conditionCmimi . '_' . $city . '_' . $forSale . '_' . $forRent . '_' . $propertyType, 15,
            function () use ($gjuha, $conditionSiperfaqe, $conditionCmimi, $city, $forSale, $forRent, $propertyType) {
                return DB::select('SELECT
									ArtID as id,
									ArtTitull_' . $gjuha . ' as title,
									case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
									Cmimi AS price,
									ArtPermbajtja_' . $gjuha . ' as body,
                                    Llojiprones_' . $gjuha . ' as type,
									Qyteti_' . $gjuha . ' as city,
									Forsale as sale,
									Forrent as rent,
									Siperfaqia as area,
									kodi
									from `cms_artikujt`
									WHERE
									ifnull(fshih,0)=0
									AND Qyteti_gj2 = ?
									AND (Forsale=? || Forrent =?)
									AND Llojiprones_gj2 in ("Commercial")
									AND deleted_at IS NULL
                                        ' . $conditionSiperfaqe . '
                                        ' . $conditionCmimi . '
                                    ORDER BY priority DESC, ArtRend ASC, created_at DESC, id DESC', [$city, $forSale, $forRent, $propertyType]);
            });

        return $searchResult;
    }

    /**
     * Search for villa.
     *
     * @param Request $request
     * @param string $gjuha
     * @param string $searchFor
     * @param string $saleStatus
     * @param int $forSale
     * @param int $forRent
     * @return mixed
     */
    public function searchVilla(Request $request, string $gjuha, string &$searchFor, string $saleStatus, int $forSale, int $forRent)
    {
        $city = $request->input('city');
        $propertyType = $request->input('propertyType');

        $searchFor = trans('messages.villa') . ", " . $searchFor;
        $siperfaqe = $request->input('shperndarja-villa');
        $cmimi = $request->input('cmimi-villa');
        $cmimiMore = ($cmimi == "more") ? $cmimi . $saleStatus : $cmimi;
        $conditionSiperfaqe = '';
        $conditionCmimi = '';
		
        if ($request->statesearch == "opened") {
            switch ($siperfaqe) {
                case 4:
                    $conditionSiperfaqe = 'AND dhomaGjumi=4';
                    $searchFor = $searchFor . trans('messages.fourroomsvilla') . ", ";
                    break;
                case 2:
                    $conditionSiperfaqe = 'AND dhomaGjumi in (2,3)';
                    $searchFor = $searchFor . trans('messages.tworoomsvilla') . ", ";
                    break;
                case 5:
                    $conditionSiperfaqe = 'AND dhomaGjumi=5';
                    $searchFor = $searchFor . trans('messages.fiveroomsvilla') . ", ";
                    break;
                case "more":
                    $conditionSiperfaqe = 'AND dhomaGjumi>5';
                    $searchFor = $searchFor . "> " . trans('messages.fiveroomsvilla') . ", ";
                    break;
                default:
                    $conditionSiperfaqe = '';
                    break;
            }
            switch ($cmimiMore) {
                case "0-1000":
                    $conditionCmimi = 'AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)<=1000';
                    $searchFor = $searchFor . "<1000 Euro ";
                    break;
                case "1000-2000":
                    $conditionCmimi = ' AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)>1000 AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)<=2000';
                    $searchFor = $searchFor . "1000-2000 Euro ";
                    break;
                case "2000-3000":
                    $conditionCmimi = 'AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)>2000 AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)<=3000 ';
                    $searchFor = $searchFor . "2000-3000 Euro ";
                    break;
                case "0-100.000":
                    $conditionCmimi = 'AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)>0 AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)<=100000 ';
                    $searchFor = $searchFor . "0-100.000 Euro ";
                    break;
                case "100.000-300.000":
                    $conditionCmimi = 'AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)>100000 AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)<=300000 ';
                    $searchFor = $searchFor . "100.000-300.000 Euro ";
                    break;
                case "300.000-500.000":
                    $conditionCmimi = 'AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)>300000 AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)<=500000 ';
                    $searchFor = $searchFor . "300.000-500.000 Euro ";
                    break;
                case "morerent":
                    $conditionCmimi = 'AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)>3000';
                    $searchFor = $searchFor . ">3000 Euro ";
                    break;
                case "moresale":
                    $conditionCmimi = 'AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)>500000';
                    $searchFor = $searchFor . ">500.000 Euro ";
                    break;
                default:
                    $conditionCmimi = '';
                    break;
            }
        }

        $searchResult = Cache::remember('searchResult3' . $gjuha . '_' . $conditionSiperfaqe . '_' . $conditionCmimi . '_' . $city . '_' . $forSale . '_' . $forRent . '_' . $propertyType, 15,
            function () use ($gjuha, $conditionSiperfaqe, $conditionCmimi, $city, $forSale, $forRent, $propertyType) {
                return DB::select('SELECT
									ArtID as id,
									ArtTitull_' . $gjuha . ' as title,
									case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
									Cmimi AS price,
									ArtPermbajtja_' . $gjuha . ' as body,
                                    Llojiprones_' . $gjuha . ' as type,
									Qyteti_' . $gjuha . ' as city,
									Forsale as sale,
									Forrent as rent,
									Siperfaqia as area,
									kodi
									from `cms_artikujt`
									WHERE
									ifnull(fshih,0)=0
									AND Qyteti_gj2 = ?
									AND (Forsale=? || Forrent =?)
									AND Llojiprones_gj2=?
									AND deleted_at IS NULL
                                        ' . $conditionSiperfaqe . '
                                        ' . $conditionCmimi . '
                                    ORDER BY priority DESC, ArtRend ASC, created_at DESC, id DESC', [$city, $forSale, $forRent, $propertyType]);
            });

        return $searchResult;
    }

    /**
     * Search for office.
     *
     * @param Request $request
     * @param string $gjuha
     * @param string $searchFor
     * @param string $saleStatus
     * @param int $forSale
     * @param int $forRent
     * @return mixed
     */
    public function searchOffice(Request $request, string $gjuha, string &$searchFor, string $saleStatus, int $forSale, int $forRent)
    {
        $city = $request->input('city');
        $propertyType = $request->input('propertyType');

        $searchFor = trans('messages.office') . ", " . $searchFor;
        $siperfaqe = $request->input('siperfaqeZyra');
        $cmimi = $request->input('cmimiZyra');
        $cmimiMore = ($cmimi == "more") ? $cmimi . $saleStatus : $cmimi;
        $conditionSiperfaqe = '';
        $conditionCmimi = '';

        if ($request->statesearch == "opened") {
            switch ($siperfaqe) {
                case "0-50":
                    $conditionSiperfaqe = 'AND Siperfaqia>0 AND Siperfaqia<=50';
                    $searchFor = $searchFor . " 0-50 m2, ";
                    break;
                case "50-100":
                    $conditionSiperfaqe = 'AND Siperfaqia>50 AND Siperfaqia<=100';
                    $searchFor = $searchFor . " 50-100 m2, ";
                    break;
                case "100-200":
                    $conditionSiperfaqe = 'AND Siperfaqia>100 AND Siperfaqia<=200';
                    $searchFor = $searchFor . " 100-200 m2, ";
                    break;
                case "more":
                    $conditionSiperfaqe = 'AND Siperfaqia>200';
                    $searchFor = $searchFor . "> 200 m2, ";
                    break;
                default:
                    $conditionSiperfaqe = '';
                    break;
            }
            switch ($cmimiMore) {
                case "0-300":
                    $conditionCmimi = 'AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)<=300';
                    $searchFor = $searchFor . "<300 Euro, ";
                    break;
                case "300-500":
                    $conditionCmimi = ' AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)>300 AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)<=500';
                    $searchFor = $searchFor . "300-500 Euro ";
                    break;
                case "500-1000":
                    $conditionCmimi = 'AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)>500 AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)<=1000 ';
                    $searchFor = $searchFor . "500-1000 Euro, ";
                    break;
                case "1000-2000":
                    $conditionCmimi = 'AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)>1000 AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)<=2000 ';
                    $searchFor = $searchFor . "1000-2000 Euro, ";
                    break;
                case "0-100.000":
                    $conditionCmimi = 'AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)<=100000 ';
                    $searchFor = $searchFor . "0-100.000 Euro, ";
                    break;
                case "100.000-150.000":
                    $conditionCmimi = 'AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)>100000 AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)<=150000 ';
                    $searchFor = $searchFor . "100.000-150.000 Euro, ";
                    break;
                case "150.000-200.000":
                    $conditionCmimi = 'AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)>150000 AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)<=200000 ';
                    $searchFor = $searchFor . "150.000-200.000 Euro, ";
                    break;
                case "morerent":
                    $conditionCmimi = 'AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)>2000';
                    $searchFor = $searchFor . ">2000 Euro, ";
                case "moresale":
                    $conditionCmimi = 'AND Cmcast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)imi>200000';
                    $searchFor = $searchFor . ">200.000 Euro, ";
                default:
                    $conditionCmimi = '';
                    break;
            }
        }

        $searchResult = Cache::remember('searchResult4' . $gjuha . '_' . $conditionSiperfaqe . '_' . $conditionCmimi . '_' . $city . '_' . $forSale . '_' . $forRent . '_' . $propertyType, 15,
            function () use ($gjuha, $conditionSiperfaqe, $conditionCmimi, $city, $forSale, $forRent, $propertyType) {
                return DB::select('SELECT
									ArtID as id,
									ArtTitull_' . $gjuha . ' as title,
									case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
									Cmimi AS price,
									ArtPermbajtja_' . $gjuha . ' as body,
                                    Llojiprones_' . $gjuha . ' as type,
									Qyteti_' . $gjuha . ' as city,
									Forsale as sale,
									Forrent as rent,
									Siperfaqia as area,
									kodi
									from `cms_artikujt`
									WHERE
									ifnull(fshih,0)=0
									AND Qyteti_gj2 = ?
									AND (Forsale=? || Forrent =?)
									AND Llojiprones_gj2=?
									AND deleted_at IS NULL
                                        ' . $conditionSiperfaqe . '
                                        ' . $conditionCmimi . '
                                    ORDER BY priority DESC, ArtRend ASC, created_at DESC, id DESC', [$city, $forSale, $forRent, $propertyType]);
            });

        return $searchResult;
    }

    /**
     * Search for warehouse.
     *
     * @param Request $request
     * @param string $gjuha
     * @param string $searchFor
     * @param string $saleStatus
     * @param int $forSale
     * @param int $forRent
     * @return mixed
     */
    public function searchWarehouse(Request $request, string $gjuha, string &$searchFor, string $saleStatus, int $forSale, int $forRent)
    {
        $city = $request->input('city');
        $propertyType = $request->input('propertyType');

        $searchFor = trans('messages.warehouse') . ", " . $searchFor;
        $siperfaqe = $request->input('siperfaqe-mag');
        $cmimi = $request->input('cmimi-mag');
        $cmimiMore = ($cmimi == "more") ? $cmimi . $saleStatus : $cmimi;
        $conditionSiperfaqe = '';
        $conditionCmimi = '';

        if ($request->statesearch == "opened") {
            switch ($siperfaqe) {
                case "0-500":
                    $conditionSiperfaqe = 'AND Siperfaqia>0 AND Siperfaqia<=500';
                    $searchFor = $searchFor . " 0-500 m2, ";
                    break;
                case "500-1000":
                    $conditionSiperfaqe = 'AND Siperfaqia>500 AND Siperfaqia<=1000';
                    $searchFor = $searchFor . " 500-1000 m2, ";
                    break;
                case "1000-2000":
                    $conditionSiperfaqe = 'AND Siperfaqia>1000 AND Siperfaqia<=2000';
                    $searchFor = $searchFor . " 1000-2000 m2, ";
                    break;
                case "more":
                    $conditionSiperfaqe = 'AND Siperfaqia>2000';
                    $searchFor = $searchFor . "> 2000 m2, ";
                    break;
                default:
                    $conditionSiperfaqe = '';
                    break;
            }
            switch ($cmimiMore) {
                case "0-500":
                    $conditionCmimi = 'AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)<=500';
                    $searchFor = $searchFor . "0-500 Euro, ";
                    break;
                case "500-1000":
                    $conditionCmimi = ' AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)>500 AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)<=1000';
                    $searchFor = $searchFor . "500-1000 Euro ";
                    break;
                case "1000-2000":
                    $conditionCmimi = 'AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)>1000 AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)<=2000 ';
                    $searchFor = $searchFor . "1000-2000 Euro, ";
                    break;
                case "morerent":
                    $conditionCmimi = 'AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)>2000';
                    $searchFor = $searchFor . ">2000 Euro, ";
                    break;
                default:
                    $conditionCmimi = '';
                    break;
            }
        }

        $searchResult = Cache::remember('searchResult5' . $gjuha . '_' . $conditionSiperfaqe . '_' . $conditionCmimi . '_' . $city . '_' . $forSale . '_' . $forRent . '_' . $propertyType, 15,
            function () use ($gjuha, $conditionSiperfaqe, $conditionCmimi, $city, $forSale, $forRent, $propertyType) {
                return DB::select('SELECT
									ArtID as id,
									ArtTitull_' . $gjuha . ' as title,
									case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
									Cmimi AS price,
									ArtPermbajtja_' . $gjuha . ' as body,
                                    Llojiprones_' . $gjuha . ' as type,
									Qyteti_' . $gjuha . ' as city,
									Forsale as sale,
									Forrent as rent,
									Siperfaqia as area,
									kodi
									from `cms_artikujt`
									WHERE
									ifnull(fshih,0)=0
									AND Qyteti_gj2 = ?
									AND (Forsale=? || Forrent =?)
									AND Llojiprones_gj2=?
									AND deleted_at IS NULL
                                        ' . $conditionSiperfaqe . '
                                        ' . $conditionCmimi . '
                                    ORDER BY priority DESC, ArtRend ASC, created_at DESC, id DESC', [$city, $forSale, $forRent, $propertyType]);
            });

        return $searchResult;
    }

    /**
     * Search for land.
     *
     * @param Request $request
     * @param string $gjuha
     * @param string $searchFor
     * @param string $saleStatus
     * @param int $forSale
     * @param int $forRent
     * @return mixed
     */
    public function searchLand(Request $request, string $gjuha, string &$searchFor, string $saleStatus, int $forSale, int $forRent)
    {
        $city = $request->input('city');
        $propertyType = $request->input('propertyType');

        $searchFor = trans('messages.land') . ", " . $searchFor;
        $siperfaqe = $request->input('siperfaqe-toka');
        $cmimi = $request->input('cmimi-toka');
        $cmimiMore = ($cmimi == "more") ? $cmimi . $saleStatus : $cmimi;
        $conditionSiperfaqe = '';
        $conditionCmimi = '';

        if ($request->statesearch == "opened") {
            switch ($siperfaqe) {
                case "0-1000":
                    $conditionSiperfaqe = 'AND Siperfaqia>0 AND Siperfaqia<=1000';
                    $searchFor = $searchFor . " 0-1000 m2, ";
                    break;
                case "1000-2000":
                    $conditionSiperfaqe = 'AND Siperfaqia>1000 AND Siperfaqia<=2000';
                    $searchFor = $searchFor . " 1000-2000 m2, ";
                    break;
                case "2000-5000":
                    $conditionSiperfaqe = 'AND Siperfaqia>2000 AND Siperfaqia<=5000';
                    $searchFor = $searchFor . " 2000-5000 m2, ";
                    break;
                case "more":
                    $conditionSiperfaqe = 'AND Siperfaqia>5000';
                    $searchFor = $searchFor . "> 5000 m2, ";
                    break;
                default:
                    $conditionSiperfaqe = '';
                    break;
            }
            switch ($cmimiMore) {
                case "0-50.000":
                    $conditionCmimi = 'AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)<=50000';
                    $searchFor = $searchFor . "0-50.000 Euro, ";
                    break;
                case "50.000-100.000":
                    $conditionCmimi = ' AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)>50000 AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)<=100000';
                    $searchFor = $searchFor . "50.000-100.000 Euro ";
                    break;
                case "moresale":
                    $conditionCmimi = 'AND cast(replace(replace(cmimi,",",""),".","") AS UNSIGNED)>100000';
                    $searchFor = $searchFor . ">100.000 Euro, ";
                default:
                    $conditionCmimi = '';
                    break;
            }
        }


        $searchResult = Cache::remember('searchResult5' . $gjuha . '_' . $conditionSiperfaqe . '_' . $conditionCmimi . '_' . $city . '_' . $forSale . '_' . $forRent . '_' . $propertyType, 15,
            function () use ($gjuha, $conditionSiperfaqe, $conditionCmimi, $city, $forSale, $forRent, $propertyType) {
                return DB::select('SELECT
									ArtID as id,
									ArtTitull_' . $gjuha . ' as title,
									case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
									Cmimi AS price,
									ArtPermbajtja_' . $gjuha . ' as body,
                                    Llojiprones_' . $gjuha . ' as type,
									Qyteti_' . $gjuha . ' as city,
									Forsale as sale,
									Forrent as rent,
									Siperfaqia as area,
									kodi
									from `cms_artikujt`
									WHERE
									ifnull(fshih,0)=0
									AND Qyteti_gj2 = ?
									AND (Forsale=? || Forrent =?)
									AND Llojiprones_gj2=?
									AND deleted_at IS NULL
                                        ' . $conditionSiperfaqe . '
                                        ' . $conditionCmimi . '
									ORDER BY priority DESC, ArtRend ASC, created_at DESC, id DESC', [$city, $forSale, $forRent, $propertyType]);
            });

        return $searchResult;
    }
}