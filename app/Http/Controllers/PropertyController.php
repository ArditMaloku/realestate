<?php

namespace App\Http\Controllers;

use App\Mail\RegisterPropertyMail;
use App\Repositories\PropertyRepo;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;
use Storage;

class PropertyController extends Controller
{
    private $repo;

    public function __construct(PropertyRepo $propertyRepo)
    {
        $this->repo = $propertyRepo;
    }

    /**
     * Show property details in English.
     *
     * @param $title
     * @param $id
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function enShow($title, $id, Request $request)
    {
// 		Cache::flush();
		
		if(strpos($title, '_') > 0 && strpos($title, '.html') > 0)
		{
			return $this->redirectArticle("en",$id);
		}
		
        App::setLocale("en");

        $pronat = Cache::remember('pronaen' . $id, 15, function () use ($id) {
            return DB::select('select ArtID as id, ArtTitull_gj2 as title,  ArtTitull_gj1 as alternatetitle, kati, case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
					Cmimi AS price, Meta_gj2 as meta_desc, Keywords_gj2 as keywords,ArtPermbajtja_gj2 as body,Llojiprones_gj2 as type, Qyteti_gj2 as city,
					Forsale as sale, Forrent as rent, Siperfaqia as area, planimetri as planimetri,
					dhomaGjumi as rooms, tualete as bathrooms,  fshih as fshih,kodi,koordinata,
					ballkone as balcony, parking as parking, shperndarje as ambients, lartesi as height, adresa as address
					from `cms_artikujt` WHERE `ArtID`=? AND deleted_at IS NULL', [
                $id
            ]);
        });

        $prona = $pronat[0] ?? null;

        if (!$prona) {
            //Log::error($message);
            abort(404);
        }

        if ($prona->rent == 1) {
            $action = "rent";
        } else {
            $action = "sale";
        }

        //Redirect if property is marked for HIDE.
        if ($prona->fshih == 1) {
            $perme = ($prona->sale) ? "for" : "for";
            return redirect()->route('category-cities', ['in' => 'in', 'perme' => $perme, 'lang' => 'en', 'propertyType' => strtolower($prona->type), 'city' => $prona->city, 'action' => $action],301);
        }

        $similarPropertiesQuery = 'select
				ArtID as id,
				ArtTitull_gj2 as title,case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
				Cmimi AS price,
				Keywords_gj2 as keywords,
				ArtPermbajtja_gj2 as body,
				Llojiprones_gj2 as typeselect,
				Qyteti_gj2 as city,
				Forsale as sale,
				Forrent as rent,
				Siperfaqia as area,
				kodi as kodiprones,
                kodi
				from `cms_artikujt`
				WHERE
				ifnull(fshih,0)=0 AND
				Llojiprones_gj2 ="' . $prona->type . '"
				AND
				Qyteti_gj2 ="' . $prona->city . '" AND deleted_at IS NULL';

        $similarPropertiesQuery .= ($prona->sale) ? " AND Forsale=" . $prona->sale : '';
        $similarPropertiesQuery .= ($prona->rent) ? " AND Forrent=" . $prona->rent : '';
        $similarPropertiesQuery .= ($prona->type == "Apartment" || $prona->type == "Villa") ? " AND dhomaGjumi=" . $prona->rooms : '';

        $similarPropertiesQuery .= ($prona->type == "Office" && $prona->area > 0 && $prona->area <= 50) ? " AND Siperfaqia BETWEEN 0 AND 50" : '';
        $similarPropertiesQuery .= ($prona->type == "Office" && $prona->area > 50 && $prona->area <= 100) ? " AND Siperfaqia BETWEEN 50 AND 100" : '';
        $similarPropertiesQuery .= ($prona->type == "Office" && $prona->area > 100 && $prona->area <= 200) ? " AND Siperfaqia BETWEEN 100 AND 200" : '';
        $similarPropertiesQuery .= ($prona->type == "Office" && $prona->area > 200) ? " AND Siperfaqia > 200" : '';

        $similarPropertiesQuery .= ($prona->type == "Warehouse" && $prona->area > 0 && $prona->area <= 500) ? " AND Siperfaqia BETWEEN 0 AND 500" : '';
        $similarPropertiesQuery .= ($prona->type == "Warehouse" && $prona->area > 500 && $prona->area <= 1000) ? " AND Siperfaqia BETWEEN 500 AND 1000" : '';
        $similarPropertiesQuery .= ($prona->type == "Warehouse" && $prona->area > 1000 && $prona->area <= 2000) ? " AND Siperfaqia BETWEEN 1000 AND 2000" : '';
        $similarPropertiesQuery .= ($prona->type == "Warehouse" && $prona->area > 2000) ? " AND Siperfaqia > 200" : '';

        $similarPropertiesQuery .= ($prona->type == "Land" && $prona->area > 0 && $prona->area <= 1000) ? " AND Siperfaqia BETWEEN 0 AND 1000" : '';
        $similarPropertiesQuery .= ($prona->type == "Land" && $prona->area > 1000 && $prona->area <= 2000) ? " AND Siperfaqia BETWEEN 1000 AND 2000" : '';
        $similarPropertiesQuery .= ($prona->type == "Land" && $prona->area > 2000 && $prona->area <= 5000) ? " AND Siperfaqia BETWEEN 2000 AND 5000" : '';
        $similarPropertiesQuery .= ($prona->type == "Land" && $prona->area > 5000) ? " AND Siperfaqia > 5000" : '';

        $similarPropertiesQuery .= ' ORDER BY RAND()
				LIMIT 0,1000';

        $similarProperties = Cache::remember('PropertyController_similarPropertiesshow' . $id, 15, function () use ($similarPropertiesQuery, $prona) {
            $results = DB::select($similarPropertiesQuery);

            $basePrice = (float)clean_price($prona->price);

            return $this->repo->getSimilarPrices($results, $basePrice, !!$prona->rent)->take(8)->toArray();
        });

        $pictures = DB::select('SELECT SkedarPath_gj1 as path FROM `cms_skedaret` WHERE SkedarArtID = ?', [$id]);

        $popUpImages = [];

        foreach ($pictures as $pic) {
            $popUpImages[] = [
                'src' => url('/thumbs/640x/skedaret/' . $pic->path)
            ];
        }


		if($prona->rooms<1)
		{
			$propertySelection = $this->findSelection($prona->area,$prona->type);
		}else{
			$propertySelection = $this->findSelection($prona->rooms,$prona->type);			
		}


        return view('contents.property.show', [
	        'propertySelection' => $propertySelection,            
	        'prona' => $prona,
            'similar' => $similarProperties,
            'pics' => $pictures,
            'popUpImages' => json_encode($popUpImages),
        ]);
    }

    /**
     * Show property details in Albanian.
     *
     * @param $title
     * @param $id
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function sqShow($title, $id, Request $request)
    {
// 	    Cache::flush();
	    
        if (Str::contains(strtolower($title), ['apartment', 'warehouse', 'office', 'villa', 'commercial', 'land'])) {
            return redirect()->to('/', 301);
        }
        
        if(strpos($title, '_') > 0 && strpos($title, '.html') > 0)
		{
			return $this->redirectArticle("sq",$id);
		}

		
        App::setLocale("sq");
        $pronat = Cache::remember('pronashowsq' . $id, 15, function () use ($id) {
            return DB::select('select ArtID as id, ArtTitull_gj1 as title, ArtTitull_gj2 as alternatetitle, kati,
					case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic, Cmimi AS price, Meta_gj1 as meta_desc, Keywords_gj1 as keywords,ArtPermbajtja_gj1 as body,Llojiprones_gj1 as type,
					Qyteti_gj1 as city, Forsale as sale, Forrent as rent, Siperfaqia as area, planimetri as planimetri, dhomaGjumi as rooms, tualete as bathrooms,
					ballkone as balcony, parking as parking, shperndarje as ambients, lartesi as height, adresa as address, fshih as fshih,kodi,koordinata from `cms_artikujt` WHERE `ArtID`=? AND deleted_at IS NULL', [
                $id
            ]);
        });

        $prona = $pronat[0] ?? null;

        if (!$prona) {
            abort(404);
        }

        if ($prona->rent == 1) {
            $action = "qera";
        } else {
            $action = "shitje";
        }

        //Redirect if property is marked for HIDE.
        if ($prona->fshih == 1) {
            $perme = ($prona->sale) ? "ne" : "me";
            return redirect()->route('category-cities', ['in' => 'ne', 'perme' => $perme, 'lang' => 'sq', 'propertyType' => strtolower($prona->type), 'city' => $prona->city, 'action' => $action],301);
        }

        $similarPropertiesQuery = 'select
				ArtID as id,
				ArtTitull_gj1 as title,
				case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
				Cmimi AS price,
				Keywords_gj1 as keywords,
				ArtPermbajtja_gj1 as body,
				Tipi as type,
				Qyteti_gj1 as city,
				Forsale as sale,
				Forrent as rent,
				Siperfaqia as area,
				kodi as kodiprones,
                kodi
				from `cms_artikujt`
				WHERE
				IFNULL(fshih,0)=0 AND
				Llojiprones_gj1="' . $prona->type . '"
				AND
				Qyteti_gj1 ="' . $prona->city . '"
				AND ifnull(fshih,0)=0';

        $similarPropertiesQuery .= ($prona->sale) ? " AND Forsale=" . $prona->sale : '';
        $similarPropertiesQuery .= ($prona->rent) ? " AND Forrent=" . $prona->rent : '';
        $similarPropertiesQuery .= ($prona->type == "Apartament" || $prona->type == "Vile") ? " AND dhomaGjumi=" . $prona->rooms : '';

        $similarPropertiesQuery .= ($prona->type == "Zyre" && $prona->area > 0 && $prona->area <= 50) ? " AND Siperfaqia BETWEEN 0 AND 50" : '';
        $similarPropertiesQuery .= ($prona->type == "Zyre" && $prona->area > 50 && $prona->area <= 100) ? " AND Siperfaqia BETWEEN 50 AND 100" : '';
        $similarPropertiesQuery .= ($prona->type == "Zyre" && $prona->area > 100 && $prona->area <= 200) ? " AND Siperfaqia BETWEEN 100 AND 200" : '';
        $similarPropertiesQuery .= ($prona->type == "Zyre" && $prona->area > 200) ? " AND Siperfaqia > 200" : '';

        $similarPropertiesQuery .= ($prona->type == "Magazine-Depo" && $prona->area > 0 && $prona->area <= 500) ? " AND Siperfaqia BETWEEN 0 AND 500" : '';
        $similarPropertiesQuery .= ($prona->type == "Magazine-Depo" && $prona->area > 500 && $prona->area <= 1000) ? " AND Siperfaqia BETWEEN 500 AND 1000" : '';
        $similarPropertiesQuery .= ($prona->type == "Magazine-Depo" && $prona->area > 1000 && $prona->area <= 2000) ? " AND Siperfaqia BETWEEN 1000 AND 2000" : '';
        $similarPropertiesQuery .= ($prona->type == "Magazine-Depo" && $prona->area > 2000) ? " AND Siperfaqia > 200" : '';

        $similarPropertiesQuery .= ($prona->type == "Toke" && $prona->area > 0 && $prona->area <= 1000) ? " AND Siperfaqia BETWEEN 0 AND 1000" : '';
        $similarPropertiesQuery .= ($prona->type == "Toke" && $prona->area > 1000 && $prona->area <= 2000) ? " AND Siperfaqia BETWEEN 1000 AND 2000" : '';
        $similarPropertiesQuery .= ($prona->type == "Toke" && $prona->area > 2000 && $prona->area <= 5000) ? " AND Siperfaqia BETWEEN 2000 AND 5000" : '';
        $similarPropertiesQuery .= ($prona->type == "Toke" && $prona->area > 5000) ? " AND Siperfaqia > 5000" : '';

        $similarPropertiesQuery .= ' ORDER BY RAND()
				LIMIT 0,1000';

        $similarProperties = Cache::remember('similarPropertiesshowsq' . $id, 15, function () use ($similarPropertiesQuery, $prona) {
            $results = DB::select($similarPropertiesQuery);

            $basePrice = (float)clean_price($prona->price);

            return $this->repo->getSimilarPrices($results, $basePrice, !!$prona->rent)->take(8)->toArray();
        });

        $pictures = DB::select('SELECT SkedarPath_gj1 as path FROM `cms_skedaret` WHERE SkedarArtID = ?', [$id]);

        $popUpImages = [];

        foreach ($pictures as $pic) {
            $popUpImages[] = [
                'src' => url('/thumbs/640x/skedaret/' . $pic->path)
            ];
        }

		if($prona->rooms<1)
		{
			$propertySelection = $this->findSelection($prona->area,$prona->type);
		}else{
			$propertySelection = $this->findSelection($prona->rooms,$prona->type);			
		}

        return view('contents.property.show', [
	        'propertySelection' => $propertySelection,
            'prona' => $prona,
            'similar' => $similarProperties,
            'pics' => $pictures,
            'popUpImages' => json_encode($popUpImages),
        ]);
    }

    /**
     * Show properties by category.
     *
     * @param $lang
     * @param $propertyType
     * @param $perme
     * @param $action
     * @param $in
     * @param $city
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function category($lang, $propertyType, $perme, $action, $in, $city, $page = 1, Request $request)
    {
	    Cache::flush();
	    
        if ($lang === 'sq' && Str::contains(strtolower($propertyType), ['apartment', 'warehouse', 'office', 'villa', 'commercial', 'land'])) {
            return redirect()->to('/', 301);
        }
		
		if($lang=='sq' && ($perme=='for' || $action=='rent' || $action=='sale' || $in=='in'))
		{
			$in = 'ne';
			$perme = 'me';
			if($action=='shitje')
			{
				$perme = 'ne';
			}
			if($action=='sale')
			{
				$perme = 'ne';
				$action = 'shitje';
			}elseif($action=='rent')
			{
				$action = 'qera';
			}
			return redirect()->route('category-cities', ['in' => $in, 'perme' => $perme, 'lang' => $lang, 'propertyType' => strtolower($propertyType), 'city' => $city, 'action' => $action]);
		}
		
		if($lang=='en' && ($perme=='ne' || $perme=='me' || $action=='qera' || $action=='shitje' || $in=='ne'))
		{
			$in = 'in';
			$perme = 'for';
			if($action=='shitje')
			{
				$action = 'sale';
			}elseif($action=='qera')
			{
				$action = 'rent';
			}
			return redirect()->route('category-cities', ['in' => $in, 'perme' => $perme, 'lang' => $lang, 'propertyType' => strtolower($propertyType), 'city' => $city, 'action' => $action]);
		}
		
		$descriptionP1 = '';
		$descriptionP2 = '';

        App::setLocale($lang ?? 'en');

        $perme = trim($perme);
        $permefor = trim($request->input('permefor'));
        $perme = ($perme == "" && $permefor != "") ? $permefor : $perme;
        $propertyType = ucfirst($propertyType);
        $city = ucfirst($city);
		
		if($action=='shitje' || $action=='sale')
		{
			$descriptionP1 = trans('messages.cat-'.strtolower($propertyType).'-shitje-descriptionp1');
			$descriptionP2 = trans('messages.cat-'.strtolower($propertyType).'-shitje-descriptionp2');	
		}else
		{
			$descriptionP1 = trans('messages.cat-'.strtolower($propertyType).'-descriptionp1');
			$descriptionP2 = trans('messages.cat-'.strtolower($propertyType).'-descriptionp2');
		}
		
		$descriptionP1 = str_ireplace('{action}', $request->action, $descriptionP1);
		$descriptionP1 = str_ireplace('{city}', $request->city, $descriptionP1);			
		$descriptionP2 = str_ireplace('{action}', $request->action, $descriptionP2);
		$descriptionP2 = str_ireplace('{city}', $request->city, $descriptionP2);			
		
        try {
            if ($propertyType == "Apartment" || $propertyType == "Apartament") {

				
//                 list($propertiesFirstColumn, $propertiesSecondColumn, $propertiesThirdColumn, $propertiesFourthColumn, $propertiesFifthColumn) = $this->repo->categoryApartment($request);
				
				$properties = $this->repo->categoryApartment($request);

                $titleFirstColumn = trans('messages.onebedroom');
                $titleSecondColumn = trans('messages.twobedroom');
                $titleThirdColumn = trans('messages.threebedroom');
                $titleFourthColumn = trans('messages.morethanthreebedroom');
                $titleFifthColumn = '';

                $linkFirstColumn = route('category-cities-selection-page', [
                    'lang' => $request->lang,
                    'propertyType' => $request->propertyType,
                    'propertySelection' => trans('messages.onebedroomlink'),
                    'perme' => $request->perme,
                    'action' => $request->action,
                    'in' => $request->in,
                    'city' => $request->city
                ]);
                $linkSecondColumn = route('category-cities-selection-page', [
                    'lang' => $request->lang,
                    'propertyType' => $request->propertyType,
                    'propertySelection' => trans('messages.twobedroomlink'),
                    'perme' => $request->perme,
                    'action' => $request->action,
                    'in' => $request->in,
                    'city' => $request->city
                ]);
                $linkThirdColumn = route('category-cities-selection-page', [
                    'lang' => $request->lang,
                    'propertyType' => $request->propertyType,
                    'propertySelection' => trans('messages.threebedroomlink'),
                    'perme' => $request->perme,
                    'action' => $request->action,
                    'in' => $request->in,
                    'city' => $request->city
                ]);
                $linkFourthColumn = route('category-cities-selection-page', [
                    'lang' => $request->lang,
                    'propertyType' => $request->propertyType,
                    'propertySelection' => trans('messages.morethanthreebedroomlink'),
                    'perme' => $request->perme,
                    'action' => $request->action,
                    'in' => $request->in,
                    'city' => $request->city
                ]);

                $titleMore = trans('messages.moreapartments');
                $classNrColumns = "four_columns_template";

            } elseif ($propertyType == "Villa" || $propertyType == "Vile") {
//                 list($propertiesFirstColumn, $propertiesSecondColumn, $propertiesThirdColumn, $propertiesFourthColumn, $propertiesFifthColumn) = $this->repo->categoryVilla($request);
				$properties = $this->repo->categoryVilla($request);

				
                $titleFirstColumn = trans('messages.tworoomsvilla');
                $titleSecondColumn = trans('messages.fourroomsvilla');
                $titleThirdColumn = trans('messages.fiveroomsvilla');
                $titleFourthColumn = trans('messages.moreroomsvilla');
                $titleFifthColumn = trans('messages.morethanfivebedroom');

                $titleMore = trans('messages.morevillas');
                $classNrColumns = "four_columns_template";

                $linkFirstColumn = route('category-cities-selection-page', [
                    'lang' => $request->lang,
                    'propertyType' => $request->propertyType,
                    'propertySelection' => trans('messages.tworoomsvillalink'),
                    'perme' => $request->perme,
                    'action' => $request->action,
                    'in' => $request->in,
                    'city' => $request->city
                ]);
                $linkSecondColumn = route('category-cities-selection-page', [
                    'lang' => $request->lang,
                    'propertyType' => $request->propertyType,
                    'propertySelection' => trans('messages.fourroomsvillalink'),
                    'perme' => $request->perme,
                    'action' => $request->action,
                    'in' => $request->in,
                    'city' => $request->city
                ]);
                $linkThirdColumn = route('category-cities-selection-page', [
                    'lang' => $request->lang,
                    'propertyType' => $request->propertyType,
                    'propertySelection' => trans('messages.fiveroomsvillalink'),
                    'perme' => $request->perme,
                    'action' => $request->action,
                    'in' => $request->in,
                    'city' => $request->city
                ]);
                $linkFourthColumn = route('category-cities-selection-page', [
                    'lang' => $request->lang,
                    'propertyType' => $request->propertyType,
                    'propertySelection' => trans('messages.moreroomsvillalink'),
                    'perme' => $request->perme,
                    'action' => $request->action,
                    'in' => $request->in,
                    'city' => $request->city
                ]);
            } elseif ($propertyType == "Office" || $propertyType == "Zyre") {

//                 list($propertiesFirstColumn, $propertiesSecondColumn, $propertiesThirdColumn, $propertiesFourthColumn, $propertiesFifthColumn) = $this->repo->categoryOffice($request);
				$properties = $this->repo->categoryOffice($request);

                $titleFirstColumn = '0-50 m2';
                $titleSecondColumn = '50-100 m2';
                $titleThirdColumn = '100-200 m2';
                $titleFourthColumn = '200+ m2';
                $titleFifthColumn = trans('messages.morethanthreebedroom');

                $titleMore = trans('messages.moreoffice');
                $classNrColumns = "four_columns_template";


                $linkFirstColumn = route('category-cities-selection-page', [
                    'lang' => $request->lang,
                    'propertyType' => $request->propertyType,
                    'propertySelection' => trans('messages.from0to50m2squarelink'),
                    'perme' => $request->perme,
                    'action' => $request->action,
                    'in' => $request->in,
                    'city' => $request->city
                ]);
                $linkSecondColumn = route('category-cities-selection-page', [
                    'lang' => $request->lang,
                    'propertyType' => $request->propertyType,
                    'propertySelection' => trans('messages.from50to100m2squarelink'),
                    'perme' => $request->perme,
                    'action' => $request->action,
                    'in' => $request->in,
                    'city' => $request->city
                ]);
                $linkThirdColumn = route('category-cities-selection-page', [
                    'lang' => $request->lang,
                    'propertyType' => $request->propertyType,
                    'propertySelection' => trans('messages.from100to200m2squarelink'),
                    'perme' => $request->perme,
                    'action' => $request->action,
                    'in' => $request->in,
                    'city' => $request->city
                ]);
                $linkFourthColumn = route('category-cities-selection-page', [
                    'lang' => $request->lang,
                    'propertyType' => $request->propertyType,
                    'propertySelection' => trans('messages.morethan200m2squarelink'),
                    'perme' => $request->perme,
                    'action' => $request->action,
                    'in' => $request->in,
                    'city' => $request->city
                ]);

            } elseif ($propertyType == "Warehouse" || $propertyType == "Magazine") {

//                 list($propertiesFirstColumn, $propertiesSecondColumn, $propertiesThirdColumn, $propertiesFourthColumn, $propertiesFifthColumn) = $this->repo->categoryWarehouse($request);
                $properties = $this->repo->categoryWarehouse($request);

                $titleFirstColumn = '0-500 m2';
                $titleSecondColumn = '500-1000 m2';
                $titleThirdColumn = '1000-2000 m2';
                $titleFourthColumn = '2000+ m2';
                $titleFifthColumn = trans('messages.morethanthreebedroom');

                $titleMore = trans('messages.morewarehouse');
                $classNrColumns = "four_columns_template";


                $linkFirstColumn = route('category-cities-selection-page', [
                    'lang' => $request->lang,
                    'propertyType' => $request->propertyType,
                    'propertySelection' => trans('messages.from0to500m2squarelink'),
                    'perme' => $request->perme,
                    'action' => $request->action,
                    'in' => $request->in,
                    'city' => $request->city
                ]);
                $linkSecondColumn = route('category-cities-selection-page', [
                    'lang' => $request->lang,
                    'propertyType' => $request->propertyType,
                    'propertySelection' => trans('messages.from500to1000m2squarelink'),
                    'perme' => $request->perme,
                    'action' => $request->action,
                    'in' => $request->in,
                    'city' => $request->city
                ]);
                $linkThirdColumn = route('category-cities-selection-page', [
                    'lang' => $request->lang,
                    'propertyType' => $request->propertyType,
                    'propertySelection' => trans('messages.from1000to2000m2squarelink'),
                    'perme' => $request->perme,
                    'action' => $request->action,
                    'in' => $request->in,
                    'city' => $request->city
                ]);
                $linkFourthColumn = route('category-cities-selection-page', [
                    'lang' => $request->lang,
                    'propertyType' => $request->propertyType,
                    'propertySelection' => trans('messages.morethan2000m2squarelink'),
                    'perme' => $request->perme,
                    'action' => $request->action,
                    'in' => $request->in,
                    'city' => $request->city
                ]);
            } elseif ($propertyType == "Land" || $propertyType == "Toke") {

//                 list($propertiesFirstColumn, $propertiesSecondColumn, $propertiesThirdColumn, $propertiesFourthColumn, $propertiesFifthColumn) = $this->repo->categoryLand($request);
				$properties = $this->repo->categoryLand($request);

                $titleFirstColumn = '0-1000 m2';
                $titleSecondColumn = '1000-2000 m2';
                $titleThirdColumn = '2000-5000 m2';
                $titleFourthColumn = '5000+ m2';
                $titleFifthColumn = trans('messages.morethanthreebedroom');

                $titleMore = trans('messages.moreland');
                $classNrColumns = "four_columns_template";

                $linkFirstColumn = route('category-cities-selection-page', [
                    'lang' => $request->lang,
                    'propertyType' => $request->propertyType,
                    'propertySelection' => trans('messages.from0to1000m2squarelink'),
                    'perme' => $request->perme,
                    'action' => $request->action,
                    'in' => $request->in,
                    'city' => $request->city
                ]);
                $linkSecondColumn = route('category-cities-selection-page', [
                    'lang' => $request->lang,
                    'propertyType' => $request->propertyType,
                    'propertySelection' => trans('messages.from1000to2000m2squarelink'),
                    'perme' => $request->perme,
                    'action' => $request->action,
                    'in' => $request->in,
                    'city' => $request->city
                ]);
                $linkThirdColumn = route('category-cities-selection-page', [
                    'lang' => $request->lang,
                    'propertyType' => $request->propertyType,
                    'propertySelection' => trans('messages.from2000to5000m2squarelink'),
                    'perme' => $request->perme,
                    'action' => $request->action,
                    'in' => $request->in,
                    'city' => $request->city
                ]);
                $linkFourthColumn = route('category-cities-selection-page', [
                    'lang' => $request->lang,
                    'propertyType' => $request->propertyType,
                    'propertySelection' => trans('messages.morethan5000m2squarelink'),
                    'perme' => $request->perme,
                    'action' => $request->action,
                    'in' => $request->in,
                    'city' => $request->city
                ]);
            } elseif ($propertyType == "Commercial" || $propertyType == "Ambient tregtar") {

//                 list($propertiesFirstColumn, $propertiesSecondColumn, $propertiesThirdColumn, $propertiesFourthColumn, $propertiesFifthColumn) = $this->repo->categoryCommercial($request);
				$properties = $this->repo->categoryCommercial($request);

                $titleFirstColumn = '0-50 m2';
                $titleSecondColumn = '50-100 m2';
                $titleThirdColumn = '100-200 m2';
                $titleFourthColumn = '200+ m2';
                $titleFifthColumn = trans('messages.morethanthreebedroom');

                $titleMore = trans('messages.morecommercial');
                $classNrColumns = "four_columns_template";
                $linkFirstColumn = route('category-cities-selection-page', [
                    'lang' => $request->lang,
                    'propertyType' => $request->propertyType,
                    'propertySelection' => trans('messages.from0to50m2squarelink'),
                    'perme' => $request->perme,
                    'action' => $request->action,
                    'in' => $request->in,
                    'city' => $request->city
                ]);
                $linkSecondColumn = route('category-cities-selection-page', [
                    'lang' => $request->lang,
                    'propertyType' => $request->propertyType,
                    'propertySelection' => trans('messages.from50to100m2squarelink'),
                    'perme' => $request->perme,
                    'action' => $request->action,
                    'in' => $request->in,
                    'city' => $request->city
                ]);
                $linkThirdColumn = route('category-cities-selection-page', [
                    'lang' => $request->lang,
                    'propertyType' => $request->propertyType,
                    'propertySelection' => trans('messages.from100to200m2squarelink'),
                    'perme' => $request->perme,
                    'action' => $request->action,
                    'in' => $request->in,
                    'city' => $request->city
                ]);
                $linkFourthColumn = route('category-cities-selection-page', [
                    'lang' => $request->lang,
                    'propertyType' => $request->propertyType,
                    'propertySelection' => trans('messages.morethan200m2squarelink'),
                    'perme' => $request->perme,
                    'action' => $request->action,
                    'in' => $request->in,
                    'city' => $request->city
                ]);
            }

            $actionMeta = $action;
            $actionMeta = ($actionMeta == "qera") ? "me qera" : $actionMeta;
            $actionMeta = ($actionMeta == "shitje") ? "ne shitje" : $actionMeta;
            
            $keywordsMeta = trans('messages.cat-' . strtolower($propertyType) . '-keywords');
			$keywordsMeta = str_ireplace('{action}', $actionMeta, $keywordsMeta);
			$keywordsMeta = str_ireplace('{city}', $city, $keywordsMeta);

			if($propertyType == "Vile")
			{
				$descriptionMeta = trans('messages.cat-' . strtolower($propertyType) . '-shitje-description');
			}else
			{
				$descriptionMeta = trans('messages.cat-' . strtolower($propertyType) . '-description');			
			}
			
            $descriptionMeta = str_ireplace('{action}', $actionMeta, $descriptionMeta);
            $descriptionMeta = str_ireplace('{city}', $city, $descriptionMeta);
            
            $titleMeta = trans('messages.cat-' . strtolower($propertyType) . '-title');
            $titleMeta = str_ireplace('{action}', $actionMeta, $titleMeta);
            $titleMeta = str_ireplace('{city}', $city, $titleMeta);
            
            $perPage = 40;
			$arrayTotalProperties = $properties;
			$pagination = new LengthAwarePaginator($arrayTotalProperties, count($arrayTotalProperties), $perPage, $page, ['path' => $request->url(), 'query' => $request->query()]);
			
			if (!empty($arrayTotalProperties)) {
	            $offset = ($page - 1) * $perPage;
	            $arrayTotalProperties = array_slice($arrayTotalProperties, $offset, $perPage);
	        } else {
		        if($page!=1)
		        {
	            	return redirect()->route('category-cities', ['in' => $in, 'perme' => $perme, 'lang' => $lang, 'propertyType' => strtolower($propertyType), 'city' => $city, 'action' => $action]);
	            }else{
					return response()->view('errors.' . '404', [], 404);
				}
	        }


            return view('contents.property.category', [
	            'pagination' => $pagination,
	            'properties' => $arrayTotalProperties,
	            'totalproperties' => count($arrayTotalProperties),
/*
                'propertiesFirstColumn' => $propertiesFirstColumn,
                'propertiesSecondColumn' => $propertiesSecondColumn,
                'propertiesThirdColumn' => $propertiesThirdColumn,
                'propertiesFourthColumn' => $propertiesFourthColumn,
                'propertiesFifthColumn' => $propertiesFifthColumn,
*/
                'titleFirstColumn' => $titleFirstColumn,
                'titleSecondColumn' => $titleSecondColumn,
                'titleThirdColumn' => $titleThirdColumn,
                'titleFourthColumn' => $titleFourthColumn,
                'linkFirstColumn' => $linkFirstColumn,
                'linkSecondColumn' => $linkSecondColumn,
                'linkThirdColumn' => $linkThirdColumn,
                'linkFourthColumn' => $linkFourthColumn,
                'titleFifthColumn' => $titleFifthColumn,
                'propertyType' => $propertyType,
                'action' => $action,
                'city' => $city,
                'perme' => $perme,
                'classnrcolumns' => $classNrColumns,
                'titleMore' => $titleMore,
/*
                'totalpropertiesFirstColumn' => ((count($propertiesFirstColumn) - 6) > 0) ? (count($propertiesFirstColumn) - 6) : 0,
                'totalpropertiesSecondColumn' => ((count($propertiesSecondColumn) - 6) > 0) ? (count($propertiesSecondColumn) - 6) : 0,
                'totalpropertiesThirdColumn' => ((count($propertiesThirdColumn) - 6) > 0) ? (count($propertiesThirdColumn) - 6) : 0,
                'totalpropertiesFourthColumn' => ((count($propertiesFourthColumn) - 6) > 0) ? (count($propertiesFourthColumn) - 6) : 0,
*/
                'descriptionMeta' => $descriptionMeta,
                'keywordsMeta' => $keywordsMeta,
                'descriptionP1' => $descriptionP1,
				'descriptionP2' => $descriptionP2,
				'titleMeta' => $titleMeta
            ]);

        } catch (\Exception $e) {
// 	        Storage::append('error500.txt', 'Error 500');
            return $e->getMessage();
        }
    }

    /**
     * List properties by category selection.
     *
     * @param Request $request
     * @param $lang
     * @param $propertyType
     * @param $propertySelection
     * @param $perme
     * @param $action
     * @param $in
     * @param $city
     * @param int $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function categorySelection(Request $request, $lang, $propertyType, $propertySelection, $perme, $action, $in, $city, $page = 1)
    {
	    Cache::flush();
	    
        if ($lang === 'sq' && Str::contains(strtolower($propertyType), ['apartment', 'warehouse', 'office', 'villa', 'commercial', 'land'])) {
            return redirect()->to('/', 301);
        }
        
        if($lang=='sq' && ($perme=='for' || $action=='rent' || $action=='sale' || $in=='in'))
		{
			$in = 'ne';
			$perme = 'me';
			if($action=='shitje')
			{
				$perme = 'ne';
			}
			if($action=='sale')
			{
				$perme = 'ne';
				$action = 'shitje';
			}elseif($action=='rent')
			{
				$action = 'qera';
			}
			return redirect()->route('category-cities-selection-page', ['in' => $in, 'perme' => $perme, 'propertySelection' => $propertySelection, 'lang' => $lang, 'propertyType' => strtolower($propertyType), 'city' => $city, 'action' => $action]);
		}
		
		if($lang=='en' && ($perme=='ne' || $perme=='me' || $action=='qera' || $action=='shitje' || $in=='ne'))
		{
			$in = 'in';
			$perme = 'for';
			if($action=='shitje')
			{
				$action = 'sale';
			}elseif($action=='qera')
			{
				$action = 'rent';
			}
			return redirect()->route('category-cities-selection-page', ['in' => $in, 'perme' => $perme, 'propertySelection' => $propertySelection, 'lang' => $lang, 'propertyType' => strtolower($propertyType), 'city' => $city, 'action' => $action]);
		}

		
        App::setLocale($lang ?? 'en');

        $perme = trim($perme);
        $permefor = trim($request->input('permefor'));
        $perme = ($perme == "" && $permefor != "") ? $permefor : $perme;
        $propertyType = ucfirst($propertyType);
        $city = ucfirst($city);

        $searchResult = [];
        $metaDescription = '';
		$titleMeta = '';
		$keywordsMeta = '';
		$descriptionBox = '';
		
        if ($propertyType == "Apartment" || $propertyType == "Apartament") {
	        
            $propertySelection = 0;
            $propertySelection = ($request->propertySelection == "1+1" || $request->propertySelection == "one+bedroom") ? 1 : $propertySelection;
            $propertySelection = ($request->propertySelection == "2+1" || $request->propertySelection == "two+bedroom") ? 2 : $propertySelection;
            $propertySelection = ($request->propertySelection == "3+1" || $request->propertySelection == "three+bedroom") ? 3 : $propertySelection;
            $propertySelection = ($request->propertySelection == "me+shume+se+3+dhoma" || $request->propertySelection == "more+than+three+bedroom") ? 4 : $propertySelection;
			
            $messageSelection = '';
            $messageSelection = ($propertySelection == 1) ? trans('messages.onebedroom') : $messageSelection;
            $messageSelection = ($propertySelection == 2) ? trans('messages.twobedroom') : $messageSelection;
            $messageSelection = ($propertySelection == 3) ? trans('messages.threebedroom') : $messageSelection;
            $messageSelection = ($propertySelection == 4) ? trans('messages.morethanthreebedroom') : $messageSelection;

            if($action=='shitje' || $action=='sale')
			{
				$keywordsMeta = ($propertySelection == 1) ? trans('messages.cat-'.strtolower($propertyType).'-onebedroom-shitje-keywords') : $keywordsMeta;
	            $keywordsMeta = ($propertySelection == 2) ? trans('messages.cat-'.strtolower($propertyType).'-twobedroom-shitje-keywords') : $keywordsMeta;
	            $keywordsMeta = ($propertySelection == 3) ? trans('messages.cat-'.strtolower($propertyType).'-threebedroom-shitje-keywords') : $keywordsMeta;
	            $keywordsMeta = ($propertySelection == 4) ? trans('messages.cat-'.strtolower($propertyType).'-morethanthreebedroom-shitje-keywords') : $keywordsMeta;

				
	            $metaDescription = ($propertySelection == 1) ? 'onebedroom-shitje' : $metaDescription;
				$metaDescription = ($propertySelection == 2) ? 'twobedroom-shitje' : $metaDescription;
				$metaDescription = ($propertySelection == 3) ? 'threebedroom-shitje' : $metaDescription;
	            $metaDescription = ($propertySelection == 4) ? 'morethanthreebedroom-shitje' : $metaDescription;

				
				$descriptionBox = ($propertySelection == 1) ? trans('messages.cat-'.strtolower($propertyType).'-onebedroom-shitje-description-box') : $descriptionBox;
				$descriptionBox = ($propertySelection == 2) ? trans('messages.cat-'.strtolower($propertyType).'-twobedroom-shitje-description-box') : $descriptionBox;
				$descriptionBox = ($propertySelection == 3) ? trans('messages.cat-'.strtolower($propertyType).'-threebedroom-shitje-description-box') : $descriptionBox;
				$descriptionBox = ($propertySelection == 4) ? trans('messages.cat-'.strtolower($propertyType).'-morethanthreebedroom-shitje-description-box') : $descriptionBox;
	        }else{
		        
		        $keywordsMeta = ($propertySelection == 1) ? trans('messages.cat-'.strtolower($propertyType).'-onebedroom-keywords') : $keywordsMeta;
	            $keywordsMeta = ($propertySelection == 2) ? trans('messages.cat-'.strtolower($propertyType).'-twobedroom-keywords') : $keywordsMeta;
	            $keywordsMeta = ($propertySelection == 3) ? trans('messages.cat-'.strtolower($propertyType).'-threebedroom-keywords') : $keywordsMeta;
	            $keywordsMeta = ($propertySelection == 4) ? trans('messages.cat-'.strtolower($propertyType).'-morethanthreebedroom-keywords') : $keywordsMeta;


		        $metaDescription = ($propertySelection == 1) ? 'onebedroom' : $metaDescription;
				$metaDescription = ($propertySelection == 2) ? 'twobedroom' : $metaDescription;
				$metaDescription = ($propertySelection == 3) ? 'threebedroom' : $metaDescription;
	            $metaDescription = ($propertySelection == 4) ? 'morethanthreebedroom' : $metaDescription;

				$descriptionBox = ($propertySelection == 1) ? trans('messages.cat-'.strtolower($propertyType).'-onebedroom-description-box') : $descriptionBox;
				$descriptionBox = ($propertySelection == 2) ? trans('messages.cat-'.strtolower($propertyType).'-twobedroom-description-box') : $descriptionBox;
				$descriptionBox = ($propertySelection == 3) ? trans('messages.cat-'.strtolower($propertyType).'-threebedroom-description-box') : $descriptionBox;
			    $descriptionBox = ($propertySelection == 4) ? trans('messages.cat-'.strtolower($propertyType).'-morethanthreebedroom-description-box') : $descriptionBox;
	        }
	            
          
			$titleMeta = ($propertySelection == 1) ? trans('messages.cat-'.strtolower($propertyType).'-onebedroom-meta-title') : $titleMeta;
            $titleMeta = ($propertySelection == 2) ? trans('messages.cat-'.strtolower($propertyType).'-twobedroom-meta-title') : $titleMeta;
            $titleMeta = ($propertySelection == 3) ? trans('messages.cat-'.strtolower($propertyType).'-threebedroom-meta-title') : $titleMeta;
            $titleMeta = ($propertySelection == 4) ? trans('messages.cat-'.strtolower($propertyType).'-morethanthreebedroom-meta-title') : $titleMeta;
            
            if($propertySelection==0)
            {
	            return redirect()->route('category-cities', ['in' => $in, 'perme' => $perme, 'lang' => $lang, 'propertyType' => strtolower($propertyType), 'city' => $city, 'action' => $action]);
            }
            
            $conditionDhomaGjumi = ($propertySelection < 4) ? "dhomaGjumi=" . $propertySelection : "dhomaGjumi>3";

            $searchResult = $this->repo->categorySelectionApartment($request, $conditionDhomaGjumi);

            $titleFirstColumn = trans('messages.onebedroom');
            $titleSecondColumn = trans('messages.twobedroom');
            $titleThirdColumn = trans('messages.threebedroom');
            $titleFourthColumn = trans('messages.morethanthreebedroom');
            $titleFifthColumn = '';
			
			
			
            $titleMore = trans('messages.moreapartments');
            $classNrColumns = "four_columns_template";

        } elseif ($propertyType == "Villa" || $propertyType == "Vile") {
	        $permeShqip = ($action == "rent") ? "me" : "ne";
			$permeEnglish = ($action == "rent") ? "for" : "for";

			if($request->propertySelection == "me+shume+se+4+dhoma")
			{
				return redirect('/sq/vile-me+shume+se+5+dhoma-'.$permeShqip.'-'.$action.'-ne-'.$city,301);
			}
			
			if($request->propertySelection == "more+than+4+bedrooms")
			{
				return redirect('/en/villa-more+than+5+bedrooms-'.$permeEnglish.'-'.$action.'-in-'.$city,301);				
			}
			
            $propertySelection = 1;
            $propertySelection = ($request->propertySelection == "2+dhoma" || $request->propertySelection == "2+bedrooms") ? 2 : $propertySelection;
            $propertySelection = ($request->propertySelection == "3+dhoma" || $request->propertySelection == "3+bedrooms") ? 2 : $propertySelection;
            $propertySelection = ($request->propertySelection == "4+dhoma" || $request->propertySelection == "4+bedrooms") ? 4 : $propertySelection;
            $propertySelection = ($request->propertySelection == "5+dhoma" || $request->propertySelection == "5+bedrooms") ? 5 : $propertySelection;
            $propertySelection = ($request->propertySelection == "me+shume+se+5+dhoma" || $request->propertySelection == "more+than+5+bedrooms") ? 6 : $propertySelection;

            $messageSelection = '';
            $messageSelection = ($propertySelection == 2) ? trans('messages.tworoomsvilla') : $messageSelection;
            $messageSelection = ($propertySelection == 4) ? trans('messages.fourroomsvilla') : $messageSelection;
            $messageSelection = ($propertySelection == 5) ? trans('messages.fiveroomsvilla') : $messageSelection;
            $messageSelection = ($propertySelection == 6) ? trans('messages.bigvilla') : $messageSelection;

            $metaDescription = ($propertySelection == 2) ? 'tworoomsvilla' : $metaDescription;
            $metaDescription = ($propertySelection == 4) ? 'fourroomsvilla' : $metaDescription;
            $metaDescription = ($propertySelection == 5) ? 'fiveroomsvilla' : $metaDescription;
            $metaDescription = ($propertySelection == 6) ? 'moreroomsvilla' : $metaDescription;
            
            $keywordsMeta = ($propertySelection == 2) ? trans('messages.cat-'.strtolower($propertyType).'-tworooms-keywords') : $keywordsMeta;
            $keywordsMeta = ($propertySelection == 4) ? trans('messages.cat-'.strtolower($propertyType).'-fourrooms-keywords') : $keywordsMeta;
            $keywordsMeta = ($propertySelection == 5) ? trans('messages.cat-'.strtolower($propertyType).'-fiverooms-keywords') : $keywordsMeta;
            $keywordsMeta = ($propertySelection == 6) ? trans('messages.cat-'.strtolower($propertyType).'-morerooms-keywords') : $keywordsMeta;
            
            $titleMeta = ($propertySelection == 2) ? trans('messages.cat-'.strtolower($propertyType).'-tworooms-meta-title') : $titleMeta;
            $titleMeta = ($propertySelection == 4) ? trans('messages.cat-'.strtolower($propertyType).'-fourrooms-meta-title') : $titleMeta;
            $titleMeta = ($propertySelection == 5) ? trans('messages.cat-'.strtolower($propertyType).'-fiverooms-meta-title') : $titleMeta;
            $titleMeta = ($propertySelection == 6) ? trans('messages.cat-'.strtolower($propertyType).'-morerooms-meta-title') : $titleMeta;

			$descriptionBox = ($propertySelection == 2) ? trans('messages.cat-'.strtolower($propertyType).'-tworooms-description-box') : $descriptionBox;
            $descriptionBox = ($propertySelection == 4) ? trans('messages.cat-'.strtolower($propertyType).'-fourrooms-description-box') : $descriptionBox;
            $descriptionBox = ($propertySelection == 5) ? trans('messages.cat-'.strtolower($propertyType).'-fiverooms-description-box') : $descriptionBox;
            $descriptionBox = ($propertySelection == 6) ? trans('messages.cat-'.strtolower($propertyType).'-morerooms-description-box') : $descriptionBox;

            $conditionDhomaGjumi = ($propertySelection < 6) ? "dhomaGjumi=" . $propertySelection : "dhomaGjumi>5";

            $searchResult = $this->repo->categorySelectionVilla($request, $conditionDhomaGjumi);

            $titleFirstColumn = trans('messages.tworoomsvilla');
            $titleSecondColumn = trans('messages.fourroomsvilla');
            $titleThirdColumn = trans('messages.fiveroomsvilla');
            $titleFourthColumn = trans('messages.moreroomsvilla');
            $titleFifthColumn = '';

            $titleMore = trans('messages.morevillas');
            $classNrColumns = "four_columns_template";

        } elseif ($propertyType == "Office" || $propertyType == "Zyre") {
            $propertySelection = 1;
            $propertySelection = ($request->propertySelection == "nga+0+deri+50+m2" || $request->propertySelection == "from+0+to+50+m2") ? "0 and 50" : $propertySelection;
            $propertySelection = ($request->propertySelection == "nga+50+deri+100+m2" || $request->propertySelection == "from+50+to+100+m2") ? "50 and 100" : $propertySelection;
            $propertySelection = ($request->propertySelection == "nga+100+deri+200+m2" || $request->propertySelection == "from+100+to+200+m2") ? "100 and 200" : $propertySelection;
            $propertySelection = ($request->propertySelection == "me+shume+se+200+m2" || $request->propertySelection == "more+than+200+m2") ? 5 : $propertySelection;

            $conditionSiperfaqe = ($propertySelection != 5) ? "Siperfaqia BETWEEN " . $propertySelection : "Siperfaqia>200";

            $messageSelection = '';
            $messageSelection = ($propertySelection == "0 and 50") ? trans('messages.from0to50m2square') : $messageSelection;
            $messageSelection = ($propertySelection == "50 and 100") ? trans('messages.from50to100m2square') : $messageSelection;
            $messageSelection = ($propertySelection == "100 and 200") ? trans('messages.from100to200m2square') : $messageSelection;
            $messageSelection = ($propertySelection == 5) ? trans('messages.morethan200m2square') : $messageSelection;


            $metaDescription = ($propertySelection == "0 and 50") ? 'from0to50m2square' : $metaDescription;
            $metaDescription = ($propertySelection == "50 and 100") ? 'from50to100m2square' : $metaDescription;
            $metaDescription = ($propertySelection == "100 and 200") ? 'from100to200m2square' : $metaDescription;
            $metaDescription = ($propertySelection == 5) ? 'morethan200m2square' : $metaDescription;

            $searchResult = $this->repo->categorySelectionOffice($request, $conditionSiperfaqe);

            $titleFirstColumn = '0-50 m2';
            $titleSecondColumn = '50-100 m2';
            $titleThirdColumn = '100-200 m2';
            $titleFourthColumn = '200+ m2';
            $titleFifthColumn = trans('messages.morethanthreebedroom');

            $titleMore = trans('messages.moreoffice');
            $classNrColumns = "four_columns_template";

        } elseif ($propertyType == "Warehouse" || $propertyType == "Magazine") {
            $propertySelection = "0 and 500";
            $propertySelection = ($request->propertySelection == "nga+0+deri+500+m2" || $request->propertySelection == "from+0+to+500+m2") ? "0 and 500" : $propertySelection;
            $propertySelection = ($request->propertySelection == "nga+500+deri+1000+m2" || $request->propertySelection == "from+500+to+1000+m2") ? "500 and 1000" : $propertySelection;
            $propertySelection = ($request->propertySelection == "nga+1000+deri+2000+m2" || $request->propertySelection == "from+1000+to+2000+m2") ? "1000 and 2000" : $propertySelection;
            $propertySelection = ($request->propertySelection == "me+shume+se+2000+m2" || $request->propertySelection == "more+than+2000+m2") ? 5 : $propertySelection;

            $conditionSiperfaqe = ($propertySelection != 5) ? "Siperfaqia BETWEEN " . $propertySelection : "Siperfaqia>2000";

            $messageSelection = '';
            $messageSelection = ($propertySelection == "0 and 500") ? trans('messages.from0to500m2square') : $messageSelection;
            $messageSelection = ($propertySelection == "500 and 1000") ? trans('messages.from500to1000m2square') : $messageSelection;
            $messageSelection = ($propertySelection == "1000 and 2000") ? trans('messages.from1000to2000m2square') : $messageSelection;
            $messageSelection = ($propertySelection == 5) ? trans('messages.morethan2000m2square') : $messageSelection;

            $metaDescription = ($propertySelection == "0 and 50") ? 'from0to500m2square' : $metaDescription;
            $metaDescription = ($propertySelection == "50 and 100") ? 'from500to1000m2square' : $metaDescription;
            $metaDescription = ($propertySelection == "100 and 200") ? 'from1000to2000m2square' : $metaDescription;
            $metaDescription = ($propertySelection == 5) ? 'morethan2000m2square' : $metaDescription;

            $searchResult = $this->repo->categorySelectionWarehouse($request, $conditionSiperfaqe);

            $titleFirstColumn = '0-500 m2';
            $titleSecondColumn = '500-1000 m2';
            $titleThirdColumn = '1000-2000 m2';
            $titleFourthColumn = '2000+ m2';
            $titleFifthColumn = trans('messages.morethanthreebedroom');

            $titleMore = trans('messages.morewarehouse');
            $classNrColumns = "four_columns_template";

        } elseif ($propertyType == "Land" || $propertyType == "Toke") {
            $propertySelection = "0 and 1000";
            $propertySelection = ($request->propertySelection == "nga+0+deri+1000+m2" || $request->propertySelection == "from+0+to+1000+m2") ? "0 and 1000" : $propertySelection;
            $propertySelection = ($request->propertySelection == "nga+1000+deri+2000+m2" || $request->propertySelection == "from+1000+to+2000+m2") ? "1000 and 2000" : $propertySelection;
            $propertySelection = ($request->propertySelection == "nga+2000+deri+5000+m2" || $request->propertySelection == "from+2000+to+5000+m2") ? "2000 and 5000" : $propertySelection;
            $propertySelection = ($request->propertySelection == "me+shume+se+5000+m2" || $request->propertySelection == "more+than+5000+m2") ? 5 : $propertySelection;

            $conditionSiperfaqe = ($propertySelection != 5) ? "Siperfaqia BETWEEN " . $propertySelection : "Siperfaqia>5000";

            $messageSelection = '';
            $messageSelection = ($propertySelection == "0 and 1000") ? trans('messages.from0to1000m2square') : $messageSelection;
            $messageSelection = ($propertySelection == "1000 and 2000") ? trans('messages.from1000to2000m2square') : $messageSelection;
            $messageSelection = ($propertySelection == "2000 and 5000") ? trans('messages.from2000to5000m2square') : $messageSelection;
            $messageSelection = ($propertySelection == 5) ? trans('messages.morethan5000m2square') : $messageSelection;

            $metaDescription = ($propertySelection == "0 and 50") ? 'from0to1000m2square' : $metaDescription;
            $metaDescription = ($propertySelection == "50 and 100") ? 'from1000to2000m2square' : $metaDescription;
            $metaDescription = ($propertySelection == "100 and 200") ? 'from2000to5000m2square' : $metaDescription;
            $metaDescription = ($propertySelection == 5) ? 'morethan5000m2square' : $metaDescription;

            $searchResult = $this->repo->categorySelectionLand($request, $conditionSiperfaqe);

            $titleFirstColumn = '0-1000 m2';
            $titleSecondColumn = '1000-2000 m2';
            $titleThirdColumn = '2000-5000 m2';
            $titleFourthColumn = '5000+ m2';
            $titleFifthColumn = trans('messages.morethanthreebedroom');

            $titleMore = trans('messages.moreland');
            $classNrColumns = "four_columns_template";

        } elseif ($propertyType == "Commercial" || $propertyType == "Ambient tregtar") {

            $propertySelection = 1;
            $propertySelection = ($request->propertySelection == "nga+0+deri+50+m2" || $request->propertySelection == "from+0+to+50+m2") ? "0 and 50" : $propertySelection;
            $propertySelection = ($request->propertySelection == "nga+50+deri+100+m2" || $request->propertySelection == "from+50+to+100+m2") ? "50 and 100" : $propertySelection;
            $propertySelection = ($request->propertySelection == "nga+100+deri+200+m2" || $request->propertySelection == "from+100+to+200+m2") ? "100 and 200" : $propertySelection;
            $propertySelection = ($request->propertySelection == "me+shume+se+200+m2" || $request->propertySelection == "more+than+200+m2") ? 5 : $propertySelection;

            $conditionSiperfaqe = ($propertySelection != 5) ? "Siperfaqia BETWEEN " . $propertySelection : "Siperfaqia>200";

            $messageSelection = '';
            $messageSelection = ($propertySelection == "0 and 50") ? trans('messages.from0to50m2square') : $messageSelection;
            $messageSelection = ($propertySelection == "50 and 100") ? trans('messages.from50to100m2square') : $messageSelection;
            $messageSelection = ($propertySelection == "100 and 200") ? trans('messages.from100to200m2square') : $messageSelection;
            $messageSelection = ($propertySelection == 5) ? trans('messages.morethan200m2square') : $messageSelection;

            $metaDescription = ($propertySelection == "0 and 50") ? 'from0to50m2square' : $metaDescription;
            $metaDescription = ($propertySelection == "50 and 100") ? 'from50to100m2square' : $metaDescription;
            $metaDescription = ($propertySelection == "100 and 200") ? 'from100to200m2square' : $metaDescription;
            $metaDescription = ($propertySelection == 5) ? 'morethan200m2square' : $metaDescription;

            $searchResult = $this->repo->categorySelectionCommercial($request, $conditionSiperfaqe);

            $titleFirstColumn = '0-50 m2';
            $titleSecondColumn = '50-100 m2';
            $titleThirdColumn = '100-200 m2';
            $titleFourthColumn = '200+ m2';
            $titleFifthColumn = trans('messages.morethanthreebedroom');

            $titleMore = trans('messages.morecommercial');
            $classNrColumns = "four_columns_template";
        }

        $values = [
            "lang" => $request->lang,
            "page" => $request->page,
            "city" => $city,
            "action" => $action,
            "propertyType" => $propertyType,
            "perme" => $perme,
            "propertySelection" => $propertySelection
        ];

        $perPage = 40;

        $pagination = new LengthAwarePaginator($searchResult, count($searchResult), $perPage, $page, ['path' => $request->url(), 'query' => $request->query()]);

        $descriptionMeta = trans('messages.cat-' . strtolower($propertyType) . '-' . $metaDescription . '-description');
//         $keywordsMeta = trans('messages.cat-' . strtolower($propertyType) . '-keywords');

        $actionMeta = $action;
        $actionMeta = ($actionMeta == "qera") ? "me qera" : $actionMeta;
        $actionMeta = ($actionMeta == "shitje") ? "ne shitje" : $actionMeta;
        $descriptionMeta = str_ireplace('{action}', $actionMeta, $descriptionMeta);
        $descriptionMeta = str_ireplace('{city}', $city, $descriptionMeta);
		$titleMeta = str_ireplace('{action}', $actionMeta, $titleMeta);
        $titleMeta = str_ireplace('{city}', $city, $titleMeta);

        $keywordsMeta = str_ireplace('{action}', $actionMeta, $keywordsMeta);
        $keywordsMeta = str_ireplace('{city}', $city, $keywordsMeta);

		$descriptionBox = str_ireplace('{action}', $actionMeta, $descriptionBox);
		$descriptionBox = str_ireplace('{city}', $city, $descriptionBox);

		
        if (!empty($searchResult)) {
            $offset = ($page - 1) * $perPage;
            $arrayTotalProperties = array_slice($searchResult, $offset, $perPage);
			if(empty($arrayTotalProperties))
			{
				return redirect()->route('category-cities', ['in' => $in, 'perme' => $perme, 'lang' => $lang, 'propertyType' => strtolower($propertyType), 'city' => $city, 'action' => $action]);
			}
        } else {
            return redirect()->route('category-cities', ['in' => $in, 'perme' => $perme, 'lang' => $lang, 'propertyType' => strtolower($propertyType), 'city' => $city, 'action' => $action]);
        }

        return view('contents.property.category-selection', [
            'pagination' => $pagination,
            'properties' => $arrayTotalProperties,
            'totalproperties' => count($arrayTotalProperties),
            'titleFirstColumn' => $titleFirstColumn,
            'titleSecondColumn' => $titleSecondColumn,
            'titleThirdColumn' => $titleThirdColumn,
            'titleFourthColumn' => $titleFourthColumn,
            'titleFifthColumn' => $titleFifthColumn,
            'propertyType' => $propertyType,
            'action' => $action,
            'city' => $city,
            'perme' => $perme,
            'propertySelection' => $request->propertySelection,
            'classnrcolumns' => $classNrColumns,
            'titleMore' => $titleMore,
            'page' => $page,
            'values' => $values,
            'messageSelection' => $messageSelection,
            'descriptionMeta' => $descriptionMeta,
            'keywordsMeta' => $keywordsMeta,
            'titleMeta' => $titleMeta,
            'descriptionBox' => $descriptionBox
        ]);
    }

    public function cityProperties($lang, $city, $page = null)
    {
        App::setLocale($lang ?? 'en');
        $city = ucfirst($city);
        $gjuha = $lang == 'en' ? 'gj2' : 'gj1';
        $gjuhaSelectLanguage = $lang == 'en' ? 'gj1' : 'gj2';

        $perPage = 24;
        $offset = ($perPage * $page) - $perPage;
//         Cache::flush();
        $result = Cache::remember('properties_city_' . $gjuha . '_' . $city . "_" . $page, 15, function () use ($gjuha, $gjuhaSelectLanguage, $city, $perPage, $offset) {
            return DB::select('SELECT
				ArtID as id,
				ArtTitull_' . $gjuha . ' as title,
				case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
				Cmimi AS price,
				ArtPermbajtja_' . $gjuha . ' as body,
				Llojiprones_' . $gjuha . ' as type,
				Qyteti_' . $gjuha . ' as city,
				Qyteti_' . $gjuhaSelectLanguage . ' as citylangswitch,
				Forsale as sale,
				Forrent as rent,
				Siperfaqia as area,
				kodi
				from `cms_artikujt`
				WHERE
				ifnull(fshih,0)=0
				AND Qyteti_' . $gjuha . ' = ?
				AND deleted_at IS NULL
                ORDER BY priority DESC, ArtRend ASC, created_at DESC, id DESC', [$city]);
        });

        $currentPage = $page ?? 1;
        $col = new Collection($result);
        $perPage = 24;
        $currentPageSearchResults = $col->slice(($currentPage - 1) * $perPage, $perPage)->all();
        $pagination = new LengthAwarePaginator($currentPageSearchResults, count($col), $perPage, $page, ['path' => request()->url(), 'query' => request()->query()]);
		
		if($lang=='en')
        {
            $description = trans('messages.properties-in-tirana-description');
        }else{
            $description = trans('messages.prona-ne-tirane-description');
        }
        
        return view('contents.property.properties-city', [
            'pagination' => $pagination,
            'properties' => $currentPageSearchResults,
            'request' => request()->all(),
            'page' => $page,
            'city' => $city,
            'description' => $description
        ]);    }

    /**
     * Register property.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|string
     * @throws \Illuminate\Validation\ValidationException
     */
    public function registerProperty(Request $request)
    {
        $forSale = false;
        $forRent = false;
        $buy = false;

        $this->validate($request, [
            'email' => 'required',
            'terms' => 'required',
            'captcha_input' => 'required|same:captcha_result',
        ], [
            'terms.required' => "Please, check AGREE at the end of the form.",
            'captcha_input.required' => "Captcha completion is required.",
            'captcha_input.same' => "Your captcha input was not correct. Try again."
        ]);

        try {
            $tipi = $request->input('tipi');
            $lloji = $request->input('lloji');

            if ($lloji == 'For rent' || $lloji == 'Me qera') {
                $forRent = true;
            }
            if ($lloji == 'For sale' || $lloji == 'Shitje') {
                $forSale = true;
            }
            if ($lloji == 'Buy' || $lloji == 'Blerje') {
                $buy = true;
            }

            $qyteti = $request->input('qyteti');
            $fshati = $request->input('fshati');
            $cmimi = $request->input('cmimi');
            $pkoment = $request->input('pkoment');
            $emri = $request->input('emri');
            $mbiemri = $request->input('mbiemri');
            $telefoni = $request->input('telefoni');
            $email = $request->input('email');
            $adresa = $request->input('adresa');
            $zip = $request->input('zip');
            $city = $request->input('city');
            $shteti = $request->input('shteti');
            $yourcomment = $request->input('yourcomment');
            $terms = $request->input('terms');

            if ($terms == 'Jam dakort') {
                $terms = true;
            }

            if (is_null($terms) || $terms === 'Nuk jam dakort') {
                return back()->with('message', 'You must agree with our terms to submit your application');
            }

            $result = DB::insert('INSERT INTO cms_regproperty
				(propertytype,
				forsale,
				forrent,
				tobuy,
				location,
				area,
				price,
				propertydescription,
				name,
				surname,
				telephone,
				email,
				address,
				zipcode,
				city2,
				state,
				yourcomment,
				agreed,
				insertdate)
				VALUES (
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				now())', [
                $tipi, $forSale, $forRent, $buy, $qyteti, $fshati, $cmimi, $pkoment, $emri, $mbiemri, $telefoni,
                $email, $adresa, $zip, $city, $shteti, $yourcomment, $terms
            ]);

            $id = DB::getPDO()->lastInsertId();

            if ($request->hasFile('datafile1')) {
                $file1 = $request->file('datafile1');
                $destinationPath = 'uploads';
                $filename1 = time() . "_" . $file1->getClientOriginalName();
                $file1->move($destinationPath, $filename1);

                DB::insert(
                    'INSERT INTO cms_regimages
					(propertyid,
					imagepath,
					imagetitle)
					VALUES
					(?,
					 ?,
					 ?)',
                    [$id, $destinationPath . '/' . $filename1, $file1]
                );
            }

            if ($request->hasFile('datafile2')) {
                $file2 = $request->file('datafile2');
                $destinationPath = 'uploads';
                $filename2 = time() . "_" . $file2->getClientOriginalName();
                $file2->move($destinationPath, $filename2);
                DB::insert(
                    'INSERT INTO cms_regimages
					(propertyid,
					imagepath,
					imagetitle)
					VALUES
					(?,
					 ?,
					 ?)',
                    [$id, $destinationPath . '/' . $filename2, $file2]
                );
            }

            if ($request->hasFile('datafile3')) {
                $file3 = $request->file('datafile3');
                $destinationPath = 'uploads';
                $filename3 = time() . "_" . $file3->getClientOriginalName();
                $file3->move($destinationPath, $filename3);
                DB::insert(
                    'INSERT INTO cms_regimages
					(propertyid,
					imagepath,
					imagetitle)
					VALUES
					(?,
					 ?,
					 ?)',
                    [$id, $destinationPath . '/' . $filename3, $file3]
                );
            }

            if ($request->hasFile('datafile4')) {
                $file4 = $request->file('datafile4');
                $destinationPath = 'uploads';
                $filename4 = time() . "_" . $file4->getClientOriginalName();
                $file4->move($destinationPath, $filename4);
                DB::insert(
                    'INSERT INTO cms_regimages
					(propertyid,
					imagepath,
					imagetitle)
					VALUES
					(?,
					 ?,
					 ?)',
                    [$id, $destinationPath . '/' . $filename4, $file4]
                );
            }

            if ($request->hasFile('datafile5')) {
                $file5 = $request->file('datafile5');
                $destinationPath = 'uploads';
                $filename5 = time() . "_" . $file5->getClientOriginalName();
                $file5->move($destinationPath, $filename5);
                DB::insert(
                    'INSERT INTO cms_regimages
					(propertyid,
					imagepath,
					imagetitle)
					VALUES
					(?,
					 ?,
					 ?)',
                    [$id, $destinationPath . '/' . $filename5, $file5]
                );
            }

            $fileUrl1 = isset($filename1) ? url('uploads/' . $filename1) : '';
            $fileUrl2 = isset($filename2) ? url('uploads/' . $filename2) : '';
            $fileUrl3 = isset($filename3) ? url('uploads/' . $filename3) : '';
            $fileUrl4 = isset($filename4) ? url('uploads/' . $filename4) : '';
            $fileUrl5 = isset($filename5) ? url('uploads/' . $filename5) : '';

            try {

                Mail::send(new RegisterPropertyMail([
                    "emri" => $emri,
                    "mbiemri" => $mbiemri,
                    "email" => $email,
                    "telefoni" => $telefoni,
                    "message" => $request->input("message"),
                    "tipi" => $tipi,
                    "lloji" => $lloji,
                    "qyteti" => $qyteti,
                    "fshati" => $fshati,
                    "pkoment" => $pkoment,
                    "zip" => $zip,
                    "city" => $city,
                    "cmimi" => $cmimi,
                    "shteti" => $shteti,
                    "fileUrl1" => $fileUrl1,
                    "fileUrl2" => $fileUrl2,
                    "fileUrl3" => $fileUrl3,
                    "fileUrl4" => $fileUrl4,
                    "fileUrl5" => $fileUrl5,
                ]));

            } catch (\Exception $exception) {
                dd($exception->getMessage());
                return back()->with('errorMessage', 'We could not sent the email.');
            }

            return back()->with('message', 'Property has been added successfully.');
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
    
    function findSelection($propertySelection,$propertyType){
	    $propertySelection = (int)$propertySelection;
	    
	    if ($propertyType == "Apartment" || $propertyType == "Apartament") {
		    
		    if($propertySelection==1){

			    if(\Lang::getLocale() == "en")
				{
			    	$propertySelection = "one+bedroom";
				}else{
					$propertySelection = "1+1";
				}
		    }else if($propertySelection==2)
		    {
   			    if(\Lang::getLocale() == "en")
				{
			 	   $propertySelection = "two+bedroom";
			 	}else{
			 		$propertySelection = "2+1";
				}
		    }else if($propertySelection==3)
		    {
			    if(\Lang::getLocale() == "en")
				{
				    $propertySelection = "three+bedroom";
				}else{
					$propertySelection = "3+1";
				}
		    }
		    else if($propertySelection>3)
		    {
			    if(\Lang::getLocale() == "en")
				{
				    $propertySelection = "more+than+three+bedroom";
				}else{
					$propertySelection = "me+shume+se+3+dhoma";
				}
		    }
		    
        } elseif ($propertyType == "Villa" || $propertyType == "Vile") {

	        if($propertySelection==2)
		    {
   			    if(\Lang::getLocale() == "en")
				{
			 	   $propertySelection = "2+bedrooms";
			 	}else{
			 		$propertySelection = "2+dhoma";
				}
		    }else if($propertySelection==3)
		    {
			    if(\Lang::getLocale() == "en")
				{
				    $propertySelection = "3+bedrooms";
				}else{
					$propertySelection = "3+dhoma";
				}
		    }else if($propertySelection==4)
		    {
			    if(\Lang::getLocale() == "en")
				{
				    $propertySelection = "4+bedrooms";
				}else{
					$propertySelection = "4+dhoma";
				}
		    }else if($propertySelection==5)
		    {
			    if(\Lang::getLocale() == "en")
				{
				    $propertySelection = "5+bedrooms";
				}else{
					$propertySelection = "5+dhoma";
				}
		    }
		    else if($propertySelection>5)
		    {
			    if(\Lang::getLocale() == "en")
				{
				    $propertySelection = "more+than+5+bedrooms";
				}else{
					$propertySelection = "me+shume+se+5+dhoma";
				}
		    }
        } elseif ($propertyType == "Office" || $propertyType == "Zyre") {
			if($propertySelection>0 && $propertySelection<=50)
		    {
   			    if(\Lang::getLocale() == "en")
				{
					$propertySelection = "from+0+to+50+m2";
			 	}else{
			 		$propertySelection = "nga+0+deri+50+m2";
				}
		    }else if($propertySelection>50 && $propertySelection<=100)
		    {
			    if(\Lang::getLocale() == "en")
				{
				    $propertySelection = "from+50+to+100+m2";
				}else{
					$propertySelection = "nga+50+deri+100+m2";
				}
		    }else if($propertySelection>100 && $propertySelection<=200)
		    {
			    if(\Lang::getLocale() == "en")
				{
				    $propertySelection = "from+100+to+200+m2";
				}else{
					$propertySelection = "nga+100+deri+200+m2";
				}
		    }else if($propertySelection>200)
		    {
			    if(\Lang::getLocale() == "en")
				{
				    $propertySelection = "more+than+200+m2";
				}else{
					$propertySelection = "me+shume+se+200+m2";
				}
		    }
        } elseif ($propertyType == "Warehouse" || $propertyType == "Magazine" || $propertyType == "Magazine-Depo") {

			if($propertySelection>0 && $propertySelection<=500)
		    {
   			    if(\Lang::getLocale() == "en")
				{
			 	   $propertySelection = "from+0+to+500+m2";
			 	}else{
			 		$propertySelection = "nga+0+deri+500+m2";
				}
		    }else if($propertySelection>500 && $propertySelection<=1000)
		    {
			    if(\Lang::getLocale() == "en")
				{
				    $propertySelection = "from+500+to+1000+m2";
				}else{
					$propertySelection = "nga+500+deri+1000+m2";
				}
		    }else if($propertySelection>1000 && $propertySelection<=2000)
		    {
			    if(\Lang::getLocale() == "en")
				{
				    $propertySelection = "from+1000+to+2000+m2";
				}else{
					$propertySelection = "nga+1000+deri+2000+m2";
				}
		    }else if($propertySelection>2000)
		    {
			    if(\Lang::getLocale() == "en")
				{
				    $propertySelection = "more+than+2000+m2";
				}else{
					$propertySelection = "me+shume+se+2000+m2";
				}
			    
		    }

        } elseif ($propertyType == "Land" || $propertyType == "Toke") {
	        
	        if($propertySelection>0 && $propertySelection<=1000)
		    {
   			    if(\Lang::getLocale() == "en")
				{
			 	   $propertySelection = "from+0+to+1000+m2";
			 	}else{
			 		$propertySelection = "nga+0+deri+1000+m2";
				}
		    }else if($propertySelection>1000 && $propertySelection<=2000)
		    {
			    if(\Lang::getLocale() == "en")
				{
				    $propertySelection = "from+1000+to+2000+m2";
				}else{
					$propertySelection = "nga+1000+deri+2000+m2";
				}
		    }else if($propertySelection>2000 && $propertySelection<=5000)
		    {
			    if(\Lang::getLocale() == "en")
				{
				    $propertySelection = "from+2000+to+5000+m2";
				}else{
					$propertySelection = "nga+2000+deri+5000+m2";
				}
		    }else if($propertySelection>5000)
		    {
			    if(\Lang::getLocale() == "en")
				{
				    $propertySelection = "more+than+5000+m2";
				}else{
					$propertySelection = "me+shume+se+5000+m2";
				}
		    }

        } elseif ($propertyType == "Commercial" || $propertyType == "Ambient tregtar") {

			if($propertySelection>0 && $propertySelection<=50)
		    {
   			    if(\Lang::getLocale() == "en")
				{
			 	   $propertySelection = "from+0+to+50+m2";
			 	}else{
			 		$propertySelection = "nga+0+deri+50+m2";
				}
		    }else if($propertySelection>50 && $propertySelection<=100)
		    {
			    if(\Lang::getLocale() == "en")
				{
				    $propertySelection = "from+50+to+100+m2";
				}else{
					$propertySelection = "nga+50+deri+100+m2";
				}
		    }else if($propertySelection>100 && $propertySelection<=200)
		    {
			    if(\Lang::getLocale() == "en")
				{
				    $propertySelection = "from+100+to+200+m2";
				}else{
					$propertySelection = "nga+100+deri+200+m2";
				}
		    }else if($propertySelection>200)
		    {
			    if(\Lang::getLocale() == "en")
				{
				    $propertySelection = "more+than+200+m2";
				}else{
					$propertySelection = "me+shume+se+200+m2";
				}
		    }
        }
        return $propertySelection;
    }
    
    public function redirect()
    {
		$count = count(request()->segments());
		$firstSegment = request()->segment($count-2); 
		$secondSegment = request()->segment($count-1);
		return redirect($firstSegment.'/'.$secondSegment, 301);
    }

    public function redirectVileSq()
    {
	    return redirect('/sq/vile-3+dhoma-me-qera-ne-Tirane',301);
    }
    
    public function redirectVileEn()
    {
	    return redirect('/en/villa-3+bedrooms-for-rent-in-Tirana',301);
    }
    
    public function redirectArticle($lang,$id)
    {
	    if($lang=='sq')
	    {
	    	$article = DB::select("select ArtTitull_gj1 as title from `cms_artikujt` where ArtID = $id");

	    	if($article)
	    	{					
				return redirect($lang.'/'.str_slug($article[0]->title).'.'.$id);
	    	}

	    	return redirect('/');
	    }else{		    
		    $article = DB::select("select ArtTitull_gj2 as title from `cms_artikujt` 		where ArtID = $id");
		    
	    	if($article)
	    	{
				return redirect($lang.'/'.str_slug($article[0]->title).'.'.$id);
			}	    
			
			return redirect('/');
	    }
    }
}
