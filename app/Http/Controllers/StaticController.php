<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class StaticController extends Controller
{

    /**
     * Render static page.
     *
     * @param $lang
     * @param $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function page($lang, $page)
    {
	    if($lang=='en'){
		    if($page=='rreth-neth' || $page=='kontakt' || $page=='harta-faqes')
		    {
			    return redirect('sq/'.$page,301);
		    }
	    }
	    
		if($lang=='sq'){
		    if($page=='about-us' || $page=='contact' || $page=='sitemap')
		    {
			    return redirect('en/'.$page,301);
		    }
	    }
        switch ($page) {
            case 'about-us':
            case 'rreth-nesh':
                return $this->aboutUs($lang, $page);
            case 'contact':
            case 'kontakt':
                return $this->contactPage($lang, $page);
            case 'sitemap':
            case 'harta-faqes':
                return $this->sitemapPage($lang, $page);
        }

        abort(404);
    }

    /**
     * About us page.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function aboutUs($lang, $page)
    {
        if ($page == "about-us") {
            App::setLocale("en");
            $title = "About Us";
            $alternate = "Rreth Nesh";
            $alternatelink = "rreth-nesh";
            $currentlink = "about-us";
        } else if ($page == "rreth-nesh") {
            App::setLocale("sq");
            $title = "Rreth nesh";
            $alternate = "About Us";
            $alternatelink = "about-us";
            $currentlink = "rreth-nesh";
        }

        return view('contents.about-us', [
            "title" => $title,
            "alternatelink" => $alternatelink,
            "currentlink" => $currentlink
        ]);
    }

    /**
     * Contact page.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function contactPage($lang, $page)
    {
        try {
            if ($page == "contact") {
                App::setLocale("en");
                $title = "Contact";
                $alternatelink = "kontakt";
                $currentlink = "contact";
            } else if ($page == "kontakt") {
                App::setLocale("sq");
                $title = "Kontakt";
                $alternatelink = "contact";
                $currentlink = "kontakt";
            }

            return view('contents.contact', [
                "title" => $title,
                "alternatelink" => $alternatelink,
                "currentlink" => $currentlink
            ]);

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function sitemapPage($lang, $page)
    {
        try {
            if ($page == "sitemap") {
                App::setLocale("en");
                $title = "Sitemap";
                $alternatelink = "harta-faqes";
                $currentlink = "sitemap";
            } else if ($page == "harta-faqes") {
                App::setLocale("sq");
                $title = "Harta e faqes";
                $alternatelink = "sitemap";
                $currentlink = "harta-faqes";
            }
            return view('/sitemap', [
                "title" => $title,
                "alternatelink" => $alternatelink,
                "currentlink" => $currentlink
            ]);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function commisions($lang)
    {
        if ($lang == "sq") {
            App::setLocale("sq");
            $title = "Komisionet";
            $alternate = "Commisions";
            $alternatelink = "commisions";
            $currentlink = "komisionet";

            return view('contents.commisions-sq', [
                "title" => $title,
                "alternatelink" => $alternatelink,
                "currentlink" => $currentlink
            ]);
        } else if ($lang == "en") {
            App::setLocale("en");
            $title = "Commisions";
            $alternate = "Komisionet";
            $alternatelink = "komisionet";
            $currentlink = "commisions";

            return view('contents.commisions-en', [
                "title" => $title,
                "alternatelink" => $alternatelink,
                "currentlink" => $currentlink
            ]);
        }
    }
}
