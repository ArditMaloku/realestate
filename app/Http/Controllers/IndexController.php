<?php

namespace App\Http\Controllers;

use App\Mail\ContactMessageMail;
use App\Mail\InquirePropertyMail;
use App\Repositories\IndexRepo;
use App\Repositories\SearchRepo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\App;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Arr;

class IndexController extends Controller
{
    protected $repo;
    protected $searchRepo;

    public function __construct(IndexRepo $indexRepo, SearchRepo $searchRepo)
    {
        $this->repo = $indexRepo;
        $this->searchRepo = $searchRepo;
    }

    /**
     * English Home Page.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function enIndex()
    {
	    Cache::flush();
	    
	    if(request()->fq=='apartaments_for_sale' && request()->gj=='gj1' && request()->kid==54 && request()->start==584)
		{
			return redirect('/',301);
		}
		
		if(isset(request()->start))
		{
			if(request()->gj=='gj1')
			{
				return app('App\Http\Controllers\PropertyController')->redirectArticle('sq',request()->start);
			}elseif(request()->gj=='gj2'){
				return app('App\Http\Controllers\PropertyController')->redirectArticle('en',request()->start);
			}
		}
	    
        App::setLocale("en");
        $gjuha = "gj2";

        $lastApsForRent = Cache::remember('lastApsForRent' . $gjuha, 15, function () {
            return DB::select('select ArtID as id, Cmimi AS price, kodi, ArtTitull_gj2 as title,ArtPermbajtja_gj2 as content, case when( rented = 1 or sold =1 ) then watermark else Artpic end as image,Forsale as forsale, Forrent as forrent from `cms_artikujt` WHERE ifnull(fshih,0)=0 AND `Llojiprones_gj1`="Apartament" AND `Forrent`=1 AND `Qyteti_gj1`="Tirane" AND `fshih`=0 AND deleted_at IS NULL ORDER BY `cms_artikujt`.priority DESC, `cms_artikujt`.`created_at` DESC, `cms_artikujt`.`ArtID` DESC LIMIT 0 , 4');
        });
        $lastApsForSale = Cache::remember('lastApsForSale' . $gjuha, 15, function () {
            return DB::select('select ArtID as id, Cmimi AS price, kodi, ArtTitull_gj2 as title,ArtPermbajtja_gj2 as content, case when( rented = 1 or sold =1 ) then watermark else Artpic end as image,Forsale as forsale, Forrent as forrent from `cms_artikujt` WHERE ifnull(fshih,0)=0 AND `Llojiprones_gj1`="Apartament" AND `Forsale`=1 AND `Qyteti_gj1`="Tirane" AND `fshih`=0 AND deleted_at IS NULL ORDER BY `cms_artikujt`.priority DESC, `cms_artikujt`.`created_at` DESC, `cms_artikujt`.`ArtID` DESC LIMIT 0 , 4');
        });
        $officeForRentTirana = Cache::remember('officeForRentTirana' . $gjuha, 15, function () {
            return DB::select('select ArtID as id, Cmimi AS price, kodi, ArtTitull_gj2 as title,ArtPermbajtja_gj2 as content, case when( rented = 1 or sold =1 ) then watermark else Artpic end as image,Forsale as forsale, Forrent as forrent from `cms_artikujt` WHERE ifnull(fshih,0)=0 AND `Llojiprones_gj1`="Zyre" AND `Forrent`=1 AND `Qyteti_gj1`="Tirane" AND `fshih`=0 AND deleted_at IS NULL ORDER BY `cms_artikujt`.priority DESC, `cms_artikujt`.`created_at` DESC, `cms_artikujt`.`ArtID` DESC LIMIT 0 , 4');
        });
        $villaForRentTirana = Cache::remember('villaForRentTirana' . $gjuha, 15, function () {
            return DB::select('select ArtID as id, Cmimi AS price, kodi, ArtTitull_gj2 as title,ArtPermbajtja_gj2 as content, case when( rented = 1 or sold =1 ) then watermark else Artpic end as image,Forsale as forsale, Forrent as forrent from `cms_artikujt` WHERE ifnull(fshih,0)=0 AND `Llojiprones_gj1`="Vile" AND `Forrent`=1 AND `Qyteti_gj1`="Tirane" AND `fshih`=0 AND deleted_at IS NULL ORDER BY `cms_artikujt`.priority DESC, `cms_artikujt`.`created_at` DESC, `cms_artikujt`.`ArtID` DESC LIMIT 0 , 4');
        });
        $latestProperties = Cache::remember('latestProperties' . $gjuha, 15, function (){
	        return DB::select('select created_at, ArtID as id, Cmimi AS price, kodi, ArtTitull_gj2 as title,ArtPermbajtja_gj2 as content, case when( rented = 1 or sold =1 ) then watermark else Artpic end as image,Forsale as forsale, Forrent as forrent from `cms_artikujt` WHERE ifnull(fshih,0)=0 AND `Llojiprones_gj1`!="Vile" and `Llojiprones_gj1`!="Zyre" and `Llojiprones_gj1`!="Apartament" and `Llojiprones_gj1` is not null AND `fshih`=0 AND deleted_at IS NULL ORDER BY `cms_artikujt`.priority DESC, `cms_artikujt`.`created_at` DESC, `cms_artikujt`.`ArtID` DESC LIMIT 0 , 4');
        });
        
        $latestPropertiesVillaSale = Cache::remember('latestPropertiesVillaSale' . $gjuha, 15, function (){
	        return DB::select('select created_at, ArtID as id, Cmimi AS price, kodi, ArtTitull_gj2 as title,ArtPermbajtja_gj2 as content, case when( rented = 1 or sold =1 ) then watermark else Artpic end as image,Forsale as forsale, Forrent as forrent from `cms_artikujt` WHERE ifnull(fshih,0)=0 AND `Llojiprones_gj1`="Vile" and `Forsale`=1 AND `fshih`=0 AND deleted_at IS NULL ORDER BY `cms_artikujt`.priority DESC, `cms_artikujt`.`created_at` DESC, `cms_artikujt`.`ArtID` DESC LIMIT 0 , 1');
        });

		if(isset($latestPropertiesVillaSale[0]))
        {
            $created_at = $latestPropertiesVillaSale[0]->created_at;
        }       
        
        foreach($latestProperties as $value => $row)
        {
	        if($value==3){
		        if($created_at > $row->created_at)
		        {
					$latestProperties[3] = $latestPropertiesVillaSale[0];
		        }
	        }
        }

		
        $firstColLink = '/en/apartment-for-rent-in-Tirana';
        $secondColLink = '/en/apartment-for-sale-in-Tirana';
        $thirdColLink = '/en/villa-for-rent-in-Tirana';
        $fourthColLink = '/en/office-for-rent-in-Tirana';
		$fifthColLink = '/en/properties-in-albania';
		
        $properties = $this->repo->getArticles($gjuha);

        return view('contents.index', [
            'apartments' => $lastApsForRent,
            'apartmentsForSale' => $lastApsForSale,
            'officeForRentTirana' => $officeForRentTirana,
            'villaForRentTirana' => $villaForRentTirana,
            'latestProperties' => $latestProperties,
            'firstColLink' => $firstColLink,
            'secondColLink' => $secondColLink,
            'thirdColLink' => $thirdColLink,
            'fourthColLink' => $fourthColLink,
            'fifthColLink' => $fifthColLink,
            'properties' => $properties
        ]);
    }

    /**
     * Shqip Home Page.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function sqIndex()
    {
	    Cache::flush();
	    
	    if(request()->fq=='apartaments_for_sale' && request()->gj=='gj1' && request()->kid==54 && request()->start==584)
		{
			return redirect('/',301);
		}
	    
	    if(isset(request()->start))
		{
			if(request()->gj=='gj1')
			{
				return app('App\Http\Controllers\PropertyController')->redirectArticle('sq',request()->start);
			}elseif(request()->gj=='gj2'){
				return app('App\Http\Controllers\PropertyController')->redirectArticle('en',request()->start);
			}
		}
		
        App::setLocale("sq");
        $gjuha = "gj1";

        $lastApsForRent = Cache::remember('lastApsForRent' . $gjuha, 15, function () {
            return DB::select('select ArtID as id, Cmimi AS price, kodi, ArtTitull_gj1 as title,ArtPermbajtja_gj1 as content, case when( rented = 1 or sold =1 ) then watermark else Artpic end as image,Forsale as forsale, Forrent as forrent from `cms_artikujt` WHERE ifnull(fshih,0)=0 AND `Llojiprones_gj1`="Apartament" AND `Forrent`=1 AND `Qyteti_gj1`="Tirane" AND `fshih`=0 AND deleted_at IS NULL ORDER BY `cms_artikujt`.priority DESC, `cms_artikujt`.`created_at` DESC, `cms_artikujt`.`ArtID` DESC LIMIT 0 , 4');
        });
        $lastApsForSale = Cache::remember('lastApsForSale' . $gjuha, 15, function () {
            return DB::select('select ArtID as id, Cmimi AS price, kodi, ArtTitull_gj1 as title,ArtPermbajtja_gj1 as content, case when( rented = 1 or sold =1 ) then watermark else Artpic end as image,Forsale as forsale, Forrent as forrent from `cms_artikujt` WHERE ifnull(fshih,0)=0 AND `Llojiprones_gj1`="Apartament" AND `Forsale`=1 AND `Qyteti_gj1`="Tirane" AND `fshih`=0 AND deleted_at IS NULL ORDER BY `cms_artikujt`.priority DESC, `cms_artikujt`.`created_at` DESC, `cms_artikujt`.`ArtID` DESC LIMIT 0 , 4');
        });
        $officeForRentTirana = Cache::remember('villaForSaleTirana' . $gjuha, 15, function () {
            return DB::select('select ArtID as id,Cmimi AS price, kodi, ArtTitull_gj1 as title,ArtPermbajtja_gj1 as content, case when( rented = 1 or sold =1 ) then watermark else Artpic end as image,Forsale as forsale, Forrent as forrent from `cms_artikujt` WHERE ifnull(fshih,0)=0 AND `Llojiprones_gj1`="Zyre" AND `Forrent`=1 AND `Qyteti_gj1`="Tirane" AND `fshih`=0 AND deleted_at IS NULL ORDER BY `cms_artikujt`.priority DESC, `cms_artikujt`.`created_at` DESC, `cms_artikujt`.`ArtID` DESC LIMIT 0 , 4');
        });
        $villaForRentTirana = Cache::remember('villaForRentTirana' . $gjuha, 15, function () {
            return DB::select('select ArtID as  id, Cmimi AS price, kodi, ArtTitull_gj1 as title,ArtPermbajtja_gj1 as content, case when( rented = 1 or sold =1 ) then watermark else Artpic end as image,Forsale as forsale, Forrent as forrent from `cms_artikujt` WHERE ifnull(fshih,0)=0 AND `Llojiprones_gj1`="Vile" AND `Forrent`=1 AND `Qyteti_gj1`="Tirane" AND `fshih`=0 AND deleted_at IS NULL ORDER BY `cms_artikujt`.priority DESC, `cms_artikujt`.`created_at` DESC, `cms_artikujt`.`ArtID` DESC LIMIT 0 , 4');
        });
         $latestProperties = Cache::remember('latestProperties' . $gjuha, 15, function (){
	        return DB::select('select created_at, ArtID as id, Cmimi AS price, kodi, ArtTitull_gj1 as title,ArtPermbajtja_gj1 as content, case when( rented = 1 or sold =1 ) then watermark else Artpic end as image,Forsale as forsale, Forrent as forrent from `cms_artikujt` WHERE ifnull(fshih,0)=0 AND `Llojiprones_gj1`!="Vile" and `Llojiprones_gj1`!="Zyre" and `Llojiprones_gj1`!="Apartament" and `Llojiprones_gj1` is not null AND `fshih`=0 AND deleted_at IS NULL ORDER BY `cms_artikujt`.priority DESC, `cms_artikujt`.`created_at` DESC, `cms_artikujt`.`ArtID` DESC LIMIT 0 , 4');
        });
        
        $latestPropertiesVillaSale = Cache::remember('latestPropertiesVillaSale' . $gjuha, 15, function (){
	        return DB::select('select created_at, ArtID as id, Cmimi AS price, kodi, ArtTitull_gj1 as title,ArtPermbajtja_gj1 as content, case when( rented = 1 or sold =1 ) then watermark else Artpic end as image,Forsale as forsale, Forrent as forrent from `cms_artikujt` WHERE ifnull(fshih,0)=0 AND `Llojiprones_gj1`="Vile" and `Forsale`=1 AND `fshih`=0 AND deleted_at IS NULL ORDER BY `cms_artikujt`.priority DESC, `cms_artikujt`.`created_at` DESC, `cms_artikujt`.`ArtID` DESC LIMIT 0 , 1');
        });

        $created_at = $latestPropertiesVillaSale[0]->created_at;

        foreach($latestProperties as $value => $row)
        {
	        if($value==3){
		        if($created_at > $row->created_at)
		        {
					$latestProperties[3] = $latestPropertiesVillaSale[0];
		        }
	        }
        }


        $firstColLink = '/sq/apartament-me-qera-ne-Tirane';
        $secondColLink = '/sq/apartament-ne-shitje-ne-Tirane';
        $thirdColLink = '/sq/vile-me-qera-ne-Tirane';
        $fourthColLink = '/sq/zyre-me-qera-ne-Tirane';
		$fifthColLink = '/sq/prona-ne-shqiperi';

        $properties = $this->repo->getArticles($gjuha);

        return view('contents.index', [
            'apartments' => $lastApsForRent,
            'apartmentsForSale' => $lastApsForSale,
            'villaForRentTirana' => $villaForRentTirana,
            'officeForRentTirana' => $officeForRentTirana,
            'latestProperties' => $latestProperties,
            'firstColLink' => $firstColLink,
            'secondColLink' => $secondColLink,
            'thirdColLink' => $thirdColLink,
            'fourthColLink' => $fourthColLink,
            'fifthColLink' => $fifthColLink,
            'properties' => $properties
        ]);
    }

    /**
     * Main search.
     *
     * @param $lang
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search($lang, Request $request)
    {
        App::setLocale($lang ?? 'en');
        $gjuha = $lang == 'en' ? 'gj2' : 'gj1';

        $city = $request->input('city');
        $propertyType = $request->input('propertyType');
        $saleStatus = $request->input('saleStatus');
        $cmimi = $request->input('cmimi');
        $organizimi = $request->input('organizimi');
        $forSale = 0;
        $forRent = 0;
        $searchResult = array();
        $values = array();
        $searchFor = '';

        $cmimiMore = ($request->cmimi == "more") ? $request->cmimi . $saleStatus : $request->cmimi;

        if ($saleStatus == "sale") {
            $forSale = 1;
            $searchFor = ($request->lang == "sq") ? $searchFor . "Shitje, " : $searchFor . "Sale, ";
        }

        if ($saleStatus == "rent") {
            $forRent = 1;
            $searchFor = ($request->lang == "sq") ? $searchFor . "Qira, " : $searchFor . "Rent, ";
        }

        $values = $request->all();

        if ($propertyType == "apartment") {

            $searchResult = $this->searchRepo->searchApartment($request, $gjuha, $searchFor, $cmimiMore, $forSale, $forRent);

            $values["statesearch"] = $request->input('statesearch');
            $values["city"] = $city;
            $values["saleStatus"] = $saleStatus;
            $values["propertyType"] = $propertyType;
            $values["cmimi"] = $cmimi;
            $values["organizimi"] = $organizimi;


        } else if ($propertyType == "commercial") {

            $siperfaqe = $request->input('siperfaqe-store');

            $searchResult = $this->searchRepo->searchCommercial($request, $gjuha, $searchFor, $saleStatus, $forSale, $forRent);

            $values["statesearch"] = $request->statesearch;
            $values["city"] = $city;
            $values["saleStatus"] = $saleStatus;
            $values["propertyType"] = $propertyType;
            $values["siperfaqe-store"] = $siperfaqe;


        } else if ($propertyType == "villa") {

            $siperfaqe = $request->input('shperndarja-villa');

            $searchResult = $this->searchRepo->searchVilla($request, $gjuha, $searchFor, $saleStatus, $forSale, $forRent);

            $values["statesearch"] = $request->statesearch;
            $values["city"] = $city;
            $values["saleStatus"] = $saleStatus;
            $values["propertyType"] = $propertyType;
            $values["shperndarja-villa"] = $siperfaqe;

        } else if ($propertyType == "office") {

            $siperfaqe = $request->input('siperfaqeZyra');

            $searchResult = $this->searchRepo->searchOffice($request, $gjuha, $searchFor, $saleStatus, $forSale, $forRent);

            $values["statesearch"] = $request->statesearch;
            $values["city"] = $city;
            $values["saleStatus"] = $saleStatus;
            $values["propertyType"] = $propertyType;
            $values["siperfaqeZyra"] = $siperfaqe;

        } else if ($propertyType == "warehouse") {

            $siperfaqe = $request->input('siperfaqe-mag');

            $searchResult = $this->searchRepo->searchWarehouse($request, $gjuha, $searchFor, $saleStatus, $forSale, $forRent);

            $values["statesearch"] = $request->statesearch;
            $values["city"] = $city;
            $values["saleStatus"] = $saleStatus;
            $values["propertyType"] = $propertyType;
            $values["siperfaqe-mag"] = $siperfaqe;


        } else if ($propertyType == "land") {
            $siperfaqe = $request->input('siperfaqe-toka');

            $searchResult = $this->searchRepo->searchLand($request, $gjuha, $searchFor, $saleStatus, $forSale, $forRent);

            $values["statesearch"] = $request->statesearch;
            $values["city"] = $city;
            $values["saleStatus"] = $saleStatus;
            $values["propertyType"] = $propertyType;
            $values["siperfaqe-toka"] = $siperfaqe;

        }

        $mapProperties = Cache::remember('index_map_result' . $gjuha . '_' . $forSale . '_' . $forRent . '_' . $city, 15,
            function () use ($gjuha, $forSale, $forRent, $city) {
                return DB::select('SELECT
				ArtID as id,
				ArtTitull_' . $gjuha . ' as title,
				case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
				Cmimi as price,
				ArtPermbajtja_' . $gjuha . ' as body,
				Llojiprones_' . $gjuha . ' as type,
				Qyteti_' . $gjuha . ' as city,
				Forsale as sale,
				Forrent as rent,
				Siperfaqia as area,
				koordinata
				from `cms_artikujt`
				WHERE
				ifnull(fshih,0)=0
				and koordinata<>""
				and koordinata IS NOT NULL
				AND (Forsale=? || Forrent =?)
				AND Qyteti_gj2 = ?
                ORDER BY priority DESC,  id DESC limit 100', [$forSale, $forRent, $city]);
            });

        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $col = new Collection($searchResult);
        $perPage = 24;
        $currentPageSearchResults = $col->slice(($currentPage - 1) * $perPage, $perPage)->all();
        $pagination = new LengthAwarePaginator($currentPageSearchResults, count($col), $perPage);

        $page = 1;
        if (isset($request->page)) {
            $page = $request->page;
        }

        $pagination->setPath(request()->url());

        return view('contents.search', [
            'pagination' => $pagination,
            'properties' => $currentPageSearchResults, // array_slice($currentPageSearchResults, $page, $perPage),
            'totalProperties' => count($currentPageSearchResults),
            "values" => $values,
            "searchFor" => rtrim(rtrim($searchFor), ','),
            'propertiesMap' => $mapProperties,
            'request' => $request->all()
        ]);
    }

    /**
     * Search by map.
     *
     * @param $lang
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function searchByMap($lang, Request $request)
    {
        App::setLocale($lang ?? 'en');
        $gjuha = $lang == 'en' ? 'gj2' : 'gj1';

        $type = $request->input('type');
        $status = $request->input('status');

        $query = "SELECT
				ArtID as id,
				ArtTitull_{$gjuha}  as title,
				case when( rented = 1 or sold =1 ) then watermark else Artpic end as pic,
				Cmimi as price,
				ArtPermbajtja_{$gjuha} as body,
				Llojiprones_{$gjuha} as type,
				Qyteti_{$gjuha} as city,
				Forsale as sale,
				Forrent as rent,
				Siperfaqia as area,
				koordinata,
                kodi
				from `cms_artikujt`
				WHERE
				ifnull(fshih,0)=0
				and koordinata<>''
				and koordinata IS NOT NULL ";

        $bindings = [];

        if ($type) {
            $query = $query . " AND Llojiprones_gj2 = ? ";

            $bindings[] = $type;
        }

        if ($status) {
            $forSale = $status == 'sale' ? 1 : 0;
            $forRent = $status == 'rent' ? 1 : 0;

            $query = $query . " AND (Forsale=? || Forrent =?) ";

            $bindings[] = $forSale;
            $bindings[] = $forRent;
        }

        $searchResult = Cache::remember('searchbymap_' . $type . '_' . $status, 15, function () use ($query, $bindings) {
            return DB::select($query . ' ORDER BY priority DESC, ArtRend ASC, created_at DESC, id DESC', $bindings);
        });
        
        if(\Lang::getLocale() == "en")
		{
	        $description = 'Sometimes the location is all that matters for your accommodation or your business facility  and we are sure to have the right one for you in our inventory of more than 5000 properties in Tirana and Albania .  Just click on the property name that you are interested in the Legend and that type of property will be displayed in the map .  Zoom on your desired location  and we certainly will have an option waiting for you . By clicking in the icon on the map it will display a brief information on that particular apartment , villa office or commercial unit  and open the property article .  For any inquire or question don’t hesitate to';
	    }else{
		    $description = 'Ndonjehere , apartamenti apo zyra juaj duhet thjeshte te jete ne vendin e caktuar . Per kete arsye kemi krijuar nje menyre te re kerkimi per pronat e paluajteshme ne Tirane apo ne te gjithe Shqiperine . Mjafton te klikoni mbi llojin e prones qe jeni te interesuar tek legjenda me poshte dhe ato prona do ju shfaqen ne harte. Zmadhoni ate pjese te territorit ku jeni te interesuar dhe sigurisht ne do kemi nje prone duke pritur per ju .  Duke klikuar me ikone ne harte do ju hapet nje informacion i shkurter mbi apartmentin , zyren , vilen apo ambientin tregtar qe ju jeni te interesuar . Per cdo pyetje apo kerkese ju lutem na';
	    }

        return view('contents.search-by-map', [
            'properties' => $searchResult,
            'description' => $description
        ]);
    }

    /**
     * Show register property view.
     *
     * @param $lang
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function registerProperty($lang)
    {
        $firstNr = rand(1, 10);
        $secondNr = rand(1, 10);

        App::setLocale($lang ?? 'en');
        return view('contents.register-property')->with([
            'firstNr' => $firstNr,
            'secondNr' => $secondNr,
        ]);
    }

    /**
     * Contact page.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function contact()
    {
        return view('contents.contact');
    }

    /**
     *
     * Send mail.
     *
     * @param string $lang
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws Exception
     * @throws \Illuminate\Validation\ValidationException
     */
    public function send(string $lang, Request $request)
    {
        if ($lang == 'sq') {
            $messages = [
                'name.required' => 'Emri eshte fushe e detyruar.',
                'email.required' => 'Email eshte fushe e detyruar.',
                'message.required' => 'Mesazhi eshte fushe e detyruar.',
            ];
        } else {
            $messages = [];
        }

        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|max:255',
            'message' => 'required'
        ], $messages);

        try {
            Mail::send(new ContactMessageMail([
                "name" => $request->input('name'),
                "email" => $request->input("email"),
                "phone" => $request->input("phone"),
                "message" => $request->input("message")
            ]));

            return redirect()->back()->with('message', 'Email was sent successfully.');
        } catch (\Exception $exception) {

            if (config('app.debug') == true) {
                return redirect()->back()->with('errorMessage', $exception->getMessage());
            }

            return redirect()->back()->with('errorMessage', 'Email could not be sent!' .$exception->getMessage());
        }
    }

    public function sendFromProperty(string $lang, int $id, Request $request)
    {
        $article = DB::select('SELECT * FROM cms_artikujt WHERE ArtId = ?', [$id]);
        $article = $article[0];

        if ($lang == 'sq') {
            $messages = [
                'name.required' => 'Emri eshte fushe e detyruar.',
                'email.required' => 'Email eshte fushe e detyruar.',
                'message.required' => 'Mesazhi eshte fushe e detyruar.',
            ];
        } else {
            $messages = [];
        }

        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|max:255',
            'message' => 'required'
        ], $messages);

        try {

            Mail::send(new InquirePropertyMail($article, [
                "name" => $request->input('name'),
                "email" => $request->input("email"),
                "phone" => $request->input("phone"),
                "message" => $request->input("message")
            ]));

            return redirect()->back()->with('message', 'Email was sent successfully.');
        } catch (\Exception $exception) {

            if (config('app.debug') == true) {
                return redirect()->back()->with('errorMessage', $exception->getMessage());
            }

            return redirect()->back()->with('errorMessage', 'Email could not be sent!');
        }
    }

    /**
     * Renders a list of links for properties in Albania.
     *
     * @param $lang
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function links($lang)
    {
        App::setLocale($lang);

        if ($lang === 'en') {
            $description = trans('messages.properties-in-albania-description');
            $title = "Properties in Albania";
            $alternatelink = "prona-ne-shqiperi";
            $currentlink = "properties-in-albania";
        } else {
            $description = trans('messages.prona-ne-shqiperi-description');
            $title = "Prona ne Shqiperi";
            $alternatelink = "properties-in-albania";
            $currentlink = "prona-ne-shqiperi";
        }

        return view('contents.links', [
            "title" => $title,
            "alternatelink" => $alternatelink,
            "currentlink" => $currentlink,
            'description' => $description
        ]);
    }
        
    public function sitemap()
    {
        //SELECT ArtID as id, ArtTitull_gj1 as titull, ArtTitull_gj2 as title FROM `cms_artikujt` WHERE fshih=0 order by ArtID desc 
        $sitemapRec = Cache::remember('sitemap', 10, function () {
            return DB::select('SELECT ArtID as id, ArtTitull_gj1 as titull, ArtTitull_gj2 as title, updated_at as lastupdate FROM `cms_artikujt` WHERE fshih=0 order by ArtID desc');
        });
        $subCats = array("https://www.realestate.al/en/apartment-one+bedroom-for-rent-in-Tirana", "https://www.realestate.al/en/apartment-two+bedroom-for-rent-in-Tirana", "https://www.realestate.al/en/apartment-three+bedroom-for-rent-in-Tirana", "https://www.realestate.al/en/apartment-more+than+three+bedroom-for-rent-in-Tirana", "https://www.realestate.al/en/apartment-one+bedroom-for-sale-in-Tirana", "https://www.realestate.al/en/apartment-two+bedroom-for-sale-in-Tirana", "https://www.realestate.al/en/apartment-three+bedroom-for-sale-in-Tirana", "https://www.realestate.al/en/apartment-more+than+three+bedroom-for-sale-in-Tirana", "https://www.realestate.al/en/villa-2+bedrooms-for-rent-in-Tirana", "https://www.realestate.al/en/villa-3+bedrooms-for-rent-in-Tirana", "https://www.realestate.al/en/villa-4+bedrooms-for-rent-in-Tirana", "https://www.realestate.al/en/villa-more+than+4+bedrooms-for-rent-in-Tirana", "https://www.realestate.al/en/villa-2+bedrooms-for-sale-in-Tirana", "https://www.realestate.al/en/villa-3+bedrooms-for-sale-in-Tirana", "https://www.realestate.al/en/villa-4+bedrooms-for-sale-in-Tirana", "https://www.realestate.al/en/villa-more+than+4+bedrooms-for-sale-in-Tirana", "https://www.realestate.al/en/office-from+0+to+50+m2-for-rent-in-Tirana", "https://www.realestate.al/en/office-from+50+to+100+m2-for-rent-in-Tirana", "https://www.realestate.al/en/office-from+100+to+200+m2-for-rent-in-Tirana", "https://www.realestate.al/en/office-more+than+200+m2-for-rent-in-Tirana", "https://www.realestate.al/en/office-from+0+to+50+m2-for-sale-in-Tirana", "https://www.realestate.al/en/office-from+50+to+100+m2-for-sale-in-Tirana", "https://www.realestate.al/en/office-from+100+to+200+m2-for-sale-in-Tirana", "https://www.realestate.al/en/office-for-sale-in-Tirana", "https://www.realestate.al/en/warehouse-from+0+to+500+m2-for-rent-in-Tirana", "https://www.realestate.al/en/warehouse-from+500+to+1000+m2-for-rent-in-Tirana", "https://www.realestate.al/en/warehouse-from+1000+to+2000+m2-for-rent-in-Tirana", "https://www.realestate.al/en/warehouse-more+than+2000+m2-for-rent-in-Tirana", "https://www.realestate.al/en/warehouse-from+0+to+500+m2-for-sale-in-Tirana", "https://www.realestate.al/en/warehouse-for-sale-in-Tirana", "https://www.realestate.al/en/warehouse-from+1000+to+2000+m2-for-sale-in-Tirana", "https://www.realestate.al/en/warehouse-for-sale-in-Tirana", "https://www.realestate.al/en/commercial-from+0+to+50+m2-for-rent-in-Tirana", "https://www.realestate.al/en/commercial-from+50+to+100+m2-for-rent-in-Tirana", "https://www.realestate.al/en/commercial-from+100+to+200+m2-for-rent-in-Tirana", "https://www.realestate.al/en/commercial-more+than+200+m2-for-rent-in-Tirana", "https://www.realestate.al/en/commercial-from+0+to+50+m2-for-sale-in-Tirana", "https://www.realestate.al/en/commercial-from+50+to+100+m2-for-sale-in-Tirana", "https://www.realestate.al/en/commercial-from+100+to+200+m2-for-sale-in-Tirana", "https://www.realestate.al/en/commercial-more+than+200+m2-for-sale-in-Tirana", "https://www.realestate.al/sq/magazine-nga+0+deri+500+m2-ne-shitje-ne-Tirane", "https://www.realestate.al/sq/magazine-nga+500+deri+1000+m2-ne-shitje-in-Tirane", "https://www.realestate.al/sq/magazine-nga+1000+deri+2000+m2-ne-shitje-in-Tirane", "https://www.realestate.al/sq/ambient%20tregtar-nga+0+deri+50+m2-me-qera-ne-Tirane", "https://www.realestate.al/sq/ambient%20tregtar-nga+50+deri+100+m2-me-qera-ne-Tirane", "https://www.realestate.al/sq/ambient%20tregtar-nga+100+deri+200+m2-me-qera-ne-Tirane", "https://www.realestate.al/sq/ambient%20tregtar-me+shume+se+200+m2-me-qera-ne-Tirane", "https://www.realestate.al/sq/ambient%20tregtar-nga+0+deri+50+m2-ne-shitje-ne-Tirane", "https://www.realestate.al/sq/ambient%20tregtar-nga+50+deri+100+m2-ne-shitje-ne-Tirane", "https://www.realestate.al/sq/ambient%20tregtar-nga+100+deri+200+m2-ne-shitje-ne-Tirane", "https://www.realestate.al/sq/ambient%20tregtar-me+shume+se+200+m2-ne-shitje-ne-Tirane");
        //These total articles rank with priority .8. These will the last added 30 articles.
        $articlesNumberTop = 30;
        return response()->view('contents.sitemap', [
            'properties' => $sitemapRec,
            'subCats' => $subCats,
            'nrArticles' => $articlesNumberTop
        ])->header('Content-Type', 'text/xml');

    }

    public function sitemap_new()
    {
        //SELECT ArtID as id, ArtTitull_gj1 as titull, ArtTitull_gj2 as title FROM `cms_artikujt` WHERE fshih=0 order by ArtID desc 
        $sitemapRec = Cache::remember('sitemap123', 10, function () {
            return DB::select('SELECT ArtID as id, ArtTitull_gj1 as titull, ArtTitull_gj2 as title, updated_at as lastupdate FROM `cms_artikujt` WHERE fshih=0 and deleted_at is null order by ArtID desc');
        });
               $subCats = array("https://www.realestate.al/en/apartment-one+bedroom-for-rent-in-Tirana", "https://www.realestate.al/en/apartment-two+bedroom-for-rent-in-Tirana", "https://www.realestate.al/en/apartment-three+bedroom-for-rent-in-Tirana", "https://www.realestate.al/en/apartment-more+than+three+bedroom-for-rent-in-Tirana", "https://www.realestate.al/en/apartment-one+bedroom-for-sale-in-Tirana", "https://www.realestate.al/en/apartment-two+bedroom-for-sale-in-Tirana", "https://www.realestate.al/en/apartment-three+bedroom-for-sale-in-Tirana", "https://www.realestate.al/en/apartment-more+than+three+bedroom-for-sale-in-Tirana", "https://www.realestate.al/en/villa-2+bedrooms-for-rent-in-Tirana", "https://www.realestate.al/en/villa-3+bedrooms-for-rent-in-Tirana", "https://www.realestate.al/en/villa-4+bedrooms-for-rent-in-Tirana", "https://www.realestate.al/en/villa-more+than+4+bedrooms-for-rent-in-Tirana", "https://www.realestate.al/en/villa-2+bedrooms-for-sale-in-Tirana", "https://www.realestate.al/en/villa-3+bedrooms-for-sale-in-Tirana", "https://www.realestate.al/en/villa-4+bedrooms-for-sale-in-Tirana", "https://www.realestate.al/en/villa-more+than+4+bedrooms-for-sale-in-Tirana", "https://www.realestate.al/en/office-from+0+to+50+m2-for-rent-in-Tirana", "https://www.realestate.al/en/office-from+50+to+100+m2-for-rent-in-Tirana", "https://www.realestate.al/en/office-from+100+to+200+m2-for-rent-in-Tirana", "https://www.realestate.al/en/office-more+than+200+m2-for-rent-in-Tirana", "https://www.realestate.al/en/office-from+0+to+50+m2-for-sale-in-Tirana", "https://www.realestate.al/en/office-from+50+to+100+m2-for-sale-in-Tirana", "https://www.realestate.al/en/office-from+100+to+200+m2-for-sale-in-Tirana", "https://www.realestate.al/en/office-for-sale-in-Tirana", "https://www.realestate.al/en/warehouse-from+0+to+500+m2-for-rent-in-Tirana", "https://www.realestate.al/en/warehouse-from+500+to+1000+m2-for-rent-in-Tirana", "https://www.realestate.al/en/warehouse-from+1000+to+2000+m2-for-rent-in-Tirana", "https://www.realestate.al/en/warehouse-more+than+2000+m2-for-rent-in-Tirana", "https://www.realestate.al/en/warehouse-from+0+to+500+m2-for-sale-in-Tirana", "https://www.realestate.al/en/warehouse-for-sale-in-Tirana", "https://www.realestate.al/en/warehouse-from+1000+to+2000+m2-for-sale-in-Tirana", "https://www.realestate.al/en/warehouse-for-sale-in-Tirana", "https://www.realestate.al/en/commercial-from+0+to+50+m2-for-rent-in-Tirana", "https://www.realestate.al/en/commercial-from+50+to+100+m2-for-rent-in-Tirana", "https://www.realestate.al/en/commercial-from+100+to+200+m2-for-rent-in-Tirana", "https://www.realestate.al/en/commercial-more+than+200+m2-for-rent-in-Tirana", "https://www.realestate.al/en/commercial-from+0+to+50+m2-for-sale-in-Tirana", "https://www.realestate.al/en/commercial-from+50+to+100+m2-for-sale-in-Tirana", "https://www.realestate.al/en/commercial-from+100+to+200+m2-for-sale-in-Tirana", "https://www.realestate.al/en/commercial-more+than+200+m2-for-sale-in-Tirana", "https://www.realestate.al/sq/apartament-2+1-me-qera-ne-Tirane/1", "https://www.realestate.al/sq/zyre-nga+50+deri+100+m2-ne-shitje-ne-Tirane/1", "https://www.realestate.al/sq/zyre-nga+100+deri+200+m2-ne-shitje-ne-Tirane/1", "https://www.realestate.al/sq/zyre-nga+100+deri+200+m2-ne-shitje-ne-Tirane/1", "https://www.realestate.al/sq/magazine-nga+0+deri+500+m2-ne-shitje-ne-Tirane", "https://www.realestate.al/sq/magazine-nga+500+deri+1000+m2-ne-shitje-in-Tirane", "https://www.realestate.al/sq/magazine-nga+1000+deri+2000+m2-ne-shitje-in-Tirane", "https://www.realestate.al/sq/ambient%20tregtar-nga+0+deri+50+m2-me-qera-ne-Tirane", "https://www.realestate.al/sq/ambient%20tregtar-nga+50+deri+100+m2-me-qera-ne-Tirane", "https://www.realestate.al/sq/ambient%20tregtar-nga+100+deri+200+m2-me-qera-ne-Tirane", "https://www.realestate.al/sq/ambient%20tregtar-me+shume+se+200+m2-me-qera-ne-Tirane", "https://www.realestate.al/sq/ambient%20tregtar-nga+0+deri+50+m2-ne-shitje-ne-Tirane", "https://www.realestate.al/sq/ambient%20tregtar-nga+50+deri+100+m2-ne-shitje-ne-Tirane", "https://www.realestate.al/sq/ambient%20tregtar-nga+100+deri+200+m2-ne-shitje-ne-Tirane", "https://www.realestate.al/sq/ambient%20tregtar-me+shume+se+200+m2-ne-shitje-ne-Tirane");        
	    //These total articles rank with priority .8. These will the last added 30 articles.
        $articlesNumberTop = 30;
        return response()->view('contents.sitemap', [
            'properties' => $sitemapRec,
            'subCats' => $subCats,
            'nrArticles' => $articlesNumberTop
        ])->header('Content-Type', 'text/xml');

    }

    public function rss()
    {
        //SELECT ArtID as id, ArtTitull_gj1 as titull, ArtTitull_gj2 as title FROM `cms_artikujt` WHERE fshih=0 order by ArtID desc 
        //Cache::flush();
        $rssRec = Cache::remember('rss', 10, function () {
            return DB::select('SELECT ArtID as id, ArtTitull_gj1 as titull, ArtTitull_gj2 as title, Meta_gj1 as pershkrim, Meta_gj2 as description   FROM `cms_artikujt` WHERE fshih=0 order by ArtID desc LIMIT 0,200');
        });

        $articlesNumberTop = 30;
        return response()->view('contents.rss', [
            'properties' => $rssRec
        ])->header('Content-Type', 'text/xml');

    }
}
