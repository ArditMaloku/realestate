<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class InquirePropertyMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $article;
    protected $data;

    /**
     * Create a new message instance.
     *
     * @param $article
     * @param $data
     */
    public function __construct($article, $data)
    {
        $this->article = $article;
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $name = $this->data['name'];
        $email = $this->data['email'];

        return $this->to([
            "info@realestate.al",
//            "albanafmeti@gmail.com"
        ])->from($email, $name)
            ->subject("Inquire nga Realestate.al!")
            ->replyTo($email, $name)
            ->view('emails.inquire-message')
            ->text('emails.inquire-message-txt')
            ->with([
                'data' => $this->data,
                'article' => $this->article,
            ]);
    }
}
