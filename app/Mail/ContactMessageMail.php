<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactMessageMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $data;

    /**
     * Create a new message instance.
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $name = $this->data['name'];
        $email = $this->data['email'];

        return $this->to([
            "info@realestate.al",
//            "albanafmeti@gmail.com"
        ])->from($email, $name)
            ->subject('Contact from ' . $name)
            ->replyTo($email, $name)
            ->view('emails.contact-message')
            ->text('emails.contact-message-txt')
            ->with([
                'data' => $this->data
            ]);
    }
}
