<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegisterPropertyMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $data;

    /**
     * Create a new message instance.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $name = $this->data['emri'] . ' ' . $this->data['mbiemri'];
        $email = $this->data['email'];

        return $this->to([
            "info@realestate.al",
//            "albanafmeti@gmail.com"
        ])->from($email, $name)
            ->subject("Kerkese per shtim prone!")
            ->replyTo($email, $name)
            ->view('emails.register-property')
            ->text('emails.register-property-txt')
            ->with([
                'data' => $this->data
            ]);
    }
}
